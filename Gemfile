source 'https://rubygems.org'

gem 'json', github: 'flori/json', branch: 'v1.8'
# https://stackoverflow.com/questions/21095098/why-wont-bundler-install-json-gem
gem 'rails', '4.2.8'
gem 'puma'
gem 'pg', '~> 0.20.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'jquery-rails'
gem 'jbuilder', '~> 2.0'
gem 'sdoc', '~> 0.4.0', group: :doc
gem 'active_model_serializers', '~> 0.10.2'
gem 'bootstrap-sass', '~> 3.3.6'

group :assets do
  gem 'ejs'
end

gem 'omniauth', '~> 1.3', '>= 1.3.1'
gem 'devise'
gem 'activeadmin', github: 'activeadmin'
gem 'active_admin_theme'
gem 'devise_token_auth'
gem "koala", "~> 2.2"
# service for verification with phone number
gem 'sinch_sms'
gem 'plivo'
gem 'twilio-ruby'

# for using .env file in each environment
gem 'dotenv-rails'
# for image operations
gem 'mini_magick'
gem 'carrierwave'
gem 'carrierwave-bombshelter'

# encapsulate business logic
gem "interactor", "~> 3.0"

gem "pry-remote"
gem "sentry-raven"

# Operations with payments
gem 'stripe'

# Ruby wrapper for the Firebase REST API.
gem 'fcm'
gem 'fog'
gem 'fog-aws'
gem 'time_diff'

# Jobs
gem 'sidekiq'
gem 'sinatra', require: false
gem "sidekiq-cron", "~> 0.6.3"

gem 'groupdate'
gem 'rack-cors', :require => 'rack/cors'
gem 'cancancan', '~> 2.0'
gem 'activeadmin_addons'
gem 'ckeditor'
gem "roo-xls"
gem 'chartkick'
gem 'acts_as_list'
gem 'activeadmin-sortable'
gem 'geocoder'
gem 'prawn'
gem 'prawn-table'
gem 'google-api-client'
gem 'gon'
gem 'rgeo'
gem 'activerecord-postgis-adapter'
gem 'kaminari'
gem 'jquery-ui-rails'
gem 'rails_sortable'
gem 'premailer-rails'
gem 'ice_cube'

group :development, :test do
  gem 'pry-byebug'
  gem 'rubocop'
end

group :test do
  gem 'rspec-rails'
  gem 'ffaker'
  gem 'database_cleaner'
  gem 'rspec-expectations'
  gem 'factory_girl_rails'
  gem 'webmock'
end

group :development do
  gem 'web-console', '~> 2.0'
  gem 'spring'
  gem "rubycritic", require: false
  gem 'rack-mini-profiler'
  gem 'heavens_door', group: :development
end
