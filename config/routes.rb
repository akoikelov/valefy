require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self) rescue ActiveAdmin::DatabaseHitDuringLoad

  authenticate :admin_user do
    namespace :admin do
      namespace :metrics do
        resources :clients, only: [] do
          get 'not_bussiness', on: :collection
          get 'bussiness', on: :collection
          get 'last_n_days', on: :collection
        end

        resources :pros, only: [:index] do
          get 'set_master', on: :collection
        end

        resources :districts, only: [:index] do
          get 'set_city',          on: :collection
          get 'average_price',     on: :collection
          get 'orders_count_cat',  on: :collection
          get 'clients_by_gender', on: :collection
          get 'clients_by_age',    on: :collection
        end
      end
      namespace :panel do
        get '/', to: 'dashboards#index'
        post '/reassing_pro', to: 'dashboards#reassign_orders_pro'
        get '/get_pro_info', to: 'dashboards#get_info_pro'
        resources :companies
        resources :stations, only: [:index, :show, :new, :create, :edit, :update] do
          get :dashboard, on: :collection
        end
        resources :pros do
          post 'get_tips', on: :collection
          resource :payment,         only: [:show]
          resources :work_hours, only: [:create, :update]
          resources :stripe_account, only: [:new, :create, :edit, :update], controller: :pro_stripe_account
        end
        resources :admin_users
        resources :order_items, only: [:new, :create, :index, :show, :edit, :update] do
          post 'cancel'    , on: :collection
          post :change_time, on: :collection
        end
        resources :settings, except: [:destroy]
        resources :profile,  only: [:index]
        resources :clients do
          get 'upload_xls', on: :collection
          post 'import_xls', on: :collection
          resources :contract,      only: [:new, :create, :edit, :update]
        end
        resources :gift_cards, only: [:index, :show, :edit, :update]
        resources :email_texts
        resources :promo_codes, only: [:index, :show, :edit, :update, :new, :create]
        resources :faq_clients
        resources :faq_pros
        resources :demo_requests, only: [:index, :show]
        resources :services
        resources :categories do
          put 'sortable_update', on: :collection
        end
        resources :subcategories do
          put 'sortable_update', on: :collection
        end
        resources :tariff_plans
        resources :teams
        resources :countries
        resources :cities
        resources :districts
        resources :banners
        resources :offices
        resources :call_mes, only: [:index, :show]
        resources :devices, only: [:index, :show, :destroy]
        resources :client_feedbacks, only: [:index, :show]
        resources :pro_feedbacks, only: [:index, :show]
        

        resources :contract_dates, only: [:new, :create]
        resources :join_pros, only: [:index, :show]
        resources :sms_histories
        resources :term_conditions, only: [:index, :edit, :update]
        resources :tariff_plans
        resources :transports
        resources :referals, only: [:index, :show]
        resources :station_services, only: [:edit, :update, :show]

        get :search_clients,        to: 'queries#search_clients'
        get :check_cards,           to: 'queries#check_cards'
        post :add_cards,            to: 'queries#add_cards'
      end
    end
    resources :metrics,       only: [:index],     to: 'metrics#index'
    resource  :admin_images,  only: [:destroy]
    get :percentage_orders_and_categories,        to: 'charts#percentage_orders_and_categories'
    get :average_price_category,                  to: 'charts#average_price_category'
    get :last_n_days,                             to: 'charts#last_n_days'
    get :dayly_rate_last_n_days,                  to: 'charts#dayly_rate_last_n_days'
    get :created_order_by_platform,               to: 'charts#created_order_by_platform'
    get :clients_by_type,                         to: 'charts#clients_by_type'
    get :orders_by_clients,                       to: 'charts#orders_by_clients'
    get :order_period_values,                     to: 'charts#order_period_values'
    get :orders_frequency,                        to: 'charts#orders_frequency'
    get :clients_retention,                       to: 'charts#clients_retention'
    get :small_large_cars_percent,                to: 'charts#small_large_cars_percent'
    get :profit_on_services,                      to: 'charts#profit_on_services'
    get :most_ordered_service,                    to: 'charts#most_ordered_service'
    get :pros_count,                              to: 'charts#pros_count'
    get :canceled_orders,                         to: 'charts#canceled_orders'
    get :completed_orders,                        to: 'charts#completed_orders'
    get :accepted_orders,                         to: 'charts#accepted_orders'
    get :masters_orders_by_client_type,           to: 'charts#masters_orders_by_client_type'
    get :masters_orders_by_category,              to: 'charts#masters_orders_by_category'
    get :orders_by_district,                      to: 'charts#orders_by_district'
    get :master_average_time_orders,              to: 'charts#master_average_time_orders'
    get :clients_by_gender,                       to: 'charts#clients_by_gender'
    get :clients_by_age,                          to: 'charts#clients_by_age'
    get :orders_by_company_platforms,             to: 'charts#orders_by_company_platforms'
    get :cars_count_by_type,                      to: 'charts#cars_count_by_type'
    get :pros_by_type,                            to: 'charts#pros_by_type'
    get :turnover_by_platfroms,                   to: 'charts#turnover_by_platfroms'
    get :clients_count_by_segment,                to: 'charts#clients_count_by_segment'
    mount Sidekiq::Web, at: '/sidekiq'
  end

  root 'welcome#index'

  namespace :api, defaults: { format: :json } do

    namespace :v2 do
      namespace :auth do
        post 'provider/facebook',    to: 'socials#facebook'
        post 'provider/google',      to: 'socials#google'
        post 'generate',             to: 'verifications#generate'
        post 'verify_code',          to: 'verifications#verify_code'

        resources :passwordsms, only: [:create]
        scope module: 'passwordsms' do
          resources :send,   only: [:create], path: '/passwordsms/send'
          resources :verify, only: [:create], path: '/passwordsms/verify'
          # TODO заменить на reource. Следовательно в контроллерах убрать
          # s
        end
      end
      namespace :users do
        resources :client, only: [:index, :show]
        resources :masters, param: :master_id, only: [:show]
      end
      namespace :customer do
        resources :accounts,      only: [:create], path: '/'
        resources :charges,       only: [:create]
        resource  :cards_present, only: [:show]
        resources :cards,         only: [:index, :update, :destroy] do
          get :default, on: :collection
        end
      end

      namespace :additional_payments do
        resources :confirms, only: [:create]
        resources :rejects,  only: [:create]
      end

      namespace :order_items do
        resources :starts,              only: [:create]
        resource  :searchs,             only: [:create]
        resource  :completes,           only: [:create]
        resource  :pays,                only: [:create]
        resource  :cancels,             only: [:create]
      end

      mount_devise_token_auth_for 'User::Client', at: 'auth', controllers: {
        sessions:      'api/v2/auth/sessions',
        registrations: 'api/v2/auth/registrations',
        passwords:     'api/v2/auth/passwords'
      }
      resources :order_items,        only: [:index, :create, :show] do
        resources :client_feedbacks, only: [:create], as: :feedback
      end

      resources :subcategories,    only: [:index, :show] # not for API
      resources :services,         only: [:index] # not for API
      resources :categories,       only: [:index, :show] do
        resources :services,       only: [:index]
      end

      resources :stationaries,               only: [:index, :show]
      resources :gift_cards,                 only: [:create]
      namespace :gift_cards do
        resources :uses, only: [:create]
      end
      resources :offices,                    only: [:index]
      resources :posts,                      only: [:index]
      resources :images,                     only: [:create]
      resources :banners,                    only: [:index]
      resources :helps,                      only: [:create]
      resources :contacts,                   only: [:index]
      resources :conditions,                 only: [:index]
      resources :faqs,                       only: [:index]
      resources :additional_payments,        only: [:show]
      resources :timetables,                 only: [:index]
      resources :feedback_apps,              only: [:create]
      resources :partners,                   only: [:index]
      resources :referal_shares,             only: [:create]
      resources :demo_requests,              only: [:create]
      namespace :promo_codes do
        resources :check, only: [:create]
      end

      namespace :client do
        resource :device, only: [:create]
      end
    end

    namespace :v3 do
      resources :timetables,                 only: [:index]
    end

    namespace :masters do
      namespace :v1 do
        namespace :auth do
          post 'provider/facebook',    to: 'socials#facebook'
          post 'generate',             to: 'verifications#generate'
          post 'verify_code',          to: 'verifications#verify_code'

          resources :passwordsms, only: [:create]
          scope module: 'passwordsms' do
            resources :send,   only: [:create], path: '/passwordsms/send'
            resources :verify, only: [:create], path: '/passwordsms/verify'
            # TODO заменить на reource. Следовательно в контроллерах убрать
            # s
          end
        end
        namespace :users do
          resources :master, only: [:index, :show]
          resources :clients, param: :client_id, only: [:show]
        end
        namespace :customer do
          resources :accounts,     only: [:create], path: '/'
          resources :charges,      only: [:create]
          resource :cards_present, only: [:show]
          resources :cards,        only: [:index, :update, :destroy]
        end
        namespace :accounts do
          resource :managed, only: [:create]
        end
        namespace :order_items do
          resources :starts,              only: [:create]
          resource  :searchs,             only: [:create]
          resource  :completes,           only: [:create]
          resource  :cancels,             only: [:create]
          resource  :accepts,             only: [:create]
        end
        mount_devise_token_auth_for 'User::ServiceMan', at: 'auth', controllers: {
          sessions:      'api/masters/v1/auth/sessions',
          registrations: 'api/masters/v1/auth/registrations',
          passwords:     'api/masters/v1/auth/passwords'
        }
        resources :order_items,        only: [:index, :create, :show, :update]
        resources :categories,       only: [:index, :show] do
          resources :services,       only: [:index]
        end

        resources :join_masters,     only: [:create], path: :joins
        resources :devices,          only: [:create]
        resources :images,           only: [:create]
        resources :helps,            only: [:create]
        resources :contacts,         only: [:index]
        resources :conditions,       only: [:index]
        resources :faqs,             only: [:index]
        resource  :onduty, only: [:update]
        resources :additional_payments, only: [:create, :show]
        resource  :master_stripe, only: [:update]
        get 'master_stripes/:id' => 'master_stripes#show'
        resources :master_feedbacks, only: [:create], as: :feedback
        resources :locations,        only: [:create], as: :locations
        resources :timetables,           only: [:index]
        resources  :schedules,            only: [:index]
      end
      namespace :v2 do
        resources :order_items,                only: [:index, :show]
        resources :permit_google_calendars,    only: [:create]

        namespace :order_items do
          resources :starts,              only: [:create]
          resource  :accepts,             only: [:create]
          resource  :completes,           only: [:create]
        end
      end
    end

    namespace :web do
      namespace :v1 do
        mount_devise_token_auth_for 'User', at: 'auth', controllers: {
          sessions:      'api/web/v1/auth/sessions', only: [:create]
        }, skip: [:registrations, :passwords, :token_validations,
         :omniauth_callbacks]
      end
    end
  end
end
