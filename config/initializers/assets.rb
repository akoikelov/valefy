# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w( panel.css )
Rails.application.config.assets.precompile += %w( stripe.css )
Rails.application.config.assets.precompile += %w( panel.js )
Rails.application.config.assets.precompile += %w( ckeditor/*)
Rails.application.config.assets.precompile += %w( panel/morris.min.js )
Rails.application.config.assets.precompile += %w( panel/morris-data.js )
Rails.application.config.assets.precompile += %w( panel/bootstrap-select.min.js )
Rails.application.config.assets.precompile += %w( metrics.js )
Rails.application.config.assets.precompile += %w( panel/stripe_form.js )
Rails.application.config.assets.precompile += %w( mailer.css )