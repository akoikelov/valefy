ActiveAdmin.register Country do
  permit_params :name, :description, :image

  index do
    selectable_column
    id_column
    column :name
    column :description
    actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row 'Image' do |country|
        if country.image.present?
          (img src: country.image.url)
        end
      end
    end
  end
end
