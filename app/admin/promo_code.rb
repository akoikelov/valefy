ActiveAdmin.register PromoCode do
  permit_params :promo_code_id, :exp_date, :discount

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'PromoCodes' do
      f.input :promo_code_id
      f.input :exp_date, label: 'Expiration date', as: :date_time_picker,
                         picker_options: {
                           min_date: Date.current + 2.days,
                           timepicker: false
                         }
      f.input :discount
    end
    f.actions
  end
end
