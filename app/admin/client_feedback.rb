ActiveAdmin.register ClientFeedback do

  menu parent: 'Feedbacks', label: 'Clients'
  actions :index, :show

  scope 'all' do |feedbacks|
    feedbacks_list(params, feedbacks)
  end

  scope 'by_client', show_count: false do |feedbacks|
    if params[:client_id].present?
      client = ::User::Client.find(params[:client_id])

      client.feedbacks
    else
      feedbacks
    end
  end

  index do
    selectable_column
    id_column
    column 'Order' do |feedback|
      link_to "Order - #{feedback.order_item.id}", admin_order_item_path(feedback.order_item)
    end
    column 'Client' do |feedback|
      link_to "Client - #{feedback.client.name}", '#'
    end
    column 'Pro' do |feedback|
      link_to "#{feedback.master.name}", '#' if feedback.master.present?
    end
    column 'Rating' do |feedback|
      icons_feedback(feedback)
    end
    column :comment
    actions
  end

  show do
    attributes_table do
      row 'Order' do |feedback|
        link_to feedback.order_item.id, admin_order_item_path(feedback.order_item)
      end
      row 'Client' do |feedback|
        link_to feedback.client.name, admin_user_client_path(feedback.client)
      end
      row 'Pro' do |feedback|
        link_to feedback.master.name, admin_pro_path(feedback.master)
      end
      row 'Rating' do |feedback|
        icons_feedback(feedback)
      end
      row :polite
      row :orderly
      row :friendly
      row :comment
    end
  end

end
