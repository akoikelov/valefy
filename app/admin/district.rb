ActiveAdmin.register District do
  permit_params :name, :description, :image, :city_id, :coords, :coordinates

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :city
    actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row 'Image' do |country|
        if country.image.present?
          (img src: country.image.url)
        end
      end
      row :city
      render partial: 'admin/districts/show', locals: { coords: resource.coords, district_name: resource.name }
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'City' do
      f.input :name
      f.input :description
      f.input :coords
        render partial: 'admin/districts/map', locals: { district_name: resource.name}
      f.input :city_id, as: :select, collection: City.all.map { |city| [ city.name, city.id ]}
      f.input :image
    end
    actions
  end

  controller do
    def create
      create_polygon_coords
      super
    end

    def update
      create_polygon_coords
      super
    end

    def create_polygon_coords
      if params[:district][:coords].present?
        coords = params[:district][:coords].split(';').map { |coord| coord.split(':')[1] + ' ' + coord.split(':')[0] }.join(',')
        params[:district][:coordinates] = 'POLYGON((' + coords + '))'
      end
    end
  end

end
