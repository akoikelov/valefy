ActiveAdmin.register TermCondition do
  actions :index, :edit, :update

  permit_params :condition
end
