ActiveAdmin.register OrderItem do
  actions :index, :show, :new
  menu label: 'Orders'

  permit_params :client_id, :service_id, :status, :description, :pets, :parking,
                :total, :address, :location_client, :order_time, :master_id

                # scope 'subcategories' do |subcategories|
                #   if params[:cat].present?
                #     Category.find(params[:cat]).subcategories
                #   else
                #     subcategories
                #   end
                # end

  scope 'all' do |orders|
    all_orders(params, orders)
  end
  scope -> { 'created' },   :created
  scope -> { 'Pending' },   :serching, default: true
  scope -> { 'Active' },    :busy
  scope -> { 'History' },   :history
  scope -> { 'Cancelled' }, :canceled

  scope 'by_client', show_count: false do |orders|
    if params[:client_id].present?
      client = ::User::Client.find(params[:client_id])

      client.order_items
    else
      orders
    end
  end

  filter :client
  filter :master
  filter :services
  filter :parking
  filter :parking
  filter :count_vehicles
  filter :platform
  filter :description, label: 'Model Auto'

  # action_item :add_master, only: :show do
  #   link_to 'Add Pro', edit_admin_order_item_path(params[:id], { assign: :true }) if resource.searching?
  # end

  action_item :cancel, only: :show do
    link_to 'Cancel order', cancel_admin_order_items_path(resource), data: {confirm:'Are you sure?'}, method: :post unless resource.canceled? || resource.completed_paid?
  end

  action_item :accept, only: :show do
    link_to 'Accept order', accept_admin_order_items_path("#{resource.id},#{params[:pro]}"), data: {confirm:'Are you sure?'}, method: :post if params[:pro].present? && resource.searching?
  end

  action_item :reject, only: :show do
    link_to 'Reject order', reject_admin_order_items_path("#{resource.id}"), data: {confirm:'Are you sure?'}, method: :post if resource.accepted?
  end

  collection_action :accept, method: :post do
    _params_arr = params[:format].split ','

    order_id  = _params_arr[0]
    master_id = _params_arr[1]

    master = ::User::ServiceMan.find(master_id)

    result = ::Api::Masters::V1::OrderItems::Accept.call(
      params: { order_id: order_id }, current_user: master
    )

    if result.success?
      run_push_worker(order, I18n.t('order_item.messages.accepted'))

      redirect_to timetable_admin_pro_path(master_id), notice: "Accepted successfully!"
    else
      redirect_to admin_order_item_path(order_id), :flash => { :error => 'Order can not be accepted!' }
    end
  end

  collection_action :reject, method: :post do
    order_id = params[:format]

    result = ::Api::Masters::V1::OrderItems::Reject.call(
      params: { order_id: order_id }
    )

    if result.success?
      run_push_worker(order, I18n.t('order_item.messages.rejected'))

      redirect_to admin_user_service_men_path, notice: 'Rejected successfully!'
    else
      redirect_to admin_order_item_path(order_id), :flash => { :error => 'Order can not be rejected!' }
    end
  end

  collection_action :cancel, method: :post do
    result = ::Api::V1::OrderItems::Cancel.call(
      params: cancel_params, current_user: order.client, author: OrderStatusHistory.authors.keys[0]
    )

    if result.success?
      run_push_worker(order, I18n.t('order_item.messages.cancel')) if order.master.present?

      redirect_to collection_path, notice: "Canceled successfully!"
    else
      redirect_to admin_order_item_path(order), :flash => { :error => 'Order is not canceled!' }
    end
  end

  collection_action :update_order_time, method: :post do

    result = ::Api::V1::UpdateOrderItem.call(
      params: update_order_time_params,
      current_user: order.client,
      order: order
    )

    result.success? ? (head :ok) : (head 500)
  end

  member_action :invoice_pdf, method: :get do
    invoice = Invoice.find(params[:invoice_id])

    pdf_creator = PdfGenerator.new(invoice)
    pdf_file = pdf_creator.render 'public/invoice.pdf'

    return send_data File.open('public/invoice.pdf').read,
            filename: "invoice.pdf", type: :pdf, :disposition => "inline"
  end

  controller do
    def update
      if resource.searching? && params[:order_item][:master_id].present?
        result = ::Api::Masters::V1::OrderItems::Accept.call(
          params: accept_params, current_user: master
        )

        if result.success?
          order = result.order
          run_push_worker(order, I18n.t('order_item.messages.accepted'))

          redirect_to admin_order_item_path(resource), flash: { notice: "Pro assigned!" }
        else
          redirect_to admin_order_item_path(resource), flash: { error: "#{result.error}" }
        end
      else
        super
      end
    end

    private

    def run_push_worker(order, title)
      unless Rails.env.eql?('test')
        Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
      end
    end

    def order
      OrderItem.find(params[:format])
    end

    def cancel_params
      { order_id: params[:format] }
    end

    def accept_params
      { order_id: resource.id }
    end

    def update_order_time_params
      { days: params[:days], milliseconds: params[:milliseconds] }
    end

    def run_push_worker(order, title)
      unless Rails.env.eql?('test')
        Api::Masters::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
      end
    end

    def master
      User::ServiceMan.find(params[:order_item][:master_id])
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'OrderItem' do
      if params[:assign].present?
        if resource.searching?
          f.inputs 'Add Pro' do
            f.input :master_id,
                    as: :search_select,
                    url: admin_user_service_men_path,
                    minimum_input_length: 0,
                    fields: [:name, :id, :email],
                    display_name: :email
          end
        end
      else
        f.input :client_id, as: :select, collection: User::Client.all, include_blank: false

        f.input :service_id, as: :nested_select, level_1: {
          attribute: :category_id,
          collection: Category.general,
          display_name: :title,
          minimum_input_length: 0
        },
        level_2: {
          attribute: :subcategory_id,
          url: api_v2_subcategories_path,
          display_name: :title,
          minimum_input_length: 0
        },
        level_3: {
          attribute: :service_id,
          url: api_v2_services_path,
          display_name: :title,
          minimum_input_length: 0
        }


        f.input :order_time, as: :date_time_picker
        # f.input :status, include_blank: false
        f.input :description
        f.input :address
      end
    end
    f.actions
  end

  index row_class: ->order { order.late } do
    selectable_column
    id_column
    column 'Client' do |order_item|
      order_item.client.name
    end
    column 'Pro' do |order_item|
      if order_item.master.present?
        ul do
          li do
            "ID: #{order_item.master.id}"
          end
          li do
            "NAME: #{order_item.master.name}"
          end
        end
      elsif order_item.created?
        span("waiting for payment", style: 'color: red')
      elsif order_item.searching?
        span("Pro doesn't accept order", style: 'color: red')
      end
    end
    column 'Category' do |order|
      order.category if order.services.present?
    end
    column 'Price' do |order|
      "€ #{order.services.map(& :price).sum.to_i}"
    end
      column 'Status' do |order|
      b span order.status_name, style: "color: green"
    end
    column 'Order Time' do |order|
      order.order_time.strftime('%d - %A %m, %H:%M, %y') if order.order_time.present?
    end
    column :pets
    column :parking
    column 'Created At' do |order|
      order.created_at.strftime('%d - %A %m, %H:%M, %y')
    end
    column 'Model Auto' do |order|
      order.description
    end
    actions
  end

  show do
    attributes_table do
      row :client
      row 'pro' do |order_item|
        if order_item.master.present?
          order_item.master
        else
          span "Pro doesn't accept this order", style: 'color: red'
        end
      end
      row 'Model Auto' do |order|
        order.description
      end
      row :address
      row :pets
      row :parking
      row 'When' do |order_item|
        order_item.order_time
      end
      row 'Duration' do |order_item|
        order_item.duration_date
      end
      row :status do |order_item|
        b order_item.status, style: 'color: green'
      end
      row 'Additional information' do |order|
        ul do
          li do
            link_to "Category - #{order.category.title}", admin_category_path(order.category) if order.services.present?
          end
          li do
            link_to "Sub category - #{order.subcategory.title}", admin_subcategory_path(order.subcategory) if order.services.present?
          end
          li do
            ul do
              order.services.each do |service|
                li do
                  link_to "Service - #{service.title}", admin_service_path(service)
                end
              end
            end
          end
        end
      end
      row :images do |order_item|
        ul do
          order_item.images.inject("") {|links, data| links += content_tag :li, image_tag(image_path(data)) }.html_safe
        end
      end
      row :before_work_images do |order_item|
        ul do
          order_item.before_work_images.inject("") {|links, data| links += content_tag :li, image_tag(image_path(data), class: 'after_work') }.html_safe
        end
      end
      row :after_work_images do |order_item|
        ul do
          order_item.after_work_images.inject("") {|links, data| links += content_tag :li, image_tag(image_path(data), class: 'after_work') }.html_safe
        end
      end
      row 'Price of order' do |order_item|
        "€ #{order_item.services.map{ |e| e.price.to_i }.sum}"
      end
      row 'Additional Payment' do |order_item|
        if order_item.additional_payment.present?
          if order_item.additional_payment.client_confirm?
            span "€ #{order_item.additional_payment.additional_price.to_i} - Client confirmed", style: 'color: green'
          else
            span "€ #{order_item.additional_payment.additional_price.to_i} - Client not confirm", style: 'color: red'
          end
        else
          span "No", style: 'color: red'
        end

      end
      row 'Time' do |order_item|
        ul do
          li do
            "Time order: #{order_item.order_time.strftime('%d - %A %m, %H:%M, %y')}" if order_item.order_time.present?
          end
          li do
            "Accepted at: #{order_item.accepted_time.strftime('%d - %A %m, %H:%M, %y')}" if order_item.accepted_time.present?
          end
          li do
            "Begin work of order: #{order_item.start_work.strftime('%d - %A %m, %H:%M, %y')}" if order_item.start_work.present?
          end
          li do
            "Complete work of order: #{order_item.end_work_time.strftime('%d - %A %m, %H:%M, %y')}" if order_item.end_work_time.present?
          end
          if order_item.completed_paid?
            li do
              diff = Time.diff(order_item.end_work_time, order_item.start_work, '%H %N %S')[:diff]
              "Total time of order - #{diff}"
            end
          end
        end

      end
      row 'Feedback from Client' do |order_item|
        if order_item.feedbacks.present?
          feedback = order_item.feedbacks.first

          link_to "Feedback - #{feedback.id}", admin_client_feedback_path(feedback)
        else
          'empty'
        end
      end

      row 'Feedback from Pro' do |order_item|
        if order_item.master_feedbacks.present?
          feedback = order_item.master_feedbacks.first

          link_to "Feedback - #{feedback.id}", admin_master_feedback_path(feedback)
        else
          'empty'
        end
      end
      row 'Created At' do |order_item|
        order_item.created_at.strftime('%d - %A %m, %H:%M, %y')
      end

      if resource.end_work?
        row 'Payment Breakdown' do |order_item|
          render 'admin/order_items/payment_breakdown', { order: order_item }
        end
        row 'Booking Payments' do |order_item|
          render 'admin/order_items/booking_payments', { order: order_item }
        end
      end

        row 'Invoices' do |order_item|
          render 'admin/order_items/invoices', { order: order_item }
        end
      # row 'Pro Location' do |order_item|
      #   if order_item.master.present?
      #     if order_item.master.locations.present?
      #       location = order_item.master.locations.last
      #
      #       render partial: 'admin/users/masters/locations', locals: { location: location, updated: location.updated_at.strftime("%A - %I:%m%p") }
      #     else
      #       span "Pro doesn't have any locations yet", style: 'color: red'
      #     end
      #   else
      #     span "Pro doesn't accept this order", style: 'color: red'
      #   end
      # end
    end
  end
end
