ActiveAdmin.register Device do
  actions :index, :destroy
  #
  index  do
    selectable_column
    id_column
    column :id_registration_firebase
    column :app
    column :token_client_id
    column :platform
    column 'Client' do |device|
      device.client
    end
    column 'Pro' do |device|
      device.master
    end

    actions
  end


end
