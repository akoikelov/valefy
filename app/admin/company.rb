include ActiveAdminHelper

ActiveAdmin.register AdminUser, as: 'Companies' do
  permit_params :email, :role, :title, :description,
                :password, :password_confirmation,
                access_items: [],
                company_locations_attributes: [:id, :admin_user_id,
                  :title, :address, :lat,
                  :lon, :work_schedule, :phone_number
                ]

  scope :companies, default: true

  action_item :add_comopany_location, only: :show do
    link_to 'Add company location', new_admin_company_location_path
  end

  controller do
    def create
      params[:admin_user][:access_items].delete('')
      super
    end

    def update
      params[:admin_user][:access_items].delete('')
      params[:admin_user].delete :password
      params[:admin_user].delete :password_confirmation
      super
    end
  end

  filter :email
  filter :title
  filter :description
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  index do
    selectable_column
    id_column
    column :email
    column :title
    column :description
    column :start_time
    column :end_time

    actions
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.object.role = AdminUser::roles['company']

    f.inputs 'Company' do
      f.input :email
      f.input :role, as: :hidden
      f.input :title
      f.input :description
      f.input :access_items, as: :check_boxes, collection: form_menu_items
      f.input :password
      f.input :password_confirmation

      # f.has_many :company_locations, allow_destroy: false do |u|
      #   u.input :title
      #   u.input :address
      #   u.input :phone_number
      #   u.input :lat, as: :hidden
      #   u.input :lon, as: :hidden
      # end
    end
    f.actions
  end


  show do
    attributes_table do
      row :email
      row :title
      row :description

      panel 'Locations' do
        table_for resource.company_locations do
          column :id
          column :title
          column :address
          column :phone_number
          column 'Actions' do |cl|
            links = ''.html_safe

            links += link_to 'View ', admin_company_location_path(cl)
            links += link_to ' Edit', edit_admin_company_location_path(cl)

            links
          end
        end
      end

      panel 'Company services' do
        table_for resource.company_services do |f|
          column 'Category' do |cs|
            cs.service.subcategory.category.title
          end
          column 'Subcategory' do |cs|
            cs.service.subcategory.title
          end
          column 'Service' do |cs|
            cs.service.title
          end
          column :price
          column 'Actions' do |cs|
            links = ''.html_safe

            links += link_to 'Edit ', edit_admin_company_service_path(cs)
            links += link_to ' View', admin_company_service_path(cs)

            links
          end
        end
      end
    end
  end
end