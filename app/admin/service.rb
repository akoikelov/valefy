ActiveAdmin.register Service do
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  menu parent: 'Structure', label: 'Services'
  permit_params :id, :subcategory_id, :title, :title_ru, :description,
                :price, :duration, :short_title

  scope 'subcategory' do |services|
    params[:subcat].present? ? services.by_subcategory(params[:subcat]) : services
  end

  scope 'services' do |services|
    params[:cat].present? ? services.by_category(params[:cat]) : services
  end

  controller do
    def new
      link = fetch_prev_link

      if link.present?
        id = /\d+/.match(link.to_s).to_s

        @service = Service.new category_id: id
        super
      else
        super
      end
    end

    def create
      params[:service].delete :category_id

      super do |format|
        format.html { redirect_to admin_subcategory_path(@service.subcategory) }
      end
    end

    def update
      params[:service].delete :category_id

      super
    end

    def fetch_prev_link
      /\/admin\/categories\/\d+/.match(request.referrer)
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Service' do
      f.input :subcategory_id, as: :nested_select, level_1: {
        attribute: :category_id,
        collection: Category.general,
        display_name: :title,
        minimum_input_length: 0
      },
      level_2: {
        attribute: :subcategory_id,
        url: api_v2_subcategories_path,
        display_name: :title,
        minimum_input_length: 0
      }
      f.input :title
      f.input :title_ru
      f.input :short_title
      f.input :price
      f.input :duration
      f.input :description
    end
    f.actions
  end

  show do
    attributes_table do
      row :id
      row :title
      row :title_ru
      row :short_title
      row :price
      row :subcategory
      row :duration
      row :description
    end
  end

  index do
    sortable_handle_column
    selectable_column
    id_column
    column :position
    column 'Title' do |service|
      b service.title
    end
    column 'Title RU' do |service|
      b service.title_ru
    end
    column 'Short Title' do |service|
      b service.short_title
    end
    column 'Price' do |service|
      b service.price
    end
    column 'Subcategory' do |service|
      link_to service.subcategory.title, admin_subcategory_path(service.subcategory) if service.subcategory.present?
    end
    column 'Category' do |service|
      link_to service.category.title, admin_category_path(service.category)
    end
    actions
  end
end
