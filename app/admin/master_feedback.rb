ActiveAdmin.register MasterFeedback, as: 'Pro_Feedback' do

  actions :index, :show
  menu parent: 'Feedbacks', label: 'Pro'

  index do
    selectable_column
    id_column
    column 'Order' do |feedback|
      feedback.order_item
    end
    column 'Client' do |feedback|
      feedback.order_item.client
    end
    column 'Pro' do |feedback|
      feedback.order_item.master
    end
    column 'Rating' do |feedback|
      icons_feedback(feedback)
    end
    column :message
    actions
  end

  show do
    attributes_table do
      row 'Order' do |feedback|
        link_to feedback.order_item.id, admin_order_item_path(feedback.order_item)
      end
      row 'Client' do |feedback|
        client = feedback.order_item.client

        link_to client.name, admin_user_client_path(client)
      end
      row 'Pro' do |feedback|
        master = feedback.order_item.master

        link_to master.name, admin_pro_path(master)
      end
      row 'Rating' do |feedback|
        icons_feedback(feedback)
      end
      row :comment
    end
  end


end
