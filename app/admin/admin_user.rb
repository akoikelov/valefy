include ActiveAdminHelper

ActiveAdmin.register AdminUser do
  permit_params :email, :role, :password, :password_confirmation, access_items: []

  controller do
    def create
      params[:admin_user][:access_items].delete('')
      super
    end

    def update
      params[:admin_user][:access_items].delete('')
      params[:admin_user].delete :password if !params[:admin_user][:password].present?
      params[:admin_user].delete :password_confirmation if !params[:admin_user][:password].present?
      super
    end
  end

  index do
    selectable_column
    id_column
    column :email
    column :role
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :role, as: :select, include_blank: false
      f.input :access_items, as: :check_boxes, collection: form_menu_items
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
