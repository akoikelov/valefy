ActiveAdmin.register_page 'Map' do
  content do
    render partial: 'admin/order_items/list_orders_wit_masters', locals: { orders: OrderItem.waiting, masters: User::ServiceMan.active }
  end
end
