ActiveAdmin.register MasterStripeInfo, as: 'Pro_Stripe_Info' do
  menu false
  permit_params :tax_id, :personal_id, :id_document_image, :address_line1,
                :address_line2, :state, :postal_code, :city, :master_id,
                :routing_number, :account_number

  controller do
    def new
      redirect_to :back unless session[:pro_id]

      super
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Stripe Info' do
      f.input :master_id, as: :select, collection: User::ServiceMan.where(id: session[:pro_id]),

                          minimum_input_length: 0,
                          fields: [:name, :id, :email]
      f.input :tax_id
      f.input :personal_id
      f.input :address_line1
      f.input :address_line2
      f.input :state
      f.input :city
      f.input :id_document_image
      f.input :account_number, label: 'IBAN'
      # f.input :routing_number
    end
    f.actions
  end

  index  do
    selectable_column
    id_column

    column :master

    actions
  end

  show do
    attributes_table do
      row :master
    end
  end

end
