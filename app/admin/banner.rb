ActiveAdmin.register Banner do

  permit_params :title, :description, :image

  show do
    attributes_table do
      row :title
      row :description
      row 'Banner' do |banner|
        if banner.image.present?
          (img src: banner.image.url) << (link_to('Delete Image', admin_images_path(resource: banner.id), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end
    end
  end

  index do
    selectable_column
    id_column
    column :title
    column :description
    column 'Banner' do |banner|
      img src: banner.image.url
    end
    actions
  end

end
