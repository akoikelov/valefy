ActiveAdmin.register Subcategory do
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  menu parent: 'Structure', label: 'Subcategory'
  permit_params :id, :title, :title_ru, :category_id, :image, :icon, :icon_web,
                :visible, :code

  scope 'subcategories' do |subcategories|
    if params[:cat].present?
      Category.find(params[:cat]).subcategories
    else
      subcategories
    end
  end

  action_item :super_action, only: :show do
    link_to 'Create service', new_admin_service_path if resource.class.name.eql?('Subcategory')
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Subcategory' do
      f.input :category_id, as: :select, collection: Category.general, include_blank: false
      f.input :title
      f.input :title_ru
      f.input :image
      f.input :icon
      f.input :icon_web
      f.input :visible
      f.input :code, as: :select, collection: Subcategory.codes.map{ |s| [s[0], s[1]]}
    end
    f.actions
  end

  index do
    selectable_column
    id_column
    sortable_handle_column
    column :position
    column 'title' do |subcategory|
      subcategory.title
    end
    column 'title RU' do |subcategory|
      subcategory.title_ru
    end
    column :icon do |subcategory|
      img src: subcategory.icon.url
    end
    column :icon_web do |subcategory|
      img src: subcategory.icon_web.url
    end
    column :visible
    actions default: true do |subcategory|
      if subcategory.services.present?
        link_to "Services", admin_services_path(scope: 'subcategory', subcat: subcategory) if subcategory.services.present?
      end
    end
  end

  show do
    attributes_table do
      row :title
      row :title_ru
      row :category
      row :icon do |subcategory|
        if subcategory.icon.present?
          (img src: subcategory.icon.url) << (link_to('Delete Image', admin_images_path(resource: subcategory.id, class: 'Subcategory', field: 'icon'), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end
      row :icon_web do |subcategory|
        if subcategory.icon_web.present?
          (img src: subcategory.icon_web.url) << (link_to('Delete Image', admin_images_path(resource: subcategory.id, class: 'Subcategory', field: 'icon_web'), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end

      #services
      if subcategory.services.count > 0
        row 'services' do |subcategory|
          ul do
            subcategory.services.order(:title).inject("") {|links, service| links += content_tag :li, link_to(service.title, [:admin, service]) }.html_safe
          end
        end
      else
        row 'services' do |subcategory|
          b div "empty"
        end
      end
      row :visible
    end
  end

end
