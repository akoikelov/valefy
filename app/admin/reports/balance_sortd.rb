ActiveAdmin.register_page "Sortd Balance" do


  menu parent: 'Reports', label: 'Sortd Balance'



  controller do
    def index
      @result = Stripe::Balance.retrieve()
    end
  end

  content title: proc{ "Sortd Balance" } do
    columns do
      column do
        span "Currency - EUR"
        ul do
          li { span "Available  - #{result.available[0][:amount] / 100}" }
          li { span "Pending - #{result.pending[0][:amount] / 100 }" }
        end
      end
    end
  end
end
