ActiveAdmin.register CompanyService do
  menu false
  permit_params :service_id, :price

  controller do
    def update
      super do |success,failure|
        success.html { redirect_to admin_company_path(resource.admin_user) }
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Category' do
      f.input :service
      f.input :admin_user_id, as: :hidden
      f.input :price
    end
    f.actions
  end
end
