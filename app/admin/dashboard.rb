ActiveAdmin.register_page "Dashboard" do


  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }



  controller do
    before_filter :show_graph, only: [:index]

    def index
      case params[:q]
      when 'last_7_days'
        @orders = OrderItem.group_by_day(:created_at, last: 7).count
        @order_labels = @orders.map { |key, value| key.strftime('%a %d-%m').to_s }
        @data = data('Last 7 days')
      when 'month'
        @orders = OrderItem.group_by_month(:created_at).count
        @order_labels = @orders.map { |key, value| key.strftime('%a %d-%m').to_s }
        @data = data('Month')
      when 'last_one_hour'
        @orders_count = OrderItem.where('created_at >= ?', 1.hour.ago).count
      when 'year'
        @orders = OrderItem.group_by_month(:created_at, last: Time.now.month).count
        @order_labels = @orders.map { |key, value| key.strftime('%B').to_s }
        @data = data('Year')
      when 'week'
        @orders = OrderItem.group_by_week(:created_at, range: 1.month.ago.at_beginning_of_month..Time.now).count
        @order_labels = @orders.map { |key, value| key.strftime('%b %d').to_s }
        @data = data('From 1 month ago to now by week')
      else
        @show_graph = false
      end
    end

    def show_graph
      @show_graph = true
    end

    def data(title)
      minmax = @orders.values.minmax
      {
        min_value: minmax[0],
        title: title,
        step: minmax[1] > 10 ? minmax[1] / 10 : 1
      }
    end
  end

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        render 'admin/dashboard/data'
      end
    end
  end
end
