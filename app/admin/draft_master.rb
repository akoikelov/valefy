ActiveAdmin.register DraftMaster, as: 'Join Pro' do
  actions :index, :show

  index do
    selectable_column
    id_column
    column :name
    column :phone_number
    column :email
    actions
  end

  show do
    attributes_table do
      row :name
      row :phone_number
      row :email
      row 'ID document' do |pro|
        if is_image?(pro.id_document)
          image_tag(image_path(pro.id_document), class: 'after_work')
        else
          link_to('PDF', pro.id_document)
        end
      end
      row 'CV' do |pro|
        ul do
          pro.cv.inject("") {|links, data| links += content_tag :li, is_image?(data) ? image_tag(image_path(data), class: 'after_work') : link_to('PDF', data)}.html_safe
        end
      end
      row :comment
    end
  end
end
