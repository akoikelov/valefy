ActiveAdmin.register Category do
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  menu parent: 'Structure', label: 'Category'
  permit_params :id, :title, :title_ru, :parent_id, :image, :icon,
                :icon_web, :visible, :position, :description, :corp

  action_item(:index) do
    link_to 'Upload XLS', :action => 'upload_xls'
  end

  collection_action :upload_xls do
    render "admin/xls/upload_xls"
  end

  collection_action :import_xls, :method => :post do
    result = Category.import(params[:dump][:file])
    if result
      redirect_to admin_categories_path, :notice => "XLS imported successfully!"
    else
      redirect_to admin_categories_path, flash: { error: "Unknown file type: #{params[:dump][:file].original_filename}" }
    end
  end



  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Category' do
      f.input :title
      f.input :title_ru
      f.input :description
      f.input :image
      f.input :icon
      f.input :icon_web
      f.input :visible
      f.input :corp
    end
    f.actions
  end

  index do
    sortable_handle_column
    selectable_column
    id_column
    column :position
    column 'title' do |category|
      category.title
    end
    column 'title RU' do |category|
      category.title_ru
    end
    column :description
    column :icon do |category|
      img src: category.icon.thumb_default.url
    end
    column :icon_web do |category|
      img src: category.icon_web.url
    end
    column :visible
    column :corp
    actions do |category|
      links = ''.html_safe

      links += link_to "Subcategories ", admin_subcategories_path(scope: 'subcategories', cat: category.id)
      links += link_to "Services", admin_services_path(scope: 'services', cat: category.id)

      links
    end
  end

  show do
    attributes_table do
      row :title
      row :title_ru
      row :description
      row :parent
      row :icon do |category|
        if category.icon.present?
          (img src: category.icon.thumb_default.url) << (link_to('Delete Image', admin_images_path(resource: category.id, class: 'Category', field: 'icon'), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end
      row :icon_web do |category|
        if category.icon_web.present?
          (img src: category.icon_web.url) << (link_to('Delete Image', admin_images_path(resource: category.id, class: 'Category', field: 'icon_web'), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end
      row :visible
      row :corp

      if category.subcategories.count > 0
        row 'subcategories' do |category|
          ul do
            category.subcategories.inject("") {|links, category| links += content_tag :li, link_to(category.title, [:admin, category]) }.html_safe
          end
        end
      else
        row 'sub categories' do |category|
          b div "empty"
        end
      end
    end
  end

end
