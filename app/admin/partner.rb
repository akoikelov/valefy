ActiveAdmin.register Partner do
  permit_params :title, :description, :image

  show do
    attributes_table do
      row :title
      row :description
      row 'Image' do |partner|
        if partner.image.present?
          (img src: partner.image.url) << (link_to('Delete Image', admin_images_path(resource: partner.id, class: 'Partner', field: 'image'), data: { confirm:'Are you sure?' }, method: :delete))
        end
      end
    end
  end
end
