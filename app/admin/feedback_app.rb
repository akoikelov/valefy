ActiveAdmin.register FeedbackApp do
  menu parent: 'Feedbacks', label: 'App'

  actions :index, :show

  # scope -> { 'By clients' }, :by_clients, default: true
  # scope -> { "By pro" }, :by_masters
  scope -> { "WEB" }, :platform_web
  scope -> { "ANDROID" }, :platform_android
  scope -> { "IOS" }, :platform_ios


  index do
    selectable_column
    id_column
    column 'Client' do |feedback|
      feedback.client
    end
    column 'Pro' do |feedback|
      feedback.master
    end
    column :message
    column :platform
    actions
  end
end
