ActiveAdmin.register User::ServiceMan, as: 'Pro' do
  menu parent: 'Users', label: 'Pro'

  filter :name
  filter :email
  filter :created_at, label: 'Registration date'
  # filter :activity_in, as: :select,  label: 'Activity', collection: %w[ active ]

  permit_params :id, :email, :name, :last_name, :nickname, :onduty,
                :phone_number, :b_day, :lon, :lat, :password, :password_confirmation,
                :need_send_location,
                stripe_info_attributes: [:tax_id, :personal_id, :id_document_image, :address_line1,
                                           :address_line2, :state, :postal_code, :city],
                calendar_attributes: [:master_id, :time_plan, :start_work, :end_work_time],
                category_ids: []

  action_item :add_stripe_info, only: :show do
    if resource.class.name.eql?('User::ServiceMan') && resource.managed_account.nil?
      session[:pro_id] = resource.id
      link_to 'Add bank details', new_admin_pro_stripe_info_path
    end
  end

  member_action :payouts, method: :get do
    @result ||= ::Api::Masters::V1::Payouts.call(master: resource)

    @result.success? ? @payouts = @result.payouts : @payouts = {}

    render 'admin/users/masters/payout/index'
  end

  member_action :timetable, method: :get do
    result = Api::Masters::V2::OrdersList.call(
      current_user: resource, params: { sort: '1', timetable: '1' }
    )

    render 'admin/users/masters/timetable/index', locals: {
      available_orders: result.available_orders,
      completed_orders: resource.order_items.history,
      master_id: resource.id,
      orders: resource.order_items.active
    }
  end

  before_create do|service_man|
    service_man.last_sign_in_ip = request.remote_ip
  end

  before_save do |pro|
    result = ::Api::Masters::V2::BuildSchedule.call(params: params)

    if result.success?
      pro.build_pro_information(work_schedule: result.schedule)
    end
  end

  scope -> { 'Active' },      :active
  scope -> { 'Available' },   :available

  controller do
  	def update
      params[:user_service_man].delete :password if params[:user_service_man][:password].empty?
      params[:user_service_man].delete :password_confirmation if params[:user_service_man][:password_confirmation].empty?
  		super
  	end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Pro' do
      f.input :email
      f.input :name
      f.input :last_name
      f.input :onduty
      f.input :phone_number
      f.input :categories, :as => :check_boxes, collection: Category.visible
      f.input :b_day, as: :date_time_picker, picker_options: {
                                                  max_date: Date.current - 18.years,
                                                  timepicker: false
                                                }
      f.input :need_send_location
      f.input :password
      f.input :password_confirmation
      f.input :lon, as: :hidden
      f.input :lat, as: :hidden
    end

    panel "Pro's address" do
      render partial: 'admin/users/masters/init_coords'
    end

    panel "Pro's schedule" do
      render partial: 'admin/users/masters/schedule/index'
    end

    f.actions
  end

  index do
    selectable_column
    id_column
    column :email
    column :name
    column :phone_number
    # column 'Created at' do |master|
    #   master.created_at.strftime('%Y-%m-%d %a')
    # end
    column 'Avarage rating' do |master|
      avarage_rating_icons(master)
    end
    column 'Sum of orders (€)' do |master|
      sum_of_orders(master)
    end
    column 'Number of orders' do |master|
      master.order_items.count
    end
    actions default: true do |master|
      link_to "Schedule", timetable_admin_pro_path(master)
    end
  end

  show do
    attributes_table do
      row :id
      row :email
      row :name
      row :last_name
      row :phone_number
      row :need_send_location
      row 'B Day' do |master|
        master.bithday
      end
      row 'Reviews/Feedback' do |master|
        if master.feedbacks.count > 0
          link_to 'Feedbacks list', admin_client_feedbacks_path(scope: 'all', master_id: master.id)
        else
          "No feedbacks"
        end
      end
      row 'Orders see the History' do |master|
        if master.order_items.count > 0
          link_to 'Orders list', admin_order_items_path(scope: 'all', master_id: master.id)
        else
          'No orders'
        end
      end
      row 'Position' do |master|
        ul do
          master.categories.inject("") {|links, category| links += content_tag :li, link_to(category.title, [:admin, category]) }.html_safe
        end
      end
      row 'Count of completed works' do |master|
        master.order_items.where(status: 11).count
      end
      row 'Average earnings' do |master|
        avarage_earnings(master)
      end
      row 'Stripe Account' do |master|
        result = Api::Masters::V1::StripeLib::RetrieveAccount.call(current_user: master)
        if result.success?
          (span 'Not created!', style: "color: red")
        else
          ul do
            li { (span 'Account: Created!', style: "color: green") }
            li { (span "Status: #{result.stripe_account.legal_entity.verification.status}", style: "color: green") }
          end
        end
      end
      row 'Payouts' do |master|
        link_to 'payouts', payouts_admin_pro_path if master.managed_account.present?
      end
      row 'Timetable' do |master|
        link_to 'timetable', timetable_admin_pro_path
      end
      row 'Schedule' do |master|
        if master.pro_information.present?
          schedule = master.pro_information.work_schedule
          ul do
            schedule.keys.each do |day|
              li { span schedule_info(schedule, day) }
            end
          end
        end
      end
      div do
        render 'admin/users/masters/dayly_rate', { master: resource }
      end
      if resource.order_items.do_order.count > 0
        row 'Locations' do
          resource.locations.present? ? (render partial: 'admin/users/masters/locations', locals: { master: resource }) : (span "Master doesn't have active order now", style: 'color: red')
        end
      else
        row 'Locations' do
          span "Master doesn't have any locations", style: 'color: red'
        end
      end
    end
  end

end
