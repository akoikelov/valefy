ActiveAdmin.register User::Client do
  menu parent: 'Users', label: 'Clients'
  permit_params :email, :phone_number, :confirm_phone_number, :corp, :address,
                :name, :tariff_plan_id, :pay_with, :corp_discount,
                offices_attributes: [:id, :title], client_extra_info_attributes: [ :gender, :family_status, :profession ]

  scope_to :current_admin_user, if:  proc{ current_admin_user.company? }

  filter :name
  filter :phone_number
  filter :email
  filter :category
  filter :admin_user, as: :select, collection: proc { AdminUser.companies }, label: 'Companies', if: proc { current_admin_user.admin? }

  scope -> { 'Clients' }, :not_bussiness
  scope -> { 'Corporate clients' },     :bussiness

  action_item :super_action, only: :show do
    link_to 'Add card', new_admin_customer_account_path
  end

  member_action :card_list, method: :get do
    result = Api::V1::Customer::Card::ListCardCustomer.call(current_user: resource)
    @client = resource

    result.success? ? @cards = result.list['data'] : @cards = {}

    render 'admin/users/clients/card_list'
  end

  collection_action :remove_card, method: :delete do
    _params_arr = params[:format].split(',')
    current_user = ::User::Client.find(_params_arr[0])

    result = ::Api::V2::Customer::Card::Delete.call(
      current_user: current_user, card_id: _params_arr[1]
    )

    if result.success?
      redirect_to card_list_admin_user_client_path(current_user), notice: "Card removed successfully!"
    else
      redirect_to :back, :flash => { :error => 'Card can not be removed' }
    end
  end


  action_item(:index) do
    link_to 'Upload XLS', :action => 'upload_xls'
  end

  collection_action :upload_xls do
    render "admin/xls/upload_xls"
  end

  collection_action :import_xls, :method => :post do
    Thread.new do
      upload_xls = UploadClientsFromExcel.new(params.try(:[], :dump).try(:[], :file), current_admin_user)
      upload_xls.parse
    end

    redirect_to admin_user_clients_path, :notice => "Data importing...Please wait"
  end

  before_create do|client|
  	client.password = 'password'
  end

  controller do
  	def update
  		@client = User::Client.find(params[:id])
  		@client.password = 'password'
  		super
  	end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Client' do
      f.input :name
      f.input :email
      f.input :phone_number
      f.input :address
      f.input :confirm_phone_number
      f.input :corp
      if resource.new_record? || !resource.corp?
        f.input :tariff_plan_id, as: :select, collection: TariffPlan.all.map{|u| ["#{u.plan}", u.id]}
        f.input :pay_with, as: :select
        f.input :corp_discount
      elsif resource.corp?
        f.input :tariff_plan_id, as: :select, collection: TariffPlan.all.map{|u| ["#{u.plan}", u.id]}
        f.input :pay_with, as: :select
        f.input :corp_discount
      end

      f.has_many :offices, allow_destroy: false do |u|
        u.input :title
      end

      f.has_many :client_extra_info, allow_destroy: false do |u|
        u.input :gender, as: :select, collection: ClientExtraInfo.genders.keys
        u.input :family_status, as: :select, collection: ClientExtraInfo.family_statuses.keys
        u.input :profession
      end

    end
    f.actions
  end

  index do
    selectable_column
    id_column
    column :email
    column :name
    column 'B Day' do |client|
      client.bithday
    end
    column 'Phone Number' do |client|
      client.phone_number.eql?(User::PHONE_NOT_EXIST) ? span("нет номера", style: 'color: red') : client.phone_number
    end
    column 'Created at' do |client|
      client.created_at.strftime('%Y-%m-%d %a')
    end
    column 'Sum of orders (€)' do |client|
      sum_of_orders(client)
    end
    column 'Count of orders' do |client|
      client.all_orders.count
    end
    column 'Average check' do |client|
      client.average_check
    end
    column 'Category' do |client|
      client.category.title if client.category.present?
    end
    actions
  end

  show do
    attributes_table do
      row :id
      row :email
      row :name
      row 'B Day' do |client|
        client.bithday
      end
      row :corp
      if resource.corp?
        row 'Corp discount' do |client|
          "-#{client.corp_discount}%"
        end

        if resource.corp_information.present?
          row 'Company position' do |client|
            client.corp_information.position_in_company
          end

          row 'Company name' do |client|
            client.corp_information.name_in_company
          end
        end
      end
      row 'Phone Number' do |client|
        client.phone_number.eql?(User::PHONE_NOT_EXIST) ? span("No phone number", style: 'color: red') : client.phone_number
      end
      row :address
      row 'Category' do |client|
        client.category.title if client.category.present?
      end
      row 'Feedback from Client' do |client|
        if client.feedbacks.present?
          link_to "Feedbacks", admin_client_feedbacks_path(scope: 'by_client', client_id: client.id)
        else
          'The customer has not yet created feedbacks'
        end
      end
      row 'Orders see the History' do |client|
        if client.order_items.count > 0
          link_to "Orders", admin_order_items_path(scope: 'by_client', client_id: client.id)
        else
          "The customer has not yet created orders"
        end
      end
      row 'Count orders' do |client|
        ul do
          li { span("Completed and paid orders: #{client.completed_paid_orders.count}") }
          li { span("Active orders: #{client.active_orders.count}") }
          li { span("Canceled orders: #{client.canceled_orders.count}") }
          li { span("Total: #{client.all_orders.count}") }
        end
      end
      row 'Confirm phone number' do |client|
        client.confirm_phone_number? ? span("Yes") : span("No", style: 'color: red')
      end
      row 'List of cards' do |client|
        link_to 'Cards', card_list_admin_user_client_path(client)
      end
    end
  end
end
