ActiveAdmin.register Employee do
  scope_to :current_admin_user, if:  proc{ current_admin_user.company? }

  permit_params :name, :last_name, :position, :email, :phone_number, :b_day,
                :admin_user_id

  filter :name
  filter :last_name
  filter :position
  filter :email
  filter :admin_user_id, as: :select, collection: proc { AdminUser.companies }, label: 'Companies', if: proc { current_admin_user.admin? }
  filter :created_at, label: 'Registration date'

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Pro' do
      f.input :name
      f.input :last_name
      f.input :position
      f.input :email
      f.input :phone_number
      f.input :b_day, as: :date_time_picker, picker_options: { timepicker: false }
      f.input :admin_user_id, as: :hidden, input_html: { value: "#{current_admin_user.id}" }
    end

    f.actions
  end


end
