ActiveAdmin.register Faq, as: 'FaqPro' do
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  menu parent: 'Faqs', label: 'Pro'
  permit_params :question, :answer, :type_user, :position

  scope :master, default: true

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.object.type_user = Faq::TYPE_USERS.first
    f.object.type_user = 'master'
    f.inputs 'Faq Pro' do
      f.input :question
      f.input :answer
      f.input :type_user, as: :hidden
    end
    f.actions
  end

  index do
    sortable_handle_column
    selectable_column
    column :question
    column :answer
  end

end
