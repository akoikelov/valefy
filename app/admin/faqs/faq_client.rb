ActiveAdmin.register Faq, as: 'FaqClient' do
  config.sort_order = 'position_asc'
  config.paginate   = false
  sortable

  menu parent: 'Faqs', label: 'Clients'
  permit_params :question, :answer, :type_user, :position

  scope :client, default: true

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.object.type_user = Faq::TYPE_USERS.last
    f.inputs 'Faq Client' do
      f.input :question
      f.input :answer
      f.input :type_user, as: :hidden
    end
    f.actions
  end

  index do
    sortable_handle_column
    selectable_column
    column :question
    column :answer
  end

end
