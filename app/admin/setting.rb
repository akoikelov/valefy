ActiveAdmin.register Setting do
  actions :index, :show, :edit, :update
  permit_params :radius, :send_email_invoice_client, :send_email_created_order,
                :time_send_location, :send_welcome_email

  form do |f|
    f.inputs "Settings" do
      f.input :radius
      f.input :send_welcome_email
      f.input :send_email_invoice_client
      f.input :send_email_created_order
      f.input :time_send_location
    end

    f.actions
  end

  show do
    attributes_table do
      row :radius
      row :send_welcome_email
      row :send_email_invoice_client
      row :send_email_created_order
      row 'Time to send location (min)' do |setting|
        setting.time_send_location
      end
    end
  end
end
