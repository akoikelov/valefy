ActiveAdmin.register TariffPlan do
  permit_params :plan, :plan_value

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'OrderItem' do
      f.input :plan
      f.input :plan_value, hint: 'The value should be in the week'
    end
    f.actions
  end
end
