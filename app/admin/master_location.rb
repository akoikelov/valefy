ActiveAdmin.register MasterLocation, as: 'Pro_Locations' do
  actions :index, :show, :new, :create

  permit_params :master_id, :lon, :lat

  index do
    selectable_column
    id_column
    column 'Lon' do |location|
      location.lon
    end
    column 'Lat' do |location|
      location.lat
    end
    column 'Pro' do |location|
      location.master
    end
    column 'Created At (h:m:s)' do |location|
      time_with_sec(location.created_at)
    end

    actions
  end

  show do
    attributes_table do
      row :lon
      row :lat
      row 'Pro' do |location|
        location.master
      end
      row 'Created At' do |location|
        time_with_sec(location.created_at)
      end
    end
  end
end
