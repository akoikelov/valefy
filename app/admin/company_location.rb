ActiveAdmin.register CompanyLocation do
  menu false
  permit_params :admin_user_id, :title, :address, :lat, :lon, :phone_number,
                :image

  controller do
    def create
      super do |success,failure|
        success.html { redirect_to admin_company_path(resource.admin_user) }
      end
    end
    def update
      super do |success,failure|
        success.html { redirect_to admin_company_path(resource.admin_user) }
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Category' do
      f.input :admin_user, as: :select, collection: AdminUser.companies
      f.input :title
      f.input :address
      f.input :image
      f.input :lat, as: :hidden
      f.input :lon, as: :hidden
      # TODO f.input :work_schedule
      f.input :phone_number
    end

    panel "Pro's address" do
      render partial: 'admin/company_locations/init_coords'
    end
    f.actions
  end
end