ActiveAdmin.register CustomerAccount do
  menu false
  permit_params :client_id, :customer_id

  actions :new

  controller do
    def new
      link = fetch_prev_link

      if link.present?
        id = /\d+/.match(link.to_s).to_s

        @customer_account = CustomerAccount.new client_id: id
        super
      end
    rescue => e
      Raven.capture_exception(e)
      Rails.logger.error("#{e.message}: #{e.backtrace}")

      redirect_to admin_user_clients_path, flash: { error: 'Unexpected error' }
    end
    def create
      client = User::Client.find(permitted_params[:customer_account][:client_id])

      result = Api::V1::Customer::CreateCustomer.call(
        params: customer_params, current_user: client
      )

      if result.success?
        client.reload

        redirect_to admin_user_client_path(client), flash: { notice: "New card added!" }
      else
        redirect_to admin_user_clients_path, flash: { error: result.error }
      end
    rescue => e
      Raven.capture_exception(e)
      Rails.logger.error("#{e.message}: #{e.backtrace}")

      redirect_to admin_user_clients_path, flash: { error: 'Unexpected error' }
    end

    def customer_params
      params.permit(:stripe_token)
    end

    def fetch_prev_link
      /\/admin\/user_clients\/\d+/.match(request.referrer)
    end
  end

  form do |f|
    f.inputs '' do
      f.input :client_id, as: :hidden
    end
    panel 'Add card' do
      render partial: 'admin/customer_accounts/add_card'
    end

  end
end
