ActiveAdmin.register City do
  permit_params :name, :description, :image, :country_id

  index do
    selectable_column
    id_column
    column :name
    column :description
    actions
  end

  show do
    attributes_table do
      row :name
      row :description
      row 'Image' do |country|
        if country.image.present?
          (img src: country.image.url)
        end
      end
      row :country
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'City' do
      f.input :name
      f.input :description
      f.input :image
      f.input :country_id, as: :select, collection: Country.all.map { |country| [ country.name, country.id ]}
    end
    actions
  end

end
