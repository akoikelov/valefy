json.array! schedule do |e|
  json.id e[:id]
  json.master_id e[:master_id]
  json.start e[:start]
  json.end e[:end]
  json.edit_url admin_panel_pro_work_hour_path(e[:master_id], e[:id])
end