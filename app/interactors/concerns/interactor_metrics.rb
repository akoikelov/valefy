module InteractorMetrics

  private

  def params
    context.params
  end

  def current_admin_user
    context.current_admin_user
  end

  def range
    params[:range].split.map { |e| e.downcase }.join('_')
  end

  def is_corp
    params[:is_corp]
  end

  def master
    master_id.present? ? User::ServiceMan.find(master_id) : context.fail!(error: 'Master not found')
  end

  def master_id
    params[:master_id]
  end

  def city
    city_id.present? ? City.find(city_id) : context.fail!(error: 'City id not found')
  end

  def city_id
    params[:city_id]
  end

  def district
    district_id.present? ? @district ||= District.find(district_id) : context.fail!(error: 'District not found!')
  end

  def district_id
    params[:district_id]
  end

  def client_id
    params[:client_id]
  end

  def client
    client_id.present? ? @client ||= User::Client.find(client_id) : context.fail!(error: 'Master not found!')
  end

  def last_quarter_days_count
    Time.days_in_month(DateTime.now.month) + Time.days_in_month(DateTime.now.month - 1) + Time.days_in_month(DateTime.now.month - 2)
  end

  def current_year_days_count
    Date.civil(DateTime.now.year, 02, -1).mday == 29 ? 366 : 365
  end
end