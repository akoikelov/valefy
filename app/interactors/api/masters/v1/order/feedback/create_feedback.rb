module Api
  module Masters
    module V1
      module Order
        module Feedback
          # Save Feedback from master
          # --rating (0..5)
          # --comment
          class CreateFeedback
            include Interactor

            def call
              feedback = MasterFeedback.new params

              if feedback.valid?
                feedback.save
                context.feedback = feedback
              else
                context.fail!(errors: feedback.errors)
              end
            end

            private

            def params
              context.params
            end
          end
        end
      end
    end
  end
end
