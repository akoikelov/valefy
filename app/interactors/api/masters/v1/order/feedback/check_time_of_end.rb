module Api
  module Masters
    module V1
      module Order
        module Feedback
          # --check time - client can set feedback after hour of completed
          class CheckTimeOfEnd
            include Interactor

            def call
              order = OrderItem.find(order_id)

              context.fail!(errors: error_message) if time_is_over?(order)
            end

            private

            def time_is_over?(order)
              Time.zone.now - order.updated_at <= 1.hour
            end

            def order_id
              context.params[:order_item_id]
            end

            def error_message
              'You can left feedback 1 hour of completed'
            end
          end
        end
      end
    end
  end
end
