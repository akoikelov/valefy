module Api
  module Masters
    module V1
      module Order
        module Feedback
          # --check status of order - should be completed
          class CheckStatusOrder
            include Interactor

            def call
              order = OrderItem.find(order_id)

              context.fail!(errors: error_message) unless order.completed_paid?
              context.fail!(errors: feedback_twice_error) if feedback_come_twice?(order)
            end

            private

            def order_id
              context.params[:order_item_id]
            end

            def feedback_come_twice?(order)
              order.master_feedbacks.count.eql?(1)
            end

            def error_message
              'You can left feedback when order will be completed'
            end

            def feedback_twice_error
              'You can leave feedback only once'
            end
          end
        end
      end
    end
  end
end
