module Api
  module Masters
    module V1
      class JoinMaster
        include Interactor

        def call
          master = DraftMaster.new name: params[:name],
                                   phone_number: params[:phone_number],
                                   email: params[:email]
          if master.valid?
            master.save

            Api::Masters::V1::JoinMasterWorker.perform_async(
              draft_master_id: master.id,
              cv: cv,
              id_document: id_document
            ) unless Rails.env.eql?('test')

            context.draft_master = master
          else
            context.fail!(error: 'You need to fill an email, phone_number, name')
          end
        end

        private

        def params
          context.params
        end

        def cv
          begin
            params[:cv].path
          rescue => e
            ""
          end
        end

        def id_document
          begin
            params[:id_document].path
          rescue => e
            ""
          end
        end
      end
    end
  end
end
