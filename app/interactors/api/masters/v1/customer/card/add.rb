module Api
  module Masters
    module V1
      module Customer
        module Card
          # Create stripe customer with token, became from fill Visa card
          class Add
            include Interactor

            def call
              customer = Stripe::Customer.retrieve(customer_account.customer_id)
              customer.sources.create({:source => stripe_token})
            end

            private

            def customer_account
              context.customer_account
            end

            def stripe_token
              context.stripe_token
            end
          end
        end
      end
    end
  end
end
