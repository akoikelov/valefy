module Api
  module Masters
    module V1
      class CreateOrderItem
        include Interactor

        def call
          context.fail!(error: 'Phone number not confirm') unless confirm_phone?

          order = current_user.order_items.new(order_params)

          if order.valid?
            context.fail! unless service_valid?

            order_images.map { |item| order.images << item } if order_images.present?
            order.save
            context.order = order
          else
            context.order  = order
            context.error = order.errors
            context.fail!
          end
        end

        private

        def order_params
          context.params.except(:images)
        end

        def order_images
          context.params[:images]
        end

        def current_user
          context.current_user
        end

        def service_valid?
          Service.find(order_params[:service_id]).present?
        end

        def confirm_phone?
          current_user.confirm_phone_number
        end
      end
    end
  end
end
