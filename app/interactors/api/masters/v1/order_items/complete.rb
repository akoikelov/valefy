module Api
  module Masters
    module V1
      module OrderItems
        # Complete order
        class Complete
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])
            order.end_work_time = DateTime.now

            # TODO не понятно для чего я это сделал, но скорее всего это надо удалить
            # order.order_time = Time.diff(Time.parse(order.start_work.to_s), Time.parse(order.end_work.to_s), '%H %N %S')[:diff]

            if order.valid?
              order_images.map { |item| order.after_work_images << item } if order_images.present?
              order.save!
              order.end_work!
              context.order = order
            else
              context.fail!(error: order.errors)
            end
          end

          private

          def params
            context.params
          end

          def order_images
            context.params[:after_work_images]
          end
        end
      end
    end
  end
end
