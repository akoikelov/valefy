module Api
  module Masters
    module V1
      module OrderItems
        # Create managed account stripe, which connected to our platform
        class Start
          include Interactor

          def call
            order = OrderItem.find(order_id)
            order.start_work = DateTime.now

            if order.valid?
              order_images.map { |item| order.before_work_images << item } if order_images.present?
              order.save!
              order.in_progress_confirmed!
              context.order = order
            else
              context.fail!(error: order.errors)
            end
          end

          private

          def params
            context.params
          end

          def order_id
            params[:order_id]
          end

          def order_images
            context.params[:before_work_images]
          end
        end
      end
    end
  end
end
