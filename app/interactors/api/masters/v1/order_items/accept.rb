module Api
  module Masters
    module V1
      module OrderItems
        # Complete order
        class Accept
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])

            context.fail!(error: I18n.t('order_item.errors.incorrect_status_order_error'), code: :update_available_orders) unless order.searching?
            context.fail!(error: I18n.t('order_item.errors.accept_same_time_error'), code: :accept_same_time_error) if order_time_same?(order)
            # context.fail!(error: I18n.t('order_item.errors.no_account_stripe_error'), code: :no_account_stripe_error) unless current_user.stripe_account.present?

            order.master_id = current_user.id
            order.accepted_time = DateTime.now

            if order.save
              order.accepted!
              context.order = order
            else
              context.fail!(error: order.errors)
            end
          end

          private

          def params
            context.params
          end

          def current_user
            context.current_user
          end

          def order_time_same?(order_want)
            order_want_time = order_want.order_time

            current_user.active_orders.each do |order|
              return true if order_want_time >= order.order_time && order_want_time <= order.duration_date
            end

            false
          end
        end
      end
    end
  end
end
