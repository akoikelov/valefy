module Api
  module Masters
    module V1
      module OrderItems
        # Complete order
        class Cancel
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])
            order.canceled!

            context.order = order
          end

          private

          def params
            context.params
          end

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
