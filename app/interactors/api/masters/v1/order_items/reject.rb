module Api
  module Masters
    module V1
      module OrderItems
        # Complete order
        class Reject
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])

            context.fail!(error: "Cannot reject order") unless order.accepted?

            order.master_id = nil
            order.accepted_time = nil

            if order.save
              order.searching!
              context.order = order
            else
              context.fail!(error: order.errors)
            end
          end

          private

          def params
            context.params
          end
        end
      end
    end
  end
end
