module Api
  module Masters
    module V1
      module Master
        class AddBankAccount
          include Interactor

          def call
            info = current_user.managed_account
            if info.present?
              # TODO need refactor
              # not return managed account id, only account number and routing number
              info.account_number = params[:account_number]
              info.routing_number = params[:routing_number]

              if info.save
                context.info = info
              else
                context.fail!(error: info.errors)
              end
            else
              context.fail!(error: "can not add bank account, you should create stripe account before it")
            end
          end

          private

          def current_user
            context.current_user
          end

          def params
            context.params
          end
        end
      end
    end
  end
end
