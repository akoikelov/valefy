module Api
  module Masters
    module V1
      module Master
        class OndutyUpdate
          include Interactor

          def call
            current_user.update!(onduty: onduty_value)
            context.current_user = current_user
          end

          private

          def current_user
            context.current_user
          end

          def onduty_value
            context.params[:onduty]
          end
        end
      end
    end
  end
end
