module Api
  module Masters
    module V1
      class HelpCreate
        include Interactor

        def call
          help = Help.new question: params[:question], master: current_user

          if help.valid?
            help.save
          else
            context.fail!(error: help.errors)
          end
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end
      end
    end
  end
end
