module Api
  module Masters
    module V1
      module Auth
        class CheckAndSendSms
          include Interactor::Organizer

          organize CheckRequest, SendSms
        end
      end
    end
  end
end
