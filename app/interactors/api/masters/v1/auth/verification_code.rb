module Api
  module Masters
    module V1
      module Auth
        # Verify code which came from sms
        class VerificationCode
          include Interactor

          def call
            user = object.find_by_phone_number(phone)
            context.fail!(error: I18n.t('phone_verification.user_does_not_have_phone')) if user.nil?

            record = user.sms_histories.last

            if record.present?
              confirm_phone_and_status!(user, record)
            else
              context.fail!(error: I18n.t('phone_verification.user_not_found'))
            end
          end

          private

          def search_params
            { code: code,
              phone_number: phone,
              status: false }
          end

          def code
            context.params[:code]
          end

          def phone
            context.params[:phone_number]
          end

          def object
            context.object
          end

          def confirm_phone_and_status!(user, record)
            user.update(confirm_phone_number: true)
            record.update(status: true)
          end
        end
      end
    end
  end
end
