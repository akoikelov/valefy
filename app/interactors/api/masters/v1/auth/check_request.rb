module Api
  module Masters
    module V1
      module Auth
        class CheckRequest
          include Interactor

          def call
            context.fail!(error: "Phone number can't be empty") unless phone_number.present?
            phone = ::Support::ChangePhoneNumber.call(phone_number: phone_number).phone

            context.master = object.where(phone_number: phone).first

            if context.master.nil?
              context.fail!(error: "VALEFYER doesn't exist")
            elsif context.master.present?
              history = context.master.sms_histories.where('created_at >= ?', 10.minutes.ago)

              if history.count >= SmsHistory::COUNT_QUERIES
                diff = Time.diff(Time.now, history.last.created_at, '%H %N %S')

                if diff[:minute] < SmsHistory::COUNT_MINUTES
                  time = SmsHistory::COUNT_MINUTES - diff[:minute]

                  context.fail!(error: "Next query after #{time}")
                end
              end
            end
          end

          private

          def params
            context.params
          end

          def object
            context.object
          end

          def phone_number
            context.params[:phone_number]
          end
        end
      end
    end
  end
end
