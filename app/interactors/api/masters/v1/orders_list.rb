module Api
  module Masters
    module V1
      class OrdersList
        include Interactor

        def call
          if params[:sort].present?
            orders = orders_with_sort
          else
            orders = OrderItem.where.not(status: [7,8,9,10])
          end

          context.orders_list = orders
        end

        private

        def orders_with_sort
          if params[:sort].eql?('0')
            OrderItem.where(status: orders_by_all_statuses).order(id: :desc)
          elsif params[:sort].eql?('1')
            OrderItem.where(id: available_orders_arr)
                     .order(order_time: :asc)
                     .page(page)
                     .per(OrderItem::COUNT_ORDERS_PER_PAGE)
          elsif params[:sort].eql?('2')
            current_user.order_items.busy.order(order_time: :asc)
          elsif params[:sort].eql?('3')
            current_user.order_items.history.order(order_time: :desc)
          else
            context.fail!(error: 'Not correct param sort')
          end
        end

        def current_user
          context.current_user
        end

        def params
          context.params
        end

        def page
          params[:page]
        end

        def available_orders_arr
          OrderItem.waiting
                    .near(coordinates_ar, User::ServiceMan.radius, units: :km, order: false)
                    .where(service_id: need_services)
                    .map(& :id)
                    # .includes(:services)
                    # .where('services.id IN (?)', need_services)
                    # .references(:services).map(& :id)

                    # OrderItem.where(status: available_orders)
                    #           .near(coordinates_ar, User::ServiceMan::RADIUS, units: :km, order: false)
                    #           .where(service_id: need_services)
                    #           .map(& :id)
                    #           .includes(:services)
                    #           .where(services: {id: need_services}).map(& :id)
        end

        def orders_by_all_statuses
          # TODO надо подумать и избавиться от этого метода, чтобы
          # вызывать его в с модели
          # 3 - in pool
          # 4 - accept order
          # 5 - master arriving
          # 7 - master started work but not confirmed from client
          # 8 - client confirme the order
          # 9 - master end work
          # 10 - master completed but not paid
          # 11 -completed and paid

          [3,4,5,8,9,11]
        end

        def need_services
          current_user.services
        end

        def coordinates_ar
          [current_user.lat, current_user.lon]
        end
      end
    end
  end
end
