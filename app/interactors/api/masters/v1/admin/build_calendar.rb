module Api
  module Masters
    module V1
      module Admin
        class BuildCalendar
          include Interactor

          def call
            context.params = calendar
          end

          private

          def params
            context.params
          end

          def master
            context.master
          end

          def calendar
          end
        end
      end
    end
  end
end
