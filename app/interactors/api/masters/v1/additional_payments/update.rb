module Api
  module Masters
    module V1
      module AdditionalPayments
        class Update
          include Interactor

          def call
            payment = AdditionalPayment.find(params[:id])

            context.fail!(error: "client already confirmed") if payment.client_confirm?


            payment.description      = params[:description]
            payment.additional_price = params[:additional_price]

            if payment.save
              context.payment = payment
              context.order   = payment.order
            else
              context.fail!(error: payment.errors)
            end
          end

          private

          def current_user
            context.current_user
          end

          def params
            context.params
          end

          def client_confirm? payment
            payment.client_confirm?
          end
        end
      end
    end
  end
end
