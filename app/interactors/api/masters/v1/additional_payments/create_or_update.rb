module Api
  module Masters
    module V1
      module AdditionalPayments
        class CreateOrUpdate
          include Interactor

          before do
            context.order = OrderItem.find(order_id)
          end

          after do
            context.order.reload
          end

          def call
            context.fail!(error: "Payment already confirmed", code: :confirm_payment_error) if additional_payment_confirmed?

            if order.additional_payment.present?
              order.additional_payment.update_attributes(
                additional_price: price,
                description: description,
                rejected: false
              )
            else
              order.create_additional_payment!(
                additional_price: price,
                description: description
              )
            end
          end

          private

          def additional_payment_confirmed?
            if order.additional_payment.present?
              return order.additional_payment.client_confirm? ? true : false
            end

            return false
          end

          def additional_payment_present?
            order.additional_payment.present?
          end

          def order_id
            context.order_id
          end

          def price
            context.additional_price
          end

          def description
            context.description
          end

          def order
            context.order
          end
        end
      end
    end
  end
end
