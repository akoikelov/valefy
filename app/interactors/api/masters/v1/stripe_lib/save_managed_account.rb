module Api
  module Masters
    module V1
      module StripeLib
        # For creating Stripe managed account
        class SaveManagedAccount
          include Interactor

          def call
            managed_account = ManagedAccount.new(
              serviceman_id: current_user.master.id,
              managed_id:    account.id
            )

            if managed_account.valid?
              managed_account.save
            else
              context.fail!(error: managed_account.errors)
            end
          end

          private

          def account
            context.account
          end

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
