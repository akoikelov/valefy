module Api
  module Masters
    module V1
      module StripeLib
        class AddBankAccount
          include Interactor

          def call
            if add_bank_account_stripe
              save_bank_account_uid
            else
              context.fail!(error: context.error)
            end
          end

          private

          def add_bank_account_stripe
            begin
              context.bank_account = Stripe::Token.create(
                :bank_account => {
                  :country => "IE",
                  :currency => "eur",
                  :account_holder_name => "Name Bank account",
                  :account_holder_type => "individual",
                  :account_number => context.stripe_account.account_number
                }
              )
            rescue => e
              context.error = "Add Bank Account error: #{e.message}"

              false
            end
          end

          def save_bank_account_uid
            context.stripe_account.update!(bank_account_uid: context.bank_account.id)
          end
        end
      end
    end
  end
end