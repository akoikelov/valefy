module Api
  module Masters
    module V1
      module StripeLib
        class AddIdDocument
          include Interactor

          def call
            if upload_file_to_stripe
              save_uid_file
            else
              context.fail!(error: context.error)
            end
          end

          private

          def upload_file_to_stripe
            begin
              context.file = Stripe::FileUpload.create(
                :file => File.new(context.stripe_account.id_document_image.path),
                :purpose => "identity_document",
              )
            rescue => e
              context.error = "Upload file error: #{e.message}"

              false
            end
          end

          def save_uid_file
            context.stripe_account.update!(file_uid: context.file.id)
          end
        end
      end
    end
  end
end