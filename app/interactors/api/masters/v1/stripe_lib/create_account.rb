module Api
  module Masters
    module V1
      module StripeLib
        class CreateAccount
          include Interactor

          def call
            if create_stripe_account
              save_account_uid
            else
              context.fail!(error: context.error)
            end
          end

          private

          def create_stripe_account
            begin
              context.account = Stripe::Account.create(
                managed: true,
                country: 'IE',
                default_currency: 'eur',
                email: context.stripe_account.pro.email,
                tos_acceptance: {
                  ip: '127.0.0.1',
                  date: Time.now.to_i
                },
                legal_entity: {
                  type: 'individual',
                  personal_id_number: context.stripe_account.personal_id,
                  dob: {
                    day: context.stripe_account.pro.b_day.day,
                    month: context.stripe_account.pro.b_day.month,
                    year: context.stripe_account.pro.b_day.year
                  },
                  address: {
                    city: context.stripe_account.city,
                    line1: context.stripe_account.address_line1,
                    state: context.stripe_account.state
                  },
                  verification: {
                    document: context.stripe_account.file_uid
                  },
                  first_name: context.stripe_account.pro.name,
                  last_name: context.stripe_account.pro.last_name,
                  business_name: "stripe - #{context.stripe_account.pro.name}",
                  business_tax_id: context.stripe_account.tax_id
                }
              )
            rescue => e
              context.error = "Create Account Error: #{e.message}"

              false
            end
          end

          def save_account_uid
            context.stripe_account.update!(account_uid: context.account.id)
          end
        end
      end
    end
  end
end
