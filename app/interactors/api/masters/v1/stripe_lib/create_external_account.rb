module Api
  module Masters
    module V1
      module StripeLib
        class CreateExternalAccount
          include Interactor

          def call
            create_external_account
            context.bank_account = context.account = nil
          end

          private

          def create_external_account
            begin
              context.account.external_accounts.create(external_account: context.bank_account.id)
            rescue => e
              context.fail!(error: "Create Ext Account Error: #{e.message}")
            end
          end
        end
      end
    end
  end
end
