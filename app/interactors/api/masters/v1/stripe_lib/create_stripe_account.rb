module Api
  module Masters
    module V1
      module StripeLib
        # For creating Stripe managed account
        class CreateStripeAccount
          include Interactor::Organizer

          organize AddIdDocument, AddBankAccount, CreateAccount, CreateExternalAccount
        end
      end
    end
  end
end
