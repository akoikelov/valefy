module Api
  module Masters
    module V1
      module StripeLib
        # Retrieve stripe managed account for check exist or not
        class RetrieveAccount
          include Interactor

          def call
            account = ManagedAccount.find_by_serviceman_id(current_user.id)
            if account.present?
              context.stripe_account = Stripe::Account.retrieve(account.managed_id)
              context.fail!(error: "Account already exist")
            end
          end

          private

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
