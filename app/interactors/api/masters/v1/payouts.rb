module Api
  module Masters
    module V1
      class Payouts
        include Interactor

        def call
          if master.stripe_account.present?
            account = Stripe::Account.retrieve(master.stripe_account.account_uid)
            external_account = account.external_accounts.data[0]

            context.payouts = Stripe::Payout.list(
              {
                :destination => external_account.id,
                :limit => 100,
                :status => 'pending'
               },
              { :stripe_account => account.id },
            )
          else
            context.fail!(error: "VALEFYER doesn't have stripe account")
          end
        end

        private

        def master
          context.master
        end
      end
    end
  end
end
