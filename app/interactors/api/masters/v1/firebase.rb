module Api
  module Masters
    module V1
      # Create Customer in Stripe
      class Firebase
          include Interactor

          def call
            results = Device.where(id_registration_firebase: id_firebase, app: 'pro')

            if results.present?
              device = results.first
              device.update(token_client_id: last_client_id)
            else
              device = Device.new master_id: current_user.id,
                                  id_registration_firebase: id_firebase,
                                  platform: platform,
                                  token_client_id: last_client_id,
                                  app: 'pro'
              unless device.save
                context.fail!(error: device.errors)
              end
            end
          end

          private

          def last_client_id
            current_user.tokens.keys.last
          end

          def current_user
            context.current_user
          end

          def params
            context.params
          end

          def id_firebase
            params[:id_firebase]
          end

          def platform
            params[:platform]
          end
        end
    end
  end
end
