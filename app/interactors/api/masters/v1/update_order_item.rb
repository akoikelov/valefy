module Api
  module Masters
    module V1
      class UpdateOrderItem
        include Interactor

        def call
          order = OrderItem.find(params[:id])
          if params[:time_zone].eql?('Asia/Bishkek')
            date = DateTime.parse("#{params['start_work']} +0600").utc
          else
            date = DateTime.parse("#{params['start_work']}")
          end

          order.start_work = date

          if order.save
            context.order = order
          else
            context.fail!(error: order.errors)
          end
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end
      end
    end
  end
end
