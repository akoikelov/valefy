module Api
  module Masters
    module V2
      class BuildSchedule
        include Interactor

        def call
          context.schedule = {
            monday: {
              schedule: monday_params,
              available: checked?('monday')
            },
            tuesday: {
              schedule: tuesday_params,
              available: checked?('tuesday')
            },
            wednesday: {
              schedule: wednesday_params,
              available: checked?('wednesday')
            },
            thursday: {
              schedule: thursday_params,
              available: checked?('thursday')
            },
            friday: {
              schedule: friday_params,
              available: checked?('friday')
            },
            saturday: {
              schedule: saturday_params,
              available: checked?('saturday')
            },
            sunday: {
              schedule: sunday_params,
              available: checked?('sunday')
            },
          }
        end

        private

        def params
          context.params[:user_service_man]
        end

        def monday_params
          { start: params[:monday_start], end: params[:monday_end] }
        end

        def tuesday_params
          { start: params[:tuesday_start], end: params[:tuesday_end] }
        end

        def wednesday_params
          { start: params[:wednesday_start], end: params[:wednesday_end] }
        end

        def thursday_params
          { start: params[:thursday_start], end: params[:thursday_end] }
        end

        def friday_params
          { start: params[:friday_start], end: params[:friday_end] }
        end

        def saturday_params
          { start: params[:saturday_start], end: params[:saturday_end] }
        end

        def sunday_params
          { start: params[:sunday_start], end: params[:sunday_end] }
        end

        def checked?(day)
          params[day].eql?('on')
        end
      end
    end
  end
end
