module Api
  module Masters
    module V2
      class OrdersForSchedule
        include Interactor

        def call
          context.available = core_available_orders
          context.active    = current_user.order_items.active
        end

        private

        def current_user
          context.current_user
        end

        def core_available_orders
          OrderItem.where(status: available_orders)
                   .near(coordinates_ar, User::ServiceMan.radius, units: :km, order: false)
                   .includes(:services)
                   .where(services: {id: need_services})
        end

        def available_orders
          # 2 - search or wait, while master accept this order

          [2]
        end

        def need_services
          current_user.services
        end

        def coordinates_ar
          [current_user.lat, current_user.lon]
        end
      end
    end
  end
end
