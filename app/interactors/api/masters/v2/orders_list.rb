module Api
  module Masters
    module V2
      class OrdersList
        include Interactor

        def call
          if params[:sort].present?
            orders = orders_with_sort
          else
            orders = OrderItem.where.not(status: [7,8,9,10])
          end

          context.orders_list = orders
        end

        private

        def orders_with_sort
          if current_user.try(:team).try(:contracts).present?
            if params[:sort].eql?('1')
              if current_user.contract_ids.present?
                OrderItem.waiting.where(contract_id: current_user.contract_ids)
              else
                []
              end
            elsif params[:sort].eql?('2')
              current_user_busy_orders
            elsif params[:sort].eql?('3')
              current_user_history_orders
            end
          else
            if params[:sort].eql?('0')
              OrderItem.where(status: orders_by_all_statuses).order(id: :desc)
            elsif params[:sort].eql?('1')
              if params[:start_date].present? && params[:start_date] != '0'
                with_filter
              elsif params[:end_date].present? && params[:end_date] != '0'
                with_filter
              elsif params[:timetable].eql?('1')
                core_available_orders
              else
                OrderItem.where(id: available_orders_arr)
                        .where('order_time >= ?', Time.zone.now.beginning_of_day)
                        .order(id: :desc)
                        .page(page)
                        .per(OrderItem::COUNT_ORDERS_PER_PAGE)
              end
            elsif params[:sort].eql?('2')
              current_user_busy_orders
            elsif params[:sort].eql?('3')
              current_user_history_orders
            else
              context.fail!(error: 'Not correct param sort')
            end
          end
        end

        def current_user
          context.current_user
        end

        def current_user_busy_orders
          current_user.order_items.busy.order(id: :desc)
        end

        def current_user_history_orders
          current_user.order_items.history.order(id: :desc)
        end

        def params
          context.params
        end

        def with_filter
          if params[:start_date] != '0' && params[:end_date] != '0'
            start_date = Time.at(params[:start_date].to_i).to_datetime.strftime('%a, %d %b %Y %H:%M:%S')
            end_date = Time.at(params[:end_date].to_i).to_datetime.strftime('%a, %d %b %Y %H:%M:%S')

            result = OrderItem.where(id: available_orders_arr)
                     .where(order_time: start_date..end_date)
                     .order(id: :desc)
                     .page(page)
                     .per(OrderItem::COUNT_ORDERS_PER_PAGE)
          elsif params[:start_date] == '0' && params[:end_date] != '0'
            end_date = Time.at(params[:end_date].to_i).to_datetime.strftime('%a, %d %b %Y %H:%M:%S')

            result = OrderItem.where(id: available_orders_arr)
                     .where('order_time <= ?', end_date)
                     .order(id: :desc)
                     .page(page)
                     .per(OrderItem::COUNT_ORDERS_PER_PAGE)
          else
            start_date = Time.at(params[:start_date].to_i).to_datetime.strftime('%a, %d %b %Y %H:%M:%S')
            result = OrderItem.where(id: available_orders_arr)
                     .where('order_time >= ?', start_date)
                     .order(id: :desc)
                     .page(page)
                     .per(OrderItem::COUNT_ORDERS_PER_PAGE)
          end

          result
        end

        def page
          params[:page]
        end

        def available_orders_arr
          core_available_orders.map(& :id)
        end

        def core_available_orders
          list = OrderItem.waiting
                          .near(coordinates_ar, User::ServiceMan.radius, units: :km, order: false)
                          .includes(:services)
                          .where(services: {id: need_services})
          context.available_orders = list

          list
        end

        def orders_by_all_statuses
          # TODO нужно как то вместо этой функции вызывать с модели
          # 3 - in pool
          # 4 - accept order
          # 5 - master arriving
          # 7 - master started work but not confirmed from client
          # 8 - client confirme the order
          # 9 - master end work
          # 10 - master completed but not paid
          # 11 -completed and paid

          [3,4,5,8,9,11]
        end

        def need_services
          current_user.services
        end

        def coordinates_ar
          [current_user.lat, current_user.lon]
        end
      end
    end
  end
end
