module Api
  module Masters
    module V2
      module OrderItems
        # Complete order
        class Complete
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])
            order.end_work_time = DateTime.now

            if order.valid?
              if order_images.present? && order.after_work_images.empty?
                order_images.map { |item| order.after_work_images << item }
                order.save!
              end

              if order.client.corp? || order.master.company.valefy?
                order.end_work!
              else
                if order.master.use_bank_details?
                  result = ::Api::V2::Customer::CreateTransfer.call(order: order)
                  result.success? ? order.end_work! : context.fail!(error: result.error)
                else
                  context.fail!(error: "Cannot transfer money to your account, because you don't have bank details.")
                  # TODO Need fix ASAP
                end
              end

              context.order = order
            else
              context.fail!(error: order.errors)
            end
          end

          private

          def params
            context.params
          end

          def order_images
            context.params[:after_work_images]
          end
        end
      end
    end
  end
end
