module Api
  module V3
    class AvailableSchedule
      include Interactor

      def call
        context.fail!(error: 'Missing parametr date') if !date.present?
        context.schedule = []

        need_date = Date.parse(date)
        day_name  = need_date.strftime('%A')

        if Time.diff(need_date, Time.now)[:day] > bisy_days_for_order
          result = ScheduleWorkload.where(date: need_date.beginning_of_day..need_date.end_of_day)

          if result.present?
            schedule = result.first.schedule

            context.uniq_schedule  = uniq_schedule(schedule)
            context.schedule_day   = make_hash_other(day_name)
          else
            make_schudele_by_free_pros(day_name.downcase)

            schedule = ScheduleWorkload.create(
              date: need_date,
              schedule: context.schedule
            )
            context.uniq_schedule = uniq_schedule(context.schedule)
            context.schedule_day  = make_hash_other(day_name)
          end
        else
          context.schedule_day = { "#{day_name}": [] }
        end
      end

      private

      def uniq_schedule(schedule)
        ar = []

        schedule.each do |hash|
          ar << { 'time'=> hash['time'], 'available'=> hash['available'] } if uniq_value?(ar, hash['time']) && hash['available'].eql?(true)
        end
        # new_tracks.uniq { |track| [track[:name], track[:artist]].join(":") } # TODO может попробовать вот это как за алгоритм взятия уникального хэша

        ar
      end

      def bisy_days_for_order
        Setting.try(:first).try(:bisy_days_for_order) || 0
      end

      def uniq_value?(ar, value)
        return true if ar.empty? #=> false

        !(ar.any? {|h| h['time'].eql?(value)})
      end

      def date
        context.date
      end

      def make_hash_other(day_name)
        { "#{day_name}"=>context.uniq_schedule } 
      end

      def template_day_default
        template = []

        ProInformation::HOURS.each_with_index do |hour_minutes, index|
          template << { 'time'=> hour_minutes, 'available'=> false }
        end

        template
      end

      def make_schudele_by_free_pros(day)
        pros = ::User::ServiceMan.free_pros_arr_by(day)

        pros.each do |pro|
          _available     = false
          info           = pro.pro_information
          pro_start_time = info.work_schedule[day]['schedule']['start']
          pro_end_time   = info.work_schedule[day]['schedule']['end']

          template_day_default.each do |sc, v|
            _available = true if sc['time'].eql?(pro_start_time)

            context.schedule << { 'time'=> sc['time'], 'available'=> true, 'pro_id'=> pro.id } if _available

            _available = false if sc['time'].eql?(pro_end_time)
          end
        end
      end

      # template day =>
      # [
      #   {:id=>1, :time=>"7:30", :available=>false},
      #   {:id=>2, :time=>"8:00", :available=>false},
      #   {:id=>3, :time=>"8:30", :available=>false},
      #   {:id=>4, :time=>"9:00", :available=>false},
      #   {:id=>5, :time=>"9:30", :available=>false},
      #   {:id=>6, :time=>"10:00", :available=>false},
      #   {:id=>7, :time=>"10:30", :available=>false},
      #   {:id=>8, :time=>"11:00", :available=>false},
      #   {:id=>9, :time=>"11:30", :available=>false},
      #   {:id=>10, :time=>"12:00", :available=>false},
      #   {:id=>11, :time=>"12:30", :available=>false},
      #   {:id=>12, :time=>"13:00", :available=>false},
      #   {:id=>13, :time=>"13:30", :available=>false},
      #   {:id=>14, :time=>"14:00", :available=>false},
      #   {:id=>15, :time=>"14:30", :available=>false},
      #   {:id=>16, :time=>"15:00", :available=>false},
      #   {:id=>17, :time=>"15:30", :available=>false},
      #   {:id=>18, :time=>"16:00", :available=>false},
      #   {:id=>19, :time=>"16:30", :available=>false}
      # ]
    end
  end
end
