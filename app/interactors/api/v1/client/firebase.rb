module Api
  module V1
    module Client
      # Create Customer in Stripe
      class Firebase
        include Interactor

        def call
          device = Device.new client_id: current_user.id,
                              id_registration_firebase: id_client_firebase,
                              platform: platform

          unless device.save
            context.fail!(error: device.errors)
          end
        end

        private

        def current_user
          context.current_user
        end

        def params
          context.params
        end

        def id_client_firebase
          params[:id_client_firebase]
        end

        def platform
          params[:platform]
        end
      end
    end
  end
end
