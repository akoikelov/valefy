module Api
  module V1
    module Order
      module Feedback
        # Save Feedback from client
        # --rating (0..5)
        # --comment
        class CreateFeedback
          include Interactor

          def call
            feedback = current_user.feedbacks.new params.merge({ serviceman_id: master.id })

            if feedback.save
              context.feedback = feedback
            else
              context.fail!(errors: feedback.errors)
            end
          end

          private

          def params
            context.params
          end

          def master
            context.order.master
          end

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
