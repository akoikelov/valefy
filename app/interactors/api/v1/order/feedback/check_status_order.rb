module Api
  module V1
    module Order
      module Feedback
        # --check status of order - should be completed
        class CheckStatusOrder
          include Interactor

          def call
            order = OrderItem.find(order_id)

            if order.completed_paid?
              context.order = order
            else
              context.fail!(errors: error_message)
            end
          end

          private

          def order_id
            context.params[:order_item_id]
          end

          def error_message
            'You can left feedback when order will be completed and paid'
          end
        end
      end
    end
  end
end
