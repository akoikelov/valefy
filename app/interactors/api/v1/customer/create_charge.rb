module Api
  module V1
    module Customer
      # pre - authorize charged from customer
      class CreateCharge
        include Interactor

        def call
          order = OrderItem.find(params[:order_id])

          begin
            charge = Stripe::Charge.create({
              :amount => order.in_cents,
              :currency => "eur",
              :customer => current_user.customer_account.customer_id,
              :capture  => false,
              :description => "Charge for #{current_user.email}. Order is #{order.id}"
            })
          rescue => error
            context.fail!(error: error.message)
          end

          if charge.status.eql?('succeeded')
            result = SaveCharge.call(order: order, charge: charge)

            if result.success?
              order.pre_authorize_charged!
              context.order = order
            else
              context.fail!(error: 'Can not save the charge locally')
            end
          else
            context.fail!(error: 'Charge not succeeded')
          end
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end
      end
    end
  end
end
