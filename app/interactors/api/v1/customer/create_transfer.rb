module Api
  module V1
    module Customer

      class CreateTransfer
        include Interactor

        def call
          captured_charge = order.charges.where(capture: true).first
          connect_account = order.master.stripe_account.account_uid

          begin
            transfer = Stripe::Transfer.create(
              :amount => order.rest,
              :currency => "eur",
              :destination => connect_account,
              :transfer_group => "order-#{order.id}",
              :source_transaction => captured_charge.charge_id
            )

            Transfer.create!(
              order_item_id: order.id,
              transfer_id: transfer.id,
              amount: order.rest,
              destination_payment: transfer.destination_payment,
              customer_account_id: order.client.customer_account.id,
              managed_account_id: connect_account,
              balance_transaction: transfer.balance_transaction
            )
          rescue => e
            context.fail!(error: e.message)
          end
        end

        private

        def order
          context.order
        end
      end
    end
  end
end
