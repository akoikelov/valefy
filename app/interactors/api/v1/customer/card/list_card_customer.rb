module Api
  module V1
    module Customer
      module Card
        # Retrieve all cards of current user
        class ListCardCustomer
          include Interactor

          def call
            customer = current_user.customer_account

            if customer.present?
              context.list = Stripe::Customer.retrieve(customer.customer_id)
                                             .sources
                                             .all(:object => 'card')

              context.fail!(error: 'No card') if context.list.count.zero?
            else
              context.fail!(error: 'No such customer')
            end
          end

          private

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
