module Api
  module V1
    module Customer
      # Create stripe customer with token, became from fill Visa card
      class CreateCustomer
        include Interactor::Organizer

        organize CreateOrAddCard
      end
    end
  end
end
