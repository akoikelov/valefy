module Api
  module V1
    module Customer

      class CaptureCharge
        include Interactor

        def call
          stripe_account = Stripe::Account.retrieve(account.managed_id)
          context.fail!(error: I18n.t('order_item.errors.pay_order_error')) unless account_present?(stripe_account)

          charge = order.charges.where(capture: false).first

          if charge.present?
            ch = Stripe::Charge.retrieve(charge.charge_id)
            ch.capture
            charge.capture!
          else
            context.fail!(error: I18n.t('order_item.errors.fees_not_found_error'))
          end
        end

        private

        def order
          context.order
        end

        def account
          order.master.stripe_account
        end

        def account_present?(stripe_account)
          stripe_account.legal_entity.verification.status.eql?('verified')
        end
      end
    end
  end
end
