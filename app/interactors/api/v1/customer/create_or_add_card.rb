module Api
  module V1
    module Customer
      class CreateOrAddCard
        include Interactor

        def call
          if current_user.customer_account.present?
            add_card
          else
            create_and_save_customer
          end
        end

        private

        def create_and_save_customer
          customer = create_account_and_add_card_in_stripe
          account = build_account(customer)

          if account.valid?
            account.save
          else
            context.fail!(error: account.errors)
          end
        end

        def add_card
          begin
            Card::Add.call(
              customer_account: current_user.customer_account,
              stripe_token: params[:stripe_token]
            )
          rescue => error
            context.fail!(error: error.message)
          end
        end

        def create_account_and_add_card_in_stripe
          begin
            Stripe::Customer.create(
              :email => current_user.email,
              :source => params[:stripe_token]
            )
          rescue => error
            context.fail!(error: error.message)
          end
        end

        def build_account(customer)
          CustomerAccount.new(
            client_id: current_user.id,
            customer_id: customer.id
          )
        end

        def current_user
          context.current_user
        end

        def params
          context.params
        end
      end
    end
  end
end
