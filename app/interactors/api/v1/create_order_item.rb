module Api
  module V1
    class CreateOrderItem
      include Interactor

      def call
        # context.fail!(error: 'Phone number not confirm', code: :phone_not_confirm) unless confirm_phone?
        set_time_zone

        order = current_user.order_items.new(order_params)

        if order.valid?
          # context.fail! unless service_valid?

          order_images.map { |item| order.images << item } if order_images.present?

          context.fail!(error: "Can not create with different subcategories", code: "different_subcategories_error") unless services_valid?(order)
          if order.save
            order.services << Service.where(id: service_ids_arr) if service_ids_arr.present?
            context.order = order
          else
            context.order  = order
            context.error = order.errors
            context.fail!
          end
        else
          context.order  = order
          context.error = order.errors
          context.fail!
        end
      end

      private

      def order_params
        context.params.except(:images, :time_zone, :service_ids)
      end

      def order_images
        context.params[:images]
      end

      def service_ids_arr
        context.params[:service_ids]
      end

      def current_user
        context.current_user
      end

      def set_time_zone
        Time.zone = OrderItem::TIME_ZONES[time_zone] if time_zone.present?
      end

      def service_valid?
        Service.find(order_params[:service_id]).present?
      end

      def services_valid?(order)
        return true unless service_ids_arr.present?

        first_id = service_ids_arr.first

        subcategory = Service.find(first_id).subcategory

        return subcategory.services.where(id: service_ids_arr).count >=  service_ids_arr.count
      end

      def time_zone
        context.params[:time_zone]
      end

      def services_arr

      end

      def confirm_phone?
        current_user.confirm_phone_number
      end
    end
  end
end
