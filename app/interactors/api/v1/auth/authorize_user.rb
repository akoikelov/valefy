module Api
  module V1
    module Auth
      class AuthorizeUser
        include Interactor::Organizer

        organize FetchUser, SignInUser
      end
    end
  end
end
