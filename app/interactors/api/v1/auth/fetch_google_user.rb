module Api
  module V1
    module Auth
      # Fetch data from Facebook ( name, email )
      class FetchGoogleUser
        include Interactor

        def call
          context.fail!(error: "access_token can't be blank") unless access_token.present?

          response = HTTParty.get("https://www.googleapis.com/oauth2/v2/userinfo",
                            headers: {"Access_token"  => access_token,
                                      "Authorization" => "OAuth #{access_token}"})
          if response.code == 200
            data = JSON.parse(response.body)
            context.profile = data
          else
            context.fail!(error: response['error']['message'])
          end
        end

        private

        def access_token
          context.params[:access_token]
        end
      end
    end
  end
end
