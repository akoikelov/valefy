module Api
  module V1
    module Auth
      class CheckAndSendSms
        include Interactor::Organizer

        organize CheckRequest, SendSms
      end
    end
  end
end
