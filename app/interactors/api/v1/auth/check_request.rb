module Api
  module V1
    module Auth
      class CheckRequest
        include Interactor

        def call
          context.fail!(error: "Phone number can't be empty") unless phone_number.present?

          # result = Support::ValidatePhoneNumber.call(phone_number: phone_number)

          # context.fail!(error: result.error) unless result.success?

          context.client = object.where(phone_number: params[:phone_number]).first

          if context.client.nil?
            context.fail!(error: "Client doesn't exist")
          elsif context.client.present?
            history = context.client.sms_histories.where('created_at >= ?', 10.minutes.ago)

            if history.count >= SmsHistory::COUNT_QUERIES
              diff = Time.diff(Time.now, history.last.created_at, '%H %N %S')

              if diff[:minute] < SmsHistory::COUNT_MINUTES
                time = SmsHistory::COUNT_MINUTES - diff[:minute]

                context.fail!(error: "Next query after #{time}")
              end
            end
          end
        end

        private

        def params
          context.params
        end

        def object
          context.object
        end

        def phone_number
          params[:phone_number]
        end
      end
    end
  end
end
