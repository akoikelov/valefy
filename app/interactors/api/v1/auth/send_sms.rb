module Api
  module V1
    module Auth
      class SendSms
        include Interactor

        def call
          twilio = Twilio::REST::Client.new(ENV['TWILIO_ACCOUNT_SID'], ENV['TWILIO_AUTH_TOKEN'])

          begin
            unless Rails.env.eql?('test')
              response = send_sms(twilio, sender_id: 'SORTD') # TODO подрефакторить
            end
          rescue Twilio::REST::RestError => error
            if error.code == 21606
              response = send_sms(twilio, sender_id: nil)
            end
          end

          if response.present? || Rails.env.eql?('test')
            client.sms_histories.create! phone_number: phone, code: code
          else
            context.fail!(error: "SMS does't sent.")
          end
        end

        private

        def send_sms(twilio, sender_id: sender)
          default = sender_id || twilio_number

          response = twilio.api.account.messages.create(
             :from => default,
             :to => phone,
             :body => "Your code is - #{code}"
          )

          response
        end

        def twilio_number
          '+12567438278'
        end

        def verification_params
          { phone_number: phone, code: code }
        end

        def generate_code
          Random.rand(10000..99999).to_s
        end

        def code
          @code = @code.nil? ? generate_code : @code
        end

        def sinch_app_key
          context.params[:sinch_app_key]
        end

        def sinch_app_secret
          context.params[:sinch_app_secret]
        end

        def phone
          context.params[:phone_number]
        end

        def message
          "Your code is #{code}"
        end

        def client
          context.client
        end
      end
    end
  end
end
