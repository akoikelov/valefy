module Api
  module V1
    module Auth
      class AuthorizeGoogleUser
        include Interactor::Organizer

        organize FetchGoogleUser, SignInGoogleUser
      end
    end
  end
end
