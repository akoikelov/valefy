module Api
  module V1
    class UpdateOrderItem
      include Interactor::Organizer

      organize UpdateOrderTime, SendNotification
    end
  end
end
