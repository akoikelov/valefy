module Api
  module V1
    module Master
      module Work
        class UpdateStatus
          include Interactor

          def call
            order = OrderItem.find(params[:order_id])

            context.fail!(error: 'Order is not found') if order.nil?
            context.fail!(error: "Order already in #{order.status} status") if order.in_progress?

            if OrderItem.statuses.value?(status)
              order.update(status: 6, start_work: Time.now)
              context.order = order
            else
              context.fail!(error: 'Status is invalid parameter')
            end
          end

          private

          def params
            context.params
          end

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
