module Api
  module V1
    class SendNotification
      include Interactor

      def call
        begin
          unless Rails.env.eql?('test')
            ::Api::V1::UpdateOrderTimeEmailWorker.perform_async(order_id: order.id, old_time: old_time)
            ::Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: "Order time is changed. Order ID - #{order.id}")
          end
        rescue => error
          Raven.capture_exception(error)

          Rails.logger.error("#{error.message}: #{error.backtrace}")
        end
      end

      private

      def order
        context.order
      end

      def old_time
        context.old_time
      end
    end
  end
end
