module Api
  module V1
    class UpdateOrderTime
      include Interactor

      def call
        date = order.order_time
        context.old_time = date

        time = Time.parse(date.to_s)
        time = time + (days).days
        time = time + (hour).hours

        order.update(order_time: time.to_s)
      end

      private

      def current_user
        context.current_user
      end

      def params
        context.params
      end

      def order
        context.order
      end

      def days
        params[:days].to_i || 0
      end

      def milliseconds
        params[:milliseconds].to_i
      end

      def sign
        milliseconds > 0 ? 1 : -1
      end

      def min
        milliseconds.abs / 60_000
      end

      def hour
        (min.abs / 60.to_f)*sign
      end
    end
  end
end
