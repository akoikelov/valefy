module Api
  module V1
    module OrderItems
      # Complete order
      class Cancel
        include Interactor

        def call
          order = OrderItem.find(params[:order_id])
          order.canceled!
          order_status_history(order)
          context.order = order
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end

        def order_status_history(order)
          OrderStatusHistory.create(order_item: order, status: :cancelled, author: context.author) if context.author.present?
        end
      end
    end
  end
end
