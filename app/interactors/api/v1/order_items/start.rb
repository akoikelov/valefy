module Api
  module V1
    module OrderItems
      # Create managed account stripe, which connected to our platform
      class Start
        include Interactor

        def call
          order = OrderItem.find(order_id)
          context.fail!(error: 'Order id not correct') unless order.present?

          order.in_progress_confirmed!
          context.order = order
        end

        private

        def params
          context.params
        end

        def order_id
          params[:order_id]
        end
      end
    end
  end
end
