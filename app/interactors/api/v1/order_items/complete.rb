module Api
  module V1
    module OrderItems
      # Complete order and paid
      class Complete
        include Interactor::Organizer

        organize CompletedUnpaidStatusCheck, CompletedPaid
      end
    end
  end
end
