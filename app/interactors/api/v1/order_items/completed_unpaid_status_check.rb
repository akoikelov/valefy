module Api
  module V1
    module OrderItems
      # Complete order but not paid
      class CompletedUnpaidStatusCheck
        include Interactor

        def call
          context.order = OrderItem.find(params[:order_id])

          context.fail!(error: 'status should be completed_unpaid') unless context.order.completed_unpaid?
        end

        private

        def params
          context.params
        end
      end
    end
  end
end
