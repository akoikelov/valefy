module Api
  module V1
    module OrderItems
      # Complete order
      class Complete
        include Interactor::Organizer

        organize CompletedNotPaid
      end
    end
  end
end
