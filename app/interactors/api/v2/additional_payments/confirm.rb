module Api
  module V2
    module AdditionalPayments
      class Confirm
        include Interactor

        def call
          payment = AdditionalPayment.find(params[:payment_id])

          payment.client_confirm = true
          payment.rejected = false

          if payment.valid?
            charge = create_charge(payment)
            save_in_db_charge(charge, payment)
            update_order_total_price(payment)
            payment.save!
          end

          context.order = payment.order
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end

        def create_charge(payment)
          begin
            Stripe::Charge.create({
              :amount => payment.in_cents,
              :currency => "eur",
              :customer => current_user.customer_account.customer_id,
              :capture  => false,
              :description => "Charge for additional payment. User - #{current_user.email}"
            })
          rescue => error
            context.fail!(error: error.message)
          end
        end

        def save_in_db_charge(charge, payment)
          if charge.status.eql?('succeeded') # TODO нужно посмотреть какие еще статусы бывают
            order  = payment.order
            if order.charges.present?
              _charge = order.charges.last

              _charge.update!(
                additional_payment_charge_id: charge.id,
                additional_payment_capture: false
              )
            else
              # TODO надо снимать деньги как то если корп
              order.charges.create!(
                additional_payment_charge_id: charge.id,
                additional_payment_capture: false
              )
            end

          else
            context.fail!(error: 'Can not save charge')
          end
        end

        def update_order_total_price(payment)
          order       = payment.order.reload
          total_price = order.total_price
          order.update(total_price: payment.additional_price + total_price)
        end
      end
    end
  end
end
