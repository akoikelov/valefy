module Api
  module V2
    module AdditionalPayments
      class Reject
        include Interactor

        def call
          context.payment = AdditionalPayment.find(params[:payment_id])
          context.payment.update!(client_confirm: false, rejected: true)
          context.order = context.payment.order
        end

        private

        def params
          context.params
        end
      end
    end
  end
end
