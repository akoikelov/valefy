module Api
  module V2
    class CreatePromoCode
      include Interactor

      def call
        context.promo_code = PromoCode.create! promo_code_params
      end

      private

      def promo_code_params
        {
          promo_code_id: promo_code_id,
          discount: rand(2..3),
          exp_date: 3.days.from_now,
          client_id: context.resource.id
        }
      end

      def promo_code_id
        SecureRandom.urlsafe_base64(nil, false)
      end
    end
  end
end