module Api
  module V2
    class UpdateOrderItem
      include Interactor::Organizer

      def call
        update_order
        calculate_and_update_total_price
      end

      private

      def update_order
        context.order.update_attributes(context.params.except(:service_ids))
        context.order.services = Service.where(id: context.params[:service_ids])
        context.save
      end

      def calculate_and_update_total_price
        _order          = context.order.reload
        client          = _order.client
        client_services = client.client_services
        price           = 0
        _amount         = 0

        
        _order.services.each do |service|
          _service = client_services.where(service_id: service.id)
          if _service.present?
            _service = _service.first

            case _service.service.subcategory.code
            when 'car'
              _amount = _order.car_count * _service.price.to_i
            when 'suv'
              _amount = _order.suv_count * _service.price.to_i
            when 'minivan'
              _amount = _order.minivan_count * _service.price.to_i
            when 'pram'
              _amount = _service.price.to_i
            end
          else
            case service.subcategory.code
            when 'car'
              _amount = _order.car_count * service.price.to_i
            when 'suv'
              _amount = _order.suv_count * service.price.to_i
            when 'minivan'
              _amount = _order.minivan_count * service.price.to_i
            when 'pram'
              _amount = service.price.to_i
            end
          end

          price    = _amount + price
          _amount  = 0
        end

        baby_price = _order.baby_seat * 5
        price = price + baby_price

        _order.update!(total_price: price)
      end
    end
  end
end
