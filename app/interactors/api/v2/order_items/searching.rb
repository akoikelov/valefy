module Api
  module V2
    module OrderItems
      # Create managed account stripe, which connected to our platform
      class Searching
        include Interactor

        def call
          order = OrderItem.find(order_id)

          # context.fail!(error: "Unable confirm the order, you should pay") if !current_user.corp? && order.charges.empty?
          # context.fail!(error: "Unable confirm the order, you should pay") if !current_user.cash? && order.charges.empty?
          # TODO

          order.searching!
          created_order_stationary_email(order) if order.stationary_id.present?
          need_date = order.order_time
          result = ScheduleWorkload.where(date: need_date.beginning_of_day..need_date.end_of_day)
          if result.present?
            object   = result.first
            schedule = result.first.schedule
            hour_minutes = order.order_time.strftime('%H:%M')
            schedule_reverse = schedule.reverse

            schedule.reverse.each do |hash|
              if hour_minutes.include?(hash['time']) && hash['available'].eql?(true)
                hash['available'] = false
                pro_id = hash['pro_id']
                next_time = (order.order_time+3.hours).strftime('%H:%M')
                prev_time = (order.order_time-3.hours).strftime('%H:%M')
                i = 30

                loop do
                  hour_minutes_next = (order.order_time+i.minutes).strftime('%H:%M')
                  hour_minutes_prev = (order.order_time-i.minutes).strftime('%H:%M')
                  break if next_time.include?(hour_minutes_next) && prev_time.include?(hour_minutes_prev)

                  schedule.each do |h|
                    if h['pro_id'].eql?(pro_id) && hour_minutes_next.include?(h['time'])
                      h['available'] = false
                    elsif h['pro_id'].eql?(pro_id) && hour_minutes_prev.include?(h['time'])
                      h['available'] = false
                    end
                  end
                  i = i+30
                end
                break
              end
            end

            object.update(schedule: schedule)
          end

          context.order = order
        end

        private

        def params
          context.params
        end

        def order_id
          params[:order_id]
        end

        def current_user
          context.current_user
        end

        def created_order_stationary_email(order)
          unless Rails.env.eql?('test')
            ::Api::V1::SendEmailWorker.perform_async(
              client_id: order.client.id,
              email_method: 'client_order_stationary_email',
              order_id: order.id
            )
          end
        end
      end
    end
  end
end
