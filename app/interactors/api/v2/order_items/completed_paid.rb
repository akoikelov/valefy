module Api
  module V2
    module OrderItems
      # Complete order but not paid
      class CompletedPaid
        include Interactor

        def call
          result = Customer::CaptureCharge.call(order: order)

          if result.success?
            result = Customer::CreateTransfer.call(order: order)

            if result.success?
              order.completed_paid!
              context.order = order

              # TODO hard fix needed
              if order.subscription.present?
                subscription = order.subscription

                order = subscription.order_items.regular.first

                result = ::Api::V2::Customer::CreateCharge.call(
                  params: { order_id: order.id }, current_user: current_user, charge_type: CustomerCharge.payment
                )

                result.order.searching!
              end
            else
              context.fail!(error: 'Can not make the transfer')
            end
          else
            context.fail!(error: result.error)
          end
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end

        def order
          context.order
        end
      end
    end
  end
end
