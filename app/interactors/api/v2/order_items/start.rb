module Api
  module V2
    module OrderItems
      # Create managed account stripe, which connected to our platform
      class Start
        include Interactor

        def call
          order = OrderItem.find(order_id)
          context.fail!(error: 'Order id not correct') unless order.present?

          order.in_progress_confirmed!
          order.start_work = DateTime.now
          order.save!
          context.order = order.reload
        end

        private

        def params
          context.params
        end

        def order_id
          params[:order_id]
        end
      end
    end
  end
end
