module Api
  module V2
    module OrderItems
      class UpdateOrder
        include Interactor

        after do
          context.order.reload
        end

        def call
          order.total = order.sum_services + order.sum_additional_payment
          order.save!

          Api::V2::CreateInvoicePro.call(order: order, current_user: current_user)
        end

        private

        def order
          context.order
        end

        def current_user
          context.user
        end
      end
    end
  end
end
