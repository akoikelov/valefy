module Api
  module V2
    module OrderItems
      # Complete order and paid
      class Complete
        include Interactor::Organizer

        organize CompletedNotPaid, CompletedPaid, UpdateOrder
      end
    end
  end
end
