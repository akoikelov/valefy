module Api
  module V2
    module OrderItems
      # Complete order for corp client
      class CompletesCorpClient
        include Interactor

        def call
          context.fail!(error: 'Client is not corp') if client_not_corp?

          order = OrderItem.find(params[:order_id])
          order.completed_paid!
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end

        def client_not_corp?
          current_user.corp? ? false : true
        end
      end
    end
  end
end
