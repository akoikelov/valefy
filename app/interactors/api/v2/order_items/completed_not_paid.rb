module Api
  module V2
    module OrderItems
      # Complete order but not paid
      class CompletedNotPaid
        include Interactor

        def call
          order = OrderItem.find(params[:order_id])

          order.completed_unpaid!
          context.order = order
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end
      end
    end
  end
end
