module Api
  module V2

    class AvailableSchedule
      include Interactor

      def call
        context.schedule      = template_day
        context.schedule_week = {}

        7.times do |item|
          current_time     = Time.now + item.days
          current_day_name = current_time.strftime('%A')
          end_hour         = ProInformation::END_DAY
          context.schedule_week.merge!("#{current_day_name}": [])

          create_schedule(end_hour, current_day_name)

          context.schedule = template_day
        end
      end

      private

      def create_schedule(end_hour, current_day_name)
        make_schudele_by_free_pros(end_hour, current_day_name.downcase)
        add_to_week_schedule(current_day_name)
      end

      def add_to_week_schedule(current_day_name)
        context.schedule_week[current_day_name.to_sym] = context.schedule
      end

      def make_schudele_by_free_pros(end_hour, day)
        pros = ::User::ServiceMan.free_pros_arr

        pros.each do |pro|
          info = pro.pro_information
          make_schedule(info, day) if pro_available?(info, day)
        end
      end

      def pro_available?(info, day)
        info.work_schedule[day]['available'] ? true : false if info.present?
      end

      def make_schedule(info, day)
        pro_start_time = info.work_schedule[day]['schedule']['start']
        pro_end_time   = info.work_schedule[day]['schedule']['end']

        set_true_at_time(pro_start_time, pro_end_time)
      end

      def set_true_at_time(pro_start_time, pro_end_time)
        _available = false

        context.schedule.each do |rec, v|
          _available = true   if rec[:time].eql?(pro_start_time)

          rec[:available] = true if _available

          _available = false  if rec[:time].eql?(pro_end_time)
        end
      end

      def template_day
        default_template = []

        ProInformation::HOURS.each_with_index do |hour, index|
          default_template << { id: index+1, time: hour, available: false }
        end

        default_template
      end

      # template day =>
      # [
      #   {:id=>1, :time=>"7:30", :available=>false},
      #   {:id=>2, :time=>"8:00", :available=>false},
      #   {:id=>3, :time=>"8:30", :available=>false},
      #   {:id=>4, :time=>"9:00", :available=>false},
      #   {:id=>5, :time=>"9:30", :available=>false},
      #   {:id=>6, :time=>"10:00", :available=>false},
      #   {:id=>7, :time=>"10:30", :available=>false},
      #   {:id=>8, :time=>"11:00", :available=>false},
      #   {:id=>9, :time=>"11:30", :available=>false},
      #   {:id=>10, :time=>"12:00", :available=>false},
      #   {:id=>11, :time=>"12:30", :available=>false},
      #   {:id=>12, :time=>"13:00", :available=>false},
      #   {:id=>13, :time=>"13:30", :available=>false},
      #   {:id=>14, :time=>"14:00", :available=>false},
      #   {:id=>15, :time=>"14:30", :available=>false},
      #   {:id=>16, :time=>"15:00", :available=>false},
      #   {:id=>17, :time=>"15:30", :available=>false},
      #   {:id=>18, :time=>"16:00", :available=>false},
      #   {:id=>19, :time=>"16:30", :available=>false}
      # ]
    end
  end
end
