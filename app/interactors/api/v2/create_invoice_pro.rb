module Api
  module V2
    class CreateInvoicePro
      include Interactor

      def call
        invoice = Invoice.new(
          order_id: order.id,
          client_id: order.client.id,
          number: "#{order.id}#{rand(1000...9000)}P",
          currency: 'euro',
          type_invoice: Invoice::INVOICE_TYPE_PROVIDER
        )

        if invoice.save
          context.invoice = invoice

          order.services.each do |service|
            # TODO вынести в отдельный класс создание LineItem
            invoice.line_items.create(service_id: service.id,price: service.price)
          end
        end
      end

      private

      def order
        context.order
      end

      def current_user
        context.current_user
      end
    end
  end
end
