module Api
  module V2
    module PromoCodes
      class Check
        include Interactor

        def call
          context.fail!(error: 'invalid or expired promo code', code: 'invalid_or_expired_promo_code') \
            if invalid_promo_code?

          context.data = data(order, context.promo_code.first)
        end

        private

        def data(order,promo_code)
          {
            promo_code_id: promo_code.id,
            promo_code: promo_code.promo_code_id,
            total_price: order.total_price.to_i,
            total_price_with_discount: total_price_with_discount(order, promo_code)
          }
        end

        def params
          context.params
        end

        def invalid_promo_code?
          if order.station
            context.promo_code = order.station.promo_codes.where(promo_code_id: params[:promo_code])
          else
            context.promo_code = PromoCode.available.where(promo_code_id: params[:promo_code])
          end

          !context.promo_code.present?
        end

        def order
          @order ||= OrderItem.find(params[:order_id])
        end

        def total_price_with_discount(order, promo_code)
          (order.total_price - promo_code.discount).to_i
        end
      end
    end
  end
end