module Api
  module V2
    module Customer
      # pre - authorize charged from customer
      class CreateCharge
        include Interactor

        def call
          begin
            charge = Stripe::Charge.create({
              :amount => amount,
              :currency => currency,
              :customer => current_user.customer_account.customer_id,
              :description => description,
              :transfer_group => transfer_group,
            })
          rescue => error
            context.fail!(error: error.message)
          end

          if charge.status.eql?('succeeded')
            result = SaveCharge.call(order: order, charge: charge, charge_type: context.charge_type)

            if result.success?
              order.pre_authorize_charged!
              context.order = order
            else
              context.fail!(error: 'Can not save the charge locally')
            end
          else
            context.fail!(error: 'Charge not succeeded')
          end
        end

        private

        def params
          context.params
        end

        def transfer_group
          params[:transfer_group].presence || "order-#{order.id}"
        end

        def amount
          params[:amount]
        end

        def description
          params[:description]
        end

        def order
          params[:order]
        end

        def current_user
          context.current_user
        end

        def currency
          params[:currency]
        end
      end
    end
  end
end
