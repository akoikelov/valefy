module Api
  module V2
    module Customer

      class CreateTransfer
        include Interactor

        def call
          begin
            lib = TransfersLib.new(transfer_params)
            transfer = lib.transfer

            Transfer.create!(
              order_item_id: context.order.id,
              transfer_id: transfer.id,
              amount: transfer_params[:amount],
              destination_payment: transfer.destination_payment,
              customer_account_id: context.order.client.customer_account.id,
              managed_account_id: transfer_params[:destination],
              balance_transaction: transfer.balance_transaction
            )
          rescue => e
            context.fail!(error: e.message)
          end
        end

        private

        def transfer_params
          {
            amount: context.order.rest,
            currency: 'eur',
            destination: context.order.master.stripe_account.account_uid,
            order_id: context.order.id,
            charge_id: context.order.charges.where(capture: true).first.charge_id
          }
        end
      end
    end
  end
end
