module Api
  module V2
    module Customer
      # save Customer on locally
      class SaveCharge
        include Interactor

        def call
          charge = order.charges.new
          charge.charge_id = charge_object.id
          charge.customer_account_id = order.client.customer_account.id
          charge.try("#{context.charge_type}!")

          charge.save!
        end

        private

        def order
          context.order
        end

        def charge_object
          context.charge
        end
      end
    end
  end
end
