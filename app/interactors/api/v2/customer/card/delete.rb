module Api
  module V2
    module Customer
      module Card
        # Create stripe customer with token, became from fill Visa card
        class Delete
          include Interactor

          def call
            context.fail!(error: 'id card is not present') unless card_id.present?
            customer = Stripe::Customer.retrieve(customer_id)
            customer.sources.retrieve(card_id).delete()
          end

          private

          def current_user
            context.current_user
          end

          def card_id
            context.card_id
          end

          def customer_id
            current_user.customer_account.customer_id
          end
        end
      end
    end
  end
end
