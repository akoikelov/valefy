module Api
  module V2
    module Customer
      module Card
        # Create stripe customer with token, became from fill Visa card
        class CheckCustomerCard
          include Interactor::Organizer

          organize Check::Customer, Check::Card
        end
      end
    end
  end
end
