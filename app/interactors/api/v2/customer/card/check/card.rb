module Api
  module V2
    module Customer
      module Card
        module Check
          # check exist or card from customer
          class Card
            include Interactor

            def call
              card_list = Stripe::Customer.retrieve(customer.customer_id).sources.all(:object => "card")
              context.fail! if card_list.count <= 0
            end

            private

            def current_user
              context.current_user
            end

            def customer
              context.customer
            end
          end
        end
      end
    end
  end
end
