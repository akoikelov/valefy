module Api
  module V2
    module Customer
      module Card
        module Check
          # Check exist or not Customer
          class Customer
            include Interactor

            def call
              customer = current_user.customer_account

              if customer.present?
                # context.customer = Stripe::Customer.retrieve(customer.customer_id)
                context.customer = customer
              else
                context.fail!(error: "No such customer")
              end
            end

            private

            def current_user
              context.current_user
            end
          end
        end
      end
    end
  end
end
