module Api
  module V2
    module Customer
      module Card
        # Create stripe customer with token, became from fill Visa card
        class Default
          include Interactor

          def call
            customer = current_user.customer_account

            if customer.present?
              cards = Stripe::Customer.retrieve(customer.customer_id)
                                             .sources
                                             .all(:object => 'card')
              context.card = cards.data.first
            else
              context.fail!(error: 'No cards')
            end
          end

          private

          def current_user
            context.current_user
          end
        end
      end
    end
  end
end
