module Api
  module V2
    module Customer
      # save customer on locally
      class Update
        include Interactor

        def call
          customer = Stripe::Customer.retrieve(customer_id)
          customer.default_source = card_id
          customer.save
          context.card = customer.sources.retrieve(card_id)
        end

        private

        def current_user
          context.current_user
        end

        def card_id
          context.card_id
        end

        def customer_id
          current_user.customer_account.customer_id
        end
      end
    end
  end
end
