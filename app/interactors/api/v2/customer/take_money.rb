module Api
  module V2
    module Customer
      # pre - authorize charged from customer
      class TakeMoney
        include Interactor

        def call
          context.order = OrderItem.find(params[:order_id])
          if params[:promocode_id].present?
            promocode = PromoCode.find(params[:promocode_id])

            if promocode
              total_price = context.order.reload.total_price.to_i
              context.order.update_attributes(promo_code_id: promocode.id, total_price: total_price - promocode.discount)
              promocode.update(used: true)
            end
            context.order.reload
          end

          result = CreateCharge.call(
            params: charge_params, current_user: current_user, charge_type: CustomerCharge.payment
          )

          context.fail!(error: result.error) unless result.success?
        end

        private

        def params
          context.params
        end

        def current_user
          context.current_user
        end

        def charge_params
          {
            amount: context.order.in_cents,
            description: "Charge for order #{context.order.id}",
            order: context.order,
            currency: 'eur'
          }
        end
      end
    end
  end
end
