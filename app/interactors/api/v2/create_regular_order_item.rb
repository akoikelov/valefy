module Api
  module V2
    class CreateRegularOrderItem
      include Interactor

      def call
        subscription = current_user.subscriptions.create!(
          started_at: started_at,
          closed_at: closed_at
        )
      end

      private

      def create_list_orders(subscription, every_n_day)
        range = (started_at.to_datetime..closed_at.to_datetime).to_a

        range.each_with_index do |item, index|
          create_order(item, subscription) if index % every_n_day == 0
        end

        context.order = subscription.order_items.first
      end

      def create_order(order_time, subscription)
        params = order_params.merge(
          order_time: order_time,
          subscription_id: subscription.id
        )

        order = current_user.order_items.new(params)

        if order.valid?
          order_images.map { |item| order.images << item } if order_images.present?

          if order.save
            order.services << Service.where(id: service_ids_arr) if service_ids_arr.present?
            order.regular!
          else
            context.order  = order
            context.error = order.errors
            context.fail!
          end
        else
          context.order  = order
          context.error = order.errors
          context.fail!
        end

        order
      end

      def current_user
        context.current_user
      end

      def order_params
        context.params.except(:images, :time_zone, :service_ids, :order_time)
      end

      def closed_at
        Time.zone.parse(context.params[:order_time]) + 1.month
      end

      def started_at
        Time.zone.parse(context.params[:order_time])
      end

      def order_images
        context.params[:images]
      end

      def service_ids_arr
        context.params[:service_ids]
      end

      def subcategory
        service_id = service_ids_arr.first

        subcategory = Service.find(service_id).subcategory
      end
    end
  end
end
