module Api
  module V2
    class CreateInvoice
      include Interactor

      def call
        invoice = Invoice.new(
          order_id: order.id,
          client_id: order.client.id,
          number: "#{order.id}#{rand(1000...9000)}",
          currency: 'euro',
          type_invoice: Invoice::INVOICE_TYPE_CUSTOMER
        )

        if invoice.save
          context.invoice = invoice

          order.services.each do |service|
            # TODO вынести в отдельный класс создание LineItem
            invoice.line_items.create(service_id: service.id,price: service.price)
          end
        end
      end

      private

      def order
        context.order
      end

      def current_user
        context.current_user
      end

      def check_clients_referal_user
        if current_user.share_email.present? and curren_user.order_items.count == 1
          shared_user = User::Client.where('email like :email', email: "#{current_user.share_email}%")
          result = CreatePromoCode.call(resource: shared_user) ## Метод create_promo_code в User::CLient приватный и не может быть вызван мне класса. Не стал переделвать модификатор доступа этого метода
          if result.success?
            ReferalShare.create(client: current_user, shared_client: shared_user, promo_code: result.promo_code)
          ::Api::V1::SharedUsersFirstOrderWorker.perform_async(shared_client_id: shared_user.id, client_id: current_user.id, promo_code_id: result.promo_code.id)
          end
        end
      end
    end
  end
end
