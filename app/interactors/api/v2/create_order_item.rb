module Api
  module V2
    class CreateOrderItem
      include Interactor

      def call
        context.fail!(error: 'Phone number not confirm', code: :phone_not_confirm) unless confirm_phone?

        set_time_zone

        context.order = current_user.order_items.new(order_params)
        init_location_of_station if context.order.stationary_id.present?

        if context.order.valid?
          if context.order.save
            context.order.services << Service.where(id: service_ids_arr) if service_ids_arr.present?
            if context.order.station.present?
              update_order_if_station
            else
              calculate_and_update_total_price
            end
          else
            context.fail!(error: context.order.errors)
          end
        else
          context.fail!(error: context.order.errors)
        end
      end

      private

      def update_order_if_station
        object = Orders::Calculates.new(context.order)
        context.order.update(total_price: object.total_price_if_station)
      end

      def calculate_and_update_total_price
        _order          = context.order.reload
        client          = _order.client
        client_services = client.client_services
        price           = 0
        _amount         = 0

        
        _order.services.each do |service|
          _service = client_services.where(service_id: service.id)
          if _service.present?
            _service = _service.first

            case _service.service.subcategory.code
            when 'car'
              _amount = _order.car_count * _service.price.to_i
            when 'suv'
              _amount = _order.suv_count * _service.price.to_i
            when 'minivan'
              _amount = _order.minivan_count * _service.price.to_i
            when 'pram'
              _amount = _service.price.to_i
            end
          else
            case service.subcategory.code
            when 'car'
              _amount = _order.car_count * service.price.to_i
            when 'suv'
              _amount = _order.suv_count * service.price.to_i
            when 'minivan'
              _amount = _order.minivan_count * service.price.to_i
            when 'pram'
              _amount = service.price.to_i
            end
          end

          price    = _amount + price
          _amount  = 0
        end

        baby_price = _order.baby_seat * 5
        price = price + baby_price

        _order.update!(total_price: price)
      end

      def init_location_of_station
        cl = CompanyLocation.find(order_params[:stationary_id])

        context.order.lat     = cl.lat
        context.order.lon     = cl.lon
        context.order.address = cl.address
      end

      def order_params
        context.params.except(:images, :time_zone, :service_ids)
      end

      def service_ids_arr
        context.params[:service_ids] ||= []
      end

      def current_user
        context.current_user
      end

      def set_time_zone
        Time.zone = OrderItem::TIME_ZONES[time_zone] if time_zone.present?
      end

      def service_valid?
        Service.find(order_params[:service_id]).present?
      end

      def time_zone
        context.params[:time_zone]
      end

      def confirm_phone?
        return true if order_params[:platform].eql?('ADMIN')

        current_user.confirm_phone_number
      end
    end
  end
end
