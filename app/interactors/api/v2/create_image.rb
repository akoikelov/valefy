module Api
  module V2
    class CreateImage
      include Interactor

      def call
        context.fail!(error: "image can't be blank") unless image_present?
        context.fail!(error: 'Not correct format')   if valid?

        file = Image.new(link: image)

        if file.save
          context.link = file.link.url
        else
          context.error = file.errors
          context.fail!
        end
      end

      private

      def params
        context.params
      end

      def image
        params[:image].present? ? params[:image] : ''
      end

      def valid?
        image.class == Float || image.class == Fixnum || image.class == String
      end

      def image_present?
        image.present? || image.nil?
      end
    end
  end
end
