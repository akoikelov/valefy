module Api
  module V2
    module Order
      module Feedback
        # 3 check steps
        # --check status of order - should be completed
        # --check time - client can set feedback after hour of completed
        # --create feedback if all steps are successfully
        class CreateClientFeedback
          include Interactor::Organizer

          organize CheckStatusOrder, CreateFeedback
        end
      end
    end
  end
end
