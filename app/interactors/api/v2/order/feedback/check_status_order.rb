module Api
  module V2
    module Order
      module Feedback
        # --check status of order - should be completed
        class CheckStatusOrder
          include Interactor

          def call
            context.order = OrderItem.find(order_id)

            context.fail!(error: error_message) unless context.order.end_work?
          end

          private

          def order_id
            context.params[:order_item_id]
          end

          def error_message
            'You can left feedback when order will be completed'
          end
        end
      end
    end
  end
end
