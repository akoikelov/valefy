module Api
  module V2
    module Order
      module Feedback
        # Save Feedback from client
        # --rating (0..5)
        # --comment
        class CreateFeedback
          include Interactor

          def call
            context.feedback = current_user.feedbacks.new feedback_params
            context.feedback.create_call_me!(client_id: context.feedback.client_id, recall: true) if context.params[:recall].eql?('true')
            
            if amount > 0 && context.params[:tip] == 'true'
              result_charge = Api::V2::Customer::CreateCharge.call(params: charge_params, current_user: current_user, order: order.reload, charge_type: CustomerCharge.tip)
              if result_charge.success?
                result_transfer = Api::V2::OrderItems::Customer::CreateTransfer.call(order: order.reload)
                context.fail!(error: result_charge.error) if !result_transfer.success?
              else
                context.fail!(error: result_charge.error)
              end
            end
            
            context.fail!(error: 'Unable params') unless context.feedback.save
          end

          private

          def charge_params
            {
              amount: amount,
              description: "tip for order - #{order.id}",
              currency: 'eur',
              transfer_group: "tip - order - #{order.id}"
            }
          end

          def master
            context.order.master
          end

          def order
            context.order
          end

          def current_user
            context.current_user
          end

          def amount
            if context.params[:amount].present?
              (context.params[:amount].to_r * 100).to_i
            else
              0
            end
          end

          def params
            context.params.except(:recall, :tip, :tip_amount)
          end

          def feedback_params
            params.merge(
              {
                serviceman_id: master.id,
                stationary_id: order.stationary_id,
                tip: amount
              }
            )
          end
        end
      end
    end
  end
end
