module Api
  module V2
    class OrdersList
      include Interactor

      def call
        context.fail!(error: 'Invalid param - status') unless valid_status?

        if status.present?
          if status == '0'
            list = current_user.order_items.where(status: [0,1,2,3,4,5,6,7,8,10,13]).order(id: :desc)
          elsif status == '1'
            list = current_user.order_items.where(status: [9,11]).order(id: :desc)
          end
        else
          list = current_user.order_items.order(id: :desc)
        end

        context.list = list
      end

      private

      def params
        context.params
      end

      def current_user
        context.current_user
      end

      def valid_status?
        return true unless status

        OrderItem::statuses.value?(status.to_i) if correct_status?
      end

      def status
        params[:status]
      end

      def correct_status?
        /\d/.match(status).to_s.present?
      end
    end
  end
end
