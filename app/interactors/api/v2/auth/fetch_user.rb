module Api
  module V2
    module Auth
      # Fetch data from Facebook ( name, email )
      class FetchUser
        include Interactor

        def call
          fb_object = Koala::Facebook::API.new(access_token, app_secret)

          context.profile = profile_fb_user(fb_object)
        end

        private

        def profile_fb_user fb_object
          fb_object.get_object('me', fields: %w(name email))
        end

        def access_token
          context.params[:access_token]
        end

        def app_secret
          ENV['APP_SECRET']
        end
      end
    end
  end
end
