module Api
  module V2
    module Auth
      # search or create a new user, and log in him
      class SignInGoogleUser
        include Interactor

        def call
          context.user = find_or_create
          set_data_user
        end

        private

        def find_or_create
          profile['email'].present? ? search_by_email : search_by_google_id
        end

        def set_data_user
          if context.user.new_record?
            set_user_type
            set_image
            set_user_password
            set_phone_number
            set_name_user
            set_last_name
          end
          set_image if !context.user.image.present?
          set_google_id
          set_email
          set_token_for_user
        end

        def set_user_password
          password = SecureRandom.urlsafe_base64(nil, false)
          context.user.password              = password
          context.user.password_confirmation = password
        end

        def search_by_google_id
          object.where(google_id: profile['id']).first_or_initialize
        end

        def search_by_email
          object.where(email: profile['email']).first_or_initialize
        end

        def set_phone_number
          context.user.phone_number = object::PHONE_NOT_EXIST
        end

        def set_email
          context.user.email = profile['email'].present? ? profile['email'] : fake_email
        end

        def set_image
          context.user.image = profile['picture'].present? ? profile['picture'] : ''
        end

        def set_last_name
          if profile['family_name'].present?
            context.user.last_name = profile['family_name']
          else
            context.user.last_name = object::LAST_NAME_NOT_EXIST
          end
        end

        def fake_email
          "#{profile['id']}@google.com"
        end

        def set_google_id
          context.user.google_id = profile['id']
        end

        def set_user_type
          context.user.type = 'User::Client'
        end

        def set_token_for_user
          create_token_info
          context.user.tokens[context.client_id] = {
            token: BCrypt::Password.create(context.token),
            expiry: context.expiry
          }
        end

        def create_token_info
          context.client_id = SecureRandom.urlsafe_base64(nil, false)
          context.token     = SecureRandom.urlsafe_base64(nil, false)
          context.expiry    = (Time.now + DeviseTokenAuth.token_lifespan).to_i
        end

        def set_name_user
          context.user.name = name
        end

        def profile
          context.profile
        end

        def name
          profile['given_name']
        end

        def params
          context.params
        end

        def object
          context.object
        end
      end
    end
  end
end
