module Api
  module V2
    module Auth
      class SendSms
        include Interactor

        def call
          twilio   = TwilioClient.new(phone)
          response = twilio.send_sms

          if response[:success]
            client.sms_histories.create! phone_number: phone, code: response[:code]
          else
            context.fail!(error: response[:error])
          end
        end

        private

        def phone
          context.params[:phone_number]
        end

        def client
          context.client
        end
      end
    end
  end
end
