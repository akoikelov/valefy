module Api
  module V2
    module Auth
      class AuthorizeUser
        include Interactor::Organizer

        organize FetchUser, SignInUser
      end
    end
  end
end
