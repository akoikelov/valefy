module Api
  module V2
    module Auth
      class AuthorizeGoogleUser
        include Interactor::Organizer

        organize FetchGoogleUser, SignInGoogleUser
      end
    end
  end
end
