module Api
  module V2
    module GiftCards
      class Create
        include Interactor

        def call
          to_client = ::User::Client.find_by_email(params[:client_email])
          
          if to_client
            context.gift         = from_client.from_me_gift_cards.create!(gift_params_client_exist(to_client))
            context.to_client    = to_client
            context.client_exist = true
          else
            context.gift         = from_client.from_me_gift_cards.create!(gift_params_client_not_exist)
            context.client_exist = false
          end
        end

        private

        def params
          context.params
        end

        def from_client
          context.current_user
        end

        def gift_params_client_exist(to_client)
          {
            to_client_id: to_client.id,
            to_email: params[:client_email],
            amount: service.price,
            message: params[:message],
            exp_date: DateTime.now + 2.days,
            service_id: service.id
          }
        end

        def gift_params_client_not_exist
          {
            to_email:  params[:client_email],
            amount:    service.price,
            message:   params[:message],
            exp_date:   DateTime.now + 2.days,
            service_id: service.id
          }
        end

        def service
          Service.find(params[:service_id])
        end
      end
    end
  end
end
