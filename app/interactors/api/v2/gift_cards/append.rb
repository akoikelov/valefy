module Api
  module V2
    module GiftCards
      class Append
        include Interactor

        def call
          result = GiftCard.available.where(to_email: context.resource.email)

          if result.present?
            result.each do |gift|
              gift.update(to_client_id: context.resource.id)
            end
          end
        end
      end
    end
  end
end
