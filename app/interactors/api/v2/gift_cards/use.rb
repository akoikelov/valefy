module Api
  module V2
    module GiftCards
      class Use
        include Interactor

        def call
          result = CreateOrderItem.call(params: order_params, current_user: current_user)

          if result.success?
            context.order = result.order

            charge = CustomerCharge.find(gift_card.customer_charge_id)
            charge.update(order_item_id: context.order.id)
            context.order.pre_authorize_charged!
            context.order.searching!
            gift_card.update(used: true)
          else
            context.fail!(error: result.error)
          end
        end

        private

        def current_user
          context.current_user
        end

        def order_params
          {
            address: params[:address],
            description: params[:description],
            order_time: params[:order_time],
            lon: params[:lon],
            lat: params[:lat],
            time_zone: params[:time_zone],
            pets: params[:pets],
            platform: params[:platform],
            count_vehicles: params[:count_vehicles],
            parking: params[:parking],
            service_ids: [gift_card.service_id],
            images: params[:images],
            gift_card_id: gift_card.id
          }
        end

        def gift_card
          GiftCard.find(params[:gift_card_id])
        end

        def params
          context.params
        end
      end
    end
  end
end