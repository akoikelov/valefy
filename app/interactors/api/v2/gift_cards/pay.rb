module Api
  module V2
    module GiftCards
      class Pay
        include Interactor

        def call
          if Rails.env.qa? || Rails.env.development? || Rails.env.production?
            begin
              charge = Stripe::Charge.create({
                :amount => context.gift.in_cents,
                :currency => 'eur',
                :customer => from_client.customer_account.customer_id,
                :description => params[:message],
                :transfer_group => "gift-card-#{context.gift.id}"
              })
            rescue => e
              context.fail!(error: e.message)
            end

            if charge.status.eql?('succeeded')
              _charge = CustomerCharge.new
              _charge.charge_id = charge.id
              _charge.customer_account_id = from_client.customer_account.id

              if _charge.valid?
                _charge.save!
                context.gift.update(customer_charge_id: _charge.id)
              else
                context.fail!(error: 'Can not save the charge locally')
              end
            else
              context.fail!(error: 'Charge not succeeded')
            end
          end
        end

        private

        def params
          context.params
        end

        def from_client
          context.current_user
        end
      end
    end
  end
end