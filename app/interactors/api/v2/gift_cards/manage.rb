module Api
  module V2
    module GiftCards
      class Manage
        include Interactor::Organizer

        organize Create, Pay
      end
    end
  end
end