module Charts
  class ClientsByGender
    include Interactor
    include InteractorMetrics

    def call
      male, female = if params[:range].present?
        get_clients_count
      else
        get_clients_by_gender(7)
      end
      context.data = [['Male', male], ['Female', female]]
    end

    def get_clients_count
      case range
        when 'last_7_days'
          get_clients_by_gender(7)
        when 'last_30_days'
          get_clients_by_gender(30)
        when 'yesterday'
          get_clients_by_gender(2)
        when 'today'
          get_clients_by_gender(1, true)
        when 'last_3_months'
          get_clients_by_gender(last_quarter_days_count)
        when 'last_year'
          get_clients_by_gender(current_year_days_count)
        when 'all_time'
          [
            User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}, users: { corp: is_corp }).uniq.count,
            User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}, users: { corp: is_corp }).uniq.count
          ]
      end
    end

    def get_clients_by_gender(period, today = nil)
      unless today
        [
          User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}, users: { corp: is_corp }, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).uniq.count,
          User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}, users: { corp: is_corp }, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).uniq.count
        ]
      else
        [
          User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}, users: { corp: is_corp }, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count,
          User::Client.joins(:client_extra_info, :order_items).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}, users: { corp: is_corp }, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count
        ]
      end
    end
  end
end