module Charts
  class OrdersCountByClients
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        corp_orders, individual_orders = get_orders
      else
        corp_orders, individual_orders = get_orders_by_period(7)
      end
      context.data = [['individual orders', individual_orders], ['corporative orders', corp_orders]]
    end

    def get_orders
      case range
        when 'last_7_days'
          get_orders_by_period(7)
        when 'last_30_days'
          get_orders_by_period(30)
        when 'yesterday'
          get_orders_by_period(2)
        when 'today'
          get_orders_by_period(1, true)
        when 'last_3_months'
          get_orders_by_period(last_quarter_days_count)
        when 'last_year'
          get_orders_by_period(current_year_days_count)
        when 'all_time'
          [OrderItem.joins(:client).where(users: { corp: true }).count, OrderItem.joins(:client).where(users: { corp: false }).count]
      end
    end

    def get_orders_by_period(period, today = nil)
      unless today
        [
          OrderItem.joins(:client).where(users: { corp: true }, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).count,
          OrderItem.joins(:client).where(users: { corp: false }, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).count
        ]
      else
        [
          OrderItem.joins(:client).where(users: { corp: true }, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count,
          OrderItem.joins(:client).where(users: { corp: false }, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
        ]
      end
    end
  end
end