module Charts
  class SmallLargeCars
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        context.data = get_small_large_cars_percent
      else
        context.data = get_orders_percentage_value_by_period(7)
      end
    end

    def get_small_large_cars_percent
      case range
        when 'last_7_days'
          get_orders_percentage_value_by_period(7)
        when 'last_30_days'
          get_orders_percentage_value_by_period(30)
        when 'yesterday'
          get_orders_percentage_value_by_period(2)
        when 'today'
          get_orders_percentage_value_by_period(1, true)
        when 'last_3_months'
          get_orders_percentage_value_by_period(last_quarter_days_count)
        when 'last_year'
          get_orders_percentage_value_by_period(current_year_days_count)
        when 'all_time'
          get_orders_percentage_value_by_period(1, false, true)
      end
    end

    def get_orders_percentage_value_by_period(period, today = nil, all_time = nil)
      large_cars = large_car_orders_count(period, today, all_time)
      small_cars = small_car_orders_count(period, today, all_time)
      multiple_cars = multiple_orders_count(period, today, all_time)
      prams = pram_orders_count(period, today, all_time)
      total = get_total_orders_count(period, today, all_time)
      small_cars_percent = get_orders_percentage_value(small_cars, total)
      large_cars_percent = get_orders_percentage_value(large_cars, total)
      prams_percent = get_orders_percentage_value(prams, total)
      multiple_cars_percent = get_orders_percentage_value(multiple_cars, total)
      is_corp == 'true' ? [['Small cars', small_cars_percent], ['Large cars', large_cars_percent], ['Multiple car orders', multiple_cars_percent]] : [['Small cars', small_cars_percent], ['Large cars', large_cars_percent], ['Multiple car orders', multiple_cars_percent], ['PRAM', prams_percent]]
    end

    def large_car_orders_count(period, today = nil, all_time = nil)
      if today
        OrderItem.joins({services: :subcategory}, :client).where('subcategories.title like (?) and order_items.created_at between ? and ? and users.corp = ?', '%SUV%', (DateTime.now.beginning_of_day), DateTime.now.end_of_day, is_corp).count
      elsif all_time
        OrderItem.joins({services: :subcategory}, :client).where('subcategories.title like (?) and users.corp = ?', '%SUV%', is_corp).count
      else
        OrderItem.joins({services: :subcategory}, :client).where('subcategories.title like (?) and order_items.created_at between ? and ? and users.corp = ?', '%SUV%', (DateTime.now - period.days), DateTime.now, is_corp).count
      end
    end

    def small_car_orders_count(period, today = nil, all_time = nil)
      if today
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'Car (small/saloon)' }, order_items: { created_at:  (DateTime.now.beginning_of_day)..DateTime.now.end_of_day }).uniq.count
      elsif all_time
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'Car (small/saloon)' }).uniq.count
      else
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'Car (small/saloon)' }, order_items: { created_at:  (DateTime.now - period.days)..DateTime.now }).uniq.count
      end
    end

    def pram_orders_count(period, today = nil, all_time = nil)
      if today
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: [ 'Car (small/saloon)', '%SUV%'] }, order_items: { created_at:  (DateTime.now.beginning_of_day)..DateTime.now.end_of_day }).uniq.count
      elsif all_time
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: [ 'Car (small/saloon)', '%SUV%'] }).uniq.count
      else
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: [ 'Car (small/saloon)', '%SUV%'] }, order_items: { created_at:  (DateTime.now - period.days)..DateTime.now }).uniq.count
      end
    end

    def multiple_orders_count(period, today = nil, all_time = nil)
      if today
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'PRAM' }, order_items: { created_at:  (DateTime.now.beginning_of_day)..DateTime.now.end_of_day, count_vehicles: 2..Float::INFINITY }).uniq.count
      elsif all_time
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'PRAM' }, order_items: { count_vehicles: 2..Float::INFINITY }).uniq.count
      else
        OrderItem.joins({services: :subcategory}, :client).where(users: { corp: is_corp }, subcategories: { title: 'PRAM' }, order_items: { created_at:  (DateTime.now - period.days)..DateTime.now, count_vehicles: 2..Float::INFINITY }).uniq.count
      end
    end

    def get_total_orders_count(period, today = nil, all_time = nil)
      if today
        OrderItem.joins(:client).where(users: { corp: is_corp }, order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
      elsif all_time
        OrderItem.joins(:client).where(users: { corp: is_corp }).count
      else
        OrderItem.joins(:client).where(users: { corp: is_corp }, order_items: { created_at: DateTime.now - period.days..DateTime.now }).count
      end
    end

    def get_orders_percentage_value(value, total)
      total == 0 ? 0 : ((value * 100.to_f)/total).round(2)
    end
  end
end