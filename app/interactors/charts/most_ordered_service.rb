module Charts
  class MostOrderedService
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_orders_count_each_category
      sort_data_by_asc
    end

    def get_orders_count_each_category
      Category.visible.map do |category|
        if params[:range].present?
          orders_count = get_orders_count_by_category(category)
        else
          orders_count = get_orders_count_by_period(category, 7)
        end
        orders_count.present? ? [category.title, orders_count] : [category.title, 0]
      end
    end

    def get_orders_count_by_category(category)
      case range
        when 'last_7_days'
          get_orders_count_by_period(category, 7)
        when 'last_30_days'
          get_orders_count_by_period(category, 30)
        when 'yesterday'
          get_orders_count_by_period(category, 2)
        when 'today'
          get_orders_count_by_period(category, 1, true)
        when 'last_3_months'
          get_orders_count_by_period(category, last_quarter_days_count)
        when 'last_year'
          get_orders_count_by_period(category, current_year_days_count)
        when 'all_time'
          get_orders_count_by_period(category, 1, false, true)
      end
    end

    def get_orders_count_by_period(category, period, today = nil, all_time = nil)
      if today
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count
      elsif all_time
        category_orders_by_client_type(category).uniq.count
      else
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }).uniq.count
      end
    end

    def sort_data_by_asc
      context.data = context.data.sort{ |a, b| b[1] <=> a[1]}
    end

    def category_orders_by_client_type(category)
      category.order_items.joins(:client).where(users: { corp: is_corp })
    end
  end
end
