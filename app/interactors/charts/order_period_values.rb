module Charts
  class OrderPeriodValues
    include Interactor
    include InteractorMetrics

    def call
      get_by_period_type
    end

    def get_by_period_type
      case period_type
        when 'hours'
          get_orders_count_by_hours
        when 'weekdays'
          get_orders_count_by_weekdays
        when 'months'
          get_orders_count_by_months
      end
    end

    def get_orders_count_by_hours
      all, morning_orders, dinner_orders, evening_orders, night_orders = if params[:range].present?
        get_orders
      else
        get_orders_by_periods(7)
      end
      context.data = [
        ['Morning orders', (morning_orders * all).round(2) ],
        ['Dinner orders', (dinner_orders * all).round(2) ],
        ['Evening orders', (evening_orders * all).round(2)],
        ['Night orders', (night_orders * all).round(2)]
      ]
    end

    def get_orders_count_by_weekdays
      all, monday_orders, tuesday_orders, wendesday_orders, thursday_orders, friday_orders, saturday_orders, sunday_orders = if params[:range].present?
        get_orders
      else
        get_orders_by_periods(7)
      end
      context.data = [
        ['Monday', (monday_orders * all).round(2) ],
        ['Tuesday', (tuesday_orders * all).round(2) ],
        ['Wednesday', (wendesday_orders * all).round(2)],
        ['Thursday', (thursday_orders * all).round(2)],
        ['Friday', (friday_orders * all).round(2)],
        ['Saturday', (saturday_orders * all).round(2)],
        ['Sunday', (sunday_orders * all).round(2)]
      ]
    end

    def get_orders_count_by_months
      all, january, february, march , april, may, june, july, august, september, october, november, december = if params[:range].present?
        get_orders
      else
        get_orders_by_periods(7)
      end
      context.data = [
        ['january', (january * all).round(2) ],
        ['february', (february * all).round(2) ],
        ['march', (march * all).round(2)],
        ['april', (april * all).round(2)],
        ['may', (may * all).round(2)],
        ['june', (june * all).round(2)],
        ['july', (july * all).round(2)],
        ['august', (august * all).round(2)],
        ['september', (september * all).round(2)],
        ['october', (october * all).round(2)],
        ['november', (november * all).round(2)],
        ['december', (december * all).round(2)]
      ]
    end

    def get_orders
      case range
        when 'last_7_days'
          get_orders_by_periods(7)
        when 'last_30_days'
          get_orders_by_periods(30)
        when 'yesterday'
          get_orders_by_periods(2)
        when 'today'
          get_orders_by_periods(1, true)
        when 'last_3_months'
          get_orders_by_periods(last_quarter_days_count)
        when 'last_year'
          get_orders_by_periods(current_year_days_count)
        when 'all_time'
          get_orders_by_periods(1, false, true )
      end
    end

    def get_orders_by_periods(period, today = nil, all_time = nil)
      case period_type
        when 'hours'
          get_orders_by_hours(period, today, all_time)
        when 'weekdays'
          get_orders_by_week_days(period, today, all_time)
        when 'months'
          get_orders_by_months(period, today, all_time)
      end
    end

    def get_orders_by_hours(period, today = nil, all_time = nil)
      if today
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count).round(2),
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '06', end_time: '11', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '11', end_time: '17', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '17', end_time: '23', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '23', end_time: '05', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count
        ]
      elsif all_time
        [
          get_orders_by_client_type.count == 0 ? 0 : (100.to_f / get_orders_by_client_type.count).round(2),
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time', start_time: '06', end_time: '11').count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time', start_time: '11', end_time: '17').count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time', start_time: '17', end_time: '23').count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time', start_time: '23', end_time: '05').count
        ]
      else
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count).round(2),
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '06', end_time: '11', start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '11', end_time: '17', start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '17', end_time: '23', start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(hour from order_items.created_at) >= :start_time and extract(hour from order_items.created_at) <= :end_time and order_items.created_at between :start_date and :end_date', start_time: '23', end_time: '05', start_date: DateTime.now - period.days, end_date: DateTime.now).count
        ]
      end
    end

    def get_orders_by_week_days(period, today = nil, all_time = nil)
      if today
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count).round(2),
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 1, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 2, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 3, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 4, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 5, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 6, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 7, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count
        ]
      elsif all_time
        [
          get_orders_by_client_type.count == 0 ? 0 : (100.to_f / get_orders_by_client_type.count).round(2),
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 1).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 2).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 3).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 4).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 5).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 6).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week', day_week: 7).count
        ]
      else
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count).round(2),
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 1, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 2, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 3, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 4, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 5, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 6, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(dow from order_items.created_at::timestamp) = :day_week and order_items.created_at between :start_date and :end_date', day_week: 7, start_date: DateTime.now - period.days, end_date: DateTime.now).count
        ]
      end
    end

    def get_orders_by_months(period, today = nil, all_time = nil)
      if today
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count).round(2),
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 1, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 2, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 3, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 4, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 5, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 6, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 7, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 5, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 6, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 7, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 8, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 9, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 10, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 11, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 12, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).count
        ]
      elsif all_time
        [
          get_orders_by_client_type.count == 0 ? 0 : (100.to_f / get_orders_by_client_type.count).round(2),
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 1).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 2).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 3).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 4).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 5).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 6).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 7).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 8).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 9).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 10).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 11).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name', month_name: 12).count
        ]
      else
        [
          get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count == 0 ? 0 : (100.to_f / get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count).round(2),
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 1, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 2, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 3, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 4, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 5, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 6, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 7, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 8, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 9, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 10, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 11, start_date: DateTime.now - period.days, end_date: DateTime.now).count,
          get_orders_by_client_type.where('extract(month from order_items.created_at::timestamp) = :month_name and order_items.created_at between :start_date and :end_date', month_name: 12, start_date: DateTime.now - period.days, end_date: DateTime.now).count
        ]
      end
    end

    def period_type
      params[:period_type]
    end

    def get_orders_by_client_type
      OrderItem.joins(:client).where(users: { corp: is_corp })
    end
  end
end
