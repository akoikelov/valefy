module Charts
  class CarsCountByType
    include Interactor
    include InteractorMetrics

    def call
      car, suv, minivan = if params[:range].present?
       get_cars
      else
        get_cars_by_type(7)
      end
      context.data = [['Car', car], ['SUV', suv], ['Minivan', minivan]]
    end

    def get_cars
      case range
        when 'last_7_days'
          get_cars_by_type(7)
        when 'last_30_days'
          get_cars_by_type(30)
        when 'yesterday'
          get_cars_by_type(2)
        when 'today'
          get_cars_by_type(1, true)
        when 'last_3_months'
          get_cars_by_type(last_quarter_days_count)
        when 'last_year'
          get_cars_by_type(current_year_days_count)
        when 'all_time'
          [
            OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title', corp: is_corp,  title: 'Car%').pluck(:count_vehicles).compact.sum,
            OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title', corp: is_corp,  title: '%SUV%').pluck(:count_vehicles).compact.sum,
            OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title', corp: is_corp,  title: '%MINIVAN%').pluck(:count_vehicles).compact.sum
          ]
      end
    end

    def get_cars_by_type(period, today = nil)
      unless today
        [
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: 'Car%', start_date: (DateTime.now - period.days), end_date: DateTime.now).pluck(:count_vehicles).compact.sum,
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: '%SUV%', start_date: (DateTime.now - period.days), end_date: DateTime.now).pluck(:count_vehicles).compact.sum,
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: '%MINIVAN%', start_date: (DateTime.now - period.days), end_date: DateTime.now).pluck(:count_vehicles).compact.sum
        ]
      else
        [
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: 'Car%', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).pluck(:count_vehicles).compact.sum,
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: '%SUV%', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).pluck(:count_vehicles).compact.sum,
          OrderItem.joins(:client, services: :subcategory).where('users.corp = :corp and subcategories.title like :title and order_items.created_at between :start_date and :end_date', corp: is_corp,  title: '%MINIVAN%', start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).pluck(:count_vehicles).compact.sum
        ]
      end
    end
  end
end