module Charts
  module Districts
    class GetOrdersCount
      include Interactor
      include InteractorMetrics

      def call
        context.data = get_orders_count_by_districts
        context.districts = city.districts
      end

      def get_orders_count_by_districts
        city.districts.present? ? city.districts.map { |district| [ district.name,  count_of_orders_by_period(district)] } : context.fail!(notice: "City #{city} has no districts!")
      end

      def count_of_orders_by_period(district)
        if master_id.present?
          case range
            when 'last_7_days'
              count_of_orders_for_master(district, 7)
            when 'last_30_days'
              count_of_orders_for_master(district, 30)
            when 'yesterday'
              count_of_orders_for_master(district, 2)
            when 'today'
              count_of_orders_for_master(district, 1, true)
            when 'last_3_months'
              count_of_orders_for_master(district, last_quarter_days_count)
            when 'last_year'
              count_of_orders_for_master(district, current_year_days_count)
            when 'all_time'
              count_of_orders_for_master(district, 1, false, true)
          end
        else
          case range
            when 'last_7_days'
              count_of_orders_in_district(district, 7)
            when 'last_30_days'
              count_of_orders_in_district(district, 30)
            when 'yesterday'
              count_of_orders_in_district(district, 2)
            when 'today'
              count_of_orders_in_district(district, 1, true)
            when 'last_3_months'
              count_of_orders_in_district(district, last_quarter_days_count)
            when 'last_year'
              count_of_orders_in_district(district, current_year_days_count)
            when 'all_time'
              count_of_orders_in_district(district, 1, false, true)
          end
        end
      end

      def count_of_orders_in_district(district, period, today = nil, all_time = nil)
        if today
          OrderItem.where('ST_Intersects(:district_coords, coordinates) and order_items.created_at between :start_date and :end_date', district_coords: district.coordinates, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day).uniq.count
        elsif all_time
          OrderItem.where('ST_Intersects(:district_coords, coordinates)', district_coords: district.coordinates).uniq.count
        else
          OrderItem.where('ST_Intersects(:district_coords, coordinates) and order_items.created_at between :start_date and :end_date', district_coords: district.coordinates, start_date: DateTime.now - period.days, end_date: DateTime.now).uniq.count
        end
      end

      def count_of_orders_for_master(district, period, today = nil, all_time = nil)
        if today
          OrderItem.joins(:master).where('ST_Intersects(:district_coords, order_items.coordinates) and order_items.created_at between :start_date and :end_date and users.id = :master_id', district_coords: district.coordinates, start_date: DateTime.now.beginning_of_day, end_date: DateTime.now.end_of_day, master_id: master_id).uniq.count
        elsif all_time
          OrderItem.joins(:master).where('ST_Intersects(:district_coords, order_items.coordinates) and users.id = :master_id', district_coords: district.coordinates, master_id: master_id).uniq.count
        else
          OrderItem.joins(:master).where('ST_Intersects(:district_coords, order_items.coordinates) and order_items.created_at between :start_date and :end_date and users.id = :master_id', district_coords: district.coordinates, start_date: DateTime.now - period.days, end_date: DateTime.now, master_id: master_id).uniq.count
        end
      end
    end
  end
end