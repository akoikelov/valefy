module Charts
  module Districts
    class ClientsByGender
      include Interactor
      include InteractorMetrics

      def call
        context.data = get_clients_by_gender
      end

      def get_clients_by_gender
        clients_count = count_of_clients_by_gender
        [['Male', clients_count[0]], ['Female', clients_count[1]]]
      end

      def count_of_clients_by_gender
        case range
          when 'last_7_days'
            clients_by_gender_in_period(district, 7)
          when 'last_30_days'
            clients_by_gender_in_period(district, 30)
          when 'yesterday'
            clients_by_gender_in_period(district, 2)
          when 'today'
            clients_by_gender_in_period(district, 1, true)
          when 'last_3_months'
            clients_by_gender_in_period(district, last_quarter_days_count)
          when 'last_year'
            clients_by_gender_in_period(district, current_year_days_count)
          when 'all_time'
            clients_by_gender_in_period(district, 1, false, true)
        end
      end

      def clients_by_gender_in_period(district, period, today = nil, all_time = nil)
        if today
          [
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count,
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count
          ]
        elsif all_time
          [
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}).uniq.count,
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}).uniq.count
          ]
        else
          [
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:male]}, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).uniq.count,
            User::Client.joins(:client_extra_info, :order_items).where('ST_Intersects(:district_coords, order_items.coordinates)', district_coords: district.coordinates).where(client_extra_infos: { gender:  ClientExtraInfo.genders[:female]}, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).uniq.count
          ]
        end
      end
    end
  end
end