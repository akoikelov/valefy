module Charts
  module Districts
    class OrdersCountEachCategory
      include Interactor
      include InteractorMetrics

      def call
        context.data = get_average_on_categories
      end

      def get_average_on_categories
        Category.visible.map do |cat|
          orders_count = if params[:range].present?
            get_orders_count(cat)
          else
            get_orders_count_by_period(cat, 7)
          end
          [ cat.title, orders_count ]
        end
      end

      def get_orders_count(category)
        case range
          when 'last_7_days'
            get_orders_count_by_period(category, 7)
          when 'last_30_days'
            get_orders_count_by_period(category, 30)
          when 'yesterday'
            get_orders_count_by_period(category, 2)
          when 'today'
            get_orders_count_by_period(category, 1, true)
          when 'last_3_months'
            get_orders_count_by_period(category, last_quarter_days_count)
          when 'last_year'
            get_orders_count_by_period(category, current_year_days_count)
          when 'all_time'
            get_orders_count_by_period(category, 1, false, true)
        end
      end

      def get_orders_count_by_period(category, period, today = nil, all_time = nil)
        if today
          category_orders(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
        elsif all_time
          category_orders(category).count
        else
          category_orders(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }).count
        end
      end

      def category_orders(category)
        category.order_items.joins(:client).where('ST_Intersects(:district_coords, coordinates)', district_coords: district.coordinates)
      end
    end
  end
end