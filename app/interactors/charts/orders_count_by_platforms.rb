module Charts
  class OrdersCountByPlatforms
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_orders_count
    end

    def get_orders_count
      case range
        when 'last_7_days'
          get_orders_count_by_period(7)
        when 'last_30_days'
          get_orders_count_by_period(30)
        when 'yesterday'
          get_orders_count_by_period(2)
        when 'today'
          get_orders_count_by_period(1, true)
        when 'last_3_months'
          get_orders_count_by_period(last_quarter_days_count)
        when 'last_year'
          get_orders_count_by_period(current_year_days_count)
        when 'all_time'
          get_orders_count_by_period(1, false, true)
      end
    end

    def get_orders_count_by_period(period, today = nil, all_time = nil)
      if today
        [
          [
            :valefy, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, companies: { platform: Company.platforms[:valefy] }, users: { corp: is_corp }).uniq.count,
          ],
          [
            :freelancing, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, companies: { platform: Company.platforms[:freelancing] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :aggregator, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, companies: { platform: Company.platforms[:aggregator] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :park, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, companies: { platform: Company.platforms[:park] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :franchise, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, companies: { platform: Company.platforms[:franchise] }, users: { corp: is_corp }).uniq.count
          ]
        ]
      elsif all_time
        [
          [
            :valefy, OrderItem.joins(:client, master: :company).where(companies: { platform: Company.platforms[:valefy] }, users: { corp: is_corp }).uniq.count,
          ],
          [
            :freelancing, OrderItem.joins(:client, master: :company).where(companies: { platform: Company.platforms[:freelancing] }, users: { corp: is_corp }).uniq.count,
          ],
          [
            :aggregator, OrderItem.joins(:client, master: :company).where(companies: { platform: Company.platforms[:aggregator] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :park, OrderItem.joins(:client, master: :company).where(companies: { platform: Company.platforms[:park] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :franchise, OrderItem.joins(:client, master: :company).where(companies: { platform: Company.platforms[:franchise] }, users: { corp: is_corp }).uniq.count
          ]
        ]
      else
        [
          [
            :valefy, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, companies: { platform: Company.platforms[:valefy] }, users: { corp: is_corp }).uniq.count,
          ],
          [
            :freelancing, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, companies: { platform: Company.platforms[:freelancing] }, users: { corp: is_corp }).uniq.count,
          ],
          [
            :aggregator, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, companies: { platform: Company.platforms[:aggregator] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :park, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, companies: { platform: Company.platforms[:park] }, users: { corp: is_corp }).uniq.count
          ],
          [
            :franchise, OrderItem.joins(:client, master: :company).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, companies: { platform: Company.platforms[:franchise] }, users: { corp: is_corp }).uniq.count
          ]
        ]
      end
    end
  end
end