module Charts
  module Pros
    class ProsByType
      include Interactor
      include InteractorMetrics

      def call
        pros = get_pros_count_by_type
        context.data = [[ 'Mobile',  pros[0]], ['Stationary', pros[1]]]
      end

      def get_pros_count_by_type
        case range
          when 'yesterday'
            get_pros_count_by_period(2)
          when 'today'
            get_pros_count_by_period(1, true)
          when 'last_7_days'
            get_pros_count_by_period(7)
          when 'last_30_days'
            get_pros_count_by_period(30)
          when 'last_3_months'
            get_pros_count_by_period(last_quarter_days_count)
          when 'last_year'
            get_pros_count_by_period(current_year_days_count)
          when 'all_time'
            get_pros_count_by_period(1, false, true)
        end
      end

      def get_pros_count_by_period(period, today = nil, all_time = nil)
        if today
          [
            User::ServiceMan.is_mobile(true).where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count,
            User::ServiceMan.is_mobile(false).where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count
          ]
        elsif all_time
          [
            User::ServiceMan.is_mobile(true).count,
            User::ServiceMan.is_mobile(false).count
          ]
        else
          [
            User::ServiceMan.is_mobile(true).where(created_at: DateTime.now - period.days..DateTime.now).count,
            User::ServiceMan.is_mobile(false).where(created_at: DateTime.now - period.days..DateTime.now).count
          ]
        end
      end
    end
  end
end