module Charts
  module Pros
    class OrdersCountByClientType
      include Interactor
      include InteractorMetrics

      def call
        context.data = [
          [ "Orders count for corporative clients", get_corp_client_orders_count ],
          [ "Orders count for individual clients", get_individual_client_orders_count ]
        ]
      end

      def get_corp_client_orders_count
        case range
          when 'yesterday'
            get_pros_orders_count_by_period(2, true)
          when 'today'
            get_pros_orders_count_by_period(1, true, true)
          when 'last_7_days'
            get_pros_orders_count_by_period(7, true)
          when 'last_30_days'
            get_pros_orders_count_by_period(30, true)
          when 'last_3_months'
            get_pros_orders_count_by_period(last_quarter_days_count, true)
          when 'last_year'
            get_pros_orders_count_by_period(current_year_days_count, true)
          when 'all_time'
            get_pros_orders_count_by_period(1, true, false, true)
        end
      end

      def get_individual_client_orders_count
        case range
          when 'yesterday'
            get_pros_orders_count_by_period(2, false)
          when 'today'
            get_pros_orders_count_by_period(1, false, true)
          when 'last_7_days'
            get_pros_orders_count_by_period(7, false)
          when 'last_30_days'
            get_pros_orders_count_by_period(30, false)
          when 'last_3_months'
            get_pros_orders_count_by_period(last_quarter_days_count, false)
          when 'last_year'
            get_pros_orders_count_by_period(current_year_days_count, false)
          when 'all_time'
            get_pros_orders_count_by_period(1, false, false, true)
        end
      end

      def get_pros_orders_count_by_period(period, is_corp, today = nil, all_time = nil)
        if today
          master.order_items.joins(:client).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }, users: { corp: is_corp }).count
        elsif all_time
          master.order_items.joins(:client).where(users: { corp: is_corp }).count
        else
          master.order_items.joins(:client).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }, users: { corp: is_corp }).count
        end
      end
    end
  end
end