module Charts
  module Pros
    class ProsCount
      include Interactor
      include InteractorMetrics

      def call
        context.data = [[ 'Professionals', get_pros_count ]]
      end

      def get_pros_count
        case range
          when 'yesterday'
            get_pros_count_by_period(2)
          when 'today'
            get_pros_count_by_period(1, true)
          when 'last_7_days'
            get_pros_count_by_period(7)
          when 'last_30_days'
            get_pros_count_by_period(30)
          when 'last_3_months'
            get_pros_count_by_period(last_quarter_days_count)
          when 'last_year'
            get_pros_count_by_period(current_year_days_count)
          when 'all_time'
            get_pros_count_by_period(1, false, true)
        end
      end

      def get_pros_count_by_period(period, today = nil, all_time = nil)
        if today
          User::ServiceMan.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count
        elsif all_time
          User::ServiceMan.count
        else
          User::ServiceMan.where(created_at: DateTime.now - period.days..DateTime.now).count
        end
      end
    end
  end
end