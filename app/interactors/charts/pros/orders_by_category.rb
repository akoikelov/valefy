module Charts
  module Pros
    class OrdersByCategory
      include Interactor
      include InteractorMetrics

      def call
        context.data = correct_data(get_orders_count_by_category)
      end

      def get_orders_count_by_category
        case range
          when 'yesterday'
            get_orders_count_by_period(2)
          when 'today'
            get_orders_count_by_period(1,true)
          when 'last_7_days'
            get_orders_count_by_period(7)
          when 'last_30_days'
            get_orders_count_by_period(30)
          when 'last_3_months'
            get_orders_count_by_period(last_quarter_days_count)
          when 'last_year'
            get_orders_count_by_period(current_year_days_count)
          when 'all_time'
            get_orders_count_by_period(1, false, true)
        end
      end

      def get_orders_count_by_period(period, today = nil, all_time = nil)
        if today
          master.order_items.joins(services: :category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).group('categories.id').count
        elsif all_time
          master.order_items.joins(services: :category).group('categories.id').count
        else
          master.order_items.joins(services: :category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }).group('categories.id').count
        end
      end

      def correct_data(data)
        data.map { |item| item[0].present? ? [ Category.find(item[0]).title, item[1] ] : ['', 0] }
      end
    end
  end
end