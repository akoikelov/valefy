module Charts
  module Pros
    class AverageTimeOrders
      include Interactor
      include InteractorMetrics

      def call
        context.data = average_time_by_categories
      end

      def average_time_by_categories
        Category.visible.map do | category |
          [ category.title, get_average_time(category) ]
        end
      end

      def get_average_time(category)
        data = case range
            when 'yesterday'
              get_average_time_by_period(category, 2)
            when 'today'
              get_average_time_by_period(category, 1, true)
            when 'last_7_days'
              get_average_time_by_period(category, 7)
            when 'last_30_days'
              get_average_time_by_period(category, 30)
            when 'last_3_months'
              get_average_time_by_period(category, last_quarter_days_count)
            when 'last_year'
              get_average_time_by_period(category, current_year_days_count)
            when 'all_time'
              get_average_time_by_period(category, 1, false, true)
        end
        convert_to_mins(data)
      end

      def get_average_time_by_period(category, period, today = nil, all_time = nil)
        if today
          OrderItem.joins(:master, services: :category ).where(users: { id: master.id }, categories: { id: category.id }, order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).pluck('SUM((order_items.end_work_time - order_items.start_work))', 'COUNT(order_items.id)')
        elsif all_time
          OrderItem.joins(:master, services: :category ).where(users: { id: master.id }, categories: { id: category.id }).pluck('SUM((order_items.end_work_time - order_items.start_work))', 'COUNT(order_items.id)')
        else
          OrderItem.joins(:master, services: :category ).where(users: { id: master.id }, categories: { id: category.id }, order_items: { created_at: DateTime.now - period.days..DateTime.now }).pluck('SUM((order_items.end_work_time - order_items.start_work))', 'COUNT(order_items.id)')
        end
      end

      def convert_to_mins(data)
        if data.present? and data[0].present? and data[0][0].present? and data[0][1].present?
          period = data[0][0].split(':')
          orders_count = data[0][1].to_i
          (((period[0].to_f * 60) + period[1].to_f.round(2) + (period[2].to_f/60).round(2)) / orders_count).round(2)
        else
          0
        end
      end
    end
  end
end