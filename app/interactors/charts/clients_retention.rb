module Charts
  class ClientsRetention
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        ios_retention, android_retention, web_retention = get_clients_retention
      else
        ios_retention, android_retention, web_retention = clients_retention_by_period(7)
      end
      context.data = [
        [ 'ios', (ios_retention.count.to_f / get_clients_by_type.count).round(2) ],
        [ 'android', (android_retention.count.to_f / get_clients_by_type.count).round(2) ],
        [ 'web', (web_retention.count.to_f / get_clients_by_type.count).round(2) ]
      ]
    end

    def get_clients_retention
      case range
        when 'last_7_days'
          clients_retention_by_period(7)
        when 'last_30_days'
          clients_retention_by_period(30)
        when 'yesterday'
          clients_retention_by_period(2)
        when 'today'
          clients_retention_by_period(1, true)
        when 'last_3_months'
          clients_retention_by_period(last_quarter_days_count)
        when 'last_year'
          clients_retention_by_period(current_year_days_count)
        when 'all_time'
          [
            get_clients_by_type.joins(:order_items).where(order_items: { platform: 'ios' }).having("count(order_items) > 2").group("users.id").uniq.count,
            get_clients_by_type.joins(:order_items).where(order_items: { platform: 'android'}).having("count(order_items) > 2").group("users.id").uniq.count,
            get_clients_by_type.joins(:order_items).where(order_items: { platform: 'web' }).having("count(order_items) > 2").group("users.id").uniq.count
          ]
      end
    end

    def clients_retention_by_period(period, today = nil)
      unless today
        [
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'ios', created_at: DateTime.now - period.days..DateTime.now }).having("count(order_items) > 2").group("users.id").uniq.count,
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'android', created_at: DateTime.now - period.days..DateTime.now }).having("count(order_items) > 2").group("users.id").uniq.count,
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'web', created_at: DateTime.now - period.days..DateTime.now }).having("count(order_items) > 2").group("users.id").uniq.count
        ]
      else
        [
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'ios', created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).having("count(order_items) > 2").group("users.id").uniq.count,
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'android', created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).having("count(order_items) > 2").group("users.id").uniq.count,
          get_clients_by_type.joins(:order_items).where(order_items: { platform: 'web', created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).having("count(order_items) > 2").group("users.id").uniq.count
        ]
      end
    end

    def get_clients_by_type
      User::Client.where(corp: is_corp)
    end
  end
end