module Charts
  class ClientsBySegment
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_clients_count
    end

    def get_clients_count
      if params[:range].present?
        get_clients_count_by_segment
      else
        get_clients_count_all_segments(7)
      end
    end

    def get_clients_count_by_segment
      case range
        when 'last_7_days'
          get_clients_count_all_segments(7)
        when 'last_30_days'
          get_clients_count_all_segments(30)
        when 'yesterday'
          get_clients_count_all_segments(2)
        when 'today'
          get_clients_count_all_segments(1, true)
        when 'last_3_months'
          get_clients_count_all_segments(last_quarter_days_count)
        when 'last_year'
          get_clients_count_all_segments(current_year_days_count)
        when 'all_time'
          get_clients_count_all_segments(1, false, true)
      end
    end

    def get_clients_count_all_segments(period, today = nil, all_time = nil)
      if today
        [
          [
            :laywer, clients_by_segment(ClientSegment.titles[:laywer]).where(users: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
          ],
          [
            :garage, clients_by_segment(ClientSegment.titles[:garage]).where(users: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
          ],
          [
            :hotel, clients_by_segment(ClientSegment.titles[:hotel]).where(users: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
          ],
          [
            :taxi, clients_by_segment(ClientSegment.titles[:taxi]).where(users: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
          ]
        ]
      elsif all_time
        [
          [
            :laywer, clients_by_segment(ClientSegment.titles[:laywer]).count
          ],
          [
            :garage, clients_by_segment(ClientSegment.titles[:garage]).count
          ],
          [
            :hotel, clients_by_segment(ClientSegment.titles[:hotel]).count
          ],
          [
            :taxi, clients_by_segment(ClientSegment.titles[:taxi]).count
          ]
        ]
      else
        [
          [
            :laywer, clients_by_segment(ClientSegment.titles[:laywer]).where(users: { created_at: DateTime.now - period.days..DateTime.now }).count
          ],
          [
            :garage, clients_by_segment(ClientSegment.titles[:garage]).where(users: { created_at: DateTime.now - period.days..DateTime.now }).count
          ],
          [
            :hotel, clients_by_segment(ClientSegment.titles[:hotel]).where(users: { created_at: DateTime.now - period.days..DateTime.now }).count
          ],
          [
            :taxi, clients_by_segment(ClientSegment.titles[:taxi]).where(users: { created_at: DateTime.now - period.days..DateTime.now }).count
          ]
        ]
      end
    end

    def clients_by_segment(segment)
      User::Client.joins(:client_segment).where(client_segments: { title: segment })
    end
  end
end