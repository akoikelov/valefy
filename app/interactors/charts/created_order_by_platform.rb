module Charts
  class CreatedOrderByPlatform
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        android, ios, web, admin = get_orders
      else
        android, ios, web, admin = get_orders_by_platform_period(7)
      end
      context.data = [['android', android], ['ios', ios], ['web', web], ['admin panel', admin]]
    end

    def get_orders
      case range
        when 'yesterday'
          get_orders_by_platform_period(2)
        when 'today'
          get_orders_by_platform_period(1, true)
        when 'last_7_days'
          get_orders_by_platform_period(7)
        when 'last_30_days'
          get_orders_by_platform_period(30)
        when 'last_3_months'
          get_orders_by_platform_period(last_quarter_days_count)
        when 'last_year'
          get_orders_by_platform_period(current_year_days_count)
        when 'all_time'
          all = (100.to_f/get_total_orders_count).to_f.round(2)
          [
            get_orders_by_client_type.created_by_android.count * all,
            get_orders_by_client_type.created_by_ios.count     * all,
            get_orders_by_client_type.created_by_web.count     * all,
            get_orders_by_client_type.created_by_admin.count   * all
          ]
      end
    end

    def get_orders_by_platform_period(period, today = nil)
      unless today
        orders_count = get_orders_by_client_type.where(created_at: DateTime.now - period.days..DateTime.now).count
        all = orders_count > 0 ?  (100.to_f/orders_count).to_f.round(2)  : 0
        [
          get_orders_by_client_type.created_by_android.where(created_at: DateTime.now - period.days..DateTime.now).count * all,
          get_orders_by_client_type.created_by_ios.where(created_at: DateTime.now - period.days..DateTime.now).count     * all,
          get_orders_by_client_type.created_by_web.where(created_at: DateTime.now - period.days..DateTime.now).count     * all,
          get_orders_by_client_type.created_by_admin.where(created_at: DateTime.now - period.days..DateTime.now).count   * all
        ]
      else
        orders_count = get_orders_by_client_type.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count
        all = orders_count > 0 ? (100.to_f/orders_count).to_f.round(2) : 0
        [
          get_orders_by_client_type.created_by_android.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count * all,
          get_orders_by_client_type.created_by_ios.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count     * all,
          get_orders_by_client_type.created_by_web.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count     * all,
          get_orders_by_client_type.created_by_admin.where(created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day).count   * all
        ]
      end
    end

    def get_total_orders_count
      OrderItem.joins(:client).where(users: { corp: is_corp }).count
    end

    def get_orders_by_client_type
      OrderItem.joins(:client).where(users: { corp: is_corp })
    end
  end
end