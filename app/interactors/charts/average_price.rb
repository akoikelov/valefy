module Charts
  class AveragePrice
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_average_on_categories
    end

    def get_average_on_categories
      Category.visible.map do |cat|
        if params[:range].present?
          completed_orders = get_completed_orders(cat)
        else
          completed_orders = get_orders_by_period(cat, 7)
        end
        prices = completed_orders.map { |e| e.services.map(& :price).sum.to_f  } if completed_orders.count > 0
        average_value(cat, prices, completed_orders)
      end
    end

    def get_completed_orders(category)
      case range
        when 'last_7_days'
          get_orders_by_period(category, 7)
        when 'last_30_days'
          get_orders_by_period(category, 30)
        when 'yesterday'
          get_orders_by_period(category, 2)
        when 'today'
          get_orders_by_period(category, 1, true)
        when 'last_3_months'
          get_orders_by_period(category, last_quarter_days_count)
        when 'last_year'
          get_orders_by_period(category, current_year_days_count)
        when 'all_time'
          get_orders_by_period(category, 1, false, true)
      end
    end

    def get_orders_by_period(category, period, today = nil, all_time = nil)
      if today
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day })
      elsif all_time
        category_orders_by_client_type(category)
      else
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now })
      end
    end

    def category_orders_by_client_type(category)
      category.order_items.joins(:client).where(users: { corp: is_corp }).history
    end

    def average_value(cat, prices, completed_orders)
      prices.present? ? ["#{cat.title} €", (prices.sum / completed_orders.count).round(2)] : ["#{cat.title} €", 0]
    end
  end
end