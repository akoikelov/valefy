module Charts
  class ClientsByType
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        corp, individual = get_clients
      else
        corp, individual = get_clients_by_period(7)
      end
      context.data = [['individual', individual], ['corporative', corp]]
    end

    def get_clients
      case range
        when 'last_7_days'
          get_clients_by_period(7)
        when 'last_30_days'
          get_clients_by_period(30)
        when 'yesterday'
          get_clients_by_period(2)
        when 'today'
          get_clients_by_period(1, true)
        when 'last_3_months'
          get_clients_by_period(last_quarter_days_count)
        when 'last_year'
          get_clients_by_period(current_year_days_count)
        when 'all_time'
          [User::Client.bussiness.count, User::Client.not_bussiness.count]
      end
    end

    def get_clients_by_period(period, today = nil)
      unless today
        [User::Client.bussiness.where(created_at:  DateTime.now - period.days..DateTime.now).count, User::Client.not_bussiness.where(created_at:  DateTime.now - period.days..DateTime.now).count]
      else
        [User::Client.bussiness.where(created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day).count, User::Client.not_bussiness.where(created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day).count]
      end
    end
  end
end