module Charts
  class LastMonthOrdersClientsCount
    include Interactor
    include InteractorMetrics

    def call
      context.users_count  = User::Client.where(created_at: DateTime.now - 30.days..DateTime.now, corp: is_corp).count
      context.orders_count = OrderItem.joins(:client).where(users: { corp: is_corp }, order_items: { created_at:  DateTime.now - 30.days..DateTime.now}).count
    end
  end
end