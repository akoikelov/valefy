module Charts
  class ProfitOnServices
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_profits_all_categories
      sort_data_by_asc
    end

    def get_profits_all_categories
      Category.visible.map do |category|
        if params[:range].present?
          completed_orders = get_completed_orders(category)
        else
          completed_orders = get_completed_orders_by_period(category, 7)
        end
        profit = get_total_profit_on_category(completed_orders)
        profit.present? ? ["#{category.title} €", profit] : ["#{category.title} €", 0]
      end
    end

    def get_completed_orders(category)
      case range
        when 'last_7_days'
          get_completed_orders_by_period(category, 7)
        when 'last_30_days'
          get_completed_orders_by_period(category, 30)
        when 'yesterday'
          get_completed_orders_by_period(category, 2)
        when 'today'
          get_completed_orders_by_period(category, 1, true)
        when 'last_3_months'
          get_completed_orders_by_period(category, last_quarter_days_count)
        when 'last_year'
          get_completed_orders_by_period(category, current_year_days_count)
        when 'all_time'
          get_completed_orders_by_period(category, 1, false, true)
      end
    end

    def get_completed_orders_by_period(category, period, today = nil, all_time = nil)
      if today
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day })
      elsif all_time
        category_orders_by_client_type(category)
      else
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now })
      end
    end

    def get_total_profit_on_category(orders)
      orders.map { |e| e.services.map(& :price).sum.to_f  }.sum if orders.count > 0
    end

    def sort_data_by_asc
      context.data = context.data.sort{ |a, b| b[1] <=> a[1]}
    end

    def category_orders_by_client_type(category)
      category.order_items.joins(:client).where(users: { corp: is_corp }).history
    end
  end
end