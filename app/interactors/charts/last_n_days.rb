module Charts
  class LastNDays
    include Interactor
    include InteractorMetrics

    def call
      get_orders_count
    end

    def get_orders_count
      if params[:range].present?
        if range.eql?('last_7_days')
          get_orders_by_day(7)
        elsif range.eql?('last_30_days')
          get_orders_by_day(30)
        elsif range.eql?('yesterday')
          get_orders_by_day(2)
        elsif range.eql?('today')
          get_orders_by_day(1)
        elsif range.eql?('last_3_months')
          get_orders_by_day(last_quarter_days_count)
        elsif range.eql?('last_year')
          get_orders_by_day(current_year_days_count)
        end
      else
        get_orders_by_day(7)
      end
    end

    def get_orders_by_day(days)
      context.data = if client_id.present?
        OrderItem.joins(:client).where(users: { id: client_id }).group_by_day('order_items.created_at', last: days).count
      else
        OrderItem.joins(:client).where(users: { corp: is_corp }).group_by_day('order_items.created_at', last: days).count
      end
    end
  end
end