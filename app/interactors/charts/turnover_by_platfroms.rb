module Charts
  class TurnoverByPlatfroms
    include Interactor
    include InteractorMetrics

    def call
      context.data = get_turnover_all_platforms
    end

    def get_turnover_all_platforms
      if params[:range].present?
        turnover = get_turnover_by_period
      else
        turnover = get_turnover(7)
      end
    end

    def get_turnover_by_period
      case range
        when 'last_7_days'
          get_turnover(7)
        when 'last_30_days'
          get_turnover(30)
        when 'yesterday'
          get_turnover(2)
        when 'today'
          get_turnover(1, true)
        when 'last_3_months'
          get_turnover(last_quarter_days_count)
        when 'last_year'
          get_turnover(current_year_days_count)
        when 'all_time'
          get_turnover(1, false, true)
      end
    end

    def get_turnover(period, today = nil, all_time = nil)
      ios, android, admin, web = get_orders_all_platforms(period, today, all_time)
      [['ios', get_total_turnover(ios)], ['android', get_total_turnover(android)], ['admin', get_total_turnover(admin)], ['web', get_total_turnover(web)]]
    end

    def get_orders_all_platforms(period, today = nil, all_time = nil)
      if today
        [
          ios = get_ios_orders.where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }),
          android = get_android_orders.where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }),
          admin = get_admin_orders.where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }),
          get_web_orders.where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day })
        ]
      elsif all_time
        [
          get_ios_orders,
          get_android_orders,
          get_admin_orders,
          get_web_orders
        ]
      else
        [
          get_ios_orders.where(order_items: { created_at: DateTime.now - period.days..DateTime.now }),
          get_android_orders.where(order_items: { created_at: DateTime.now - period.days..DateTime.now }),
          get_admin_orders.where(order_items: { created_at: DateTime.now - period.days..DateTime.now }),
          get_web_orders.where(order_items: { created_at: DateTime.now - period.days..DateTime.now })
        ]
      end
    end

    def get_total_turnover(orders)
      orders.count > 0 ? orders.includes(:services).map { |order| order.services.map(& :price).sum.to_f  }.sum : 0
    end

    def get_orders_by_client_type
      OrderItem.joins(:client).where(users: { corp: is_corp })
    end

    def get_ios_orders
      get_orders_by_client_type.created_by_ios.history
    end

    def get_android_orders
      get_orders_by_client_type.created_by_android.history
    end

    def get_admin_orders
      get_orders_by_client_type.created_by_admin.history
    end

    def get_web_orders
      get_orders_by_client_type.created_by_web.history
    end

  end
end