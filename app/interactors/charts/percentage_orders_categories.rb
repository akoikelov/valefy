module Charts
  class PercentageOrdersCategories
    include Interactor
    include InteractorMetrics

    def call
      get_orders_count_by_category
    end

    def get_orders_count_by_category
      if params[:range].present?
        context.data = case range
          when 'last_7_days'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, 7)/get_total_orders_count).round(2)]}
          when 'last_30_days'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, 30)/get_total_orders_count).round(2)]}
          when 'yesterday'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, 2)/get_total_orders_count).round(2)]}
          when 'today'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, 1, true)/get_total_orders_count).round(2)]}
          when 'last_3_months'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, last_quarter_days_count)/get_total_orders_count).round(2)]}
          when 'last_year'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, current_year_days_count)/get_total_orders_count).round(2)]}
          when 'all_time'
            Category.visible.map{|cat| ["#{cat.title}", (100 * get_orders_count_by_period(cat, 1, false, true)/get_total_orders_count).round(2)]}
        end
      else
        context.data = Category.visible.map{|cat| ["#{cat.title}", (100 * cat.order_items.count/OrderItem.count.to_f).round(2)]}
      end
    end

    def get_orders_count_by_period(category, period, today = nil, all_time = nil)
      if today
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
      elsif all_time
        category_orders_by_client_type(category).count
      else
        category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }).count
      end
    end

    def get_total_orders_count
      OrderItem.joins(:client).where(users: { corp: is_corp }).count.to_f
    end

    def category_orders_by_client_type(category)
      category.order_items.joins(:client).where(users: { corp: is_corp })
    end
  end
end