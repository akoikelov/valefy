module Charts
  class OrdersFrequency
    include Interactor
    include InteractorMetrics

    def call
      if params[:range].present?
        context.data = orders_frequency_each_category
      else
        context.data = Category.visible.map do |category|
          order_frequency_for_category(category, 7)
        end
      end
    end

    def orders_frequency_each_category
      Category.visible.map do |category|
        case range
          when 'last_7_days'
            order_frequency_for_category(category, 7)
          when 'last_30_days'
            order_frequency_for_category(category, 30)
          when 'yesterday'
            order_frequency_for_category(category, 2)
          when 'today'
            order_frequency_for_category(category, 1, true)
          when 'last_3_months'
            order_frequency_for_category(category, last_quarter_days_count)
          when 'last_year'
            order_frequency_for_category(category, current_year_days_count)
          when 'all_time'
            active_users_count = User::Client.joins(order_items: [services: :category]).where(categories: { id: category.id }, order_items: { status: [9, 11] }, users: { corp: is_corp }).count('distinct users.id')
            orders_count = category_orders_by_client_type(category).count
            active_users_count > 0 ? [ category.title, orders_count/active_users_count ] : [category.title, 0]
        end
      end
    end

    def order_frequency_for_category(category, period, today = nil)
      unless today
        active_users_count = User::Client.joins(order_items: [services: :category]).where(categories: { id: category.id }, order_items: { created_at: DateTime.now - period.days..DateTime.now, status: [9, 11] }, users: { corp: is_corp }).count('distinct users.id')
        orders_count = category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now - period.days..DateTime.now }).count
      else
        active_users_count = User::Client.joins(order_items: [services: :category]).where(categories: { id: category.id }, order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day, status: [9, 11] }, users: { corp: is_corp }).count('distinct users.id')
        orders_count = category_orders_by_client_type(category).where(order_items: { created_at: DateTime.now.beginning_of_day..DateTime.now.end_of_day }).count
      end
      active_users_count > 0 ? [ category.title, orders_count/active_users_count ] : [category.title, 0]
    end

    def category_orders_by_client_type(category)
      category.order_items.joins(:client).where(users: { corp: is_corp }).history
    end
  end
end