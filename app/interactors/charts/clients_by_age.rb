module Charts
  class ClientsByAge
    include Interactor
    include InteractorMetrics

    def call
      context.data = params[:range].present? ? get_clients_count : clients_by_age_intervals(7)
    end

    def get_clients_count
      case range
        when 'last_7_days'
          clients_by_age_intervals(7)
        when 'last_30_days'
          clients_by_age_intervals(30)
        when 'yesterday'
          clients_by_age_intervals(2)
        when 'today'
          clients_by_age_intervals(1, true)
        when 'last_3_months'
          clients_by_age_intervals(last_quarter_days_count)
        when 'last_year'
          clients_by_age_intervals(current_year_days_count)
        when 'all_time'
          clients_by_age_intervals(1, false, true)
      end
    end

    def get_clients_count_by_age(period, date_period, today = nil, all_time = nil)
      if today
        User::Client.joins(:order_items).where(users: { corp: is_corp, b_day: date_period[0]..date_period[1] }, order_items: { created_at:  DateTime.now.beginning_of_day..DateTime.now.end_of_day }).uniq.count
      elsif all_time
        User::Client.joins(:order_items).where(users: { corp: is_corp, b_day: date_period[0]..date_period[1] }).uniq.count
      else
        User::Client.joins(:order_items).where(users: { corp: is_corp, b_day: date_period[0]..date_period[1] }, order_items: { created_at:  DateTime.now - period.days..DateTime.now }).uniq.count
      end
    end

    def get_age_intervals
      [ [0, 18], [18, 25], [25, 40], [40, 60]]
    end

    def clients_by_age_intervals(period, today = nil, all_time = nil)
      get_age_intervals.map do |interval|
        start_date = Date.current - interval[1].years
        end_date   = Date.current - interval[0].years
        [ "#{interval[0]}-#{interval[1]}", get_clients_count_by_age(period, [start_date, end_date], today, all_time)]
      end
    end
  end
end