module Admin
  module Panel
    module Countries
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_countries
        end

        def get_countries
          countries = Country.all
          countries = get_by_name(countries)
          context.data = get_by_description(countries)
        end

        def get_by_name(countries = nil)
          if params[:name_filter].present? and params[:name].present?
            case params[:name_filter]
              when 'contains'
                countries.where('lower(name) like :name ', name: "%#{params[:name].downcase}%")
              when 'equals'
                countries.where('lower(name) = :name ', name: params[:name].downcase)
              when 'starts_with'
                countries.where('lower(name) like :name ', name: "#{params[:name].downcase}%")
              when 'ends_with'
                countries.where('lower(name) like :name ', name: "%#{params[:name].downcase}")
            end
          else
            countries
          end
        end

        def get_by_description(countries = nil)
          if params[:description_filter].present? and params[:description].present?
            case params[:description_filter]
              when 'contains'
                countries.where('lower(description) like :description ', description: "%#{params[:description].downcase}%")
              when 'equals'
                countries.where('lower(description) = :description ', description: params[:description].downcase)
              when 'starts_with'
                countries.where('lower(description) like :description ', description: "#{params[:description].downcase}%")
              when 'ends_with'
                countries.where('lower(description) like :description ', description: "%#{params[:description].downcase}")
            end
          else
            countries
          end
        end
      end
    end
  end
end