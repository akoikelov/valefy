module Admin
  module Panel
    module Offices
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_offices
        end

        def get_offices
          offices = Office.all
          offices = get_by_title(offices)
          offices = get_by_count_vehicles(offices)
          offices = get_by_client(offices)
          context.data = offices.order(id: :desc)
        end

        def get_by_title(offices = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                offices.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                offices.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                offices.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                offices.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            offices
          end
        end

        def get_by_count_vehicles(offices = nil)
          if params[:count_vehicle_filters].present? and params[:count_vehicles].present?
            case params[:count_vehicle_filters]
              when 'equals'
                offices.where('count_vehicles = :count_vehicles', count_vehicles: params[:count_vehicles])
              when 'greater_than'
                offices.where('count_vehicles > :count_vehicles', count_vehicles: params[:count_vehicles])
              when 'less_than'
                offices.where('count_vehicles < :count_vehicles', count_vehicles: params[:count_vehicles])
            end
          else
            offices
          end
        end

        def get_by_client(offices = nil)
          params[:client_id].present? ? offices.where(client_id: params[:client_id]) : offices
        end
      end
    end
  end
end