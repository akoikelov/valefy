module Admin
  module Panel
    module EmailTexts
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_email_texts
        end

        def get_email_texts
          email_texts  = EmailText.all
          email_texts  = get_by_email_type(email_texts)
          email_texts  = get_by_slug(email_texts)
          context.data = get_by_content(email_texts)
        end

        def get_by_email_type(email_texts = nil)
          if params[:email_type_filter].present? and params[:email_type].present?
            case params[:email_type_filter]
              when 'contains'
                email_texts.where('lower(email_type) like :email_type ', email_type: "%#{params[:email_type].downcase}%")
              when 'equals'
                email_texts.where('lower(email_type) = :email_type ', email_type: params[:email_type].downcase)
              when 'starts_with'
                email_texts.where('lower(email_type) like :email_type ', email_type: "#{params[:email_type].downcase}%")
              when 'ends_with'
                email_texts.where('lower(email_type) like :email_type ', email_type: "%#{params[:email_type].downcase}")
            end
          else
            email_texts
          end
        end

        def get_by_slug(email_texts = nil)
          params[:slug].present? ? email_texts.where(slug: params[:slug]) : email_texts
        end

        def get_by_content(email_texts = nil)
          if params[:content_filter].present? and params[:content].present?
            case params[:content_filter]
              when 'contains'
                email_texts.where('lower(content) like :content ', content: "%#{params[:content].downcase}%")
              when 'equals'
                email_texts.where('lower(content) = :content ', content: params[:content].downcase)
              when 'starts_with'
                email_texts.where('lower(content) like :content ', content: "#{params[:content].downcase}%")
              when 'ends_with'
                email_texts.where('lower(content) like :content ', content: "%#{params[:content].downcase}")
            end
          else
            email_texts
          end
        end
      end
    end
  end
end