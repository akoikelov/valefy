module Admin
  module Panel
    module Pros
      class WorkSchedule
        include Interactor
        include InteractorMetrics

        def call
          context.pros  = get_pros.page(params[:page])
        end

        def get_pros
          params[:work_date].present? ? get_by_work_day : all_pros
        end

        def all_pros
          return User::ServiceMan.all if((params[:company_id].present? and params[:company_id].eql?('all')) or params[:company_id].nil?)

          id = current_admin_user.try(:company).try(:id)
          id.present? ? User::ServiceMan.by_company(id) : User::ServiceMan.all
        end

        def get_weekday_name
          DateTime.parse(params[:work_date]).strftime('%A').downcase if params[:work_date].present?
        end

        def get_by_work_day
          User::ServiceMan.where(id: get_for_selected_day)
        end

        def get_for_selected_day
          all_pros.select { |pro| pro.pro_information.work_schedule[get_weekday_name]['available'] == true }.map(&:id)
        end
      end
    end
  end
end