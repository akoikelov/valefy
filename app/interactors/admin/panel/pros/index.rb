module Admin
  module Panel
    module Pros
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_pros
        end

        def get_pros
          pros = all_pros.filter(params.slice(:is_mobile))
          pros = get_by_name(pros)
          pros = get_by_phone_number(pros)
          pros = get_by_email(pros)
          pros = get_by_company(pros)
          context.data = pros.page(params[:page])
        end

        def get_by_company(pros = nil)
          return pros if params[:company_id].eql?('all')
          params[:company_id].present? ? pros.where(company_id: params[:company_id]) : pros
        end

        def get_by_name(pros = nil)
          if params[:name_filter].present? and params[:name].present?
            case params[:name_filter]
              when 'contains'
                pros.where('lower(name) like :name ', name: "%#{params[:name].downcase}%")
              when 'equals'
                pros.where('lower(name) = :name ', name: params[:name].downcase)
              when 'starts_with'
                pros.where('lower(name) like :name ', name: "#{params[:name].downcase}%")
              when 'ends_with'
                pros.where('lower(name) like :name ', name: "%#{params[:name].downcase}")
            end
          else
            pros
          end
        end

        def get_by_phone_number(pros = nil)
          if params[:phone_number_filter].present? and params[:phone_number].present?
            case params[:phone_number_filter]
              when 'contains'
                pros.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}%")
              when 'equals'
                pros.where('lower(phone_number) = :phone_number ', phone_number: params[:phone_number].downcase)
              when 'starts_with'
                pros.where('lower(phone_number) like :phone_number ', phone_number: "#{params[:phone_number].downcase}%")
              when 'ends_with'
                pros.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}")
            end
          else
            pros
          end
        end

        def get_by_email(pros = nil)
          if params[:email_filter].present? and params[:email].present?
            case params[:email_filter]
              when 'contains'
                pros.where('lower(email) like :email ', email: "%#{params[:email].downcase}%")
              when 'equals'
                pros.where('lower(email) = :email ', email: params[:email].downcase)
              when 'starts_with'
                pros.where('lower(email) like :email ', email: "#{params[:email].downcase}%")
              when 'ends_with'
                pros.where('lower(email) like :email ', email: "%#{params[:email].downcase}")
            end
          else
            pros
          end
        end

        def all_pros
          return User::ServiceMan.all if((params[:company_id].present? and params[:company_id].eql?('all')) or params[:company_id].nil?)

          id = current_admin_user.try(:company).try(:id)
          id.present? ? User::ServiceMan.by_company(id) : User::ServiceMan.all
        end
      end
    end
  end
end