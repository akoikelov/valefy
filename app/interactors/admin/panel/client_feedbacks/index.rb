module Admin
  module Panel
    module ClientFeedbacks
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_client_feedbacks
        end

        def get_client_feedbacks
          client_feedbacks = ClientFeedback.all
          client_feedbacks = get_by_order_item(client_feedbacks)
          client_feedbacks = get_by_client(client_feedbacks)
          client_feedbacks = get_by_pro(client_feedbacks)
          client_feedbacks = get_by_comment(client_feedbacks)
          client_feedbacks = get_by_polite(client_feedbacks)
          client_feedbacks = get_by_orderly(client_feedbacks)
          client_feedbacks = get_by_friendly(client_feedbacks)
          client_feedbacks = get_by_rating(client_feedbacks)
          context.data = client_feedbacks.order(id: :desc).page(params[:page])
        end

        def get_by_order_item(client_feedbacks = nil)
          params[:order_item_id].present? ? client_feedbacks.where(order_item_id: params[:order_item_id]) : client_feedbacks
        end

        def get_by_client(client_feedbacks = nil)
          params[:client_id].present? ? client_feedbacks.where(client_id: params[:client_id]) : client_feedbacks
        end

        def get_by_pro(client_feedbacks = nil)
          params[:serviceman_id].present? ? client_feedbacks.where(serviceman_id: params[:serviceman_id]) : client_feedbacks
        end

        def get_by_comment(client_feedbacks = nil)
          if params[:comment_filter].present? and params[:comment].present?
            case params[:comment_filter]
              when 'contains'
                client_feedbacks.where('lower(comment) like :comment ', comment: "%#{params[:comment].downcase}%")
              when 'equals'
                client_feedbacks.where('lower(comment) = :comment ', comment: params[:comment].downcase)
              when 'starts_with'
                client_feedbacks.where('lower(comment) like :comment ', comment: "#{params[:comment].downcase}%")
              when 'ends_with'
                client_feedbacks.where('lower(comment) like :comment ', comment: "%#{params[:comment].downcase}")
            end
          else
            client_feedbacks
          end
        end

        def get_by_polite(client_feedbacks = nil)
          if params[:polite_filter].present? and params[:polite].present?
            case params[:polite_filter]
              when 'equals'
                client_feedbacks.where('polite = :polite', polite: params[:polite])
              when 'greater_than'
                client_feedbacks.where('polite > :polite', polite: params[:polite])
              when 'less_than'
                client_feedbacks.where('polite < :polite', polite: params[:polite])
            end
          else
            client_feedbacks
          end
        end

        def get_by_orderly(client_feedbacks = nil)
          if params[:orderly_filter].present? and params[:orderly].present?
            case params[:orderly_filter]
              when 'equals'
                client_feedbacks.where('orderly = :orderly', orderly: params[:orderly])
              when 'greater_than'
                client_feedbacks.where('orderly > :orderly', orderly: params[:orderly])
              when 'less_than'
                client_feedbacks.where('orderly < :orderly', orderly: params[:orderly])
            end
          else
            client_feedbacks
          end
        end

        def get_by_friendly(client_feedbacks = nil)
          if params[:friendly_filter].present? and params[:friendly].present?
            case params[:friendly_filter]
              when 'equals'
                client_feedbacks.where('friendly = :friendly', friendly: params[:friendly])
              when 'greater_than'
                client_feedbacks.where('friendly > :friendly', friendly: params[:friendly])
              when 'less_than'
                client_feedbacks.where('friendly < :friendly', friendly: params[:friendly])
            end
          else
            client_feedbacks
          end
        end

        def get_by_rating(client_feedbacks = nil)
          if params[:rating_filter].present? and params[:rating].present?
            case params[:rating_filter]
              when 'equals'
                client_feedbacks.where('rating = :rating', rating: params[:rating])
              when 'greater_than'
                client_feedbacks.where('rating > :rating', rating: params[:rating])
              when 'less_than'
                client_feedbacks.where('rating < :rating', rating: params[:rating])
            end
          else
            client_feedbacks
          end
        end
      end
    end
  end
end