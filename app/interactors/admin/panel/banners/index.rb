module Admin
  module Panel
    module Banners
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_banners
        end

        def get_banners
          banners = Banner.all
          banners = get_by_title(banners)
          context.data = get_by_description(banners)
        end

        def get_by_title(banners = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                banners.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                banners.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                banners.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                banners.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            banners
          end
        end

        def get_by_description(banners = nil)
          if params[:description_filter].present? and params[:description].present?
            case params[:description_filter]
              when 'contains'
                banners.where('lower(description) like :description ', description: "%#{params[:description].downcase}%")
              when 'equals'
                banners.where('lower(description) = :description ', description: params[:description].downcase)
              when 'starts_with'
                banners.where('lower(description) like :description ', description: "#{params[:description].downcase}%")
              when 'ends_with'
                banners.where('lower(description) like :description ', description: "%#{params[:description].downcase}")
            end
          else
            banners
          end
        end
      end
    end
  end
end