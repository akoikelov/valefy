module Admin
  module Panel
    module SmsHistories
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_sms_histories
        end

        def get_sms_histories
          sms_histories = SmsHistory.all
          sms_histories = get_by_phone_number(sms_histories)
          sms_histories = get_by_code(sms_histories)
          sms_histories = get_by_status(sms_histories)
          sms_histories = get_by_client(sms_histories)
          sms_histories = get_by_pro(sms_histories)
          context.data  = sms_histories.order(id: :desc).page(params[:page])
        end

        def get_by_phone_number(sms_histories = nil)
          if params[:phone_number_filter].present? and params[:phone_number].present?
            case params[:phone_number_filter]
              when 'contains'
                sms_histories.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}%")
              when 'equals'
                sms_histories.where('lower(phone_number) = :phone_number ', phone_number: params[:phone_number].downcase)
              when 'starts_with'
                sms_histories.where('lower(phone_number) like :phone_number ', phone_number: "#{params[:phone_number].downcase}%")
              when 'ends_with'
                sms_histories.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}")
            end
          else
            sms_histories
          end
        end

        def get_by_code(sms_histories = nil)
          if params[:code_filter].present? and params[:code].present?
            case params[:code_filter]
              when 'equals'
                sms_histories.where('code = :code', code: params[:code])
              when 'greater_than'
                sms_histories.where('code > :code', code: params[:code])
              when 'less_than'
                sms_histories.where('code < :code', code: params[:code])
            end
          else
            sms_histories
          end
        end

        def get_by_status(sms_histories = nil)
          params[:status].present? ? sms_histories.where(status: params[:status]) : sms_histories
        end

        def get_by_client(sms_histories = nil)
          params[:client_id].present? ? sms_histories.where(client_id: params[:client_id]) : sms_histories
        end

        def get_by_pro(sms_histories = nil)
          params[:master_id].present? ? sms_histories.where(master_id: params[:master_id]) : sms_histories
        end
      end
    end
  end
end