module Admin
  module Panel
    module Cities
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_cities
        end

        def get_cities
          cities = City.all
          cities = get_by_name(cities)
          cities = get_by_description(cities)
          context.data = get_by_country(cities)
        end

        def get_by_name(cities = nil)
          if params[:name_filter].present? and params[:name].present?
            case params[:name_filter]
              when 'contains'
                cities.where('lower(name) like :name ', name: "%#{params[:name].downcase}%")
              when 'equals'
                cities.where('lower(name) = :name ', name: params[:name].downcase)
              when 'starts_with'
                cities.where('lower(name) like :name ', name: "#{params[:name].downcase}%")
              when 'ends_with'
                cities.where('lower(name) like :name ', name: "%#{params[:name].downcase}")
            end
          else
            cities
          end
        end

        def get_by_description(cities = nil)
          if params[:description_filter].present? and params[:description].present?
            case params[:description_filter]
              when 'contains'
                cities.where('lower(description) like :description ', description: "%#{params[:description].downcase}%")
              when 'equals'
                cities.where('lower(description) = :description ', description: params[:description].downcase)
              when 'starts_with'
                cities.where('lower(description) like :description ', description: "#{params[:description].downcase}%")
              when 'ends_with'
                cities.where('lower(description) like :description ', description: "%#{params[:description].downcase}")
            end
          else
            cities
          end
        end

        def get_by_country(cities = nil)
          params[:country_id].present? ? cities.where(country_id: params[:country_id]) : cities
        end
      end
    end
  end
end