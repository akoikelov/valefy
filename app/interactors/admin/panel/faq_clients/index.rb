module Admin
  module Panel
    module FaqClients
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_faq_clients
        end

        def get_faq_clients
          faq_clients = Faq.client
          faq_clients = get_by_question(faq_clients)
          context.data = get_by_answer(faq_clients)
        end

        def get_by_question(faq_clients = nil)
          if params[:question_filter].present? and params[:question].present?
            case params[:question_filter]
              when 'contains'
                faq_clients.where('lower(question) like :question ', question: "%#{params[:question].downcase}%")
              when 'equals'
                faq_clients.where('lower(question) = :question ', question: params[:question].downcase)
              when 'starts_with'
                faq_clients.where('lower(question) like :question ', question: "#{params[:question].downcase}%")
              when 'ends_with'
                faq_clients.where('lower(question) like :question ', question: "%#{params[:question].downcase}")
            end
          else
            faq_clients
          end
        end

        def get_by_answer(faq_clients = nil)
          if params[:answer_filter].present? and params[:answer].present?
            case params[:answer_filter]
              when 'contains'
                faq_clients.where('lower(answer) like :answer ', answer: "%#{params[:answer].downcase}%")
              when 'equals'
                faq_clients.where('lower(answer) = :answer ', answer: params[:answer].downcase)
              when 'starts_with'
                faq_clients.where('lower(answer) like :answer ', answer: "#{params[:answer].downcase}%")
              when 'ends_with'
                faq_clients.where('lower(answer) like :answer ', answer: "%#{params[:answer].downcase}")
            end
          else
            faq_clients
          end
        end
      end
    end
  end
end