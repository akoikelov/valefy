module Admin
  module Panel
    module OrderItems
      class List
        include Interactor
        include InteractorMetrics

        def call
          context.data = get_orders
          context.orders_count = orders_count_by_status
        end

        def get_orders
          orders = []
          orders = get_by_scope
          orders = get_by_client(orders)
          orders = get_by_pro(orders)
          orders = get_by_category(orders)
          orders = get_by_service(orders)
          orders = get_by_count_vehicles(orders)
          orders = get_by_client_type(orders)
          orders = find_by_company(orders)
          orders.page(params[:page])
        end

        def get_by_scope
          if params[:orders_scope].present?
            case params[:orders_scope]
              when 'All'
                all_orders_by_company
              when 'Created'
                all_orders_by_company.created
              when 'Pending'
                all_orders_by_company.serching
              when 'Active'
                all_orders_by_company.busy
              when 'History'
                all_orders_by_company.history
              when 'Cancelled'
                all_orders_by_company.canceled
            end
          else
            all_orders_by_company.serching.page params[:page]
          end
        end

        def orders_count_by_status
          {
             all:       OrderItem.count ,
             created:   OrderItem.created.count,
             searching: OrderItem.serching.count,
             busy:      OrderItem.busy.count,
             history:   OrderItem.history.count,
             canceled:  OrderItem.canceled.count
          }
        end

        def get_by_client(orders = nil)
          params[:client_id].present? ? orders.where(client_id: params[:client_id]) : orders
        end

        def get_by_pro(orders = nil)
          params[:pro_id].present? ? orders.where(master_id:  params[:pro_id]) : orders
        end

        def get_by_category(orders = nil)
          params[:category_id].present? ? orders.joins(services: :category).where(categories: { id:  params[:category_id]}) : orders
        end

        def get_by_service(orders = nil)
          params[:service_id].present? ? orders.joins(:services).where(services: { id: params[:service_id] }) : orders
        end

        def get_by_count_vehicles(orders = nil)
          if params[:count_vehicle_filters].present? and params[:count_vehicles].present?
            case params[:count_vehicle_filters]
              when 'equals'
                orders.where('count_vehicles = :count_vehicles', count_vehicles: params[:count_vehicles])
              when 'greater_than'
                orders.where('count_vehicles > :count_vehicles', count_vehicles: params[:count_vehicles])
              when 'less_than'
                orders.where('count_vehicles < :count_vehicles', count_vehicles: params[:count_vehicles])
            end
          else
            orders
          end
        end

        def get_by_client_type(orders = nil)
          if params[:client_type].present?
            params[:client_type] == 'corp' ? orders.joins(:client).where(users: { corp: true }) : orders.joins(:client).where(users: { corp: false })
          else
            orders
          end
        end

        def find_by_company(orders)
          if params[:company_id].present?
            return orders_arr(orders) if params[:company_id].eql?('all')
            orders.present? ? orders.by_company(params[:company_id].to_i) : orders_arr(orders)
          else
            orders.present? ? orders : orders_arr(orders)
          end
        end

        def orders_arr(orders = nil)
          orders.present? ? orders : all_orders_by_company
        end

        def all_orders_by_company
          if params[:company_id].present? and params[:company_id].eql?('all')
            OrderItem.all
          elsif params[:company_id].present? and params[:company_id].eql?('all') == false
            OrderItem.by_company(current_admin_user.try(:company).try(:id))
          else
            OrderItem.all
          end
        end
      end
    end
  end
end
