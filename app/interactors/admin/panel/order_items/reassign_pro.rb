module Admin
  module Panel
    module OrderItems
      class ReassignPro
        include Interactor
        include InteractorMetrics

        def call
          reassign_pro
        end

        def reassign_pro
          order = OrderItem.find(params[:order_id])
          master = User::ServiceMan.find(params[:master_id])
          order.master = master
          context.fail!(error: "Can't reassign VALEFYER") unless order.save
        end
      end
    end
  end
end