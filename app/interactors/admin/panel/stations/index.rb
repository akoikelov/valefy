module Admin
  module Panel
    module Stations
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_stations
        end

        def get_stations
          stations = CompanyLocation.by_company(current_admin_user.try(:company).try(:id))
          stations = get_by_title(stations)
          stations = get_by_phone_number(stations)
          stations = get_by_email(stations)
          stations = get_by_address(stations)
          stations = get_by_company(stations)
          context.data = stations.page(params[:page])
        end

        def get_by_company(stations = nil)
          params[:company_id].present? ? stations.where(company_id: params[:company_id]) : stations
        end

        def get_by_title(stations = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                stations.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                stations.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                stations.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                stations.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            stations
          end
        end

        def get_by_phone_number(stations = nil)
          if params[:phone_number_filter].present? and params[:phone_number].present?
            case params[:phone_number_filter]
              when 'contains'
                stations.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}%")
              when 'equals'
                stations.where('lower(phone_number) = :phone_number ', phone_number: params[:phone_number].downcase)
              when 'starts_with'
                stations.where('lower(phone_number) like :phone_number ', phone_number: "#{params[:phone_number].downcase}%")
              when 'ends_with'
                stations.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}")
            end
          else
            stations
          end
        end

        def get_by_email(stations = nil)
          if params[:email_filter].present? and params[:email].present?
            case params[:email_filter]
              when 'contains'
                stations.where('lower(email) like :email ', email: "%#{params[:email].downcase}%")
              when 'equals'
                stations.where('lower(email) = :email ', email: params[:email].downcase)
              when 'starts_with'
                stations.where('lower(email) like :email ', email: "#{params[:email].downcase}%")
              when 'ends_with'
                stations.where('lower(email) like :email ', email: "%#{params[:email].downcase}")
            end
          else
            stations
          end
        end

        def get_by_address(stations = nil)
          if params[:address_filter].present? and params[:address].present?
            case params[:address_filter]
              when 'contains'
                stations.where('lower(address) like :address ', address: "%#{params[:address].downcase}%")
              when 'equals'
                stations.where('lower(address) = :address ', address: params[:address].downcase)
              when 'starts_with'
                stations.where('lower(address) like :address ', address: "#{params[:address].downcase}%")
              when 'ends_with'
                stations.where('lower(address) like :address ', address: "%#{params[:address].downcase}")
            end
          else
            stations
          end
        end
      end
    end
  end
end