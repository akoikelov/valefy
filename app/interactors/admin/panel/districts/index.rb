module Admin
  module Panel
    module Districts
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_districts
        end

        def get_districts
          districts = District.all
          districts = get_by_name(districts)
          districts = get_by_description(districts)
          districts = get_by_country(districts)
          districts = get_by_city(districts)
          context.data = districts
        end

        def get_by_name(districts = nil)
          if params[:name_filter].present? and params[:name].present?
            case params[:name_filter]
              when 'contains'
                districts.where('lower(districts.name) like :name ', name: "%#{params[:name].downcase}%")
              when 'equals'
                districts.where('lower(districts.name) = :name ', name: params[:name].downcase)
              when 'starts_with'
                districts.where('lower(districts.name) like :name ', name: "#{params[:name].downcase}%")
              when 'ends_with'
                districts.where('lower(districts.name) like :name ', name: "%#{params[:name].downcase}")
            end
          else
            districts
          end
        end

        def get_by_description(districts = nil)
          if params[:description_filter].present? and params[:description].present?
            case params[:description_filter]
              when 'contains'
                districts.where('lower(districts.description) like :description ', description: "%#{params[:description].downcase}%")
              when 'equals'
                districts.where('lower(districts.description) = :description ', description: params[:description].downcase)
              when 'starts_with'
                districts.where('lower(districts.description) like :description ', description: "#{params[:description].downcase}%")
              when 'ends_with'
                districts.where('lower(districts.description) like :description ', description: "%#{params[:description].downcase}")
            end
          else
            districts
          end
        end

        def get_by_country(districts = nil)
          params[:country_id].present? ? districts.joins(city: :country).where(countries: { id: params[:country_id] }) : districts
        end

        def get_by_city(districts = nil)
          params[:city_id].present? ? districts.where(city_id: params[:city_id]) : districts
        end
      end
    end
  end
end