module Admin
  module Panel
    module ProFeedbacks
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_pro_feedbacks
        end

        def get_pro_feedbacks
          pro_feedbacks = MasterFeedback.all
          pro_feedbacks = get_by_order_item(pro_feedbacks)
          pro_feedbacks = get_by_client(pro_feedbacks)
          pro_feedbacks = get_by_pro(pro_feedbacks)
          pro_feedbacks = get_by_message(pro_feedbacks)
          pro_feedbacks = get_by_rating(pro_feedbacks)
          context.data = pro_feedbacks.order(id: :desc).page(params[:page])
        end

        def get_by_order_item(pro_feedbacks = nil)
          params[:order_item_id].present? ? pro_feedbacks.where(order_item_id: params[:order_item_id]) : pro_feedbacks
        end

        def get_by_client(pro_feedbacks = nil)
          params[:client_id].present? ? pro_feedbacks.joins(order_item: :client).where(users: { id: params[:client_id] }) : pro_feedbacks
        end

        def get_by_pro(pro_feedbacks = nil)
          params[:serviceman_id].present? ? pro_feedbacks.joins(order_item: :master).where(users: { id: params[:serviceman_id] }) : pro_feedbacks
        end

        def get_by_message(pro_feedbacks = nil)
          if params[:message_filter].present? and params[:message].present?
            case params[:message_filter]
              when 'contains'
                pro_feedbacks.where('lower(master_feedbacks.message) like :message ', message: "%#{params[:message].downcase}%")
              when 'equals'
                pro_feedbacks.where('lower(master_feedbacks.message) = :message ', message: params[:message].downcase)
              when 'starts_with'
                pro_feedbacks.where('lower(master_feedbacks.message) like :message ', message: "#{params[:message].downcase}%")
              when 'ends_with'
                pro_feedbacks.where('lower(master_feedbacks.message) like :message ', message: "%#{params[:message].downcase}")
            end
          else
            pro_feedbacks
          end
        end

        def get_by_rating(pro_feedbacks = nil)
          if params[:rating_filter].present? and params[:rating].present?
            case params[:rating_filter]
              when 'equals'
                pro_feedbacks.where('master_feedbacks.rating = :rating', rating: params[:rating])
              when 'greater_than'
                pro_feedbacks.where('master_feedbacks.rating > :rating', rating: params[:rating])
              when 'less_than'
                pro_feedbacks.where('master_feedbacks.rating < :rating', rating: params[:rating])
            end
          else
            pro_feedbacks
          end
        end
      end
    end
  end
end