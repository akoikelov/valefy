module Admin
  module Panel
    module JoinPros
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_join_pros
        end

        def get_join_pros
          join_pros = DraftMaster.all
          join_pros = get_by_name(join_pros)
          join_pros = get_by_email(join_pros)
          context.data = get_by_phone_number(join_pros)
        end

        def get_by_name(join_pros = nil)
          if params[:name_filter].present? and params[:name].present?
            case params[:name_filter]
              when 'contains'
                join_pros.where('lower(name) like :name ', name: "%#{params[:name].downcase}%")
              when 'equals'
                join_pros.where('lower(name) = :name ', name: params[:name].downcase)
              when 'starts_with'
                join_pros.where('lower(name) like :name ', name: "#{params[:name].downcase}%")
              when 'ends_with'
                join_pros.where('lower(name) like :name ', name: "%#{params[:name].downcase}")
            end
          else
            join_pros
          end
        end

        def get_by_email(join_pros = nil)
          if params[:email_filter].present? and params[:email].present?
            case params[:email_filter]
              when 'contains'
                join_pros.where('lower(email) like :email ', email: "%#{params[:email].downcase}%")
              when 'equals'
                join_pros.where('lower(email) = :email ', email: params[:email].downcase)
              when 'starts_with'
                join_pros.where('lower(email) like :email ', email: "#{params[:email].downcase}%")
              when 'ends_with'
                join_pros.where('lower(email) like :email ', email: "%#{params[:email].downcase}")
            end
          else
            join_pros
          end
        end

        def get_by_phone_number(join_pros = nil)
          if params[:phone_number_filter].present? and params[:phone_number].present?
            case params[:phone_number_filter]
              when 'contains'
                join_pros.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}%")
              when 'equals'
                join_pros.where('lower(phone_number) = :phone_number ', phone_number: params[:phone_number].downcase)
              when 'starts_with'
                join_pros.where('lower(phone_number) like :phone_number ', phone_number: "#{params[:phone_number].downcase}%")
              when 'ends_with'
                join_pros.where('lower(phone_number) like :phone_number ', phone_number: "%#{params[:phone_number].downcase}")
            end
          else
            join_pros
          end
        end
      end
    end
  end
end