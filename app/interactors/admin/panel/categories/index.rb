module Admin
  module Panel
    module Categories
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_categories
        end

        def get_categories
          categories = Category.all
          categories = get_by_title(categories)
          categories = get_by_title_ru(categories)
          categories = get_by_duration(categories)
          categories = get_by_corp(categories)
          categories = get_by_visible(categories)
          categories = get_by_subcategory(categories)
          context.data = categories.order(:position)
        end

        def get_by_title(categories = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                categories.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                categories.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                categories.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                categories.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            categories
          end
        end

        def get_by_title_ru(categories = nil)
          if params[:title_ru_filter].present? and params[:title_ru].present?
            case params[:title_ru_filter]
              when 'contains'
                categories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}%")
              when 'equals'
                categories.where('lower(title_ru) = :title_ru ', title_ru: params[:title_ru].downcase)
              when 'starts_with'
                categories.where('lower(title_ru) like :title_ru ', title_ru: "#{params[:title_ru].downcase}%")
              when 'ends_with'
                categories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}")
            end
          else
            categories
          end
        end

        def get_by_duration(categories = nil)
          if params[:duration_filter].present? and params[:duration].present?
            case params[:duration_filter]
              when 'equals'
                categories.where('duration = :duration', duration: params[:duration])
              when 'greater_than'
                categories.where('duration > :duration', duration: params[:duration])
              when 'less_than'
                categories.where('duration < :duration', duration: params[:duration])
            end
          else
            categories
          end
        end

        def get_by_corp(categories = nil)
          params[:corp].present? ? categories.where(corp: params[:corp]) : categories
        end

        def get_by_visible(categories = nil)
          params[:visible].present? ? categories.where(visible: params[:visible]) : categories
        end

        def get_by_subcategory(categories = nil)
          params[:subcategory_id].present? ? categories.joins(:subcategories).where(subcategories: { id: params[:subcategory_id].to_i }) : categories
        end
      end
    end
  end
end