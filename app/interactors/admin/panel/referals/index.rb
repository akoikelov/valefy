module Admin
  module Panel
    module Referals
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_referals
        end

        def get_referals
          referals = ReferalShare.all
          referals = get_by_promo_code(referals)
          referals = get_by_client_id(referals)
          referals = get_by_recieved_client_id(referals)
          referals = get_by_expiration_date(referals)
          context.data = referals.page(params[:page])
        end

        def get_by_promo_code(referals = nil)
          params[:promo_code_id].present? ? referals.where(promo_code_id: params[:promo_code_id].to_i) : referals
        end

        def get_by_client_id(referals = nil)
          params[:client_id].present? ? referals.where(client_id: params[:client_id].to_i) : referals
        end

        def get_by_recieved_client_id(referals = nil)
          params[:recieved_promocode_client_id].present? ? referals.where(shared_client_id: params[:recieved_promocode_client_id].to_i) : referals
        end

        def get_by_expiration_date(referals = nil)
          (params[:date_from].present? and params[:date_to].present?) ? referals.where(created_at: DateTime.parse(params[:date_from])..DateTime.parse(params[:date_to])) : referals
        end
      end
    end
  end
end