module Admin
  module Panel
    module AdminUsers
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_admin_users
        end

        def get_admin_users
          admin_users = AdminUser.all
          admin_users = get_by_email(admin_users)
          admin_users = get_by_role(admin_users)
          context.data = admin_users.order(id: :desc)
        end

        def get_by_email(admin_users = nil)
          if params[:email_filter].present? and params[:email].present?
            case params[:email_filter]
              when 'contains'
                admin_users.where('lower(email) like :email ', email: "%#{params[:email].downcase}%")
              when 'equals'
                admin_users.where('lower(email) = :email ', email: params[:email].downcase)
              when 'starts_with'
                admin_users.where('lower(email) like :email ', email: "#{params[:email].downcase}%")
              when 'ends_with'
                admin_users.where('lower(email) like :email ', email: "%#{params[:email].downcase}")
            end
          else
            admin_users
          end
        end

        def get_by_role(admin_users = nil)
          params[:role].present? ? admin_users.where(role: params[:role]) : admin_users
        end
      end
    end
  end
end