module Admin
  module Panel
    module Teams
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_teams
        end

        def get_teams
          teams = Team.all
          teams = get_by_title(teams)
          teams = get_by_pro(teams)
          context.data = teams.page(params[:page])
        end

        def get_by_title(teams = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                teams.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                teams.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                teams.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                teams.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            teams
          end
        end

        def get_by_pro(teams = nil)
          params[:pro_ids].present? ? teams.joins(:masters).where(users: { id: [pro_ids] }) : teams
        end

        def pro_ids
          params[:pro_id].present? ? params[:pro_id].split(',').map(&:to_i) : nil
        end
      end
    end
  end
end