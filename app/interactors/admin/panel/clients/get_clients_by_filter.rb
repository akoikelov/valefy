module Admin
  module Panel
    module Clients
      class GetClientsByFilter
        include Interactor
        include InteractorMetrics

        def call
          get_clients
        end

        def get_clients
          clients = []
          clients = find_by_name
          clients = find_by_corp(clients)
          clients = find_by_email(clients)
          clients = find_by_phone_number(clients)
          clients = find_by_category(clients)
          clients = find_by_company(clients)
          context.clients_count = clients.count if clients.present?
          context.data = clients.order(created_at: :desc).page(params[:page])
        end

        def find_by_name(clients = nil)
          if params[:client_name_filter].present? and params[:client_name].present?
            case params[:client_name_filter]
              when 'contains'
                clients_arr(clients).name_contains(params[:client_name])
              when 'equals'
                clients_arr(clients).name_equals(params[:client_name])
              when 'starts_with'
                clients_arr(clients).name_starts_with(params[:client_name])
              when 'ends_with'
                clients_arr(clients).name_ends_with(params[:client_name])
            end
          else
            clients.present? ? clients : all_clients
          end
        end

        def find_by_corp(clients = nil)
          params[:corp].present? ? all_clients.where(corp: params[:corp]) : clients
        end

        def find_by_email(clients)
          if params[:client_email_filter].present? and params[:client_email].present?
            case params[:client_email_filter]
              when 'contains'
                clients_arr(clients).email_contains(params[:client_email])
              when 'equals'
                clients_arr(clients).email_equals(params[:client_email])
              when 'starts_with'
                clients_arr(clients).email_starts_with(params[:client_email])
              when 'ends_with'
                clients_arr(clients).email_ends_with(params[:client_email])
            end
          else
            clients.present? ? clients : all_clients
          end
        end

        def find_by_phone_number(clients)
          if params[:client_phone_number_filter].present? and params[:client_phone_number].present?
            params[:client_phone_number].slice!(0) if params[:client_phone_number][0].to_i == 0
            case params[:client_phone_number_filter]
              when 'contains'
                clients_arr(clients).phone_number_contains(params[:client_phone_number])
              when 'equals'
                clients_arr(clients).phone_number_equals(params[:client_phone_number])
              when 'starts_with'
                clients_arr(clients).phone_number_starts_with(params[:client_phone_number])
              when 'ends_with'
                clients_arr(clients).phone_number_ends_with(params[:client_phone_number])
            end
          else
            clients.present? ? clients : all_clients
          end
        end

        def find_by_category(clients)
          if params[:client_category].present?
            clients_arr(clients).where(category_id: params[:client_category].to_i)
          else
            clients.present? ? clients : all_clients
          end
        end

        def find_by_company(clients)
          if params[:client_company].present?
            return clients_arr(clients) if params[:client_company].eql?('all')
            clients_arr(clients).where(company_id: params[:client_company].to_i)
          else
            clients.present? ? clients : all_clients
          end
        end

        def clients_arr(clients)
          clients.present? ? clients : all_clients
        end

        def all_clients
          return User::Client.all if(params[:client_company].present? and params[:client_company].eql?('all') or params[:client_company].nil?)
          User::Client.by_company(current_admin_user.try(:company).try(:id))
        end
      end
    end
  end
end