module Admin
  module Panel
    module GiftCards
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_gift_cards
        end

        def get_gift_cards
          gift_cards = GiftCard.includes(:to_client, :from_client, :service)
          gift_cards = get_by_expired(gift_cards)
          gift_cards = get_by_to_email(gift_cards)
          gift_cards = get_by_amount(gift_cards)
          gift_cards = get_by_used(gift_cards)
          gift_cards = get_by_service(gift_cards)
          gift_cards = get_by_to_client(gift_cards)
          gift_cards = get_by_from_client(gift_cards)
          gift_cards = get_by_expiration_date(gift_cards)
          context.data = gift_cards.order(id: :desc)
        end

        def get_by_expired(gift_cards = nil)
          unless params[:is_expired].present?
            gift_cards.where('exp_date > :exp_date', exp_date: DateTime.now).order(id: :desc)
          else
            if params[:is_expired] == 'expired'
              gift_cards.where('exp_date < :exp_date', exp_date: DateTime.now).order(id: :desc)
            elsif params[:is_expired] == 'not_expired'
              gift_cards.where('exp_date > :exp_date', exp_date: DateTime.now).order(id: :desc)
            end
          end
        end

        def get_by_to_email(gift_cards = nil)
          if params[:to_email_filter].present? and params[:to_email].present?
            case params[:to_email]
              when 'contains'
                gift_cards.where('lower(to_email) like :to_email ', to_email: "%#{params[:to_email].downcase}%")
              when 'equals'
                gift_cards.where('lower(to_email) = :to_email ', to_email: params[:to_email].downcase)
              when 'starts_with'
                gift_cards.where('lower(to_email) like :to_email ', to_email: "#{params[:to_email].downcase}%")
              when 'ends_with'
                gift_cards.where('lower(to_email) like :to_email ', to_email: "%#{params[:to_email].downcase}")
            end
          else
            gift_cards
          end
        end

        def get_by_amount(gift_cards = nil)
          if params[:amount_filters].present? and params[:amount].present?
            case params[:amount_filters]
              when 'equals'
                gift_cards.where('amount = :amount', amount: params[:amount])
              when 'greater_than'
                gift_cards.where('amount > :amount', amount: params[:amount])
              when 'less_than'
                gift_cards.where('amount < :amount', amount: params[:amount])
            end
          else
            gift_cards
          end
        end

        def get_by_used(gift_cards = nil)
          params[:used].present? ? gift_cards.where(used: params[:used]) : gift_cards
        end

        def get_by_service(gift_cards = nil)
          params[:service_id].present? ? gift_cards.where(service_id: params[:service_id].to_i) : gift_cards
          end

        def get_by_to_client(gift_cards = nil)
          params[:to_client_id].present? ? gift_cards.where(to_client_id: params[:to_client_id].to_i) : gift_cards
          end

        def get_by_from_client(gift_cards = nil)
          params[:from_client_id].present? ? gift_cards.where(from_client_id: params[:from_client_id].to_i) : gift_cards
        end

        def get_by_expiration_date(gift_cards = nil)
          (params[:expiration_date_from].present? and params[:expiration_date_to].present?) ? gift_cards.where(exp_date: DateTime.parse(params[:expiration_date_from])..DateTime.parse(params[:expiration_date_to])) : gift_cards
        end
      end
    end
  end
end