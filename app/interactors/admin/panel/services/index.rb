module Admin
  module Panel
    module Services
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_services
        end

        def get_services
          services = Service.all
          services = get_by_title(services)
          services = get_by_short_title(services)
          services = get_by_title_ru(services)
          services = get_by_description(services)
          services = get_by_duration(services)
          services = get_by_price(services)
          services = get_by_subcategory(services)
          context.data = services.order(:position).page(params[:page])
        end

        def get_by_title(services = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                services.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                services.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                services.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                services.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            services
          end
          end

        def get_by_short_title(subcategories = nil)
          if params[:short_title_filter].present? and params[:short_title].present?
            case params[:short_title_filter]
              when 'contains'
                subcategories.where('lower(short_title) like :short_title ', short_title: "%#{params[:short_title].downcase}%")
              when 'equals'
                subcategories.where('lower(short_title) = :short_title ', short_title: params[:short_title].downcase)
              when 'starts_with'
                subcategories.where('lower(short_title) like :short_title ', short_title: "#{params[:short_title].downcase}%")
              when 'ends_with'
                subcategories.where('lower(short_title) like :short_title ', short_title: "%#{params[:short_title].downcase}")
            end
          else
            subcategories
          end
        end

        def get_by_title_ru(subcategories = nil)
          if params[:title_ru_filter].present? and params[:title_ru].present?
            case params[:title_ru_filter]
              when 'contains'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}%")
              when 'equals'
                subcategories.where('lower(title_ru) = :title_ru ', title_ru: params[:title_ru].downcase)
              when 'starts_with'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "#{params[:title_ru].downcase}%")
              when 'ends_with'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}")
            end
          else
            subcategories
          end
        end

        def get_by_description(services = nil)
          if params[:description_filter].present? and params[:description].present?
            case params[:description_filter]
              when 'contains'
                services.where('lower(description) like :description ', description: "%#{params[:description].downcase}%")
              when 'equals'
                services.where('lower(description) = :description ', description: params[:description].downcase)
              when 'starts_with'
                services.where('lower(description) like :description ', description: "#{params[:description].downcase}%")
              when 'ends_with'
                services.where('lower(description) like :description ', description: "%#{params[:description].downcase}")
            end
          else
            services
          end
        end

        def get_by_duration(services = nil)
          if params[:duration_filter].present? and params[:duration].present?
            case params[:duration_filter]
              when 'equals'
                services.where('duration = :duration', duration: params[:duration])
              when 'greater_than'
                services.where('duration > :duration', duration: params[:duration])
              when 'less_than'
                services.where('duration < :duration', duration: params[:duration])
            end
          else
            services
          end
        end

        def get_by_price(services = nil)
          if params[:price_filter].present? and params[:price].present?
            case params[:price_filter]
              when 'equals'
                services.where('price = :price', price: params[:price])
              when 'greater_than'
                services.where('price > :price', price: params[:price])
              when 'less_than'
                services.where('price < :price', price: params[:price])
            end
          else
            services
          end
        end

        def get_by_subcategory(services = nil)
          params[:subcategory_id].present? ? services.where(subcategory_id: params[:subcategory_id].to_i) : services
        end
      end
    end
  end
end