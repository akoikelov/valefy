module Admin
  module Panel
    module Devices
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_devices
        end

        def get_devices
          devices = Device.all
          devices = get_by_app(devices)
          devices = get_by_id_registration_firebase(devices)
          devices = get_by_token_client_id(devices)
          devices = get_by_platform(devices)
          devices = get_by_client(devices)
          devices = get_by_pro(devices)
          context.data = devices.order(id: :desc).page(params[:page])
        end

        def get_by_app(devices = nil)
          params[:app].present? ? devices.where(app: params[:app]) : devices
        end

        def get_by_id_registration_firebase(devices = nil)
          if params[:id_registration_firebase_filter].present? and params[:id_registration_firebase].present?
            case params[:id_registration_firebase_filter]
              when 'contains'
                devices.where('lower(id_registration_firebase) like :id_registration_firebase ', id_registration_firebase: "%#{params[:id_registration_firebase].downcase}%")
              when 'equals'
                devices.where('lower(id_registration_firebase) = :id_registration_firebase ', id_registration_firebase: params[:id_registration_firebase].downcase)
              when 'starts_with'
                devices.where('lower(id_registration_firebase) like :id_registration_firebase ', id_registration_firebase: "#{params[:id_registration_firebase].downcase}%")
              when 'ends_with'
                devices.where('lower(id_registration_firebase) like :id_registration_firebase ', id_registration_firebase: "%#{params[:id_registration_firebase].downcase}")
            end
          else
            devices
          end
        end

        def get_by_token_client_id(devices = nil)
          if params[:token_client_id_filter].present? and params[:token_client_id].present?
            case params[:token_client_id_filter]
            when 'contains'
              devices.where('lower(token_client_id) like :token_client_id ', token_client_id: "%#{params[:token_client_id].downcase}%")
            when 'equals'
              devices.where('lower(token_client_id) = :token_client_id ', token_client_id: params[:token_client_id].downcase)
            when 'starts_with'
              devices.where('lower(token_client_id) like :token_client_id ', token_client_id: "#{params[:token_client_id].downcase}%")
            when 'ends_with'
              devices.where('lower(token_client_id) like :token_client_id ', token_client_id: "%#{params[:token_client_id].downcase}")
            end
          else
            devices
          end
        end

        def get_by_platform(devices = nil)
          params[:platform].present? ? devices.where(platform: params[:platform]) : devices
        end

        def get_by_client(devices = nil)
          params[:client_id].present? ? devices.where(client_id: params[:client_id]) : devices
        end

        def get_by_pro(devices = nil)
          params[:master_id].present? ? devices.where(master_id: params[:master_id]) : devices
        end
      end
    end
  end
end