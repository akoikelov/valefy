module Admin
  module Panel
    module Subcategories
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_subcategories
        end

        def get_subcategories
          subcategories = Subcategory.all
          subcategories = get_by_title(subcategories)
          subcategories = get_by_title_ru(subcategories)
          subcategories = get_by_code(subcategories)
          subcategories = get_by_visible(subcategories)
          subcategories = get_by_category(subcategories)
          context.data = subcategories.order(:position)
        end

        def get_by_title(subcategories = nil)
          if params[:title_filter].present? and params[:title].present?
            case params[:title_filter]
              when 'contains'
                subcategories.where('lower(title) like :title ', title: "%#{params[:title].downcase}%")
              when 'equals'
                subcategories.where('lower(title) = :title ', title: params[:title].downcase)
              when 'starts_with'
                subcategories.where('lower(title) like :title ', title: "#{params[:title].downcase}%")
              when 'ends_with'
                subcategories.where('lower(title) like :title ', title: "%#{params[:title].downcase}")
            end
          else
            subcategories
          end
        end

        def get_by_title_ru(subcategories = nil)
          if params[:title_ru_filter].present? and params[:title_ru].present?
            case params[:title_ru_filter]
              when 'contains'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}%")
              when 'equals'
                subcategories.where('lower(title_ru) = :title_ru ', title_ru: params[:title_ru].downcase)
              when 'starts_with'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "#{params[:title_ru].downcase}%")
              when 'ends_with'
                subcategories.where('lower(title_ru) like :title_ru ', title_ru: "%#{params[:title_ru].downcase}")
            end
          else
            subcategories
          end
        end

        def get_by_code(subcategories = nil)
          params[:code].present? ? subcategories.where(code: params[:code]) : subcategories
        end

        def get_by_visible(subcategories = nil)
          params[:visible].present? ? subcategories.where(visible: params[:visible]) : subcategories
        end

        def get_by_category(subcategories = nil)
          params[:category_id].present? ? subcategories.where(category_id: params[:category_id].to_i) : subcategories
        end
      end
    end
  end
end