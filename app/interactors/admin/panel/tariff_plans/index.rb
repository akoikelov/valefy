module Admin
  module Panel
    module TariffPlans
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_tariff_plans
        end

        def get_tariff_plans
          tariff_plans = TariffPlan.all
          tariff_plans = get_by_plan(tariff_plans)
          context.data = get_by_plan_value(tariff_plans)
        end

        def get_by_plan(tariff_plans = nil)
          if params[:plan_filter].present? and params[:plan].present?
            case params[:plan_filter]
              when 'contains'
                tariff_plans.where('lower(plan) like :plan ', plan: "%#{params[:plan].downcase}%")
              when 'equals'
                tariff_plans.where('lower(plan) = :plan ', plan: params[:plan].downcase)
              when 'starts_with'
                tariff_plans.where('lower(plan) like :plan ', plan: "#{params[:plan].downcase}%")
              when 'ends_with'
                tariff_plans.where('lower(plan) like :plan ', plan: "%#{params[:plan].downcase}")
            end
          else
            tariff_plans
          end
        end

        def get_by_plan_value(tariff_plans = nil)
          if params[:plan_value_filter].present? and params[:plan_value].present?
            case params[:plan_value_filter]
              when 'equals'
                tariff_plans.where('plan_value = :plan_value', plan_value: params[:plan_value])
              when 'greater_than'
                tariff_plans.where('plan_value > :plan_value', plan_value: params[:plan_value])
              when 'less_than'
                tariff_plans.where('plan_value < :plan_value', plan_value: params[:plan_value])
            end
          else
            tariff_plans
          end
        end
      end
    end
  end
end