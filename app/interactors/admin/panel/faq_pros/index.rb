module Admin
  module Panel
    module FaqPros
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_faq_pros
        end

        def get_faq_pros
          faq_pros = Faq.master
          faq_pros = get_by_question(faq_pros)
          context.data = get_by_answer(faq_pros)
        end

        def get_by_question(faq_pros = nil)
          if params[:question_filter].present? and params[:question].present?
            case params[:question_filter]
              when 'contains'
                faq_pros.where('lower(question) like :question ', question: "%#{params[:question].downcase}%")
              when 'equals'
                faq_pros.where('lower(question) = :question ', question: params[:question].downcase)
              when 'starts_with'
                faq_pros.where('lower(question) like :question ', question: "#{params[:question].downcase}%")
              when 'ends_with'
                faq_pros.where('lower(question) like :question ', question: "%#{params[:question].downcase}")
            end
          else
            faq_pros
          end
        end

        def get_by_answer(faq_pros = nil)
          if params[:answer_filter].present? and params[:answer].present?
            case params[:answer_filter]
              when 'contains'
                faq_pros.where('lower(answer) like :answer ', answer: "%#{params[:answer].downcase}%")
              when 'equals'
                faq_pros.where('lower(answer) = :answer ', answer: params[:answer].downcase)
              when 'starts_with'
                faq_pros.where('lower(answer) like :answer ', answer: "#{params[:answer].downcase}%")
              when 'ends_with'
                faq_pros.where('lower(answer) like :answer ', answer: "%#{params[:answer].downcase}")
            end
          else
            faq_pros
          end
        end
      end
    end
  end
end