module Admin
  module Panel
    module PromoCodes
      class Index
        include Interactor
        include InteractorMetrics

        def call
          get_promo_codes
        end

        def get_promo_codes
          promo_codes = PromoCode.all.order(id: :desc)
          promo_codes = get_by_promo_code_id(promo_codes)
          promo_codes = get_by_discount(promo_codes)
          promo_codes = get_by_used(promo_codes)
          promo_codes = get_by_client(promo_codes)
          promo_codes = get_by_expiration_date(promo_codes)
          context.data = promo_codes.page(params[:page])
        end

        def get_by_promo_code_id(promo_codes = nil)
          if params[:promo_code_id_filter].present? and params[:promo_code_id].present?
            case params[:promo_code_id_filter]
              when 'contains'
                promo_codes.where('lower(promo_code_id) like :promo_code_id ', promo_code_id: "%#{params[:promo_code_id].downcase}%")
              when 'equals'
                promo_codes.where('lower(promo_code_id) = :promo_code_id ', promo_code_id: params[:promo_code_id].downcase)
              when 'starts_with'
                promo_codes.where('lower(promo_code_id) like :promo_code_id ', promo_code_id: "#{params[:promo_code_id].downcase}%")
              when 'ends_with'
                promo_codes.where('lower(promo_code_id) like :promo_code_id ', promo_code_id: "%#{params[:promo_code_id].downcase}")
            end
          else
            promo_codes
          end
        end

        def get_by_discount(promo_codes = nil)
          if params[:discount_filters].present? and params[:discount].present?
            case params[:discount_filters]
              when 'equals'
                promo_codes.where('discount = :discount', discount: params[:discount])
              when 'greater_than'
                promo_codes.where('discount > :discount', discount: params[:discount])
              when 'less_than'
                promo_codes.where('discount < :discount', discount: params[:discount])
            end
          else
            promo_codes
          end
        end

        def get_by_used(promo_codes = nil)
          params[:used].present? ? promo_codes.where(used: params[:used]) : promo_codes
        end

        def get_by_client(promo_codes = nil)
          params[:client_id].present? ? promo_codes.where(client_id: params[:client_id]) : promo_codes
        end

        def get_by_expiration_date(promo_codes = nil)
          (params[:expiration_date_from].present? and params[:expiration_date_to].present?) ? promo_codes.where(exp_date: DateTime.parse(params[:expiration_date_from])..DateTime.parse(params[:expiration_date_to])) : promo_codes
        end
      end
    end
  end
end