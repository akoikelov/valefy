module Support
  class ChangePhoneNumber
    include Interactor

    def call
      context.phone = kg_full? ? phone : change_format_phone_number
    end

    private

    def phone
      context.phone_number
    end

    def change_format_phone_number
      _phone = phone           if ie_full? # +353 *** ** ** **
      _phone = '+'    << phone if ie_without_plus? # 353 *** ** **
      _phone = "+353" << phone if ie_without_code? # *** ** **
      _phone = "+353" << phone.sub!('0','') if ie_without_code_with_zero? # 0*** ** ** **

      _phone || ""
    end

    def ie_error
      context.fail!(error: 'Not correct format. Format should be like this +353*********')
    end

    def kg_error
      context.fail!(error: 'Not correct format. Format should be like this +996*********')
    end

    def kg_full?
      /^\+996\d{9}$/.match(phone).present?
    end

    def ie_full?
      /^\+353\d{9}$/.match(phone).present?
    end

    def ie_without_plus?
      /^353\d{9}$/.match(phone).present?
    end

    def ie_without_code?
      /^\d{9}$/.match(phone).present?
    end

    def ie_without_code_with_zero?
      /^0\d{9}$/.match(phone).present?
    end
  end
end
