class RemoveImagesWorker
  include Sidekiq::Worker

  sidekiq_retries_exhausted do |msg, e|
    Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
    Raven.capture_exception(e)
  end

  def perform(*args)
    completed_orders = OrderItem.history

    completed_orders.each do |order|
      images = order.images

      images.each do |image|
        _string = /link\/\d+/.match(image).to_s
        id = _string.split(/link\//).last

        if id.present?
          _image = Image.find(id)
          _image.remove_link!
          _image.save!
        end
      end

      order.before_work_images.each do |image|
        _string = /link\/\d+/.match(image).to_s
        id = _string.split(/link\//).last

        if id.present?
          _image = Image.find(id)
          _image.remove_link!
          _image.save!
        end
      end

      order.after_work_images.each do |image|
        _string = /link\/\d+/.match(image).to_s
        id = _string.split(/link\//).last

        if id.present?
          _image = Image.find(id)
          _image.remove_link!
          _image.save!
        end
      end
    end
  end
end
