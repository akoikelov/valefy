module Api
  module V1
      class UpdateOrderTimeEmailWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          order = OrderItem.find(args[0]['order_id'])
          old_time = DateTime.parse(args[0]['old_time'])

          UserMailer.order_time_email(order, old_time).deliver_now
        end
      end
    end
end
