module Api
  module V1
    class SendEmailWorker
      include Sidekiq::Worker

      sidekiq_retries_exhausted do |msg, e|
        Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
        Raven.capture_exception(e)
      end

      def perform(*args)
        client = ::User::Client.find(args[0]['client_id'])
        method = args[0]['email_method']
        order = OrderItem.find(args[0]['order_id']) if args[0]['order_id'].present?

        if args[0]['order_id'].present?
          send_email(method, order)
        else
          send_email(method, client)
        end
      end

      private

      def send_email(method, object)
        UserMailer.send(method, object).deliver_now
      end
    end
  end
end
