module Api
  module V1
    class CreateOrdersForCorpClients
      include Sidekiq::Worker

      sidekiq_retries_exhausted do |msg, e|
        Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
        Raven.capture_exception(e)
      end

      def perform(*args)
        # User::Client.bussiness.each do |client|
        #   if client.contract.present?
        #     create_order_by_contract(client)
        #   end
        # end
      end

      private

      # def create_order_by_contract
      #   Api::V2::CreateOrderItem.call(params)
      #   Api::V2::OrderItems::Searching.call(params)
      # end

      # def params
      #   {
      #     :address,
      #     :description,
      #     :order_time,
      #     :lon,
      #     :lat,
      #     :time_zone,
      #     :pets,
      #     :platform,
      #     :count_vehicles
      #     :parking,
      #     :client_id,
      #     :service_ids => []
      #   }
      # end
    end
  end
end