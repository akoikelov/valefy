module Api
  module V1
      class SendInvoiceWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          order = OrderItem.find(args[0]['order_id'])

          UserMailer.client_invoice_email(order).deliver_now
        end
      end
    end
end
