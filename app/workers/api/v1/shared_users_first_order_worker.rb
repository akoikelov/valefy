module Api
  module V1
      class SharedUsersFirstOrderWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          shared_client = User::Client.find(args[0]['shared_client_id'])
          client = User::Client.find(args[0]['client_id'])
          promod_code = PromoCode.find(args[0]['promo_code_id'])
          UserMailer.shared_user_first_order(shared_client, client, promod_code).deliver_now
        end
      end
    end
end
