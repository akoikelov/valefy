module Api
  module V1
    class CreatePromocodeAndSendNotificationsWorker
      include Sidekiq::Worker
      include WorkerHelper

      sidekiq_retries_exhausted do |msg, e|
        Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
        Raven.capture_exception(e)
      end

      def perform(*args)
        client_exist = args[0]['client_exist']

        if client_exist
          gift_id   = args[0]['gift_id']
          to_client = args[0]['client_id']

          gift      = GiftCard.find(gift_id)

          if to_client.devices.present?
            device = to_client.devices.last

            if device.platform.eql?('IOS')
              send_push_notify('body_ios_gift', to_client)
            elsif device.platform.eql?('android')
              send_push_notify('body_android_gift', to_client)
            end
          end

          UserMailer.created_gift_to_exist_client_email(gift).deliver_now
          UserMailer.owner_gift_card_email(gift).deliver_now if Setting.try(:first).try(:send_email_owner_gift_card)
        else
          gift_id   = args[0]['gift_id']
          gift      = GiftCard.find(gift_id)

          UserMailer.created_gift_to_not_exist_client_email(gift).deliver_now
          UserMailer.owner_gift_card_email(gift).deliver_now if Setting.try(:first).try(:send_email_owner_gift_card)
        end
      end
    end
  end
end
