module Api
  module Masters
    module V1
      class CreateGoogleEventWorker
        include ApplicationHelper
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          current_master = ::User::ServiceMan.find(args[0]['master_id'])
          order          = OrderItem.find(args[0]['order_id'])
          time_zone      = args[0]['time_zone']

          if current_master.google_refresh_token.present?
            google = GoogleCalendar.new nil, current_master, event_body(order, time_zone)
            client = google.authorization_client
            google.create_event client
          end
        end

        private

        def event_body(order, time_zone)
          {
            summary: 'Order',
            location: order.address,
            description: '',
            start: {
              date_time: order.order_time.strftime('%Y-%m-%dT%H:%M:%S'),
              time_zone: fetch_key_by(time_zone),
            },
            end: {
              date_time: (order.order_time + 2.hours).strftime('%Y-%m-%dT%H:%M:%S'),
              time_zone: fetch_key_by(time_zone),
            },
            recurrence: [
              'RRULE:FREQ=DAILY;COUNT=2'
            ],
            # reminders: {
            #   use_default: false,
            #   overrides: [
            #     {'method' => 'email', 'minutes': 24 * 60},
            #     {'method' => 'popup', 'minutes': 10},
            #   ],
            # }
          }
        end
      end
    end
  end
end
