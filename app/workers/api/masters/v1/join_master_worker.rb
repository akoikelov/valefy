module Api
  module Masters
    module V1
      class JoinMasterWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          cv_path          = args[0]['cv']
          id_document_path = args[0]['id_document']

          cv          = CreateImage.call(params: cv_params(cv_path))
          id_document = CreateImage.call(params: id_document_params(id_document_path))

          master = DraftMaster.find(args[0]['draft_master_id'])

          master.cv << cv.link if cv.success?
          master.id_document  = id_document.link if id_document.success?
          master.save

          UserMailer.join_master_email(master).deliver_now
        end

        private

        def cv_file(path)
          File.new(path) if path.present?
        end

        def id_document_file(path)
          File.new(path) if path.present?
        end

        def cv_params(path)
          { image: cv_file(path) }
        end

        def id_document_params(path)
          { image: id_document_file(path) }
        end
      end
    end
  end
end
