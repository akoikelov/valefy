module Api
  module Masters
    module V1
      class PushNotificationsWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          order = OrderItem.find args[0]['order_id']
          text  = args[0]['title']

          if order.master.devices.present?
            device = order.master.devices.last

            if device.platform.eql?('IOS')
              push_send_ios(order, text)
            elsif device.platform.eql?('android')
              push_send_android(order, text)
            end
          end
        end

        private

        def push_send_ios(order, text)
          note = Notifications.new(body_ios(order, text))

          note.push
        end

        def push_send_android(order, text)
          note = Notifications.new(body(order, text))

          note.push
        end

        def body order, text
          {
            data: {
              title: I18n.t('sortd'),
              detail: text,
              order_id: order.id,
              order_status: order.status
            },
            priority: 'high',
            # to: Device.where(master_id: order.master.id).last.id_registration_firebase
            registration_ids: tokens(order.master, 'android')
          }
        end

        def body_ios order, text
          {
            notification: {
              body: text,
              title: I18n.t('sortd')
            },
            data: {
              order_id: order.id,
              order_status: order.status
            },
            priority: 'high',
            # Device.where(master_id: order.master.id).last.id_registration_firebase # TODO refactor this
            registration_ids: tokens(order.master, 'IOS')
          }
        end

        def tokens user, platform
          user.devices.where(platform: platform).map(& :id_registration_firebase).uniq
        end
      end
    end
  end
end
