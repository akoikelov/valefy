module Api
  module Masters
    module V1
      class NewOrderNotifyWorker
        include Sidekiq::Worker
        include ApplicationHelper

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          order = OrderItem.find args[0]['order_id']
          text  = args[0]['title']
          platform = args[0]['platform']
          zone = args[0]['time_zone']

          # TODO это надо рефакторить. Так как здесь андроидеры получат заказ быстрее, чем айосеры

          push_send_ios(order, text, zone) if platform.eql?('IOS')
          push_send_android(order, text, zone) if platform.eql?('android')
        end

        private

        def push_send_ios(order, text, zone)
          note = Notifications.new(body_ios(order, text, zone))

          note.push
        end

        def push_send_android(order, text, zone)
          note = Notifications.new(body(order, text, zone))

          note.push
        end

        def body order, text, zone
          {
            notification: {
              title: title_string(order),
              body: body_string(order, zone)
            },
            data: {
              order_id: order.id,
              order_status: order.status
            },
            priority: 'high',
            registration_ids: masters(order, 'android')
          }
        end

        def body_ios order, text, zone
          {
            notification: {
              body: body_string(order, zone),
              title: title_string(order)
            },
            data: {
              order_id: order.id,
              order_status: order.status
            },
            priority: 'high',
            registration_ids: masters(order, 'IOS')
          }
        end

        def title_string(order)
          if order.service_id.present?
            srv = Service.find(order.service_id)
            str = "New Customer - #{srv.subcategory.category.title}"
          elsif order.services.present?
            str = "New Customer - #{order.services.first.subcategory.category.title}"
          else
            str = "error"
          end

          str
        end

        def body_string(order, zone)
          if order.service_id.present?
            srv = Service.find(order.service_id)
            str = "Service: #{srv.title}\nWhere: #{order.address}\nWhen: #{order_time_by_zone(order, zone)}\nDuration: #{srv.duration} hours\nPrice: €#{srv.price.to_i}"
          elsif order.services.present?
            str = "Services:\n#{services_list_string(order)}\nWhere: #{order.address}\nWhen: #{order_time_by_zone(order, zone)}\nDuration: #{duration(order)} hours\nPrice: €#{services_price_string(order)}"
          else
            str = ""
          end

          str
        end

        def services_list_string(order)
          str = ""
          order.services.each_with_index{ |e, i| str << "#{i+1} - #{e.title}\n" }

          str
        end

        def services_price_string(order)
          order.services.map(& :price).sum.to_i
        end

        def duration(order)
          order.services.map(& :duration).sum
        end

        def masters(order, platform)
          tokens = ::User::ServiceMan.masters_token(order, platform)

          tokens
        end
      end
    end
  end
end
