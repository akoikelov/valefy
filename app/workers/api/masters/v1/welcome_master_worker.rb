module Api
  module Masters
    module V1
      class WelcomeMasterWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          # order = DraftMaster.find args[0]['draft_master_id']
        end
      end
    end
  end
end
