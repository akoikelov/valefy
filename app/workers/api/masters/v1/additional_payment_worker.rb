module Api
  module Masters
    module V1
      class AdditionalPaymentWorker
        include Sidekiq::Worker

        sidekiq_retries_exhausted do |msg, e|
          Sidekiq.logger.warn "Failed #{msg['class']} with #{msg['args']}: #{msg['error_message']}"
          Raven.capture_exception(e)
        end

        def perform(*args)
          order = OrderItem.find args[0]['order_id']

          if order.client.devices.last.platform.eql?('IOS')
            push_send_ios order
          else
            push_send_android order
          end
        end

        private

        def push_send_ios order
          note = Notifications.new(body_ios(order))

          note.push
        end

        def push_send_android order
          note = Notifications.new(body(order))

          note.push
        end

        def body order
          {
            data: {
              title: I18n.t('sortd'),
              detail: "#{order.master.name} requested additional € #{order.additional_payment.additional_price} payment",
              order_id: order.id,
              status: 'additional_payment',
              price: order.additional_payment.additional_price,
              description: order.additional_payment.description,
              name: order.master.name,
              payment_id:  order.additional_payment.id
            },
            priority: 'high',
            # to: order.client.devices.last.id_registration_firebase
            registration_ids: tokens(order.client, 'android')
          }
        end

        def body_ios order
          {
            notification: {
              body: "#{order.master.name} requested additional € #{order.additional_payment.additional_price} payment",
              title: I18n.t('sortd')
            },
            data: {
              order_id: order.id,
              status: 'additional_payment',
              price: order.additional_payment.additional_price,
              description: order.additional_payment.description,
              name: order.master.name,
              payment_id:  order.additional_payment.id
            },
            priority: 'high',
            registration_ids: tokens(order.client, 'IOS')
          }
        end

        def tokens user, platform
          user.devices.where(platform: platform).map(& :id_registration_firebase).uniq
        end
      end
    end
  end
end
