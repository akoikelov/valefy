# Manage notifications to android and ios
class Notifications
  include ErrorsHelper

  def initialize(body)
    @body = body
  end

  def push
    post
  end

  private

  def post
    http = Net::HTTP.new(uri.host)
    request = Net::HTTP::Post.new(uri.request_uri, headers)
    request.body = @body.to_json
    response = http.request(request)

    response
  rescue => e
    capture_exception(e)
  end

  def uri
    URI 'https://fcm.googleapis.com/fcm/send'
  end

  def headers
    {'Authorization' => "#{ENV['FIREBASE_KEY']}", 'Content-Type' => 'application/json'}
  end
end
