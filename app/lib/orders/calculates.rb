class Orders::Calculates
  def initialize(order=nil)
    @order = order
  end

  attr_reader :order

  def total_price_if_station
    raise 'Object not implemented' unless order

    station_services_price + baby_seat_price
  end

  private

  def station_services_price
    amount = []
    order.services.each do |service|
      price = order.station.services.map{|e| service.id == e.service.id ? e.price : 0 }
      amount << (price.sum.to_i > 0 ? price.sum : service.price)
    end

    amount.sum.to_i
  end

  def baby_seat_price
    order.baby_seat * 5 # TODO move 5 in constants
  end
end