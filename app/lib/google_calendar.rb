class GoogleCalendar
  require 'google/apis/calendar_v3'
  require 'googleauth'

  APPLICATION_NAME = 'Google Calendar API Ruby Quickstart'.freeze
  SCOPE = Google::Apis::CalendarV3::AUTH_CALENDAR

  def initialize(token=nil, current_user, event_body)
    # TODO весь класс надо жестко почистить
    @refresh_token = token
    @current_user  = current_user
    @event_body    = event_body
  end

  def authorization_client
    if @current_user.google_refresh_token.present?
      _client = Signet::OAuth2::Client.new(
        client_id: credentials[:client_id],
        client_secret: credentials[:client_secret],
        token_credential_uri: 'https://www.googleapis.com/oauth2/v4/token',
        scope: SCOPE
      )

      _client.update!(Setting.first.authorization)
    else
      _client = Signet::OAuth2::Client.new(
        client_id: credentials[:client_id],
        client_secret: credentials[:client_secret],
        token_credential_uri: 'https://www.googleapis.com/oauth2/v4/token',
        scope: SCOPE,
        refresh_token: @refresh_token
      )

      @current_user.update(google_refresh_token: @refresh_token)
      Setting.first.update(authorization: _client.fetch_access_token!)
    end

    _client
  end

  def create_event client
    service = Google::Apis::CalendarV3::CalendarService.new
    event   = Google::Apis::CalendarV3::Event.new(@event_body)

    service.authorization = client
    service.insert_event('primary', event)

  rescue Google::Apis::AuthorizationError
    _client = Signet::OAuth2::Client.new(
      client_id: credentials[:client_id],
      client_secret: credentials[:client_secret],
      token_credential_uri: 'https://www.googleapis.com/oauth2/v4/token',
      scope: SCOPE,
      refresh_token: @current_user.google_refresh_token
    )
    
    Setting.first.update(authorization: _client.fetch_access_token!)
    client = _client

    retry
  end

  def retrieve_events client
    service = Google::Apis::CalendarV3::CalendarService.new
    service.authorization = client
    result = service.list_events('primary')

    result.items.present?

  rescue Google::Apis::AuthorizationError
    _client = Signet::OAuth2::Client.new(
        client_id: credentials[:client_id],
        client_secret: credentials[:client_secret],
        token_credential_uri: 'https://www.googleapis.com/oauth2/v4/token',
        scope: SCOPE,
        refresh_token: @refresh_token
      )

    @current_user.update(google_refresh_token: @refresh_token)
    Setting.first.update(authorization: _client.fetch_access_token!)
    client = _client

    retry
  end

  def credentials
    {
      "client_id": ENV['GOOGLE_CALENDAR_CLIENT_ID'],
      "client_secret": ENV['GOOGLE_CALENDAR_CLIENT_SECRET']
    }
  end
end