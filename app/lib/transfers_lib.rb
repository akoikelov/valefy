class TransfersLib
  def initialize(params)
    @params = {
      amount: params[:amount],
      currency: params[:currency],
      destination: params[:destination],
      order_id: params[:order_id],
      charge_id: params[:charge_id]
    }
  end

  def transfer
    Stripe::Transfer.create(
      :amount => @params[:amount],
      :currency => @params[:currency],
      :destination => @params[:destination],
      :transfer_group => "order-#{@params[:order_id]}",
      :source_transaction => @params[:charge_id]
    )
  end
end