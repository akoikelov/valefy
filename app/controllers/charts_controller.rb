class ChartsController < ApplicationController

  def percentage_orders_and_categories
    result = Charts::PercentageOrdersCategories.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  rescue => e
    Raven.capture_exception(e)
    Rails.logger.error("#{e.message}: #{e.backtrace}")

    render json: []
  end

  def average_price_category
    result = Charts::AveragePrice.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  rescue => e
    Raven.capture_exception(e)
    Rails.logger.error("#{e.message}: #{e.backtrace}")

    render json: []
  end

  def last_n_days
    result = Charts::LastNDays.call(params: params)
    if result.success?
      respond_to do |format|
        format.json { render json: result.data }
        format.js { render :last_n_days, locals: {data: result.data} }
      end
    else
      render json: []
    end
  end

  def dayly_rate_last_n_days
    master = User::ServiceMan.find(params[:master_id])

    if params[:range].present?
      if range.eql?('last_7_days')
        data = master.order_items.history.group_by_day(:created_at, last: 7).count
      elsif range.eql?('last_30_days')
        data = master.order_items.history.group_by_day(:created_at, last: 30).count
      elsif range.eql?('today')
        data = master.order_items.history.group_by_day(:created_at, last: 2).count
      end
    else
      data = master.order_items.history.group_by_day(:created_at, last: 7).count
    end

    data = JSON.parse(data.to_json)
    data.update(data){ |key, value| ((value/User::ServiceMan::DAYLY_RATE.to_f)*100).to_i }

    respond_to do |format|
      format.json { render json: data }
      format.js   { render :dayly_rate_last_n_days, locals: { data: data }}
    end
  end

  def created_order_by_platform
    result = Charts::CreatedOrderByPlatform.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def clients_by_type
    result = Charts::ClientsByType.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def orders_by_clients
    result = Charts::OrdersCountByClients.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def order_period_values
    result = Charts::OrderPeriodValues.call(params: params)
    if result.success?
      render json: result.data
    else
      render json:[]
    end
  end

  def orders_frequency
    result = Charts::OrdersFrequency.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def clients_retention
    result = Charts::ClientsRetention.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def small_large_cars_percent
    result = Charts::SmallLargeCars.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def profit_on_services
    result = Charts::ProfitOnServices.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def most_ordered_service
    result = Charts::MostOrderedService.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def pros_count
    result = Charts::Pros::ProsCount.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def canceled_orders
    result = Charts::Pros::CanceledOrdersCount.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def accepted_orders
    result = Charts::Pros::AcceptedOrdersCount.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def completed_orders
    result = Charts::Pros::CompletedOrdersCount.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def masters_orders_by_client_type
    result = Charts::Pros::OrdersCountByClientType.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def masters_orders_by_category
    result = Charts::Pros::OrdersByCategory.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def orders_by_district
    result = Charts::Districts::GetOrdersCount.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def master_average_time_orders
    result = Charts::Pros::AverageTimeOrders.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def clients_by_gender
    result = Charts::ClientsByGender.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def clients_by_age
    result = Charts::ClientsByAge.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def orders_by_company_platforms
    result = Charts::OrdersCountByPlatforms.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def cars_count_by_type
    result = Charts::CarsCountByType.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def pros_by_type
    result = Charts::Pros::ProsByType.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def turnover_by_platfroms
    result = Charts::TurnoverByPlatfroms.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  def clients_count_by_segment
    result = Charts::ClientsBySegment.call(params: params)
    if result.success?
      render json: result.data
    else
      render json: []
    end
  end

  private

  def average_value(cat, prices, completed_orders)
    prices.present? ? ["#{cat.title}", (prices.sum / completed_orders.count).round(2)] : ["#{cat.title}", 0]
  end

  def range
    params[:range].split.map { |e| e.downcase }.join('_')
  end

  def is_corp
    params[:is_corp]
  end
end
