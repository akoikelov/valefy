module Admin
  module Panel
    class AdminUsersController < ApplicationController
      layout 'panel'

      before_action :remove_password, only: [:update]

      def index
        result = AdminUsers::Index.call(params: params)
        @admin_users = result.data
      end

      def show
        @admin_user = AdminUser.find(params[:id])
      end

      def new
        @admin_user = AdminUser.new
      end

      def create
        @admin_user = AdminUser.new permit_params

        if @admin_user.save
          redirect_to admin_panel_admin_users_path
        else
          render :new
        end
      end

      def edit
        @admin_user = AdminUser.find(params[:id])
      end

      def update
        @admin_user = AdminUser.find(params[:id])

        if @admin_user.update_attributes permit_params
          redirect_to admin_panel_admin_user_path(@admin_user)
        else
          render :edit
        end
      end

      def destroy
        @admin_user = AdminUser.find(params[:id])
        if @admin_user.destroy
          flash[:success] = 'AdminUser successfully deleted!'
        else
          flash[:error] = @admin_user.errors.full_messages.first
        end
        redirect_to admin_panel_admin_users_path
      end

      private

      def permit_params
        params.require(:admin_user).permit(:email, :role, :platform, :password, :password_confirmation, access_items: [])
      end

      def remove_password
        params[:admin_user].delete :password              if !params[:admin_user][:password].present?
        params[:admin_user].delete :password_confirmation if !params[:admin_user][:password_confirmation].present?
      end
    end
  end
end