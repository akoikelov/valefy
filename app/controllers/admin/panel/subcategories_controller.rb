module Admin
  module Panel
    class SubcategoriesController < ApplicationController
      layout 'panel'

      def index
        # @subcategories = Subcategory.order(:position).all
        result = Subcategories::Index.call(params: params)
        @subcategories = result.data
      end

      def show
        @subcategory = Subcategory.find(params[:id])
      end

      def new
        @subcategory = Subcategory.new
      end

      def create
        @subcategory = Subcategory.new(subcategory_params)

        if @subcategory.save
          flash[:success] = 'Subcategory successfully created!'
          redirect_to admin_panel_subcategories_path
        else
          flash[:error] = @subcategory.errors.full_messages.first
          render :new
        end
      end

      def edit
        @subcategory = Subcategory.find(params[:id])
      end

      def update
        @subcategory = Subcategory.find(params[:id])

        if @subcategory.update_attributes(subcategory_params)
          flash[:success] = 'Subcategory successfully updated!'
          redirect_to admin_panel_subcategory_path(@subcategory)
        else
          flash[:error] = @subcategory.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @subcategory = Subcategory.find(params[:id])
        if @subcategory.destroy
          flash[:success] = 'Subcategory successfully deleted!'
        else
          flash[:error] = @subcategory.errors.full_messages.first
        end
        redirect_to admin_panel_subcategories_path
      end

      def sortable_update
        params[:Subcategory].each_with_index do |id, index|
          Subcategory.where(id: id).update_all(position: index + 1)
        end
        render json: { message: 'Success' }, status: 200
      end

      private

      def subcategory_params
        params.require(:subcategory).permit(
          :id, :title, :small_car,
          :title_ru, :category_id,
          :image, :icon, :icon_web, :visible,
          :code, :corp
        )
      end
    end
  end
end