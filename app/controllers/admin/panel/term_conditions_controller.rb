module Admin
  module Panel
    class TermConditionsController < ApplicationController
      layout 'panel'

      def index
        @term_conditions = TermCondition.all
      end

      def edit
        @term_condition = TermCondition.find(params[:id])
      end

      def update
        @term_condition = TermCondition.find(params[:id])

        if @term_condition.update_attributes(term_condition_params)
          flash[:success] = 'Term condition successfully updated!'
          redirect_to admin_panel_term_conditions_path
        else
          flash[:error] = @term_condition.errors.full_messages.first
          render :edit
        end
      end

      private

      def term_condition_params
        params.require(:term_condition).permit(:condition)
      end
    end
  end
end