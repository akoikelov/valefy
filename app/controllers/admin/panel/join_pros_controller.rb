module Admin
  module Panel
    class JoinProsController < ApplicationController
      layout 'panel'

      def index
        result = JoinPros::Index.call(params: params)
        @draft_masters = result.data
      end

      def show
        @draft_master = DraftMaster.find(params[:id])
      end
    end
  end
end