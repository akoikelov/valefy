module Admin
  module Panel
    class CountriesController < ApplicationController
      layout 'panel'

      def index
        result = Countries::Index.call(params: params)
        @countries = result.data
      end

      def show
        @country = Country.find(params[:id])
      end

      def new
        @country = Country.new
      end

      def create
        @country = Country.new(country_params)

        if @country.save
          flash[:success] = 'Country successfully created!'
          redirect_to admin_panel_countries_path
        else
          flash[:error] = @country.errors.full_messages.first
          render :new
        end
      end

      def edit
        @country = Country.find(params[:id])
      end

      def update
        @country = Country.find(params[:id])

        if @country.update_attributes(country_params)
          flash[:success] = 'Country successfully updated!'
          redirect_to admin_panel_country_path(@country)
        else
          flash[:error] = @country.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @country = Country.find(params[:id])
        if @country.destroy
          flash[:success] = 'Country successfully deleted!'
        else
          flash[:error] = @country.errors.full_messages.first
        end
        redirect_to admin_panel_countries_path
      end

      def country_params
        params.require(:country).permit(:name, :description, :image)
      end
    end
  end
end