module Admin
  module Panel
    class DemoRequestsController < ApplicationController
      layout 'panel'
      before_action :authenticate_admin_user!

      def index
        @requests = DemoRequest.all.page(params[:page])
      end

      def show
        @request = DemoRequest.find(params[:id])
      end
    end
  end
end