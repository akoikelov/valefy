module Admin
  module Panel
    class ProFeedbacksController < ApplicationController
      layout 'panel'

      def index
        result = ProFeedbacks::Index.call(params: params)
        @master_feedbacks = result.data
      end

      def show
        @master_feedback = MasterFeedback.find(params[:id])
      end
    end
  end
end