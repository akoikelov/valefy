module Admin
  module Panel
    class CategoriesController < ApplicationController
      layout 'panel'

      def index
        # @categories = Category.order(:position).all
        result = Categories::Index.call(params: params)
        @categories = result.data
      end

      def show
        @category = Category.find(params[:id])
      end

      def new
        @category = Category.new
      end

      def create
        @category = Category.new(category_params)

        if @category.save
          flash[:success] = 'Category successfully created!'
          redirect_to admin_panel_categories_path
        else
          flash[:error] = @category.errors.full_messages.first
          render :new
        end
      end

      def edit
        @category = Category.find(params[:id])
      end

      def update
        @category = Category.find(params[:id])

        if @category.update_attributes(category_params)
          if params[:category][:remove_image] == '1'
            @category.remove_image!
            @category.save
          end
          flash[:success] = 'Category successfully updated!'
          redirect_to admin_panel_category_path(@category)
        else
          flash[:error] = @category.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @category = Category.find(params[:id])
        if @category.destroy
          flash[:success] = 'Category successfully deleted!'
        else
          flash[:error] = @category.errors.full_messages.first
        end
        redirect_to admin_panel_categories_path
      end

      def sortable_update
        params[:Category].each_with_index do |id, index|
          Category.where(id: id).update_all(position: index + 1)
        end
        render json: { message: 'Success' }, status: 200
      end

      private

      def category_params
        params.require(:category).permit(:id, :title, :title_ru, :parent_id, :image, :icon, :icon_web, :visible, :position, :description, :duration, :corp)
      end
    end
  end
end