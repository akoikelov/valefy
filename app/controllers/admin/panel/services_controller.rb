module Admin
  module Panel
    class ServicesController < ApplicationController
      layout 'panel'

      def index
        result = Services::Index.call(params: params)
        @services = result.data
      end

      def show
        @service = Service.find(params[:id])
      end

      def new
        @service = Service.new
        @categories = Category.all
        gon.subcategories = Subcategory.where(visible: true).order(position: :asc)
      end

      def create
        @service = Service.new(service_params)

        if @service.save
          flash[:success] = 'Service successfully created!'
          redirect_to admin_panel_services_path
        else
          flash[:error] = @service.errors.full_messages.first
          render :new
        end
      end

      def edit
        @service = Service.find(params[:id])
        @categories = Category.all
        gon.subcategories = Subcategory.where(visible: true).order(position: :asc)
        gon.current_subcategory = @service.subcategory if @service.subcategory.present?
      end

      def update
        @service = Service.find(params[:id])

        if @service.update_attributes(service_params)
          flash[:success] = 'Service successfully updated!'
          redirect_to admin_panel_service_path(@service)
        else
          flash[:error] = @service.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @service = Service.find(params[:id])
        if @service.destroy
          flash[:success] = 'Service successfully deleted!'
        else
          flash[:error] = @service.errors.full_messages.first
        end
        redirect_to admin_panel_services_path
      end

      private

      def service_params
        params.require(:service).permit(:title, :title_ru, :description, :price, :duration, :short_title, :subcategory_id)
      end
    end
  end
end