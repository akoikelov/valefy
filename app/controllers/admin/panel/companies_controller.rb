module Admin
  module Panel
    class CompaniesController < ApplicationController
      layout 'panel'

      def index
        @companies = Company.all.page(params[:page])
      end

      def show
        @company = Company.find(params[:id])
      end

      def new
        @company = Company.new
      end

      def create
        company = Company.new(permit_params)
        company.admin_user = current_admin_user
        if company.save
          redirect_to admin_panel_companies_path
        else
          redirect_to new_admin_panel_company_path, alert: company.errors.full_messages
        end
      end

      def edit
        @company = current_admin_user.company.present? ? current_admin_user.company : Company.find(params[:id])
      end

      def update
        @company = current_admin_user.company.present? ? current_admin_user.company : Company.find(params[:id])

        if @company.update_attributes(permit_params)
          flash[:success] = 'Company successfully updated!'
          redirect_to admin_panel_company_path(@company)
        else
          flash[:error] = @company.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @company = Company.find(params[:id])
        if @company.destroy
          flash[:success] = 'Company successfully deleted!'
        else
          flash[:error] = @company.errors.full_messages.first
        end
        redirect_to admin_panel_companies_path
      end

      private

      def permit_params
        params.require(:company).permit(:title, :description, :platform, :admin_user_id)
      end
    end
  end
end