module Admin
  module Panel
    class GiftCardsController < ApplicationController
      layout 'panel'

      def index
        result = GiftCards::Index.call(params: params)
        gon.is_expired = params[:is_expired] if params[:is_expired].present?
        @gift_cards = result.data
      end

      def show
        @gift_card = GiftCard.find(params[:id])
      end

      def edit
        @gift_card = GiftCard.find(params[:id])
      end

      def update
        @gift_card = GiftCard.find(params[:id])

        if @gift_card.update_attributes(gift_card_params)
          flash[:success] = 'Gift voucher successfully updated'
          redirect_to admin_panel_gift_card_path(@gift_card)
        else
          flash[:danger] = @gift_card.errors.full_messages.first
          render :edit
        end
      end

      def gift_card_params
        params[:gift_card][:used] = false unless params[:gift_card][:used].present?
        params.require(:gift_card).permit(:amount, :exp_date, :message, :used, :to_client_id, :from_client_id, :service_id)
      end
    end
  end    
end