module Admin
  module Panel
    class OfficesController < ApplicationController
      layout 'panel'

      def index
        result = Offices::Index.call(params: params)
        @offices = result.data
      end

      def show
        @office = Office.find(params[:id])
        gon.office = @office
      end

      def new
        @office = Office.new
      end

      def create
        @office = Office.new(office_params)

        if @office.save
          flash[:success] = 'Office successfully created!'
          redirect_to admin_panel_offices_path
        else
          flash[:error] = @office.errors.full_messages.first
          render :new
        end
      end

      def edit
        @office = Office.find(params[:id])
        gon.office = @office
      end

      def update
        @office = Office.find(params[:id])

        if @office.update_attributes(office_params)
          flash[:success] = 'Office successfully updated!'
          redirect_to admin_panel_client_path(@office.client)
        else
          flash[:error] = @office.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @office = Office.find(params[:id])
        if @office.destroy
          flash[:success] = 'Office successfully deleted!'
        else
          flash[:error] = @office.errors.full_messages.first
        end 
        redirect_to admin_panel_offices_path
      end

      private

      def office_params
        params.require(:office).permit(:title, :client_id, :lat, :lon, :count_vehicles, :address)
      end
    end
  end
end