module Admin
  module Panel
    class StationsController < ApplicationController
      layout 'panel'

      def index
        # result = Stations::Index.call(params: params, current_admin_user: current_admin_user)
        @stations = CompanyLocation.all.page(params[:page])
      end

      def show
        @station = CompanyLocation.find(params[:id])
      end

      def new
        @company_location = CompanyLocation.new
      end

      def create
        @company_location = CompanyLocation.new(permit_params)

        if @company_location.save
          flash[:success] = 'Station successfully created!'
          redirect_to admin_panel_station_path(@company_location.id)
        else
          flash[:error] = @company_location.errors.full_messages.first
          render :new
        end
      end

      def edit
        @station = CompanyLocation.find(params[:id])
      end

      def update
        @station = CompanyLocation.find(params[:id])

        if @station.update_attributes(permit_params)
          flash[:success] = 'Station successfully updated!'
          redirect_to admin_panel_station_path(@station.id)
        else
          flash[:error] = @station.errors.full_messages.first
          render :edit
        end
      end

      def dashboard
        @stations ||= CompanyLocation.all
        @orders ||= orders

        gon.orders = @orders
      end

      private

      def permit_params
        params.require(:company_location).permit(
          :title, :image, :phone_number, :lat, :lon,
          :address, :email, :company_id, promo_code_ids: []
        )
      end

      def orders
        if params[:station_id].present?
          @orders ||= OrderItem.by_station(params[:station_id]).waiting_and_active
        else
          @orders ||= OrderItem.stations.waiting_and_active
        end
      end
    end
  end
end