module Admin
  module Panel
    class FaqClientsController < ApplicationController
      layout 'panel'

      def index
        result = FaqClients::Index.call(params: params)
        @faq_clients = result.data
      end

      def show
        @faq_client = Faq.find(params[:id])
      end

      def new
        @faq_client = Faq.new
      end

      def create
        @faq_client = Faq.new(faqs_client_params)
        @faq_client.type_user = Faq::TYPE_USERS.last
        if @faq_client.save
          flash[:success] = 'Faqs client successfully created'
          redirect_to admin_panel_faq_clients_path
        else
          flash[:error] = @faq_client.errors.full_messages.first
          render :new
        end
      end

      def edit
        @faq_client = Faq.find(params[:id])
      end

      def update
        @faq_client = Faq.find(params[:id])

        if @faq_client.update_attributes(faqs_client_params)
          flash[:success] = 'Faqs client successfully updated!'
          redirect_to admin_panel_faq_client_path(@faq_client)
        else
          flash[:error] = @faq_client.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @faq_client = Faq.find(params[:id])
        if @faq_client.destroy
          flash[:success] = 'Faqs client successfully deleted!'
        else
          flash[:error] = @faq_client.errors.full_messages.first
        end
        redirect_to admin_panel_faq_clients_path
      end

      private

      def faqs_client_params
        params.require(:faq).permit(:question, :answer)
      end
    end
  end
end