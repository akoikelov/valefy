module Admin
  module Panel
    class BannersController < ApplicationController
      layout 'panel'

      def index
        result = Banners::Index.call(params: params)
        @banners = result.data
      end

      def show
        @banner = Banner.find(params[:id])
      end

      def new
        @banner = Banner.new
      end

      def create
        @banner = Banner.new(banner_params)

        if @banner.save
          flash[:success] = 'Banner successfully created!'
          redirect_to admin_panel_banners_path
        else
          flash[:error] = @service.errors.full_messages.first
          render :new
        end
      end

      def edit
        @banner = Banner.find(params[:id])
      end

      def update
        @banner = Banner.find(params[:id])

        if @banner.update_attributes(banner_params)
          flash[:success] = 'Banner successfully updated!'
          redirect_to admin_panel_banner_path(@banner)
        else
          flash[:error] = @banner.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @banner = Banner.find(params[:id])
        if @banner.destroy
          flash[:success] = 'Banner successfully deleted!'
        else
          flash[:error] = @banner.errors.full_messages.first
        end
        redirect_to admin_panel_banners_path
      end

      private

      def banner_params
        params.require(:banner).permit(:title, :description, :image)
      end
    end
  end
end