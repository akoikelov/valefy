module Admin
  module Panel
    class ClientFeedbacksController < ApplicationController
      layout 'panel'

      def index
        result = ClientFeedbacks::Index.call(params: params)
        @client_feedbacks = result.data
      end

      def show
        @client_feedback = ClientFeedback.find(params[:id])
      end
    end
  end
end