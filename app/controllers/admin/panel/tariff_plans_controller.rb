module Admin
  module Panel
    class TariffPlansController < ApplicationController
      layout 'panel'

      def index
        result = TariffPlans::Index.call(params: params)
        @tariff_plans = result.data
      end

      def show
        @tariff_plan = TariffPlan.find(params[:id])
      end

      def new
        @tariff_plan = TariffPlan.new
      end

      def create
        @tariff_plan = TariffPlan.new(tariff_plan_params)

        if @tariff_plan.save
          flash[:success] = 'TariffPlan successfully created!'
          redirect_to admin_panel_tariff_plans_path
        else
          flash[:error] = @tariff_plan.errors.full_messages.first
          render :new
        end
      end

      def edit
        @tariff_plan = TariffPlan.find(params[:id])
      end

      def update
        @tariff_plan = TariffPlan.find(params[:id])

        if @tariff_plan.update_attributes(tariff_plan_params)
          flash[:success] = 'TariffPlan successfully updated!'
          redirect_to admin_panel_tariff_plan_path(@tariff_plan)
        else
          flash[:error] = @tariff_plan.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @tariff_plan = TariffPlan.find(params[:id])
        if @tariff_plan.destroy
          flash[:success] = 'TariffPlan successfully deleted!'
        else
          flash[:error] = @tariff_plan.errors.full_messages.first
        end
        redirect_to admin_panel_tariff_plans_path
      end

      def tariff_plan_params
        params.require(:tariff_plan).permit(:plan, :plan_value)
      end
    end
  end
end