module Admin
  module Panel
    class DashboardsController < ApplicationController
      layout 'panel'

      def index
        @orders = OrderItem.mobile.waiting_and_active if current_admin_user.admin?
        @orders = OrderItem.by_company(current_admin_user.try(:company).try(:id)) \
          if current_admin_user.company? || current_admin_user.content?

        gon.orders = @orders
        gon.pros = User::ServiceMan.all
        gon.clients = User::Client.all
        gon.services = Service.all
      end

      def reassign_orders_pro
        result = OrderItems::ReassignPro.call(params: params)
        if result.success?
          flash[:success] = 'VALEFYER successfully reassigned'
        else
          flash[:error] = result.error
        end
        redirect_to :back
      end

      def get_info_pro
        pro_attrs = {}
        pro = User::ServiceMan.where(id: params[:id]).preload(:order_items).take
        pro_attrs[:pro] = pro
        pro_attrs[:orders] =  pro.order_items.where(status: 12).order(order_time: :desc)
        render json: pro_attrs, status: 200
      end
    end
  end
end