module Admin
  module Panel
    class SettingsController < ApplicationController
      layout 'panel'

      def index
        @settings = Setting.all
      end

      def show
        find_resource
      end

      def edit
        find_resource
      end

      def update
        find_resource

        if @setting.update_attributes(settings_params)
          redirect_to admin_panel_setting_path(@setting)
        else
          render :edit
        end
      end

      private

      def settings_params
        params.require(:setting).permit(
          :send_email_owner_gift_card,
          :freelancing_transfer_percentage,
          :aggregator_transfer_percentage,
          :park_transfer_percentage,
          :franchise_transfer_percentage,
          :send_welcome_email,
          :send_email_invoice_client,
          :send_email_created_order,
          :time_send_location,
          :radius,
          :bisy_days_for_order
        )
      end

      def find_resource
        @setting ||= Setting.find(params[:id])
      end
    end
  end
end