module Admin
  module Panel
    class TeamsController < ApplicationController
      layout 'panel'

      def index
        result = Teams::Index.call(params: params)
        @teams = result.data
      end

      def new
        @team = Team.new
      end

      def create
        @team = Team.new(team_params)
        if @team.save
          flash[:success] = 'Team successfully created!'

          redirect_to admin_panel_team_path(@team.id)
        else
          flash[:error] = @team.errors.full_messages.first
          render :new
        end
      end

      def show
        @team = Team.find(params[:id])
      end

      def edit
        @team = Team.find(params[:id])
      end

      def update
        @team = Team.find(params[:id])

        if @team.update_attributes(team_params)
          flash[:success] = 'Team successfully updated!'

          redirect_to admin_panel_team_path(@team.id)
        else
          flash[:error] = @team.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @team = Team.find(params[:id])
        if @team.destroy
          flash[:success] = 'Team successfully deleted!'
        else
          flash[:error] = @team.errors.full_messages.first
        end
        redirect_to admin_panel_teams_path
      end

      private

      def team_params
        params.require(:team).permit(:title, master_ids: [])
      end
    end
  end
end