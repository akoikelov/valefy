module Admin
  module Panel
    class ProsController < ApplicationController
      layout 'panel'

      before_action :remove_password, only: [:update]

      def index
        result = Pros::Index.call(params: params, current_admin_user: current_admin_user)
        @pros = result.data
      end

      def show
        @pro = User::ServiceMan.find(params[:id])
      end

      def edit
        @pro = User::ServiceMan.find(params[:id])
      end

      def update
        @pro = User::ServiceMan.find(params[:id])

        if @pro.update_attributes!(permit_params)
          if params[:user_service_man][:remove_image] == '1'
            @pro.remove_image!
            @pro.save
          end
          result = ::Api::Masters::V2::BuildSchedule.call(params: params)

          if result.success?
            @pro.reload.build_pro_information(work_schedule: result.schedule)
            @pro.save!
            flash[:success] = 'VALEFYER successfully updated!'
            redirect_to admin_panel_pro_path(@pro.id)
          end
        else
          render :edit
        end
      end

      def new
        @pro = User::ServiceMan.new
      end

      def create
        @pro = User::ServiceMan.new(permit_params)
        if @pro.save
          result = ::Api::Masters::V2::BuildSchedule.call(params: params)
          if result.success?
            @pro.reload.build_pro_information(work_schedule: result.schedule)
            @pro.save!
            flash[:success] = 'VALEFYER successfully created!'
            redirect_to admin_panel_company_path(permit_params[:company_id])
          else
            flash[:error] = 'Cant create the schedule'
            render :new
          end
        else
          flash[:error] = @pro.errors.full_messages.first
          render :new
        end
      rescue => e
        render :new # TODO если имейл повторяется клиентский то надо с этим что то делать
      end

      def destroy
        @pro = User::ServiceMan.find(params[:id])
        if @pro.destroy
          flash[:success] = 'Valefyer successfully deleted!'
        else
          flash[:error] = @pro.errors.full_messages.first
        end
        redirect_to admin_panel_pros_path
      end

      def get_tips
        if params[:order_id].present?
          order = OrderItem.find(params[:order_id])
          tips = order.feedbacks.map { |feedback| feedback.tip }.sum.to_f
          render json: { tips: tips }, status: 200
        else
          render json: { tips: 'Not found orders id' }, status: 400
        end
      end

      private

      def permit_params
        params.require(:user_service_man).permit(
          :email, :name, :last_name, :onduty, :company_id,
          :b_day, :phone_number, :password, :mobile, :use_bank_details, :district_id,
          :password_confirmation, :lat, :lon, :company_id, :start_work_date,
          category_ids: [], transport_ids: []
        )
      end

      def remove_password
        params[:user_service_man].delete :password if params[:user_service_man][:password].empty?
        params[:user_service_man].delete :password_confirmation if params[:user_service_man][:password_confirmation].empty?
      end
    end
  end
end