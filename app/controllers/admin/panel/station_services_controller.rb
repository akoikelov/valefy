module Admin
  module Panel
    class StationServicesController < ApplicationController
      layout 'panel'

      def show
        @company = StationService.find(params[:id])
      end

      def edit
        @service = StationService.find(params[:id])
      end

      def update
        @service = StationService.find(params[:id])

        if @service.update_attributes(permit_params)
          flash[:success] = 'Price successfully updated!'
          redirect_to admin_panel_station_path(@service.station)
        else
          flash[:error] = @station.errors.full_messages.first
          render :edit
        end
      end

      private

      def permit_params
        params.require(:station_service).permit(:price)
      end
    end
  end
end