module Admin
  module Panel
    class CitiesController < ApplicationController
      layout 'panel'

      def index
        result = Cities::Index.call(params: params)
        @cities = result.data
      end

      def show
        @city = City.find(params[:id])
      end

      def new
        @city = City.new
      end

      def create
        @city = City.new(city_params)

        if @city.save
          flash[:success] = 'City successfully created!'
          redirect_to admin_panel_cities_path
        else
          flash[:error] = @city.errors.full_messages.first
          render :new
        end
      end

      def edit
        @city = City.find(params[:id])
      end

      def update
        @city = City.find(params[:id])

        if @city.update_attributes(city_params)
          flash[:success] = 'City successfully updated!'
          redirect_to admin_panel_city_path(@city)
        else
          flash[:error] = @city.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @city = City.find(params[:id])
        if @city.destroy
          flash[:success] = 'City successfully deleted!'
        else
          flash[:error] = @city.errors.full_messages.first
        end
        redirect_to admin_panel_cities_path
      end

      def city_params
        params.require(:city).permit(:name, :description, :image, :country_id)
      end
    end
  end
end