module Admin
  module Panel
    class PromoCodesController < ApplicationController
      layout 'panel'

      def index
        result = PromoCodes::Index.call(params: params)
        @promo_codes = result.data
      end

      def show
        @promo_code = PromoCode.find(params[:id])
      end

      def new
        @promo_code = PromoCode.new
      end

      def create
        @promo_code = PromoCode.new(promo_code_params)

        if @promo_code.save
          flash[:success] = 'Promo code successfully created!'
          redirect_to admin_panel_promo_code_path(@promo_code.id)
        else
          flash[:error] = 'Cant create the promo code'
          render :new
        end
      end

      def edit
        @promo_code = PromoCode.find(params[:id])
      end

      def update
        @promo_code = PromoCode.find(params[:id])

        if @promo_code.update_attributes(promo_code_params)
          flash[:success] = 'Promo code successfully updated!'
          redirect_to admin_panel_promo_code_path(@promo_code.id)
        else
          flash[:success] = @promo_code.errors.full_messages.first
          render :edit
        end
      end

      def promo_code_params
        params.require(:promo_code).permit(:discount, :promo_code_id, :user, :client_id, :exp_date, :description)
      end
    end
  end
end