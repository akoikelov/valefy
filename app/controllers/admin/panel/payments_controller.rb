module Admin
  module Panel
    class PaymentsController < ApplicationController
      layout 'panel'

      def show
        @order = OrderItem.find(params[:order_id])
      end
    end
  end
end