module Admin
  module Panel
    class ContractController < ApplicationController
      layout 'panel'

      def new
        @contract = client.build_contract
        @contract.contract_dates.build
      end

      def create
        @contract = Contract.new(contract_params)

        if @contract.save
          flash[:success] = 'Contract successfully added!'
          redirect_to admin_panel_client_path(@contract.client.id)
        else
          flash[:error] = @contract.errors.full_messages.first
          render :new
        end
      end

      def edit
        @contract = client.contract
      end

      def update
        contract = Contract.find(contract_params[:id])

        if contract.update_attributes(contract_params)
          flash[:success] = 'Contract successfully updated!'
          redirect_to admin_panel_client_path(contract.client.id)
        else
          flash[:error] = contract.errors.full_messages.first
          render :edit
        end
      end

      private

      def client
        User::Client.find(params[:client_id])
      end

      def contract_params
        params.require(:contract).permit(
          :id, :expiration_date,
          :client_id, :expiration_date,
          :segment_id,
          :address, :order_time,
          service_ids: [], contract_dates_attributes: [:id, :order_date, :office_id, :team_id]
        )
      end
    end
  end
end