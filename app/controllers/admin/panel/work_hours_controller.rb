
module Admin
  module Panel
    class WorkHoursController < ApplicationController
      def create
        find_resource

        @resource.work_hours.create permit_params
      end

      def update
        if work_hour_resource
          start_day = Time.zone.parse(permit_params[:start_day])
          end_day   = Time.zone.parse(permit_params[:end_day])

          if start_day.wday == work_hour_resource[:day] && end_day.wday == work_hour_resource[:day]
            work_hour_resource.update(start_day: start_day.strftime('%H:%M'), end_day: end_day.strftime('%H:%M'))

            render json: { success: true }, status: :ok
          else
            render json: { success: false }, status: 400
          end
        end
      
      rescue => error
        render json: { success: false, error: error.message }, status: 500
      end

      private

      def find_resource
        @resource ||= User::ServiceMan.find(params[:pro_id])
      end

      def work_hour_resource
        @work_hour ||= find_resource.work_hours.find(params[:id])
      end

      def permit_params
        params.permit(:start_day, :end_day, :day)
      end
    end
  end
end