module Admin
  module Panel
    class ClientsController < ApplicationController
      layout 'panel'
      before_action :skip_passwords, only: [:update]
      before_action :authenticate_admin_user!

      def index
        @categories = Category.all
        @companies = Company.all
        result = Admin::Panel::Clients::GetClientsByFilter.call(params: params, current_admin_user: current_admin_user)
        @clients = result.data
        @clients_count = result.clients_count
      end

      def show
        @client = User::Client.find(params[:id])
      end

      def new
        @client = User::Client.new
        @client.offices.build
        @client.build_client_extra_info
        @client.build_client_segment
      end

      def create
        result   = ::Api::V2::CreateImage.call(params: { image: clients_params[:image] })
        link = (result.success? ? result.link : "")
        @client  = User::Client.new(except_client_params.merge({image: link}))

        if @client.save
          @client.create_categories_for_client(clients_params[:category_ids])
          flash[:success] = 'Client successfully created!'
          redirect_to admin_panel_client_path(@client.id)
        else
          flash[:error] = @client.errors.full_messages.first
          render :new
        end
      end

      def edit
        @client = User::Client.find(params[:id])
        @client.build_client_segment if @client.client_segment.nil?
      end

      def update
        @client = User::Client.find(params[:id])

        if clients_params[:image].present?
          result = ::Api::V2::CreateImage.call(params: {image: clients_params[:image]})
          link = (result.success? ? result.link : "")
        else
          link = @client.image
        end

        if @client.update_attributes(clients_params.except(:image).merge({image: link}))
          if params[:user_client][:remove_image] == '1'
            @client.remove_image!
            @client.save
          end
          flash[:success] = 'Client successfully updated!'
          redirect_to admin_panel_client_path(@client.id)
        else
          flash[:error] = @client.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @client = User::Client.find(params[:id])
        if @client.destroy
          flash[:success] = 'Client successfully deleted!'
        else
          flash[:error] = @client.errors.full_messages.first
        end
        redirect_to admin_panel_clients_path
      end

      def upload_xls; end

      def import_xls
        Thread.new do
          upload_xls = UploadClientsFromExcel.new(params.try(:[], :dump).try(:[], :file), current_admin_user)
          upload_xls.parse
        end
    
        redirect_to admin_panel_clients_path, notice: "Data importing...Please wait"
      end

      private

      def except_client_params
        clients_params.except(:category_ids, :image)
      end

      def clients_params
        params.require(:user_client).permit(
          :name, :cash, :tariff_plan_id, :pay_with, :email, :phone_number,
          :password, :password_confirmation, :corp, :confirm_phone_number,
          :image, :doc, :address, :district_id,
          category_ids: [],
          offices_attributes: [:id, :title, :count_vehicles, :lat, :lon],
          client_extra_info_attributes: [:gender, :family_status, :profession],
          client_services_attributes: [:id, :service_id, :price, :_destroy],
          client_segment_attributes: [:id, :title]
        )
      end

      def skip_passwords
        params[:user_client].delete :password if params[:user_client][:password].empty?
        params[:user_client].delete :password_confirmation if params[:user_client][:password_confirmation].empty?
      end
    end
  end
end