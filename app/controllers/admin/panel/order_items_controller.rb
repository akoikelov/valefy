module Admin
  module Panel
    class OrderItemsController < ApplicationController
      layout 'panel'
      before_action :authenticate_admin_user!
      before_action :client_info, except: [:edit, :update]
      before_action :permit_update, only: [:edit, :update]

      def new
        @categories  = Category.visible
        gon.schedule = Api::V2::AvailableSchedule.call.schedule_week

        @order = OrderItem.new
      end

      def create
        return redirect_to :back, flash: { error: 'You should choose client' } if !params[:order_item].try(:[], :client_id).present?

        client = ::User::Client.find(permit_params[:client_id])
        result = Api::V2::CreateOrderItem.call(params: permit_params,current_user: client)

        if result.success?
          order = result.order
          charge = Api::V2::Customer::TakeMoney.call(params: {order_id: order.id}, current_user: client)

          if charge.success?
            searching = Api::V2::OrderItems::Searching.call(params: {order_id: order.id}, current_user: client)

            if searching.success?
              ['android','IOS'].map do |item|
                run_push_worker(order, I18n.t('order_item.messages.new_order'), item)
              end
              return redirect_to admin_panel_order_items_path, :notice => "Order has been created"
            else
              flash[:error] = 'Search error'
              redirect_to new_admin_panel_order_item_path(client_id: client.id)
            end
          else
            flash[:error] = "Can't pre authorised charge"
            return redirect_to new_admin_panel_order_item_path(client_id: client.id)
          end
        else
          flash[:error] = 'You have to fill all fields'

          redirect_to new_admin_panel_order_item_path(client_id: client.id)
        end
      end

      def index
        result = OrderItems::List.call(params: params, current_admin_user: current_admin_user)
        @orders_status = [['All', 'All'], ['Created', 'Created and not paid'], ['Pending', 'Pending'], ['Active', 'In progress'], ['Cancelled', 'Cancelled'], ['History', 'Completed']]
        @orders = result.data
        @orders_count = result.orders_count
        @clients = User::Client.all.order(name: :asc)
        @pros = User::ServiceMan.all.order(name: :asc)
        @services = Service.all.order(id: :desc)
        gon.orders_scope = params[:orders_scope].present? ? params[:orders_scope] : 'Pending'
        respond_to do |format|
          format.js
          format.html {}
        end
      end

      def show 
        preload_resource
        gon.pros = User::ServiceMan.all.order(name: :asc)
        gon.client = @order.client
        gon.order = @order
        if @order.master.present? && @order.master.locations.present?
          gon.latLng = [@order.master.locations.last.lat, @order.master.locations.last.lon]
          gon.master = @order.master
        end
      end

      def edit
        preload_resource
        # cards = Api::V2::Customer::Card::ListCardCustomer.call(current_user: @order.client)
        #
        # @card_list = cards.list if cards.success?
        @client    = @order.client
        gon.client = @client
        @categories  = Category.visible
        gon.schedule = Api::V2::AvailableSchedule.call.schedule_week
      end

      def update
        preload_resource
        result = Api::V2::UpdateOrderItem.call(params: permit_params,current_user: @order.client, order: @order)

        if result.success?
          flash[:success] = 'Order successfully updated!'

          redirect_to admin_panel_order_item_path(@order.id)
        else
          flash[:error] = @order.errors.full_messages.first
          render :edit
        end
      end

      def cancel
        preload_resource
        result = ::Api::V2::OrderItems::Cancel.call(
          params: { order_id: @order.id }, current_user: @order.client
        )

        if result.success?
          run_cancel_push_worker(@order, I18n.t('order_item.messages.cancel')) if @order.master.present?
          flash[:success] = 'Order canceled successfully!'
          redirect_to admin_panel_order_items_path
        else
          flash[:error] = 'Order is not canceled!'
          redirect_to admin_panel_order_item_path(order)
        end
      end

      def change_time
        preload_resource
        order.update!(order_time: params[:date])

        render json: { success: true }, status: :ok
      end

      private

      def preload_resource
        @order ||= OrderItem.find(params[:id])
      end

      def permit_params
        params.require(:order_item).permit(
          :address, :description,:order_time, :lon, :lat, :baby_seat,
          :time_zone, :pets, :platform, :count_vehicles,:parking, :reg_numbers,
          :car_count, :suv_count, :minivan_count,
          :stationary_id, :office_id, :client_id, :service_ids => []
        )
      end

      def permit_update
        order = OrderItem.find(params[:id])

        if !(order.created? || order.pre_authorize_charged? || order.searching?)
          flash[:error] = "Don't have permit for edit order"

          redirect_to admin_panel_order_item_path(order.id)
        end
      end

      def run_push_worker(order, title, platform)
        unless Rails.env.eql?('test')
          Api::Masters::V1::NewOrderNotifyWorker.perform_async(
            order_id: order.id, title: title, platform: platform,
            time_zone: params[:time_zone]
          )
        end
      end

      def run_cancel_push_worker(order, title)
        unless Rails.env.eql?('test')
          Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
        end
      end

      def client_info
        client_id = params[:client_id]

        if client_id.present?
          client = User::Client.find(client_id)

          if client
            cards = Api::V2::Customer::Card::ListCardCustomer.call(current_user: client)
            @card_list = cards.list if cards.success?
            @client    = client
            gon.client = @client
          end
        end
      rescue
        # nothing
      end
    end
  end
end