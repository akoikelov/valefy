module Admin
  module Panel
    class DevicesController < ApplicationController
      layout 'panel'

      def index
        result = Devices::Index.call(params: params)
        @devices = result.data
      end

      def show
        @device = Device.find(params[:id])
      end

      def destroy
        @device = Device.find(params[:id])

        if @device.destroy
          flash[:success] = 'Device successfully deleted!'
        else
          flash[:error] = @device.errors.full_messages.first
        end
        redirect_to admin_panel_devices_path
      end
    end
  end
end