module Admin
  module Panel
    class CallMesController < ApplicationController
      layout 'panel'

      def index
        @calls = CallMe.all.page(params[:page])
      end

      def show
        @call = CallMe.find(params[:id])
      end
    end
  end
end