module Admin
  module Panel
    class DistrictsController < ApplicationController
      before_action :create_polygon_coords, only: [:create, :update]

      layout 'panel'

      def index
        result = Districts::Index.call(params: params)
        @districts = result.data
      end

      def show
        @district = District.find(params[:id])
        gon.district = @district
      end

      def new
        @countries = Country.all
        gon.cities = City.all
        @district = District.new
      end

      def create
        @district = District.new(district_params)

        if @district.save
          flash[:success] = 'District successfully created!'
          redirect_to admin_panel_districts_path
        else
          flash[:error] = @district.errors.full_messages.first
          render :new
        end
      end

      def edit
        @countries = Country.all
        @district = District.find(params[:id])
        gon.cities = City.all
        gon.district = @district
      end

      def update
        @district = District.find(params[:id])
        if @district.update_attributes(district_params)
          flash[:success] = 'District successfully updated!'
          redirect_to admin_panel_district_path(@district)
        else
          flash[:error] = @district.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @district = District.find(params[:id])
        if @district.destroy
          flash[:success] = 'District successfully deleted!'
        else
          flash[:error] = @district.errors.full_messages.first
        end
        redirect_to admin_panel_districts_path
      end

      private

      def create_polygon_coords
        if params[:district][:coords].present?
          coords = params[:district][:coords].split(';').map { |coord| coord.split(':')[1] + ' ' + coord.split(':')[0] }.join(',')
          params[:district][:coordinates] = 'POLYGON((' + coords + '))'
        end
      end
      
      def district_params
        params.require(:district).permit(:name, :description, :image, :city_id, :coords, :coordinates)
      end
    end
  end
end