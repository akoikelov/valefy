module Admin
  module Panel
    class ReferalsController < ApplicationController
      layout 'panel'

      def index
        result = Referals::Index.call(params: params)
        @referals = result.data
      end

      def show
        @referal = ReferalShare.find(params[:id])
      end
    end
  end
end