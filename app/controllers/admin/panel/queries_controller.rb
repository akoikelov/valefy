module Admin
  module Panel
    class QueriesController < ApplicationController
      
      def search_clients
        result = if params[:q].to_i.present? and params[:q].to_i != 0
          User::Client.where("email LIKE ? or name LIKE ? or phone_number LIKE ? or id = ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%", params[:q])
        else
          User::Client.where("email LIKE ? or name LIKE ? or phone_number LIKE ?", "%#{params[:q]}%", "%#{params[:q]}%", "%#{params[:q]}%")
        end

        render json: result, each_serializer: ::Api::V2::ClientSerializer
      end

      def check_cards
        client = ::User::Client.find(params[:client_id])
        result = Api::V2::Customer::Card::CheckCustomerCard.call(current_user: client)

        if result.success?
          cards = Api::V2::Customer::Card::ListCardCustomer.call(current_user: client.reload)

          render json: { success: true, data: cards.list }, status: :ok
        else
          render json: { success: false }, status: :ok
        end
      end

      def add_cards
        client = ::User::Client.find(permit_params[:client_id])

        result = Api::V2::Customer::CreateCustomer.call(
          params: permit_params, current_user: client
        )

        if result.success?
          render json: { success: true }, status: :ok
        else
          render json: { success: false }, status: :ok
        end
      end

      private

      def permit_params
        params.permit(:stripe_token, :client_id)
      end
    end
  end
end