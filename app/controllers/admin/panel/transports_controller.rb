module Admin
  module Panel
    class TransportsController < ApplicationController
      layout 'panel'

      def index
        @transports = Transport.all.page(params[:page]).per(30)
      end

      def new
        @transport = Transport.new
      end

      def create
        @transport = Transport.new(transport_params)

        if @transport.save
          redirect_to admin_panel_transport_path(@transport.id)
        else
          render :new
        end
      end

      def show
        @transport = Transport.find(params[:id])
      end

      def edit
        @transport = Transport.find(params[:id])
      end

      def update
        @transport = Transport.find(params[:id])

        if @transport.update_attributes(transport_params)
          redirect_to admin_panel_transport_path(@transport.id)
        else
          render :edit
        end
      end

      def destroy
        @transport = Transport.find(params[:id])
        if @transport.destroy
          flash[:success] = 'Transport successfully deleted!'
        else
          flash[:error] = @transport.errors.full_messages.first
        end
        redirect_to admin_panel_transports_path
      end

      private

      def transport_params
        params.require(:transport).permit(:car)
      end
    end
  end
end