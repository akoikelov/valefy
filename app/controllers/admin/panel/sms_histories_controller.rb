module Admin
  module Panel
    class SmsHistoriesController < ApplicationController
      layout 'panel'

      def index
        result = SmsHistories::Index.call(params: params)
        @sms_histories = result.data
      end

      def show
        @sms_history = SmsHistory.find(params[:id])
      end

      def new
        @sms_history = SmsHistory.new
      end

      def create
        @sms_history = SmsHistory.new(sms_history_params)

        if @sms_history.save
          flash[:success] = 'SmsHistory successfully created!'
          redirect_to admin_panel_sms_histories_path
        else
          flash[:error] = @sms_history.errors.full_messages.first
          render :new
        end
      end

      def edit
        @sms_history = SmsHistory.find(params[:id])
      end

      def update
        @sms_history = SmsHistory.find(params[:id])

        if @sms_history.update_attributes(sms_history_params)
          flash[:success] = 'SmsHistory successfully updated!'
          redirect_to admin_panel_sms_history_path(@sms_history)
        else
          flash[:error] = @sms_history.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @sms_history = SmsHistory.find(params[:id])

        if @sms_history.destroy
          flash[:success] = 'SmsHistory successfully deleted!'
        else
          flash[:error] = @sms_history.errors.full_messages.first
        end
        redirect_to admin_panel_sms_histories_path
      end

      def sms_history_params
        params.require(:sms_history).permit(:client_id, :master_id, :phone_number, :code, :status)
      end
    end
  end
end