module Admin
  module Panel
    class FaqProsController < ApplicationController
      layout 'panel'

      def index
        result = FaqPros::Index.call(params: params)
        @faq_pros = result.data
      end

      def show
        @faq_pro = Faq.find(params[:id])
      end

      def new
        @faq_pro = Faq.new
      end

      def create
        @faq_pro = Faq.new(faqs_pro_params)
        @faq_pro.type_user = Faq::TYPE_USERS.first
        if @faq_pro.save
          flash[:success] = 'Faqs VALEFYER successfully created'
          redirect_to admin_panel_faq_pros_path
        else
          flash[:error] = @faq_client.errors.full_messages.first
          render :new
        end
      end

      def edit
        @faq_pro = Faq.find(params[:id])
      end

      def update
        @faq_pro = Faq.find(params[:id])

        if @faq_pro.update_attributes(faqs_pro_params)
          flash[:success] = 'Faqs VALEFYER successfully updated!'
          redirect_to admin_panel_faq_pro_path(@faq_pro)
        else
          flash[:error] = @faq_pro.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @faq_pro = Faq.find(params[:id])
        if @faq_pro.destroy
          flash[:success] = 'Faqs Pro successfully deleted!'
        else
          flash[:error] = @faq_pro.errors.full_messages.first
        end
        redirect_to admin_panel_faq_pros_path
      end

      private

      def faqs_pro_params
        params.require(:faq).permit(:question, :answer)
      end
    end
  end
end