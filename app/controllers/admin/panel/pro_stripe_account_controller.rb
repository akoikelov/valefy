module Admin
  module Panel
    class ProStripeAccountController < ApplicationController
      layout 'panel'
      before_action :authenticate_admin_user!

      def new
        @account = pro.build_stripe_account
      end

      def create
        @account = ProStripeAccount.new(permit_params)

        if @account.save
          result = ::Api::Masters::V1::StripeLib::CreateStripeAccount.call(stripe_account: @account)

          if result.success?
            flash[:success] = 'Bank details created!'
          else
            flash[:error] = result.error
          end

          redirect_to admin_panel_pro_path(pro.id)
        else
          flash[:error] = @account.errors.full_messages.first
          render :new
        end
      end

      def edit
        @account = ProStripeAccount.find(params[:id])
      end

      def update
        @account = ProStripeAccount.find(params[:id])

        if @account.update_attributes(permit_params)
          result = ::Api::Masters::V1::StripeLib::CreateStripeAccount.call(stripe_account: @account)

          if result.success?
            flash[:success] = 'Bank details updated!'
          else
            flash[:error] = result.error
          end

          redirect_to admin_panel_pro_path(pro.id)
        else
          flash[:error] = @account.errors.full_messages.first

          render :edit
        end
      end

      private

      def pro
        User::ServiceMan.find(params[:pro_id])
      end

      def permit_params
        params.require(:pro_stripe_account).permit(:pro_id, :tax_id, :personal_id, :address_line1, :account_number, :city, :state, :id_document_image)
      end
    end
  end
end