module Admin
  module Panel
    class EmailTextsController < ApplicationController
      layout 'panel'

      def index
        result = EmailTexts::Index.call(params: params)
        @email_texts = result.data
      end

      def show
        @email_text = EmailText.find(params[:id])
      end

      def new
        @email_text = EmailText.new
      end

      def create
        @email_text = EmailText.new(email_text_params)

        if @email_text.save
          flash[:success] = 'EmailText successfully created!'
          redirect_to admin_panel_email_texts_path
        else
          flash[:error] = @email_text.errors.full_messages.first
          render :new
        end
      end

      def edit
        @email_text = EmailText.find(params[:id])
      end

      def update
        @email_text = EmailText.find(params[:id])

        if @email_text.update_attributes(email_text_params)
          flash[:success] = 'EmailText successfully updated!'
          redirect_to admin_panel_email_text_path(@email_text)
        else
          flash[:error] = @email_text.errors.full_messages.first
          render :edit
        end
      end

      def destroy
        @email_text = EmailText.find(params[:id])
        if @email_text.destroy
          flash[:success] = 'EmailText successfully deleted!'
        else
          flash[:error] = @email_text.errors.full_messages.first
        end
        redirect_to admin_panel_email_texts_path
      end

      def email_text_params
        params.require(:email_text).permit(:email_type, :slug, :content)
      end
    end
  end
end