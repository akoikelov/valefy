module Admin
  module Metrics
    class DistrictsController < ApplicationController
      before_action :set_default_range, only: [:index]

      layout 'panel'

      def index
        @countries = Country.all
        @cities = City.all
        assign_vars_to_frontend
      end

      def set_city
        result = Charts::Districts::GetOrdersCount.call(params: params)
        data = result.success? ? [ result.data, result.districts ] : []
        render json: data
      end

      def average_price
        result = Charts::Districts::AveragePrice.call(params: params)
        if result.success?
          render json: result.data, status: 200
        else
          render json: result.error, status: 500
        end
      end

      def orders_count_cat
        result = Charts::Districts::OrdersCountEachCategory.call(params: params)
        if result.success?
          render json: result.data, status: 200
        else
          render json: result.error, status: 500
        end
      end

      def clients_by_gender
        result = Charts::Districts::ClientsByGender.call(params: params)
        if result.success?
          render json: result.data, status: 200
        else
          render json: result.error, status: 500
        end
      end

      def clients_by_age
        result = Charts::Districts::ClientsByAge.call(params: params)
        if result.success?
          render json: result.data, status: 200
        else
          render json: result.error, status: 500
        end
      end

      private

      def district_orders_count
        result = Charts::Districts::GetOrdersCount.call(params: params)
        result.success? ? result.data : []
      end

      def assign_vars_to_frontend
        gon.district_orders_count = district_orders_count
        gon.cities = @cities
      end

      def range
        params[:range].split.map { |e| e.downcase }.join('_')
      end

      def set_default_range
        params[:range] = 'Last 7 Days'
      end
    end
  end
end