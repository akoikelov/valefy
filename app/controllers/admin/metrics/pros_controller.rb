module Admin
  module Metrics
    class ProsController < ApplicationController
      before_action :set_default_range

      layout 'panel'

      def index
        @profs = User::ServiceMan.all
        @cities = City.all
        @countries = Country.all
        assign_vars_to_frontend
      end

      def set_master
        assign_vars_to_frontend
        data = [
          gon.canceled_orders,
          gon.completed_orders,
          gon.orders_by_client_type,
          gon.orders_by_category,
          gon.accepted_orders,
          gon.average_time_orders
        ]
        render json: data
      end

      def init_pros_count
        result = Charts::Pros::ProsCount.call(params: params)
        result.success? ? result.data : []
      end

      def init_canceled_orders
        result = Charts::Pros::CanceledOrdersCount.call(params: params)
        result.success? ? result.data : []
      end

      def init_completed_orders
        result = Charts::Pros::CompletedOrdersCount.call(params: params)
        result.success? ? result.data : []
      end

      def init_accepted_orders
        result = Charts::Pros::AcceptedOrdersCount.call(params: params)
        result.success? ? result.data : []
      end

      def init_orders_by_client_type
        result = Charts::Pros::OrdersCountByClientType.call(params: params)
        result.success? ? result.data : []
      end

      def init_orders_by_category
        result = Charts::Pros::OrdersByCategory.call(params: params)
        result.success? ? result.data : []
      end

      def init_average_time_orders
        result = Charts::Pros::AverageTimeOrders.call(params: params)
        result.success? ? result.data : []
      end

      def init_pros_by_type
        result = Charts::Pros::ProsByType.call(params: params)
        result.success? ? result.data : []
      end

      def assign_vars_to_frontend
        gon.pros_count = init_pros_count
        gon.canceled_orders = init_canceled_orders
        gon.completed_orders = init_completed_orders
        gon.accepted_orders = init_accepted_orders
        gon.orders_by_client_type = init_orders_by_client_type
        gon.orders_by_category = init_orders_by_category
        gon.average_time_orders = init_average_time_orders
        gon.pros_by_type = init_pros_by_type
        gon.cities = @cities
      end

      def master_id
        params[:master_id]
      end

      def range
        params[:range].split.map { |e| e.downcase }.join('_')
      end

      def set_default_range
        params[:range] = 'Last 7 Days'
      end
    end
  end
end