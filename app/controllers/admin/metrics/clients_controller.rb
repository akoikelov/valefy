module Admin
  module Metrics
    class ClientsController < ApplicationController
      before_action :set_default_range

      layout 'panel'

      def not_bussiness
        @users = User::Client.where(corp: false)
        set_is_corp(false)
        last_month_orders_clients_count
        assign_vars_to_frontend
      end

      def bussiness
        @users = User::Client.where(corp: true)
        set_is_corp(true)
        last_month_orders_clients_count
        assign_vars_to_frontend
        gon.clients_count_by_segment = init_clients_by_segment
      end

      def pros

      end

      def last_n_days
        result = Charts::LastNDays.call(params: params)
        if result.success?
          respond_to do |format|
            format.json { render json: @data }
          end
        else
          render json: []
        end
      end

      private

      def average_value(cat, prices, completed_orders)
        prices.present? ? ["#{cat.title}", (prices.sum / completed_orders.count).round(2)] : ["#{cat.title}", 0]
      end

      def get_average_check
        result = Charts::AveragePrice.call(params: params)
        result.success? ? result.data : []
      end

      def init_percentage_orders_and_categories
        result = Charts::PercentageOrdersCategories.call(params: params)
        result.success? ? result.data : []
      end

      def init_last_n_days_order
        result = Charts::LastNDays.call(params: params)
        result.success? ? result.data : []
      end

      def init_orders_by_platform
        result = Charts::CreatedOrderByPlatform.call(params: params)
        result.success? ? result.data : []
      end

      def init_orders_by_client_type
        result = Charts::OrdersCountByClients.call(params: params)
        result.success? ? result.data : []
      end

      def init_clients_by_type
        result = Charts::ClientsByType.call(params: params)
        result.success? ? result.data : []
      end

      def init_clients_retention
        result = Charts::ClientsRetention.call(params: params)
        result.success? ? result.data : []
      end

      def init_order_period_values
        params[:period_type] = 'hours'
        result = Charts::OrderPeriodValues.call(params: params)
        result.success? ? result.data : []
      end

      def last_month_orders_clients_count
        result = Charts::LastMonthOrdersClientsCount.call(params: params)
        if result.success?
          @users_count = result.users_count
          @orders_count = result.orders_count
        else
          @users_count = 0
          @orders_count = 0
        end
      end

      def orders_frequency
        result = Charts::OrdersFrequency.call(params: params)
        result.success? ? result.data : []
      end

      def init_small_large_car_orders
        result = Charts::SmallLargeCars.call(params: params)
        result.success? ? result.data : []
      end

      def init_profit_on_services
        result = Charts::ProfitOnServices.call(params: params)
        result.success? ? result.data : []
      end

      def init_most_ordered_service
        result = Charts::MostOrderedService.call(params: params)
        result.success? ? result.data : []
      end

      def init_clients_by_gender
        result = Charts::ClientsByGender.call(params: params)
        result.success? ? result.data : []
      end

      def init_clients_by_age
        result = Charts::ClientsByAge.call(params: params)
        result.success? ? result.data : []
      end

      def orders_by_company_platforms
        result = Charts::OrdersCountByPlatforms.call(params: params)
        result.success? ? result.data : []
      end

      def init_cars_count_by_type
        result = Charts::CarsCountByType.call(params: params)
        result.success? ? result.data : []
      end

      def init_turnover_by_platfroms
        result = Charts::TurnoverByPlatfroms.call(params: params)
        result.success? ? result.data : []
      end

      def init_clients_by_segment
        result = Charts::ClientsBySegment.call(params: params)
        result.success? ? result.data : []
      end

      def assign_vars_to_frontend
        gon.orders_by_platform           = init_orders_by_platform
        gon.categories                   = init_percentage_orders_and_categories
        gon.average_cheque               = get_average_check
        gon.number_of_orders_client_type = init_orders_by_client_type
        gon.number_of_client_by_type     = init_clients_by_type
        gon.last_n_days_order            = init_last_n_days_order
        gon.ios_android_retentions       = init_clients_retention
        gon.order_periods                = init_order_period_values
        gon.orders_frequency             = orders_frequency
        gon.small_large_cars             = init_small_large_car_orders
        gon.profit_on_services           = init_profit_on_services
        gon.most_ordered_service         = init_most_ordered_service
        gon.clients_by_gender            = init_clients_by_gender
        gon.clients_by_age               = init_clients_by_age
        gon.orders_by_company_platforms  = orders_by_company_platforms
        gon.cars_count_by_type           = init_cars_count_by_type
        gon.turnover_by_platfroms        = init_turnover_by_platfroms
      end

      def range
        params[:range].split.map { |e| e.downcase }.join('_')
      end

      def set_default_range
        params[:range] = 'Last 7 Days'
      end

      def set_is_corp(is_corp)
        params[:is_corp] = is_corp
      end
    end
  end
end