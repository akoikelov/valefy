class AdminImagesController < ApplicationController

  def destroy
    if params[:class].present?
      _class = params[:class]
      object = _class.constantize.find(params[:resource])
      method = "remove_#{params[:field]}!"
      object.send(method)
      object.save
    else
      banner = Banner.find(params[:resource])
      banner.remove_image!
      banner.save
    end


    redirect_to :back
  end
end
