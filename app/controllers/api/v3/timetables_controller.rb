module Api
  module V3
    class TimetablesController < Api::V2::BaseController
      def index
        result = AvailableSchedule.call(
          date:                 permit_params[:date],
          current_hour_minutes: permit_params[:current_hour_minutes]
        )

        if result.success?
          render json: {
            success: true,
            data: {}.merge(result.schedule_day)
          }
        else
          render json: { success: false, error: 'Something wrong' }
        end
      rescue => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def permit_params
        params.permit(:date, :current_hour_minutes)
      end
    end
  end
end
