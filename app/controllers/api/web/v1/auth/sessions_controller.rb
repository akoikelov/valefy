# Manage user sessions which provides sign in and sign out methods
# /api/v1/auth/sign_in
# /api/v1/auth/sign_out
class Api::Web::V1::Auth::SessionsController < DeviseTokenAuth::SessionsController
  before_action :set_user_by_token, only: [:destroy]
  after_action :reset_session, only: [:destroy]

  protected

  def render_new_error
    render json: {
      success: false,
      error: I18n.t("devise_token_auth.sessions.not_supported")
    }, status: 405
  end

  def render_create_success
    render json: {
      success: true,
      data: @resource.custom_token_validation_response
    }
  end

  def render_create_error_not_confirmed
    render json: {
      success: false,
      error: I18n.t("devise_token_auth.sessions.not_confirmed")
    }, status: 401
  end

  def render_create_error_bad_credentials
    render json: {
      success: false,
      error: I18n.t("devise_token_auth.sessions.bad_credentials")
    }, status: 401
  end

  def render_destroy_success
    render json: {
      success: true
    }, status: 200
  end

  def render_destroy_error
    render json: {
      success: false,
      error: I18n.t("devise_token_auth.sessions.user_not_found")
    }, status: 404
  end

  private

  def resource_params
    params.permit(*params_for_resource(:sign_in))
  end

end
