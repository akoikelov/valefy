module Api
  module Masters
    module V1
      # Feedback comment and rating
      class MasterFeedbacksController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def create
          result = Order::Feedback::CreateMasterFeedback.call(
            params: feedback_params,
            current_user: current_master
          )

          if result.success?
            render json: { success: true, data: result.feedback }, status: :ok
          else
            render json: { success: false, error: result.errors }, status: :ok
          end

        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")


          render json: { success: false, error: 'Record not found' }, status: :ok
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def feedback_params
          params.permit(:order_item_id, :rating, :message)
        end
      end
    end
  end
end
