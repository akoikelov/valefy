module Api
  module Masters
    module V1
      # order item controller for create each order.
      class SchedulesController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/schedules [GET]
        def index
          result = Api::Masters::V2::OrdersForSchedule.call(
            current_user: current_master
          )

          if result.success?
            render json: {
              success: true,
              data: {
                available: ActiveModelSerializers::SerializableResource.new(
                  result.available,
                  each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                ),
                active: ActiveModelSerializers::SerializableResource.new(
                  result.active,
                  each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                ),
              }
            }
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        end
      end
    end
  end
end
