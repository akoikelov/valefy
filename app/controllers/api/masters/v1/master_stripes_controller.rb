module Api
  module Masters
    module V1
      class MasterStripesController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def update
          result = Master::AddBankAccount.call(
            params: permit_params, current_user: current_master
          )

          if result.success?
            render json: {
              success: true,
              data: result.info
            }, status: :ok
          else
            render json: {
              success: false,
              error: result.error
            }, status: :ok
          end

        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        def show
          # TODO need hard refactor

          result = ManagedAccount.find(params[:id])

          if result.present?
            render json: {
              success: true,
              data: result
            }, status: :ok
          end

        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Record Not Found' }, status: 200
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:routing_number, :account_number)
        end
      end
    end
  end
end
