module Api
  module Masters
    module V1
      class TimetablesController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def index
          render json: {
            success: true,
            data: {
              'Monday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ],
              'Tuesday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ],
              'Wednesday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ],
              'Thursday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ],
              'Friday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ],
              'Saturday': [
                { time: '8:00', available: true },
                { time: '8:30', available: true },
                { time: '9:00', available: true },
                { time: '9:30', available: true },
                { time: '10:00', available: true },
                { time: '10:30', available: true },
                { time: '11:00', available: true },
                { time: '11:30', available: true },
                { time: '12:00', available: true },
                { time: '12:30', available: true },
                { time: '13:00', available: true },
                { time: '13:30', available: true },
                { time: '14:00', available: true },
                { time: '14:30', available: true },
                { time: '15:00', available: true },
                { time: '15:30', available: true },
                { time: '16:00', available: true },
                { time: '16:30', available: true },
                { time: '17:00', available: true },
                { time: '17:30', available: true },
                { time: '18:00', available: true },
                { time: '18:30', available: true },
                { time: '19:00', available: true }
              ]
            }
          }
        end

        private

        def permit_params
          params.permit(:image)
        end
      end
    end
  end
end
