module Api
  module Masters
    module V1
      module OrderItems
        # Cancel order
        class AcceptsController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          # /api/masters/v1/order_items/accepts [POST]
          def create
            result = Accept.call(
              params: permit_params, current_user: current_master
            )

            if result.success?
              order = result.order
              run_push_worker(order, I18n.t('order_item.messages.accepted'))

              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  result.order,
                  serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                )
              }, status: :ok
            else
              render json: {
                success: false,
                error: result.error,
                code: result.code
              }, status: :ok
            end
          rescue ActiveRecord::RecordNotFound => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: 'Order Not Found' }, status: 200
          rescue => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: 'Something wrong' }, status: 500
          end

          private

          def run_push_worker(order, title)
            unless Rails.env.eql?('test')
              Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
            end
          end

          def permit_params
            params.permit :order_id
          end
        end
      end
    end
  end
end
