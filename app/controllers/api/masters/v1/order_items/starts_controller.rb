module Api
  module Masters
    module V1
      module OrderItems
        class StartsController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          # /api/v1/order_items/start [POST]
          # set status (in_progress) for order
          def create
            result = Start.call(
              current_user: current_master,
              params:       permit_params
            )

            if result.success?
              order = result.order
              run_push_worker order, I18n.t('order_item.messages.starts')

              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  result.order,
                  serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                )
              }, status: :ok
            end

          rescue ActiveRecord::RecordNotFound => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Record not found'
            }, status: :ok
          rescue => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Something wrong'
            }, status: :ok
          end

          private

          def permit_params
            params.permit :order_id, :before_work_images => []
          end

          def run_push_worker order, title
            unless Rails.env.eql?('test')
              Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
            end
          end
        end
      end
    end
  end
end
