module Api
  module Masters
    module V1
      module Users
        # show profile of master
        class ClientsController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          # /api/masters/v1/users/clients/:client_id [GET]
          def show
            client = ::User::Client.find_by(id: permit_params[:client_id])

            if client.present?
              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  client,
                  each_serializer: ::Api::Masters::V1::User::ClientSerializer
                )
              }, status: :ok
            else
              render json: {
                success: false,
                error: 'Not found'
              }, status: :ok
            end

          rescue => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: 'Something wrong' }, status: 500
          end

          private

          def permit_params
            params.permit :client_id
          end
        end
      end
    end
  end
end
