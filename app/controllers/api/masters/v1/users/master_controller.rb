module Api
  module Masters
    module V1
      module Users
        # show profile of master
        class MasterController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          def index
            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                current_master,
                serializer: ::Api::Masters::V1::MasterSerializer
              )
            }
          rescue => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: 'Something wrong' }, status: 500
          end

          # /api/masters/v1/users/master/:id [GET]
          def show

            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                current_master,
                serializer: ::Api::Masters::V1::MasterSerializer
              )
            }
          rescue => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: 'Something wrong' }, status: 500
          end
        end
      end
    end
  end
end
