module Api
  module Masters
    module V1
      class JoinMastersController < Api::Masters::V1::BaseController
        before_action :check_params

        def create
          result = JoinMaster.call(params: permit_params)

          if result.success?
            render json: { success: true }, status: 201
          else
            render json: { success: false, error: result.error }, status: :ok
          end

        rescue StandardError => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:name, :phone_number, :email, :category_id, :cv, :id_document)
        end

        def check_params
          render json: { success: false, error: 'unable params' }, status: :ok unless permit_params.present?
        end
      end
    end
  end
end
