module Api
  module Masters
    module V1
      class OndutiesController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/onduty [PUT]
        # update onduty value of master
        def update
            result = Master::OndutyUpdate.call(
              params: permit_params, current_user: current_master
            )

            if result.success?
              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  current_master,
                  serializer: ::Api::Masters::V1::MasterSerializer
                )
              }, status: 200
            else
              render json: {
                success: false,
                error: result.error
              }, status: 200
            end
        rescue StandardError => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: {
            success: false,
            error: 'Something wrong'
          }, status: 500
        end

        private

        def permit_params
          params.permit(:onduty)
        end
      end
    end
  end
end
