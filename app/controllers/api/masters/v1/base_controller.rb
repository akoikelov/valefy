class Api::Masters::V1::BaseController < Api::BaseController
  helper_method :authenticate_master!, :current_master

  def authenticate_master!
    authenticate_api_masters_v1_user_service_man!
  end

  def current_master
    current_api_masters_v1_user_service_man
  end
end
