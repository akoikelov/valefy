module Api
  module Masters
    module V1
      # Additional Payments
      class AdditionalPaymentsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/
        def show
          ap = AdditionalPayment.find(params[:id])
          render json: { success: true, data: ap }, status: :ok
        rescue => e
          Raven.capture_exception(e)
          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        # /api/masters/v1/additional_payments [POST]
        # price
        # order_id
        def create
          result = AdditionalPayments::CreateOrUpdate.call(
            order_id: permit_params[:order_id],
            additional_price: permit_params[:additional_price],
            description: permit_params[:description]
          )

          if result.success?
            order = result.order
            run_push_worker order

            render json: { success: true, data: order.additional_payment }, status: :ok
          else
            render json: { success: false, error: result.error, code: result.code}, status: :ok
          end
        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Record not found' }, status: :ok
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :order_id, :additional_price, :description
        end

        def run_push_worker order
          AdditionalPaymentWorker.perform_async(order_id: order.id) unless Rails.env.eql?('test')
        end
      end
    end
  end
end
