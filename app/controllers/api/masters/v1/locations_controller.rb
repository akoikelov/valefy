module Api
  module Masters
    module V1
      class LocationsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def create
          location = current_master.locations.where(lat: permit_params[:lat], lon: permit_params[:lon]).first_or_initialize

          if location.save
            render json: { success: true }, status: 200
          else
            render json: { success: false, error: location.errors }, status: 200
          end
        rescue StandardError => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:lon, :lat)
        end
      end
    end
  end
end
