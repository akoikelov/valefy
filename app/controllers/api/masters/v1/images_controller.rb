module Api
  module Masters
    module V1
      class ImagesController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def create
          result = CreateImage.call(params: permit_params)

          if result.success?
            render json: { success: true, data: result.link }, status: 200
          else
            render json: { success: false, error: result.error }, status: 200
          end

        rescue StandardError => e
          Raven.capture_exception(e)
          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong!' }, status: 500
        end

        private

        def permit_params
          params.permit(:image)
        end
      end
    end
  end
end
