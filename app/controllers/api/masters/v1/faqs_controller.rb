module Api
  module Masters
    module V1
      # Get answers
      class FaqsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/faqs [GET]
        def index
          faqs = Faq.master

          render json: { success: true, data: faqs }, status: :ok
        end
      end
    end
  end
end
