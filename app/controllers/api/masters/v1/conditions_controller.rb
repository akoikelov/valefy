module Api
  module Masters
    module V1
      # Get data about Company
      class ConditionsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!
        before_action :create_first_condition

        def index
          text = TermCondition.last

          render json: { success: true, data: text.condition }, status: :ok
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def create_first_condition
          if TermCondition.count.zero?
            TermCondition.create! condition: 'Text olo condition'
          end
        end
      end
    end
  end
end
