module Api
  module Masters
    module V1
      module Auth
        # /api/v1/auth/passwordsms POST API
        # For reset password
        class PasswordsmsController < Api::Masters::V1::BaseController
          skip_after_action :update_auth_header, only: [:create]

          def create
            return render json: { success: false, error: 'Missing parametr - phone number' }, status: :ok if phone.nil?
            return render json: { success: false, error: 'Missing parametr - password confirmation' }, status: :ok if password_confirmation.nil?

            @resource = User::ServiceMan.find_by_phone_number(phone)

            unless password_same?
              return render json: { success: false, error: 'Passwords are not same' }, status: :ok
            end

            unless token_validate?
              return render json: { success: false, error: 'Reset password token is valid' }, status: :ok
            end

            @resource.reset_password(password, password_confirmation)

            return render json: { success: true }

          rescue => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            return render json: { success: false, error: 'Something wrong' }, status: 500
          end

          private

          def token_validate?
            @resource.reset_password_token == resource_params[:reset_password_token]
          end

          def password_same?
            password == password_confirmation
          end

          def password
            resource_params[:password]
          end

          def password_confirmation
            resource_params[:password_confirmation]
          end

          def phone
            resource_params[:phone_number]
          end

          def resource_params
            params.permit(:password,
                          :password_confirmation,
                          :phone_number,
                          :reset_password_token)
          end
        end
      end
    end
  end
end
