# Manage user registrations. Overrides error methods for change status code.
# /api/v1/auth
class Api::Masters::V1::Auth::RegistrationsController < DeviseTokenAuth::RegistrationsController

  def update
    if @resource
      if @resource.send(resource_update_method, account_update_params)
        @resource.update(confirm_phone_number: false) if changed_phone?
        yield @resource if block_given?
        render_update_success
      else
        render_update_error
      end
    else
      render_update_error_user_not_found
    end
  end

  def render_create_success
    send_welcome_email

    render json: {
      success: true,
      data:   resource_data
    }
  end

  def render_create_error_missing_confirm_success_url
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.missing_confirm_success_url')
    }, status: 422
  end

  def render_create_error_redirect_url_not_allowed
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.redirect_url_not_allowed')
    }, status: 422
  end

  def render_create_error
    render json: {
      success: false,
      data:   resource_data,
      error: message(resource_errors)
    }, status: 422
  end

  def render_create_error_email_already_exists
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.email_already_exists')
    }, status: 422
  end

  def render_update_success
      render json: {
        success: true,
        data:   resource_data
      }
    end

  def render_update_error
    render json: {
      success: false,
      error: message(resource_errors)
    }, status: 422
  end

  def render_update_error_user_not_found
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.registrations.user_not_found')
    }, status: 404
  end

  def validate_post_data which, message
    render json: {
       success: false,
       error: message
    }, status: :unprocessable_entity if which.empty?
  end

  private

  def sign_up_params
    params.permit(:email, :password, :password_confirmation, :phone_number,
                  :type, :name, :surname, :last_name, :b_day, :image)
  end

  def account_update_params
    params.permit(:email, :phone_number, :name, :nickname, :surname,
                  :last_name, :b_day, :image)
  end

  def message resource_errors
    resource_errors[:full_messages].first if resource_errors[:full_messages].any?
  end

  def changed_phone?
    if @resource.phone_number == User::PHONE_NOT_EXIST
      false
    elsif @resource.phone_number == account_update_params[:phone_number]
      true
    end
  end

  def send_welcome_email
    @resource.send_welcome_email
  end
end
