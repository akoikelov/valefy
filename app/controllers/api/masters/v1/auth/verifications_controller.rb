module Api
  module Masters
    module V1
      module Auth
        class VerificationsController < Api::Masters::V1::BaseController
          skip_before_filter :verify_authenticity_token

          def generate
            result = CheckAndSendSms.call(params: generate_params, object: ::User::ServiceMan)

            if result.success?
              render json: { success: true }, status: :ok
            else
              render json: { success: false, error: result.error }, status: :ok
            end

          rescue StandardError => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { status: false, error: 'Something wrong' }, status: 500
          end

          def verify_code
            result = VerificationCode.call(params: verify_params, object: ::User::ServiceMan)

            if result.success?
              render json: { success: true, message: I18n.t('phone_verification.success_verified') }, status: :ok
            else
              render json: { success: false, error: result.error }, status: :ok
            end

          rescue StandardError => e
            Raven.capture_exception(e)
            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { status: false, message: 'Something wrong' }, status: 500
          end

          private

          def generate_params
            { phone_number:  params[:phone_number] }.merge(sinch_config_params)
          end

          def sinch_config_params
            {
              sinch_app_key:    '03c27960-26a5-462f-b9ea-77ff79f8c63f',
              sinch_app_secret: 'l9qPzBxZsEGGuFhR8wTHqw=='
            }
          end

          def verify_params
            { phone_number: params[:phone_number], code: params[:code] }
          end
        end
      end
    end
  end
end
