module Api
  module Masters
    module V1
      module Auth
        module Passwordsms
          class VerifyController < Api::Masters::V1::BaseController
            skip_after_action :update_auth_header, only: [:create, :update]

            def create
              result = VerificationCode.call(params: resource_params, object: ::User::ServiceMan)

              @resource = User.find_by_phone_number(resource_params[:phone_number])

              if @resource
                short, token = Devise.token_generator.generate(@resource.class, :reset_password_token)
                @resource.update(reset_password_token: token)

                if result.success?
                  response.headers.merge!({ 'reset_password_token' => token })

                  render json: { success: true }, status: :ok
                else
                  render json: { success: false, error: result.error }, status: :ok
                end
              end

            rescue => e
              Raven.capture_exception(e)
              Rails.logger.error("#{e.message}: #{e.backtrace}")

              render json: { success: false, error: 'Something wrong' }, status: 500
            end

            private

            def resource_params
              params.permit(:code, :phone_number)
            end
          end
        end
      end
    end
  end
end
