module Api
  module Masters
    module V1
      module Auth
        module Passwordsms
          class SendController < Api::Masters::V1::BaseController
            skip_after_action :update_auth_header, only: [:create]

            def create
              # if phone present then send code sms
              if resource_params[:phone_number].nil?
                return render json: { success: false, error: 'You must provide the phone_number.' }, status: :bad_request
              end

              resource = ::User.where(phone_number: resource_params[:phone_number])

              if resource
                resource = resource.first
                result   = CheckAndSendSms.call(params: sms_params, object: ::User::ServiceMan)

                if result.success?
                  return render json: { success: true }, status: :ok
                else
                  return render json: { success: false, error: result.error }, status: :ok
                end
              else
                return render json: { success: false, error: 'user with number does not exist' }, status: :ok
              end

            rescue => e
              Raven.capture_exception(e)

              Rails.logger.error("#{e.message}: #{e.backtrace}")
              render json: { success: false, error: 'Something wrong' }, status: 500
            end

            private

            def resource_params
              params.permit(:phone_number)
            end

            def sms_params
              { phone_number: resource_params[:phone_number] }.merge(sinch_config_params)
            end

            def sinch_config_params
              {
                sinch_app_key:    '03c27960-26a5-462f-b9ea-77ff79f8c63f',
                sinch_app_secret: 'l9qPzBxZsEGGuFhR8wTHqw=='
              }
            end
          end
        end
      end
    end
  end
end
