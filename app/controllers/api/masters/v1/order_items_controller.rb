module Api
  module Masters
    module V1
      # order item controller for create each order.
      class OrderItemsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/order_items [GET]
        # List order items of master
        def index
          result = OrdersList.call(current_user: current_master, params: params)
          orders = result.orders_list
          orders = orders.order(id: :desc) #.page(params[:page]).per(OrderItem::COUNT_ORDERS_PER_PAGE) # TODO refactor

          if result.success?
            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                orders,
                each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
              )
            }
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        end

        # /api/v1/order_items/:id [GET]
        # order item show method
        def show
          order = OrderItem.find(params[:id])

          render json: {
            success: true,
            data: ActiveModelSerializers::SerializableResource.new(
              order,
              each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
            )
          }

        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Record Not Found' }, status: 200
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        def update
          result = UpdateOrderItem.call(
            params: update_order_params,
            current_user: current_master
          )

          if result.success?
            order = result.order

            render json: {
              success: true,
              data:  ActiveModelSerializers::SerializableResource.new(
                order,
                each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
              )
            }, status: :ok
          else
            render json: { success: false }, status: :ok
          end

        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Record not found' }, status: 200
        rescue StandardError => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def orders_params
          params.permit(:status, :time_zone)
        end

        def order_params
          params.permit(:address, :service_id, :description,
                        :order_now, :order_time)
                        .merge(images: params[:images])
        end

        def update_order_params
          params.permit(:start_work, :time_zone).merge({ id: params[:id] })
        end
      end
    end
  end
end
