module Api
  module Masters
    module V1
      # FAQ
      class HelpsController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        # /api/masters/v1/helps [POST]
        def create
          result = HelpCreate.call(
            params: help_params,
            current_user: current_master
          )

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def help_params
          params.permit(:question)
        end
      end
    end
  end
end
