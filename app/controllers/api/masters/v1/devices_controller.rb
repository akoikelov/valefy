module Api
  module Masters
    module V1
      class DevicesController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          def create

            result = Firebase.call(
              current_user: current_master,
              params: params
            )

            if result.success?
              render json: { success: true }, status: :created
            else
              render json: { success: false, error: result.error }, status: 200
            end
          rescue => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: { success: false, error: e.message }, status: 500
          end

          private

          def permit_params
            params.permit(:id_client_firebase, :platform)
          end
      end
    end
  end
end
