module Api
  module Masters
    module V1
      # Get data about Company
      class ContactsController < Api::Masters::V1::BaseController

        def index
          contacts = Contact.all

          render json: { success: true, data: contacts }, status: :ok
        end
      end
    end
  end
end
