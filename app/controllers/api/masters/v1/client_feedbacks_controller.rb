module Api
  module Masters
    module V1
      # Feedback comment and rating
      class ClientFeedbacksController < Api::Masters::V1::BaseController
        before_action :authenticate_master!

        def create
          result = Order::Feedback::CreateClientFeedback.call(params: feedback_params)

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.errors }, status: :ok
          end
        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)
          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: e.message }, status: :ok
        rescue => e
          Raven.capture_exception(e)
          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: e.message }, status: 500
        end

        private

        def feedback_params
          params.permit(:order_item_id, :client_id, :rating, :comment)
        end
      end
    end
  end
end
