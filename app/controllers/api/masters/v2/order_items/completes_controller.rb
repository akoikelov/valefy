module Api
  module Masters
    module V2
      module OrderItems
        class CompletesController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          # /api/v1/order_items/completes [POST]
          # Complete order with two steps
          # 1) Complete and not paid
          # 2) Complete and paid
          def create
            result = Complete.call(
              params: permit_params
            )

            if result.success?
              order = result.order
              run_workers(order, I18n.t('order_item.messages.completed'))

              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  result.order,
                  serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                )
              }
            else
              render json: {
                success: false,
                error: result.error
              }, status: 200
            end

          rescue ActiveRecord::RecordNotFound => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Record not found'
            }, status: :ok
          rescue => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Something wrong'
            }, status: 500
          end

          private

          def run_workers(order, title)
            unless Rails.env.eql?('test')
              ::Api::V1::PushNotificationsWorker.perform_async(order_id: order.id, title: title)
              ::Api::V1::SendInvoiceWorker.perform_async(order_id: order.id) if Setting.first.send_email_invoice_client?
            end
          end

          def permit_params
            params.permit :order_id, :card_id, :after_work_images => []
          end
        end
      end
    end
  end
end
