module Api
  module Masters
    module V2
      module OrderItems
        class StartsController < Api::Masters::V1::BaseController
          before_action :authenticate_master!

          # /api/v2/order_items/start [POST]
          # set status (in_progress) for order
          def create
            result = Start.call(
              current_user: current_master,
              params:       permit_params
            )

            if result.success?
              order = result.order

              render json: {
                success: true,
                data: ActiveModelSerializers::SerializableResource.new(
                  order,
                  serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
                )
              }
            end

          rescue ActiveRecord::RecordNotFound => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Record not found'
            }, status: :ok
          rescue => e
            Raven.capture_exception(e)

            Rails.logger.error("#{e.message}: #{e.backtrace}")

            render json: {
              success: false,
              error: 'Something wrong'
            }, status: :ok
          end

          private

          def permit_params
            params.permit :order_id, :before_work_images => []
          end
        end
      end
    end
  end
end
