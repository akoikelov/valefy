module Api
  module Masters
    module V2
      class PermitGoogleCalendarsController < Api::Masters::V2::BaseController
        before_action :authenticate_master!

        def create
          google = ::GoogleCalendar.new permit_params[:refresh_token], current_master, nil

          client = google.authorization_client
          result = google.retrieve_events client

          if result
            render json: { success: true }, status: :ok
          else
            render json: { success: false }, status: :ok
          end
        end

        private

        def permit_params
          params.permit :refresh_token
        end
      end
    end
  end
end
