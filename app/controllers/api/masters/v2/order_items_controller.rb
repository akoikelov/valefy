module Api
  module Masters
    module V2
      # order item controller for create each order.
      class OrderItemsController < Api::Masters::V2::BaseController
        before_action :authenticate_master!

        # /api/masters/v2/order_items [GET]
        # List order items of master
        def index
          result = OrdersList.call(current_user: current_master, params: params)
          orders = result.orders_list

          if result.success?
            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                orders,
                each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
              )
            }
          else
            render json: { success: false, error: result.error }, status: :ok
          end

        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        # /api/v2/order_items/:id [GET]
        # order item show method
        def show
          order = OrderItem.find(params[:id])

          render json: {
            success: true,
            message: 'Order details',
            data: ActiveModelSerializers::SerializableResource.new(
              order,
              each_serializer: ::Api::Masters::V1::OrderItemSerializer, time_zone: params[:time_zone]
            )
          }

        rescue ActiveRecord::RecordNotFound => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Record Not Found' }, status: 200
        rescue => e
          Raven.capture_exception(e)

          Rails.logger.error("#{e.message}: #{e.backtrace}")

          render json: { success: false, error: 'Something wrong' }, status: 500
        end
      end
    end
  end
end
