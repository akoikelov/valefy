module Api
  module V2
    class PartnersController < Api::V2::BaseController

      def index
        render json: {
          success: true,
          data: ActiveModelSerializers::SerializableResource.new(
            Partner.all,
            each_serializer: PartnerSerializer
          )
        }
      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong!' }, status: 500
      end
    end
  end
end
