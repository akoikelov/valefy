module Api
  module V2
    # blog
    class OfficesController < Api::V2::BaseController
      before_action :authenticate_user!

      def index
        offices = current_user.offices

        render json: {
          success: true,
          data: ActiveModelSerializers::SerializableResource.new(
            offices,
            each_serializer: OfficeSerializer
          )
        }

      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end
    end
  end
end
