module Api
  module V2
    # Get data about Company
    class ConditionsController < Api::V2::BaseController
      # before_action :authenticate_user!
      before_action :create_first_condition

      def index
        text = TermCondition.first

        render json: { success: true, condition: text.condition }, status: :ok
      rescue => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def create_first_condition
        if TermCondition.count.zero?
          TermCondition.create! condition: 'Text olo condition'
        end
      end
    end
  end
end
