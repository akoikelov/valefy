module Api
  module V2
    module Customer
      # Manage cards of customer
      class CardsController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/customer/cards API [GET]
        # Return list cards of Customer
        def index
          result = Card::ListCardCustomer.call(current_user: current_user)

          if result.success?
            render json: { success: true, data: result.list }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        # /api/v2/customer/cards/:id API [PUT]
        # Update customer and set default card
        def update
          result = Update.call(
            current_user: current_user, card_id: params[:id]
          )

          if result.success?
            render json: { success: true, data: result.card }, status: :ok
          else
            render json: { success: false, error: '' }, status: :ok
          end
        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        # /api/v2/customer/cards/id API [DELETE]
        # Destroy card of customer
        def destroy
          result = Card::Delete.call(
            current_user: current_user, card_id: params[:id]
          )

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        # /api/v2/customer/cards/default
        # Return default card of client
        def default
          result = Card::Default.call(current_user: current_user)

          if result.success?
            render json: { success: true, data: result.card }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end
      end
    end
  end
end
