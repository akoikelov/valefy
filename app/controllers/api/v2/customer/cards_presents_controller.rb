module Api
  module V2
    module Customer
      # /api/v2/customer/cards_present API [GET]
      # Check exit or not exist customer's card
      class CardsPresentsController < Api::V2::BaseController
        before_action :authenticate_user!

        def show
          result = Card::CheckCustomerCard.call(current_user: current_user)

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: 'Not found' }, status: 200
          end

        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end
      end
    end
  end
end
