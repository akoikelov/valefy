module Api
  module V2
    module Customer
      class AccountsController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/customer [POST]
        # Save create Customer
        def create
          result = Api::V2::Customer::CreateCustomer.call(
            params: permit_params, current_user: current_user
          )

          if result.success?
            render json: { success: true, data: result.card }, status: :created
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:stripe_token)
        end
      end
    end
  end
end
