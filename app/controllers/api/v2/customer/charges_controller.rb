module Api
  module V2
    module Customer
      # Charges customer
      class ChargesController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/customer/charges [POST]
        def create
          result = TakeMoney.call(
            params: params, current_user: current_user
          )

          if result.success?
            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                result.order,
                serializer: ::Api::V2::OrderItemSerializer, time_zone: params[:time_zone]
              )
            }
          else
            render json: { success: false, error: result.error }, status: :ok
          end

        rescue Stripe::CardError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::RateLimitError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue Stripe::InvalidRequestError => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        rescue ActiveRecord::RecordNotFound => e
          capture_exception(e)

          render json: { success: false, error: 'Record Not Found' }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :card_id, :order_id, :promocode_id
        end
      end
    end
  end
end
