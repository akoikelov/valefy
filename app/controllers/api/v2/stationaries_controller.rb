class Api::V2::StationariesController < Api::V2::BaseController
  before_action :authenticate_user!

  def index
    stationaries = CompanyLocation.all

    render json: {
      success: true,
      data: ActiveModelSerializers::SerializableResource.new(
        stationaries,
        each_serializer: ::Api::V2::CompanyLocationSerializer,
        service_id: permit_params[:service_id]
      )
    }
  rescue => e
    capture_exception(e)

    render json: { success: false, error: 'Something wrong' }, status: 500
  end

  def show
    cs = CompanyLocation.find(permit_params[:id])

    render json: {
      success: true,
      data: ActiveModelSerializers::SerializableResource.new(
        cs,
        serializer: ::Api::V2::CompanyLocationSerializer,
        service_id: permit_params[:service_id]
      )
    }
  end

  private

  def permit_params
    params.permit(:id, :service_id)
  end
end