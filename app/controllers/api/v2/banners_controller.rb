module Api
  module V2
    class BannersController < Api::V2::BaseController
      before_action :authenticate_user!

      def index
        banners = Banner.all

        render json: {
          success: true,
          data: ActiveModelSerializers::SerializableResource.new(
            banners,
            each_serializer: BannerSerializer
          )
        }

      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong!' }, status: 500
      end
    end
  end
end
