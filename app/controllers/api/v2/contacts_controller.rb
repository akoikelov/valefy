module Api
  module V2
    # Get data about Company
    class ContactsController < Api::V2::BaseController

      def index
        contacts = Contact.all

        render json: { success: true, contacts: contacts }, status: :ok
      end
    end
  end
end
