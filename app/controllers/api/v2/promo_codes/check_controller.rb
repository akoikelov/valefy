module Api
  module V2
    module PromoCodes
      class CheckController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/promo_codes/check [POST]
        def create
          result = Check.call(params: permit_params)

          if result.success?
            render json: { success: true, data: result.data }, status: :ok
          else
            render json: { success: false, error: result.error, code: result.code }, status: :ok
          end
        rescue ActiveRecord::RecordNotFound => e
          capture_exception(e)

          render json: { success: false, error: 'Order Not Found', code: 'service_not_found' }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong', code: 'server_error' }, status: 500
        end

        private

        def permit_params
          params.permit(:order_id, :promo_code)
        end
      end
    end
  end
end