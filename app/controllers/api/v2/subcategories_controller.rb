# Controller which return list of category and sub category
# /api/v2/categories
# /api/v2/categories/:id_category

class Api::V2::SubcategoriesController < Api::V2::BaseController
  def index
    categories = Subcategory.where(category_id: params[:q][:category_id_eq])

    render json: categories , each_serializer: ::Api::V2::SubcategorySerializer
  end

  def show
    category = Category.find(params[:id])

    render json: category.subcategories, each_serializer: ::Api::V2::CategorySerializer
  end
end
