module Api
  module V2
    class ImagesController < Api::V2::BaseController
      before_action :authenticate_user!

      def create
        result = CreateImage.call(params: permit_params)

        if result.success?
          render json: { success: true, link: result.link, data: result.link }, status: 201
        else
          render json: { success: false, error: result.error }, status: :ok
        end

      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong!' }, status: 500
      end

      private

      def permit_params
        params.permit(:image)
      end
    end
  end
end
