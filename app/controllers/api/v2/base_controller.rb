class Api::V2::BaseController < Api::BaseController
  include ApplicationHelper
  include ErrorsHelper

  helper_method :authenticate_user!, :current_user

  def authenticate_user!
    authenticate_api_v2_user_client!
  end

  def current_user
    current_api_v2_user_client
  end

  def push_notification_send(params)
    unless Rails.env.eql?('test')
      class_worker = params[:worker]
      order        = params[:order]
      title        = params[:title]

      class_worker.constantize.perform_async(order_id: order.id, title: title)
    end
  end
end
