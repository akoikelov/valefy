module Api
  module V2
    # blog
    class PostsController < BaseController

      def index
        posts = Post.all

        render json: {
          success: true,
          data: ActiveModelSerializers::SerializableResource.new(
            posts,
            each_serializer: PostSerializer
          )
        }

      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end
    end
  end
end
