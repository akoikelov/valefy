class Api::V2::Auth::PasswordsController < DeviseTokenAuth::PasswordsController
  def render_update_error_unauthorized
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.failure.unauthorized')
    }, status: 401
  end

  def render_update_error_password_not_required
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.passwords.password_not_required')
    }, status: 200
  end

  def render_update_error_missing_password
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.passwords.missing_passwords')
    }, status: 200
  end

  def render_update_success
    render json: {
      success: true,
      data: resource_data,
      message: 'Password updated successfully'
    }, status: 200
  end

  def render_update_error
    key = resource_errors.keys.last

    return render json: {
      success: false,
      error: resource_errors[key].first
    }, status: 200
  end

  def render_create_error_missing_redirect_url
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.passwords.missing_redirect_url')
    }, status: 401
  end

  def render_create_error_not_allowed_redirect_url
    render json: {
      success: false,
      data:   resource_data,
      # error: I18n.t('devise_token_auth.passwords.not_allowed_redirect_url', redirect_url: @redirect_url)
    }, status: 200
  end

  def render_create_error_missing_email
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.passwords.missing_email')
    }, status: 401
  end

end
