module Api
  module V2
    module Auth
      class VerificationsController < Api::V2::BaseController
        skip_before_filter :verify_authenticity_token
        before_action :authenticate_user!

        def generate
          return render json: { success: false, error: 'You must provide the phone_number.' },
            status: :ok if phone.empty?

          result = CheckAndSendSms.call(params: generate_params, object: ::User::Client)

          if result.success?
            render json: { success: true }, status: :ok
          else
            Rails.logger.error("#{result.error}")

            render json: { success: false, error: result.error }, status: :ok
          end

        rescue StandardError => e
          capture_exception(e)

          render json: { status: false, error: 'Something wrong' }, status: 500
        end

        def verify_code
          return render json: { success: false, error: 'You must provide the phone_number.' },
            status: :ok if phone.empty?

          result = VerificationCode.call(params: verify_params, object: ::User::Client)

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.error, code: result.code }, status: :ok
          end

        rescue StandardError => e
          capture_exception(e)

          render json: { status: false, error: 'Something wrong' }, status: 500
        end

        private

        def generate_params
          { phone_number:  phone }
        end

        def verify_params
          { phone_number: phone, code: params[:code] }
        end

        def phone
          result = Support::ChangePhoneNumber.call(phone_number: params[:phone_number])

          result.success? ? result.phone : ""
        end
      end
    end
  end
end
