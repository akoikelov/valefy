module Api
  module V2
    module Auth
      # class for sign in with socials: fb, etc...
      class SocialsController < Api::V2::BaseController

        # /api/v2/auth/provider/facebook
        # auth via facebook
        def facebook
          data = AuthorizeUser.call(
            params: permit_params, object: ::User
          )

          if data.success?
            user = data.user

            sign_in(:user, user, store: false, bypass: false)
            user.save
            auth_header(data)

            render json: { success: true, data: user.as_json }, status: :ok
          else
            render json: { success: false, error: data.error }, status: :ok
          end
        rescue Errno::ECONNREFUSED => e
          render json: { success: true, data: user.as_json }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        def google
          data = AuthorizeGoogleUser.call(
            params: permit_params, object: ::User::Client
          )

          if data.success?
            user = data.user

            sign_in(:user, user, store: false, bypass: false)
            user.save
            auth_header(data)

            render json: {
              success: true,
              data: ActiveModelSerializers::SerializableResource.new(
                user,
                each_serializer: ::Api::V2::ClientSerializer
              )
            }, status: :ok
          else
            render json: { status: false, error: data.error }, status: :ok
          end
        rescue Errno::ECONNREFUSED => e
          render json: { success: true, data: user.as_json }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :access_token, :type
        end

        def auth_header(data)
          auth_header = data.user.build_auth_header(data.token, data.client_id)
          response.headers.merge!(auth_header)
        end
      end
    end
  end
end
