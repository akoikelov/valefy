module Api
  module V2
    module Auth
      module Passwordsms
        class VerifyController < Api::V2::BaseController
          skip_after_action :update_auth_header, only: [:create, :update]

          def create
            result = VerificationCode.call(params: resource_params, object: ::User::Client)

            @resource = ::User.find_by_phone_number(phone)

            if @resource
              short, token = Devise.token_generator.generate(@resource.class, :reset_password_token)
              @resource.update(reset_password_token: token)

              if result.success?
                response.headers.merge!({ 'reset_password_token' => token })

                return render json: { success: true }, status: :ok
              else
                return render json: { success: false, error: result.error }, status: :ok
              end
            else
              return render json: { success: false, error: 'Client not found' }, status: :ok
            end

          rescue => e
            capture_exception(e)

            render json: { success: false, error: 'Something wrong' }, status: 500
          end

          private

          def phone
            result = Support::ChangePhoneNumber.call(phone_number: params[:phone_number])

            result.success? ? result.phone : ""
          end

          def resource_params
            {
              phone_number: phone,
              code:         params[:code]
            }
          end
        end
      end
    end
  end
end
