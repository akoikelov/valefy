module Api
  module V2
    module Auth
      module Passwordsms
        class SendController < Api::V2::BaseController
          skip_after_action :update_auth_header, only: [:create]

          def create
            # will be send sms code if phone present
            return render json: { success: false, error: 'You must provide the phone_number.' },
              status: :ok if phone.empty?

            resource = ::User::Client.find_by(phone_number: phone)

            if resource
              result   = CheckAndSendSms.call(params: sms_params, object: ::User::Client)

              if result.success?
                return render json: { success: true }, status: :ok
              else
                return render json: { success: false, error: result.error }, status: :ok
              end
            else
              return render json: { success: false, error: 'user with number does not exist' }, status: :ok
            end

          rescue => e
            capture_exception(e)

            render json: { success: false, error: 'Something wrong' }, status: 500
          end

          private

          def phone
            result = Support::ChangePhoneNumber.call(phone_number: resource_params[:phone_number])

            result.success? ? result.phone : ""
          end

          def resource_params
            params.permit(:phone_number)
          end

          def sms_params
            { phone_number: phone }
          end
        end
      end
    end
  end
end
