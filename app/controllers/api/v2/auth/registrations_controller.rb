# Manage user registrations. Overrides error methods for change status code.
# /api/v2/auth
class Api::V2::Auth::RegistrationsController < DeviseTokenAuth::RegistrationsController

  def create
    @resource            = resource_class.new(sign_up_params)
    @resource.provider   = "email"

    password = SecureRandom.urlsafe_base64(nil, false)
    @resource.password              = password if params[:platform].eql?('ADMIN')
    @resource.password_confirmation = password if params[:platform].eql?('ADMIN')
    @resource.share_email           = params[:share_email] if params[:platform].eql?('ADMIN') and params[:share_email].present?
    # honor devise configuration for case_insensitive_keys
    if resource_class.case_insensitive_keys.include?(:email)
      @resource.email = sign_up_params[:email].try :downcase
    else
      @resource.email = sign_up_params[:email]
    end

    # give redirect value from params priority
    @redirect_url = params[:confirm_success_url]

    # fall back to default value if provided
    @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

    # success redirect url is required
    if resource_class.devise_modules.include?(:confirmable) && !@redirect_url
      return render_create_error_missing_confirm_success_url
    end

    # if whitelist is set, validate redirect_url against whitelist
    if DeviseTokenAuth.redirect_whitelist
      unless DeviseTokenAuth.redirect_whitelist.include?(@redirect_url)
        return render_create_error_redirect_url_not_allowed
      end
    end

    begin
      # override email confirmation, must be sent manually from ctrl
      resource_class.set_callback("create", :after, :send_on_create_confirmation_instructions)
      resource_class.skip_callback("create", :after, :send_on_create_confirmation_instructions)
      if @resource.save
        yield @resource if block_given?

        unless @resource.confirmed?
          # user will require email authentication
          @resource.send_confirmation_instructions({
            client_config: params[:config_name],
            redirect_url: @redirect_url
          })

        else
          # email auth has been bypassed, authenticate user
          @client_id = SecureRandom.urlsafe_base64(nil, false)
          @token     = SecureRandom.urlsafe_base64(nil, false)

          @resource.tokens[@client_id] = {
            token: BCrypt::Password.create(@token),
            expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
          }

          @resource.save!

          update_auth_header
        end
        render_create_success
      else
        clean_up_passwords @resource
        render_create_error
      end
    rescue ActiveRecord::RecordNotUnique
      clean_up_passwords @resource
      render_create_error_email_already_exists
    end
  end

  def update
    if @resource
      @resource.update(confirm_phone_number: false) if changed_phone?
      if @resource.send(resource_update_method, account_update_params)
        yield @resource if block_given?
        render_update_success
      else
        render_update_error
      end
    else
      render_update_error_user_not_found
    end
  end

  def render_create_success
    respond_to do |format|
      format.html { redirect_to new_admin_panel_order_item_path(client_id: @resource.id) }
      format.json { render json: {success: true, data: @resource.custom_token_validation_response}}
    end
  end

  def render_create_error_missing_confirm_success_url
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.missing_confirm_success_url')
    }, status: 422
  end

  def render_create_error_redirect_url_not_allowed
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.redirect_url_not_allowed')
    }, status: 422
  end

  def render_create_error
    render json: {
      success: false,
      data:   resource_data,
      error: message(resource_errors)
    }, status: 200
  end

  def render_create_error_email_already_exists
    render json: {
      success: false,
      data:   resource_data,
      error: I18n.t('devise_token_auth.registrations.email_already_exists')
    }, status: 200
  end

  def render_update_success
      render json: {
        success: true,
        data: @resource.custom_token_validation_response
      }
    end

  def render_update_error
    render json: {
      success: false,
      error: message(resource_errors)
    }, status: 200
  end

  def render_update_error_user_not_found
    render json: {
      success: false,
      error: I18n.t('devise_token_auth.registrations.user_not_found')
    }, status: 200
  end

  def validate_post_data which, message
    render json: {
       success: false,
       error: message
    }, status: :unprocessable_entity if which.empty?
  end

  private

  def sign_up_params
    params.permit(:email, :password, :password_confirmation, :phone_number,
                  :type, :name, :surname, :last_name, :b_day)
  end

  def account_update_params
    params.permit(:email, :phone_number, :name, :nickname, :surname,
                  :last_name, :b_day, :image)
  end

  def message resource_errors
    resource_errors[:full_messages].first if resource_errors[:full_messages].any?
  end

  def changed_phone?
    if account_update_params.keys.include?('phone_number')
      result = ::Support::ChangePhoneNumber.call(phone_number: account_update_params[:phone_number])

      return true  if result.phone.empty?
      return true  if @resource.phone_number != result.phone
    end

    false
  end
end
