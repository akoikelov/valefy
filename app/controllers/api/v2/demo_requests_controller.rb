module Api
  module V2
    class DemoRequestsController < BaseController

      def create
        demo = DemoRequest.new(permit_params)
        demo.save

        redirect_to 'http://valefy.ie'
      rescue StandardError => e
        capture_exception(e)
        redirect_to 'http://valefy.ie'
      end

      private

      def permit_params
        params.require(:demo_request).permit(:company, :email, :contact, :phone_number, :notes, :message)
      end
    end
  end
end
