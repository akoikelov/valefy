module Api
  module V2
    module GiftCards
      class UsesController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/gift_cards/uses [POST]
        def create
          result = Use.call(params: permit_params, current_user: current_user)

          if result.success?
            order = result.order

            unless Rails.env.eql?('test')
              ['android','IOS'].map do |item|
                run_push_worker(order, I18n.t('order_item.messages.new_order'), item)
              end
            end

            created_order_email(order) if Setting.first.send_email_created_order?

            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end

        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:gift_card_id, :address, :description, :order_time, :lon, :lat)
        end

        def run_push_worker(order, title, platform)
          unless Rails.env.eql?('test')
            Api::Masters::V1::NewOrderNotifyWorker.perform_async(
              order_id: order.id, title: title, platform: platform,
              time_zone: params[:time_zone]
            )
          end
        end

        def created_order_email(order)
          unless Rails.env.eql?('test')
            ::Api::V1::SendEmailWorker.perform_async(
              client_id: order.client.id,
              email_method: 'client_created_order_email',
              order_id: order.id
            )
          end
        end
      end
    end
  end
end
