module Api
  module V2
    # FAQ
    class HelpsController < Api::V2::BaseController
      before_action :authenticate_user!

      # /api/v2/helps [ POST]
      def create
        result = HelpCreate.call(
          params: help_params,
          current_user: current_user
        )

        if result.success?
          render json: { success: true }, status: :ok
        else
          render json: { success: false, error: result.error }, status: :ok
        end
      rescue => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def help_params
        params.permit(:question)
      end
    end
  end
end
