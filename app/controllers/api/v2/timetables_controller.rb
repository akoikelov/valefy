module Api
  module V2
    class TimetablesController < Api::V2::BaseController
      def index
        result = AvailableSchedule.call(
          date:                 permit_params[:date],
          current_hour_minutes: permit_params[:current_hour_minutes]
        )

        if result.success?
          render json: {
            success: true,
            data: {}.merge(result.schedule_week)
          }
        else
          render json: { success: false, error: 'Something wrong' }
        end
      end

      private

      def permit_params
        params.permit(:date, :current_hour_minutes)
      end
    end
  end
end
