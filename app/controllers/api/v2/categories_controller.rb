# Controller which return list of category and sub category
# /api/v2/categories
# /api/v2/categories/:id_category

class Api::V2::CategoriesController < Api::V2::BaseController
  def index
    if current_user.present?
      if current_user.try(:categories).present?
        categories = current_user.categories
      elsif current_user.try(:corp)
        categories = Category.corporative
      else
        categories = Category.visible
      end
    else
      categories = Category.visible
    end

    render json: categories, each_serializer: ::Api::V2::CategorySerializer, current_user: current_user
  end

  def show
    category = Subcategory.find(params[:id])

    render json: category.services, each_serializer: ::Api::V2::ServiceSerializer

  rescue ActiveRecord::RecordNotFound => e
    render json: { success: false, error: 'Subcategory not found' }, status: :ok
  end
end
