# Controller which return list of category and sub category


class Api::V2::ServicesController < Api::V2::BaseController
  def index
    services = Service.where(subcategory_id: params[:q][:subcategory_id_eq])

    render json: services, each_serializer: ::Api::V2::ServiceSerializer
  end
end
