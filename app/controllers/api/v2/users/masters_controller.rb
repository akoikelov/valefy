class Api::V2::Users::MastersController < Api::V2::BaseController
  before_action :authenticate_user!

  # /api/v2/users/master/:id
  # Show master info for client
  def show
    master = ::User::ServiceMan.find_by(id: permit_params[:master_id])

    if master.present?
      render json: {
        success: true,
        data: ActiveModelSerializers::SerializableResource.new(
          master,
          each_serializer: ::Api::V2::User::MasterSerializer
        )
      }
    else
      render json: {
        success: false,
        error: 'Not found'
      }, status: :ok
    end

  rescue => e
    capture_exception(e)

    render json: { success: false, error: 'Something wrong' }, status: 500
  end

  private

  def permit_params
    params.permit :master_id
  end
end
