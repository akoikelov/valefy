class Api::V2::Users::ClientController < Api::V2::BaseController
  before_action :authenticate_user!

  # /api/v2/users/client
  # Show user info
  def index
    render json: {
      success: true,
      data: ActiveModelSerializers::SerializableResource.new(
        current_user,
        each_serializer: ::Api::V2::ClientSerializer
      )
    }
  end

  # /api/v2/users/client/:id
  # Show user info
  def show
    render json: {
      success: true,
      data: ActiveModelSerializers::SerializableResource.new(
        current_user,
        each_serializer: ::Api::V2::ClientSerializer
      )
    }
  end
end
