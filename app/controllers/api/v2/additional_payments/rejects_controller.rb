module Api
  module V2
    module AdditionalPayments
      class RejectsController < Api::V2::BaseController

        def create
          result = Reject.call(params: permit_params)

          if result.success?
            order   = result.order
            payment = result.payment

            push_notification_send({
              order: order,
              title: I18n.t('order_item.messages.reject_payment'),
              worker: '::Api::V1::AdditionalPaymentWorker'
            })

            render json: { success: true, data: payment }, status: :ok
          else
            render json: { success: false, error: '' }, status: :ok
          end

        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :payment_id
        end
      end
    end
  end
end
