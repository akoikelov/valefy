module Api
  module V2
    module AdditionalPayments
      class ConfirmsController < Api::V2::BaseController

        def create
          result = Confirm.call(
            params: permit_params, current_user: current_user
          )

          if result.success?
            order = result.order

            push_notification_send({
              order: order,
              title: I18n.t('order_item.messages.confirm_payment'),
              worker: '::Api::V1::AdditionalPaymentWorker'
            })

            render json: { success: true, data: order.additional_payment }, status: :ok
          else
            render json: { success: false, error: '' }, status: :ok
          end
        rescue ActiveRecord::RecordNotFound => e
          capture_exception(e)

          render json: { success: false, error: 'Not found' }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :payment_id
        end
      end
    end
  end
end
