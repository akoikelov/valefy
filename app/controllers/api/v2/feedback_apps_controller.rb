module Api
  module V2
    class FeedbackAppsController < Api::V2::BaseController
      before_action :authenticate_user!

      def create
        feedback = current_user.feedback_apps.new(permit_params)

        if feedback.save
          render json: { success: true }, status: 201
        else
          render json: { success: false, error: feedback.errors[:message].first }, status: :ok
        end
      end

      private

      def permit_params
        params.permit :message, :platform
      end
    end
  end
end
