module Api
  module V2
    # Additional Payments
    class AdditionalPaymentsController < Api::V2::BaseController
      before_action :authenticate_user!

      # /api/v2/
      def show
        ap = AdditionalPayment.find(params[:id])
        render json: { success: true, data: ap }, status: :ok
      end
    end
  end
end
