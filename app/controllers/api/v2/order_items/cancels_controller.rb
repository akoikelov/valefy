module Api
  module V2
    module OrderItems
      # Cancel order
      class CancelsController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/order_items/cancels [POST]
        def create
          result = Cancel.call(
            params: permit_params, current_user: current_user
          )

          if result.success?
            order = result.order

            push_notification_send({
              order: order,
              title: I18n.t('order_item.messages.cancel'),
              worker: 'Api::Masters::V1::PushNotificationsWorker'
            }) if order.master.present?

            render json: order, serializer: ::Api::V2::OrderItemSerializer, time_zone: params[:time_zone]
          else
            render json: { success: false, error: '' }, status: :ok
          end
        rescue ActiveRecord::RecordNotFound => e
          capture_exception(e)

          render json: { success: false, error: 'Order Not Found' }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit :order_id
        end
      end
    end
  end
end
