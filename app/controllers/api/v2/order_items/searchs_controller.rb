module Api
  module V2
    module OrderItems
      # Search masters when client paid for service
      class SearchsController < Api::V2::BaseController
        before_action :authenticate_user!

        # /api/v2/order_items/searchs [POST]
        def create
          result = Searching.call(params: params, current_user: current_user)

          if result.success?
            order = result.order

            unless Rails.env.eql?('test')
              ['android','IOS'].map do |item|
                run_push_worker(order, I18n.t('order_item.messages.new_order'), item)
              end
            end

            created_order_email(order) if Setting.first.send_email_created_order?

            render json: { success: true }
          else
            render json: { success: false, error: result.error }, status: :ok
          end

        rescue ActiveRecord::RecordNotFound => e
          capture_exception(e)

          render json: { success: false, error: 'Order Not Found' }, status: :ok
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def run_push_worker(order, title, platform)
          unless Rails.env.eql?('test')
            Api::Masters::V1::NewOrderNotifyWorker.perform_async(
              order_id: order.id, title: title, platform: platform,
              time_zone: params[:time_zone]
            )
          end
        end

        def created_order_email(order)
          unless Rails.env.eql?('test')
            ::Api::V1::SendEmailWorker.perform_async(
              client_id: order.client.id,
              email_method: 'client_created_order_email',
              order_id: order.id
            )
          end
        end
      end
    end
  end
end
