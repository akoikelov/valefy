module Api
  module V2
    class ReferalSharesController < Api::V2::BaseController

      before_action :authenticate_user!

      def create
        if params[:email_from].present?
          current_user.update_attributes(share_email: params[:email_from])
          render json: { success: true, data: "Share email successfully set" }, status: :ok
        else
          render json: { success: false, error: "Not found share_email parameter" }, status: :ok
        end
      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end
    end
  end
end
