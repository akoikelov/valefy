module Api
  module V2
    module Client
      class DevicesController < Api::V2::BaseController
        before_action :authenticate_user!

        def create
          result = Firebase.call(
            current_user: current_user,
            params: params
          )

          if result.success?
            render json: { success: true }, status: :ok
          else
            render json: { success: false, error: result.error }, status: :ok
          end
        rescue => e
          capture_exception(e)

          render json: { success: false, error: 'Something wrong' }, status: 500
        end

        private

        def permit_params
          params.permit(:id_client_firebase, :platform)
        end
      end
    end
  end
end
