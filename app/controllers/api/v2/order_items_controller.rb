module Api
  module V2
    # order item controller for create each order.
    class OrderItemsController < Api::V2::BaseController
      before_action :authenticate_user!
      

      def index
        orders = OrdersList.call(params: orders_params,
                                 current_user: current_user)

        if orders.success?
          render json: orders.list,
                 each_serializer: ::Api::V2::OrderItemSerializer,
                 time_zone: params[:time_zone]
        else
          render json: { success: false, error: orders.error }, status: :ok
        end
      end

      # /api/v2/order_items/:id [GET]
      # order item show method
      def show
        order = OrderItem.find(params[:id])

        render json: order,
                     serializer: ::Api::V2::OrderItemSerializer,
                     time_zone: params[:time_zone]
      rescue ActiveRecord::RecordNotFound => e
        capture_exception(e)

        render json: { success: false, error: 'Record Not Found' }, status: :ok
      rescue => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      def create
        result = CreateOrderItem.call(params: order_params,
                                      current_user: current_user)

        if result.success?
          order = result.order
          CreateInvoice.call(order: order, current_user: current_user)

          render json: {
            success: true,
            data: ActiveModelSerializers::SerializableResource.new(
              order,
              serializer: ::Api::V2::OrderItemSerializer, time_zone: params[:time_zone]
            )
          }
        else
          render json: { success: false, error: result.error, code: result.code }, status: :ok
        end

      rescue ActiveRecord::RecordNotFound => e
        capture_exception(e)
        
        render json: { success: false, error: 'Not found' }, status: :ok
      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def orders_params
        params.permit(:status)
      end

      def order_params
        params.permit(:address, :baby_seat, :description,:order_time, :lon, :lat,
                      :car_count, :suv_count, :minivan_count, :time_zone, :pets,
                      :platform, :count_vehicles,:parking, :promo_code_id, :reg_numbers,
                      :stationary_id, :office_id, :model_auto, :service_ids => [])
                      .merge(images: params[:images])
      end
    end
  end
end
