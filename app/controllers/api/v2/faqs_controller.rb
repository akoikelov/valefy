module Api
  module V2
    # Get answers
    class FaqsController < Api::V2::BaseController
      before_action :authenticate_user!

      # /api/v2/faqs [GET]
      def index
        faqs = Faq.client

        render json: { success: true, faqs: faqs }, status: :ok
      end
    end
  end
end
