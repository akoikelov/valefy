module Api
  module V2
    class GiftCardsController < Api::V2::BaseController
      before_action :authenticate_user!

      def create
        result = GiftCards::Manage.call(params: permit_params, current_user: current_user)

        if result.success?
          run_worker(result)

          render json: { success: true }, status: :ok
        else
          render json: { success: false, error: result.error }, status: :ok
        end
      rescue ActiveRecord::RecordNotFound => e
        capture_exception(e)

        render json: { success: false, error: 'Record not found' }, status: :ok
      rescue => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def permit_params
        params.permit :client_email, :service_id
      end

      def run_worker(result)
        if Rails.env.qa? || Rails.env.development? || Rails.env.production?
          if result.client_exist?
            ::Api::V1::CreatePromocodeAndSendNotificationsWorker.perform_async(
              gift_id: result.gift.id,
              client_exist: true
            )
          else
            ::Api::V1::CreatePromocodeAndSendNotificationsWorker.perform_async(
              gift_id: result.gift.id,
              client_exist: false
            )
          end
        end
      end
    end
  end
end
