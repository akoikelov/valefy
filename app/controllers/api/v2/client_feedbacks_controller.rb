module Api
  module V2
    # Feedback comment and rating
    class ClientFeedbacksController < Api::V2::BaseController
      before_action :authenticate_user!

      def create
        result = Order::Feedback::CreateClientFeedback.call(
          params: feedback_params,
          current_user: current_user
        )

        if result.success?
          if result.feedback.reload.call_me.present?
            unless Rails.env.eql?('test')
              ['android','IOS'].map do |item|
                run_push_worker(result.order, 'We will call you', item)
              end
            end
          end
          render json: { success: true, data: result.feedback }, status: :ok
        else
          render json: { success: false, error: result.error }, status: :ok
        end

      rescue ActiveRecord::RecordNotFound => e
        capture_exception(e)

        render json: { success: false, error: 'Not found' }, status: :ok
      rescue StandardError => e
        capture_exception(e)

        render json: { success: false, error: 'Something wrong' }, status: 500
      end

      private

      def feedback_params
        params.permit(:order_item_id, :rating, :comment, :polite, :orderly, :friendly, :recall, :tip, :tip_amount)
      end

      def run_push_worker(order, title, platform)
        unless Rails.env.eql?('test')
          Api::V1::PushNotificationsWorker.perform_async(
            order_id: order.id, title: title, platform: platform
          )
        end
      end
    end
  end
end
