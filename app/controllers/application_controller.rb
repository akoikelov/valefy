class ApplicationController < ActionController::Base
  protect_from_forgery

  def current_ability
    @current_ability ||= current_admin_user.ability
  end
end
