class WelcomeController < ApiController
  def index
    redirect_to admin_panel_path
  end

  def register
  end

  def login
  end

  def privacy_policy
  end

  def profile
  end

  def reset_password
  end
  
  def verify_phone
  end
  
end
