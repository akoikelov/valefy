class GiftCard < ActiveRecord::Base
  belongs_to :to_client, class_name: 'User::Client',   foreign_key: :to_client_id
  belongs_to :from_client, class_name: 'User::Client', foreign_key: :from_client_id
  belongs_to :service
  has_one :subcategory, through: :service
  has_one :category,    through: :subcategory

  def self.available
    where('exp_date > ?', Date.today).where(used: false)
  end

  def in_cents
    (amount * 100).to_i
  end
end
