class Service < ActiveRecord::Base
  include RailsSortable::Model
  set_sortable :position

  paginates_per 30

  acts_as_list scope: :subcategory
  belongs_to :subcategory
  has_one :category, through: :subcategory
  has_one :company_service # TODO надо избавляться от этой таблицы
  has_one :station_service
  has_many :contract_services
  has_many :contracts, through: :contract_services
  has_many :order_item_services
  has_many :order_items, through: :order_item_services

  validates :subcategory_id, :title, :price, :duration, presence: true

  scope :visible, -> { includes(:category).where(categories: {visible: true}) }
  # validates :title, uniqueness: true

  after_initialize :init
  after_create :add_service_to_station_services

  def init
    duration ||= 0.0
  end

  def self.by_subcategory(subcategory_id)
    Subcategory.find(subcategory_id).services
  end

  def self.by_category(category_id)
    Category.find(category_id).services
  end

  private

  def add_service_to_station_services
    CompanyLocation.all.each do |station|
      station.services.create! service_id: id, price: price
    end
  end
end
