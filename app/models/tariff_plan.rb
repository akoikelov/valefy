class TariffPlan < ActiveRecord::Base
  has_many :clients, class_name: 'User::Client'

  validates :plan, :plan_value, presence: true
end
