class CompanyLocation < ActiveRecord::Base
  belongs_to :admin_user
  has_many   :client_feedbacks, foreign_key: :stationary_id
  has_many   :services,  class_name: 'StationService', foreign_key: :station_id,  dependent: :destroy
  has_many   :promo_codes, foreign_key: :stationary_id

  validates :title, :address,
            :lat, :lon, :phone_number, presence: true # TODO :work_schedule

  mount_uploader :image, ImageUploader

  after_create :copy_paste_services

  def self.by_company(id = nil)
    id.nil? ? all : where(company_id: id).order(id: :desc)
  end

  private

  def copy_paste_services
    Service.all.each do |service|
      services.create! service_id: service.id, price: service.price
    end
  end
end
