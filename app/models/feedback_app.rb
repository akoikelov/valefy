class FeedbackApp < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan'

  validates :message, presence: true

  scope :by_clients,   -> { where.not(client_id: nil).order(created_at: :desc) }
  scope :by_masters,   -> { where.not(master_id: nil).order(created_at: :desc) }
  scope :platform_web, -> { where(platform: 'web').order(created_at: :desc) }
  scope :platform_android, -> { where(platform: 'android').order(created_at: :desc) }
  scope :platform_ios, -> { where(platform: 'ios').order(created_at: :desc) }
end
