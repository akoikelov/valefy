# users only for admin part
class AdminUser < ActiveRecord::Base
  include AdminUserRegistrable

  has_many :pros,         class_name: 'User::ServiceMan'
  has_many :user_clients, class_name: 'User::Client'
  has_many :employees
  has_one  :company,           dependent: :destroy
  has_many :company_locations, dependent: :destroy
  has_many :company_services,  dependent: :destroy

  accepts_nested_attributes_for :company_locations,     allow_destroy: true
  accepts_nested_attributes_for :company_services,      allow_destroy: true

  serialize :access_items, Array
  serialize :access_item_keys, Array
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable,
         :rememberable, :trackable, :validatable
  
  after_create do
    # send_reset_password_instructions if Rails.env.qa? || Rails.env.production?
    # add_company if company?
  end

  enum role:         [:admin, :content, :company]
  enum platform:     Company::platforms

  scope :companies,    -> { where(role: roles['company']) }

  delegate :can?, :cannot?, to: :ability

  def actual_access_items
    self.access_item_keys.reject(&:empty?) & ActiveAdminHelper::resource_items
  end

  def password_required?
    new_record? ? false : super
  end

  def ability
    @ability ||= Ability.new(self)
  end

  def display_name
    email
  end
end
