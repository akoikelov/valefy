class Office < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client', foreign_key: :client_id

  validates :title, presence: true
end
