class ClientFeedback < ActiveRecord::Base
  belongs_to :order_item
  belongs_to :client,     class_name: 'User::Client'
  belongs_to :master,     class_name: 'User::ServiceMan', foreign_key: :serviceman_id
  belongs_to :stationary, class_name: "CompanyLocation",  foreign_key: :stationary_id
  has_one    :call_me

  validates :client_id, :rating, presence: true
end
