class CustomerAccount < ActiveRecord::Base
  belongs_to :client,  class_name: 'User::Client'
  has_many   :charges, class_name: 'CustomerCharge', foreign_key: :customer_account_id

  validates :client_id, :customer_id, presence: true
end
