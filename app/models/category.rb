# Category model
class Category < ActiveRecord::Base
  include RailsSortable::Model
  set_sortable :position

  acts_as_list
  has_many :subcategories,      -> { where(visible: true).order(position: :asc) }, dependent: :destroy
  has_many :corp_subcategories, -> { where(visible: true).where(corp: true).order(position: :asc) }, class_name: 'Subcategory', dependent: :destroy
  has_many :services, through: :subcategories

  has_many :order_items, through: :services
  # has_many :clients, through: :order_items

  has_many :master_categories
  has_many :masters, class_name: 'User::ServiceMan', through: :master_categories

  validates :title, presence: true
  validates :title, uniqueness: true

  mount_uploader :image, ImageUploader
  mount_uploader :icon, ImageUploader
  mount_uploader :icon_web, SvgUploader

  scope :visible,        -> { where(visible: true).order(position: :asc) }
  scope :corporative,    -> { where(visible: true).where(corp: true).order(position: :asc) }
  scope :general,        -> { where(parent_id: nil) }
  scope :sub_categories, -> { where.not(parent_id: nil) }

  def self.childs id
    id.nil? ? where.not(parent_id: nil) : where(parent_id: id)
  end

  class << self
    def import(file)
      spreadsheet = open_spreadsheet(file)
      if spreadsheet.eql?(1)
        return false
      else
        spreadsheet.sheets.each do |sheet|
          header = spreadsheet.row(1, sheet)

          (2..spreadsheet.last_row(sheet)).each do |i|
            row = Hash[[header, spreadsheet.row(i, sheet)].transpose]

            if row['Category'].present?
              @category = Category.where(title: row['Category']).first_or_create

              if row['Subcategory'].present?
                @subcategory = @category.subcategories.where(title: row['Subcategory']).first_or_create

                if row['Service'].present?
                  @subcategory.services.create title: service(row['Service']), price: 1
                end
              end
            end

            if row['Category'].nil? && row['Subcategory'].nil? && row['Service'].present?
              @subcategory.services.create title: service(row['Service']), price: 1
            elsif row['Category'].nil? && row['Subcategory'].present?
              @subcategory = @category.subcategories.where(title: row['Subcategory']).first_or_create

              if row['Service'].present?
                @subcategory.services.create title: service(row['Service']), price: 1
              end
            end
          end
        end
      end
    end

    def open_spreadsheet(file)
      case File.extname(file.original_filename)
      when ".xls" then Roo::Excel.new(file.path)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else return 1
      end
    end

    def service(title)
      result = /\d\.0/.match(title.to_s)
      if result.present?
        result = title.to_i
      else
        result = title
      end

      result
    end
  end
end
