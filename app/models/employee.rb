class Employee < ActiveRecord::Base

  validates :name, :last_name, :position, :email, :phone_number, :b_day, presence: true
  validates :phone_number, uniqueness: true
  validates :email, uniqueness: true, email: true

  belongs_to :admin_user
end
