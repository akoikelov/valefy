class City < ActiveRecord::Base
  validates :name, presence: true

  mount_uploader :image, ImageUploader
  has_many :districts
  belongs_to :country
end
