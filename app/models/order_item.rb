# class which describe each order sent of client
class OrderItem < ActiveRecord::Base
  include ApplicationHelper

  paginates_per 30

  geocoded_by :address, latitude: :lat, longitude: :lon

  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan'
  has_one    :company, through: :client

  has_many :order_item_services
  has_many :services, through: :order_item_services

  has_many   :feedbacks, class_name: 'ClientFeedback', dependent: :destroy
  has_many   :master_feedbacks, dependent: :destroy
  has_many   :charges, class_name: 'CustomerCharge', foreign_key: :order_item_id
  has_many   :transfers
  has_one    :additional_payment, foreign_key: :order_id
  belongs_to :subscription
  has_many   :invoices, foreign_key: :order_id
  belongs_to :promo_code
  belongs_to :gift_card
  has_many   :order_status_histories
  belongs_to :station, class_name: 'CompanyLocation', foreign_key: :stationary_id

  validates :client_id, :address, :order_time, presence: true

  # statuses of order item
  #   * created - order created by client
  #   * pre_authorize_charged - charge before order(fee)
  #   * searching - searching a master
  #   * in_pool - master is found
  #   * accepted - master accept the work | send master
  #   * master_arriving - master accept the work
  #   * delayed - задерживается | send master
  #   * in_progress_unconfirmed - master is working
  #   * in_progress_confirmed - client confirm that master is working
  #   * end_work - master finished his work
  #   * completed_unpaid - client confirm of completed work, but not paid
  #   * completed_paid - completed and paid
  #   * cancelled - cancel order

#   буду передавать int значение
# 0 - все заказы(новые, активные, заверщенные) (1,3,4,5,6,7,8,9,11)
# 1 - новые (2) - searching status
# 2 - активный (3,4,5,6,7,8,9)
# 3 - заверщнные. (11)


# status: [:created,  # 0
#               :pre_authorize_charged, # 1
#               :searching, # 2
#               :in_pool, # 3
#               :accepted, #4
#               :master_arriving, #5
#               :delayed, #6
#               :in_progress_unconfirmed, #7
#               :in_progress_confirmed, #8
#               :end_work, #9
#               :completed_unpaid, #10
#               :completed_paid, #11
#               :canceled] #12

  enum status: [:created,
                :pre_authorize_charged,
                :searching,
                :in_pool,
                :accepted,
                :master_arriving,
                :delayed,
                :in_progress_unconfirmed,
                :in_progress_confirmed,
                :end_work,
                :completed_unpaid,
                :completed_paid,
                :canceled,
                :regular]

  def status_name
    status.to_sym
  end

  scope :serching,              -> { where(status: [1,2]).order(updated_at: :desc) }
  scope :active,                -> { where(status: [4,5,7,8]).order(id: :desc) }
  scope :busy,                  -> { where(status: [4,7,8]).order(id: :desc) }
  scope :do_order,              -> { where(status: [7,8]).order(id: :desc) }
  scope :history,               -> { where(status: [9, 11]).order(id: :desc) }
  scope :waiting_and_active,    -> { where(status: [2,4,7,8,9, 11]).order(id: :desc) }
  scope :mobile,                -> { where(stationary_id: nil) }
  scope :stations,              -> { where.not(stationary_id: nil) }
  scope :by_station,            ->(station_id) { where(stationary_id: station_id) }
  scope :waiting_and_active_by, ->(id) { where(status: [2,4,7,8,9, 11]).order(id: :desc) }
  scope :canceled,              -> { where(status: [12]).order(id: :desc) }
  scope :regular,               -> { where(status: [13]).order(id: :desc) }
  scope :waiting,               -> { where(status: [2]).order(id: :desc) } # searching status
  scope :created_by_android,    -> { where(platform: 'android') }
  scope :created_by_ios,        -> { where(platform: 'ios') }
  scope :created_by_web,        -> { where(platform: 'web') }
  scope :created_by_admin,      -> { where(platform: 'ADMIN') }

  FEES=20 # percentage
  COUNT_ORDERS_PER_PAGE=10

  TIME_ZONES={
    'Asia/Bishkek' => 'Dhaka',
    'Europe/Dublin' => 'Dublin'
  }

  DEFAULT_TIME_ZONE='Europe/Dublin'

  def self.by_company(id = nil)
    includes(:company).where(companies: {id: id})
  end

  def late
    diff = Time.diff(updated_at, Time.now)

    return 'late_green' if searching? && less_10_min?(diff)
    return 'late_yellow' if searching? && greater_10_less_40_min?(diff)
    return 'late_red' if searching? && greater_40_min?(diff)
  end

  # TODO old version. In future don't use it
  def service_price_arr(services)
    if services.present?
      result = services.map(& :price)
    else
      srv = Service.find(self.service_id)
      result = [srv.price.to_i]
    end

    result
  end

  def total_sum
    if self.gift_card.present?
      total = self.gift_card.amount
    else
      total = total_price
    end

    total
  end

  def in_cents
    ((total_sum).to_r * 100).to_i
  end

  def rest
    in_cents - fees_persentage
  end

  def sum_services
    services.map(& :price).sum
  end

  def sum_additional_payment
    return 0 unless additional_payment.present?

    additional_payment.client_confirm? ? additional_payment.additional_price : 0
  end

  def fee
    (fees_persentage.to_r / 100).to_f
  end

  def hours
    _hours = []

    services.each { |s| _hours << s.duration }

    _hours.empty? ? 1 : (_hours.inject{ |sum, el| sum + el }.to_f / _hours.size).to_i
  end

  def total_amount_for_master
    client_price - fee
  end

  def category
    services.first.subcategory.category if services.present?
  end

  def subcategory
    services.first.subcategory if services.present?
  end

  def duration_date
    order_time + services.map(& :duration).sum.hours
  end

  def client_price
    sum_services + sum_additional_payment
  end

  def balance_transaction
    return nil if transfers.empty?

    id_transaction = transfers.first.balance_transaction

    @transaction ||= Stripe::BalanceTransaction.retrieve(id_transaction)

    @transaction
  end

  def corp?
    client.corp?
  end

  def fees_persentage
    (FEES * in_cents) / 100
  end
end
