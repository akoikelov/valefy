# FAQ
class Faq < ActiveRecord::Base
  acts_as_list
  validates :question, :answer, :type_user, presence: true

  TYPE_USERS = %w(master client)

  scope :client, -> { where(type_user: TYPE_USERS.last) }
  scope :master, -> { where(type_user: TYPE_USERS.first) }
end
