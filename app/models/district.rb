class District < ActiveRecord::Base
  belongs_to :city

  has_many :clients, class_name: 'User::Client'
  has_many :masters, class_name: 'User::ServiceMan'

  validates :name, presence: true

  mount_uploader :image, ImageUploader
end
