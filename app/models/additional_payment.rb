class AdditionalPayment < ActiveRecord::Base
  belongs_to :order, class_name: 'OrderItem', foreign_key: :order_id
  validates :order_id, :additional_price, presence: true

  def in_cents
    (additional_price.to_r * 100).to_i
  end
end
