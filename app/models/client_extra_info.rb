class ClientExtraInfo < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'

  accepts_nested_attributes_for :client, reject_if: :all_blank

  enum gender: [ :male, :female ]
  enum family_status: [ :single, :married, :divorced ]
end
