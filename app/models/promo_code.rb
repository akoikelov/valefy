class PromoCode < ActiveRecord::Base
  has_many :client_promo_codes
  has_many :clients, through: :client_promo_codes

  has_one    :order_item
  belongs_to :station, class_name: 'CompanyLocation'

  validates :promo_code_id, :exp_date, :discount, presence: true
  validates :promo_code_id, uniqueness: true
  validate  :happened_when_invalid_exp_date

  def self.available
    where('exp_date > ?', Date.today).where(used: false, stationary_id: nil)
  end

  private

  def happened_when_invalid_exp_date
    errors.add(:exp_date, 'must be a greater than current date')if DateTime.now > exp_date
  end
end
