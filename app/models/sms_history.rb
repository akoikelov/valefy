class SmsHistory < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id
  validates_presence_of :phone_number, :code

  COUNT_QUERIES=3
  COUNT_MINUTES=10
end
