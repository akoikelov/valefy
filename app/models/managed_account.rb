class ManagedAccount < ActiveRecord::Base
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :serviceman_id
end
