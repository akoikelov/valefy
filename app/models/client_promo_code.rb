class ClientPromoCode < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :promo_code
end
