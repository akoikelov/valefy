class OrderStatusHistory < ActiveRecord::Base
  belongs_to :order_item

  enum status: [:cancelled, :accepted]
  enum author: [:admin, :client, :pro]
end
