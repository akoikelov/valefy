class ClientService < ActiveRecord::Base
  belongs_to :client, class_name: "User::Client"
  belongs_to :service

  validates :service_id, :price, presence: true
end
