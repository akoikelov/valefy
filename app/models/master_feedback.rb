class MasterFeedback < ActiveRecord::Base
  belongs_to :order_item
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id

  validates :order_item_id, :rating, presence: true
end
