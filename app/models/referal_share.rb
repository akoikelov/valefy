class ReferalShare < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :shared_client, class_name: 'User::Client', foreign_key: :shared_client_id
  belongs_to :promo_code
end
