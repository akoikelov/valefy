class DraftMaster < ActiveRecord::Base
  validates :email, :phone_number, :name, presence: true
  validates :email, email: true
  validates :email, :phone_number, uniqueness: true
end
