class MasterStripeInfo < ActiveRecord::Base
  include CreateAccountStripe

  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id

  validates :master_id, :address_line1, :state, :city, :personal_id, :tax_id,
            :id_document_image, :account_number, presence: true
end
