class MasterLocation < ActiveRecord::Base
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id

  validates :master_id, :lon, :lat, presence: true
end
