class StationService < ActiveRecord::Base
  belongs_to :service
  belongs_to :station, class_name: 'CompanyLocation', foreign_key: :station_id

  validates :service_id, :price, :station_id, presence: true

  def self.price_by(service_id)
    return 0.0 unless service_id.present?

    find_by(service_id: service_id).price.to_i
  end
end
