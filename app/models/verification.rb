class Verification < ActiveRecord::Base
	belongs_to :client, class_name: 'User::Client'
	validates_presence_of :phone_number, :code
end
