# client, which hire service man
class User::Client < User
  paginates_per 30
  before_validation :phone_number_format
  after_create :commands

  has_many :order_items, dependent: :destroy
  has_many :feedbacks, class_name: 'ClientFeedback',
                       foreign_key: :client_id, dependent: :destroy
  has_one  :customer_account,  foreign_key: :client_id
  has_one  :referal_link,      foreign_key: :client_id
  has_many :devices,           foreign_key: :client_id
  has_many :helps,           foreign_key: :client_id
  has_many :sms_histories,   foreign_key: :client_id
  has_many :feedback_apps,   foreign_key: :client_id
  has_many :subscriptions
  has_many :invoices, foreign_key: :client_id
  has_many :client_services, foreign_key: :client_id
  has_many :client_categories, foreign_key: :client_id
  has_many :categories, through: :client_categories
  belongs_to :tariff_plan
  belongs_to :admin_user
  belongs_to :company
  has_many :offices,   foreign_key: :client_id
  has_many :to_me_gift_cards,   -> { where(used: false).where('exp_date > :current_date', current_date: DateTime.now) }, class_name: 'GiftCard', foreign_key: :to_client_id, dependent: :destroy
  has_many :from_me_gift_cards, -> { where(used: false).where('exp_date > :current_date', current_date: DateTime.now) }, class_name: 'GiftCard', foreign_key: :from_client_id, dependent: :destroy
  has_many :client_promo_codes
  has_many :promo_codes, through: :client_promo_codes
  belongs_to :district
  # belongs_to :category
  has_one :corp_information,  foreign_key: :client_id
  has_one :client_extra_info, foreign_key: :client_id
  has_one :contract,          foreign_key: :client_id
  has_one :client_segment,    foreign_key: :client_id

  accepts_nested_attributes_for :offices,         :allow_destroy => true, reject_if: proc { |att| att['title'].blank? }
  accepts_nested_attributes_for :client_extra_info,                       reject_if: :all_blank
  accepts_nested_attributes_for :client_services, allow_destroy: true,    reject_if: proc { |atr| atr['service_id'].eql?('0') }
  accepts_nested_attributes_for :client_segment, reject_if: :all_blank
  validates :phone_number, uniqueness: true, allow_blank: true

  mount_uploader :doc, DocUploader

  enum pay_with: [:card, :custom]

  scope :bussiness,     -> { where(corp: true).order(created_at: :desc) }
  scope :not_bussiness, -> { where(corp: false).order(created_at: :desc) }
  

  scope :name_contains,    -> (name) { where('lower(name) like :name', name: "%#{name.downcase}%").order(created_at: :desc) }
  scope :name_equals,      -> (name) { where('lower(name) = :name', name: name.downcase).order(created_at: :desc) }
  scope :name_starts_with, -> (name) { where('lower(name) like :name', name: "#{name.downcase}%").order(created_at: :desc) }
  scope :name_ends_with,   -> (name) { where('lower(name) like :name', name: "%#{name.downcase}").order(created_at: :desc) }

  scope :email_contains,    -> (email) { where('email like :email', email: "%#{email}%").order(created_at: :desc) }
  scope :email_equals,      -> (email) { where('email = :email', email: email).order(created_at: :desc) }
  scope :email_starts_with, -> (email) { where('email like :email', email: "#{email}%").order(created_at: :desc) }
  scope :email_ends_with,   -> (email) { where('email like :email', email: "%#{email}").order(created_at: :desc) }

  scope :phone_number_contains,    -> (phone_number) { where('phone_number like :phone_number', phone_number: "%#{phone_number}%").order(created_at: :desc) }
  scope :phone_number_equals,      -> (phone_number) { where('phone_number = :phone_number', phone_number: phone_number).order(created_at: :desc) }
  scope :phone_number_starts_with, -> (phone_number) { where('phone_number like :phone_number', phone_number: "#{name}%").order(created_at: :desc) }
  scope :phone_number_ends_with,   -> (phone_number) { where('phone_number like :phone_number', phone_number: "%#{phone_number}").order(created_at: :desc) }

  # TODO на функцию написать тест
  def average_check
    prices = prices_orders_arr

    return 0 if prices.empty?

    prices.reduce(:+)/prices.size.to_f.round(2)
  end

  def custom_token_validation_response
    ::Api::V2::ClientSerializer.new( self, root: false ).as_json
  end

  def prices_orders_arr
    prices_arr = []
    completed_paid_orders.each do |order|
      order.services.map { |e| prices_arr << e.price.to_i }
    end

    prices_arr
  end

  def active_orders
    order_items.where(status: [3,4,5,7,8,9])
  end

  def canceled_orders
    order_items.where(status: 12)
  end

  def completed_paid_orders
    order_items.where(status: 11)
  end

  def all_orders
    order_items
  end

  def age
    b_day.present? ? calc_age : 'Unknown'
  end

  def referal
    "#{ENV['HOST']}?#{try(:referal_link).try(:link)}"
  end

  def create_categories_for_client(categories_params)
    if categories_params.present?
      categories_params.each do |category|
        client_categories.create(category_id: category.to_i) if category.present?
      end
    end
  end

  private

  def calc_age
    now = Time.now.utc.to_date
    now.year - b_day.year - ((now.month > b_day.month || (now.month == b_day.month && now.day >= b_day.day)) ? 0 : 1)
  end

  def send_email
    ::Api::V1::SendEmailWorker.perform_async(
      client_id: self.id, email_method: 'welcome_email', order_id: ''
    ) unless Rails.env.eql?('test')
  end

  def create_referal_link
    self.create_referal_link! link: "ref=#{self.email.split('@').first}"
  end

  def add_gift_card
    ::Api::V2::GiftCards::Append.call(resource: self)
  end

  def phone_number_format
    result = ::Support::ChangePhoneNumber.call(phone_number: self.phone_number)
    self.phone_number = result.phone
  end

  def commands
    send_email if Setting.try(:first).try(:send_welcome_email)
    create_referal_link
    add_gift_card
  end

  def self.clients_with_ios_device
    User::Client.joins(:devices).where("devices.platform = :device_platform", device_platform: 'IOS').uniq.count
  end

  def self.clients_with_android_device
    User::Client.joins(:devices).where("devices.platform = :device_platform", device_platform: 'android').uniq.count
  end
end
