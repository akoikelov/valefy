# master - user, which provide repair services
class User::ServiceMan < User
  include ActiveAdminLib
  include Filterable

  before_validation :phone_number_format

  has_many :feedbacks, class_name: 'ClientFeedback', foreign_key: :serviceman_id
  has_many :master_feedbacks, foreign_key: :master_id
  has_many :order_items, foreign_key: :master_id
  has_many :helps, foreign_key: :master_id
  has_many :master_categories
  has_many :categories, through: :master_categories
  has_one  :calendar, foreign_key: :master_id, dependent: :destroy # TODO in future, I will remove this table
  has_one  :pro_information, foreign_key: :pro_id, dependent: :destroy # TODO in future, I will remove this table
  has_many :locations, class_name: 'MasterLocation', foreign_key: :master_id
  has_many :sms_histories,   foreign_key: :master_id
  has_many :devices,   foreign_key: :master_id
  has_many :invoices,  foreign_key: :master_id
  has_many   :contracts, through: :team
  has_many   :transports, foreign_key: :master_id
  has_many   :work_hours, foreign_key: :master_id

  belongs_to :company
  belongs_to :team
  belongs_to :district

  has_one  :stripe_info, class_name: 'MasterStripeInfo', foreign_key: :master_id
  # has_one  :managed_account, foreign_key: :serviceman_id,                          :dependent => :destroy
  has_one  :stripe_account , class_name: 'ProStripeAccount', foreign_key: :pro_id, :dependent => :destroy

  validates :name, :b_day, :last_name, :email, :company_id, :phone_number, presence: true

  accepts_nested_attributes_for :stripe_info, allow_destroy: true
  accepts_nested_attributes_for :calendar,    allow_destroy: true

  scope :active,               -> { joins(:order_items).where(order_items: { status: [4,7,8] }) } # TODO разобрать что здесь использовать includes or joins. Замерить benchmaerk'ом
  scope :do_order,             -> { joins(:order_items).where(order_items: { status: [7,8] }) }
  scope :is_mobile,            -> (mobile) { where(mobile: mobile) }
  scope :not_consist_in_team,  -> { where(team_id: nil) }

  validates :phone_number, uniqueness: true, allow_blank: true

  JOIN_EMAIL = 'sortd16@gmail.com' # email where will send info about join master
  DAYLY_RATE = 3 #  The number of orders to be performed by the PRO

  after_save :create_work_hours

  def create_work_hours
    if !work_hours.present? && start_work_date.present?
      (0..6).each do |day|
        work_hours.create! start_day: ProInformation::BEGIN_DAY, end_day: ProInformation::END_DAY, day: day
      end
    end
  end

  def password_required?
    new_record? ? false : super
  end

  def services
    services_arr = []
    self.categories.each do |item|
      item.subcategories.each do |item|
        item.services.all.map { |e| services_arr << e.id  }
      end
    end

    services_arr
  end

  def phone_number_format
    result = ::Support::ChangePhoneNumber.call(phone_number: self.phone_number)
    self.phone_number = result.phone
  end

  def stripe_account_status
    if stripe_account.present? && stripe_account.try(:account_uid).present?
      account = Stripe::Account.retrieve(stripe_account.account_uid)
      return account.legal_entity.verification.status
    else
      'Not Created'
    end
  end

  def pro_schedule(start_date, end_date)
    schedule_array = []
    start_frequency = start_date ? start_date.to_date : Date.today - 1.year
    end_frequency = end_date ? end_date.to_date : Date.today + 1.year

    work_hours.each do |el|
      schedule = IceCube::Schedule.new(now = start_work_date)
      schedule.add_recurrence_rule(IceCube::Rule.weekly.day(el.day.to_sym))
      array = schedule.occurrences_between(start_frequency, end_frequency)
      array.each do |e|
        start_day = Time.zone.parse("#{e.year}-#{e.month}-#{e.day} #{el.start_day}")
        end_day   = Time.zone.parse("#{e.year}-#{e.month}-#{e.day} #{el.end_day}")

        schedule_array << { master_id: id, start: start_day, end: end_day, id: el.id }
      end
    end

    schedule_array
  end

  class << self
    def radius
      @radius = Setting.first.radius if Setting.count > 0

      @radius ||= 5 # Distance for work in km.
    end

    def near_order?(order, master)
      coords_a = [order.lat, order.lon]
      coords_b = [master.lat, master.lon]

      result = Geocoder::Calculations.distance_between(coords_a, coords_b, units: :km)

      result <= self.radius
    end

    def free_pros_arr
      free = []

      all.each do |pro|
        free << pro if pro.order_items.busy.count.zero?
      end

      free
    end

    def free_pros_arr_by(week_day)
      free = []

      not_consist_in_team.each do |pro|
        if pro.pro_information.present?
          info = pro.pro_information

          free << pro if info.work_schedule[week_day.downcase]['available']
        end
      end

      free
    end

    def free_pros_ids
      free = []

      all.each do |pro|
        free << pro.id if pro.order_items.busy.count.zero?
      end

      free
    end

    def masters_token(order, platform)
      tokens = []
      if order.service_id.present?
        service = Service.find(order.service_id)
      else
        service = order.services.first
      end

      masters = self.includes(:categories, :devices)
                    .where('categories.id = ?', service.subcategory.category.id)
                    .where('devices.platform = ?', platform)
                    .where(onduty: true)
                    .references(:categories, :devices)

      masters.each do |item|
        if near_order?(order, item)
          user_tokens = item.devices.map(& :id_registration_firebase).uniq
          user_tokens.map { |e| tokens << e }
        end
      end

      tokens
    end

    def available
      where.not(id: active.map(& :id))
    end
  end
end
