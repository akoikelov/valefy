class CustomerCharge < ActiveRecord::Base
  belongs_to :customer_account, class_name: 'CustomerAccount'
  belongs_to :order_item

  enum charge_type: [:payment, :tip]

  scope :tip_charge, -> { where(charge_type: 1).last }

  def self.tip
    'tip'
  end

  def self.payment
    'payment'
  end

  def capture!
    update(capture: true)
  end
end
