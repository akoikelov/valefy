class ReferalLink < ActiveRecord::Base
  belongs_to :client, class_name: "User::Client", foreign_key: :client_id
  validates :link, :client_id, presence: true
end
