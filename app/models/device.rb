# Store data about device info (id registration Firebase, platform, push enabled or not)
class Device < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id

  validates :id_registration_firebase, presence: true
  validates :platform,                 presence: true
  validates :token_client_id,          presence: true
  validates :app,                      presence: true
end
