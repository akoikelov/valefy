class Help < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan'

  validates :question, presence: true
end
