class Invoice < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :master, class_name: 'User::ServiceMan'
  belongs_to :order,  class_name: 'OrderItem'
  has_many   :line_items

  INVOICE_TYPE_CUSTOMER='customer'
  INVOICE_TYPE_PROVIDER='provider'

  def net_amount
    sum_services + sum_additional_payment
  end

  def tax_amount
    fees
  end

  def total
    net_amount
  end

  def total_with_fees
    net_amount - fees
  end

  private

  def sum_services
    line_items.map(& :price).sum
  end

  def sum_additional_payment
    line_items.map(& :additional_price).sum
  end

  def fees
    (((OrderItem::FEES * (net_amount.to_r * 100)) / 100)/100).to_f
  end
end
