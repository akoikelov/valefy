class WorkHour < ActiveRecord::Base
  enum day: { sunday: 0, monday: 1, tuesday: 2, wednesday: 3, thursday: 4, friday: 5, saturday: 6 }
  belongs_to :master, class_name: 'User::ServiceMan', foreign_key: :master_id

  validates :start_day, :end_day, :day, :master_id, presence: true
  validates :day, uniqueness: { scope: :master_id }
end
