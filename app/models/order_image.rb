class OrderImage < ActiveRecord::Base
	validates :link, presence: true
end
