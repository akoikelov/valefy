class Company < ActiveRecord::Base
  has_many   :locations, class_name: 'CompanyLocation',  dependent: :destroy
  has_many   :services,  class_name: 'CompanyService',   dependent: :destroy
  has_many   :pros,      class_name: 'User::ServiceMan', dependent: :destroy
  has_many   :clients,   class_name: 'User::Client',     dependent: :destroy
  has_many   :order_items, through: :clients
  belongs_to :admin_user

  validates :title, :platform, presence: true

  enum platform: [:valefy, :freelancing, :aggregator, :park, :franchise]

  after_create do
    copy_paste_services
  end

  private

  def copy_paste_services
    # TODO сделать тесты на проверку услуг при создании компании

    Service.all.each do |service|
      services.create! service_id: service.id, price: service.price
    end
  end
end
