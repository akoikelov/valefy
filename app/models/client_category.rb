class ClientCategory < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  belongs_to :category

  validates :client_id, :category_id, presence: true
end
