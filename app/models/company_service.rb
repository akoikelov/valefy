class CompanyService < ActiveRecord::Base
  belongs_to :admin_user
  belongs_to :company
  belongs_to :service

  validates :service_id, :price, :company_id, presence: true

  def self.price_by(service_id)
    return 0.0 unless service_id.present?

    find_by(service_id: service_id).price.to_i
  end
end
