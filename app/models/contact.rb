class Contact < ActiveRecord::Base
  validates :service_name, presence: true
  validates :service_name, :link, uniqueness: true
end
