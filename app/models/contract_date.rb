class ContractDate < ActiveRecord::Base
  belongs_to :contract
  belongs_to :team

  validates :order_date, :team_id, :office_id, presence: true
end
