class ProStripeAccount < ActiveRecord::Base
  belongs_to :pro, class_name: 'User::ServiceMan', foreign_key: :pro_id

  validates :tax_id, :tax_id, :personal_id, :address_line1,
            :id_document_image, :account_number,
            :city, :state, presence: true
  
  mount_uploader :id_document_image, StripeFileUploader
end
