class MasterCategory < ActiveRecord::Base
  belongs_to :master, class_name: 'User::ServiceMan'
  belongs_to :category
end
