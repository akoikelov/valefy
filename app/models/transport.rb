class Transport < ActiveRecord::Base
  validates :car, presence: true
end
