class Subscription < ActiveRecord::Base
  has_many :order_items
  belongs_to :client, class_name: 'User::Client'
end
