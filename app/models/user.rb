#model of user
class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable
          # :omniauthable, :confirmable, :omniauth_providers => [:facebook]
  include DeviseTokenAuth::Concerns::User

  PHONE_NOT_EXIST = ''
  LAST_NAME_NOT_EXIST = ''

  def self.by_company(id = nil)
    where(company_id: id).order(created_at: :desc)
  end

  def active_orders
    order_items.where(status: [3,4,5,7,8])
  end

  def custom_token_validation_response
    ::Api::Web::V1::UserSerializer.new( self, root: false ).as_json
  end

  def bithday
    b_day.present? ? b_day.strftime('%d %B %Y') : 'Unknown'
  end

  class << self
    def find_or_create_fb profile
      where( fb_params(profile) ).first_or_initialize
    end

    def confirm_phone!
      update(confirm_phone_number: true)
    end

    private

    # temp code

    def fb_email profile
      unless profile['email'].present?
        email = "fb_#{profile['id']}@example.com"
      else
        email = profile['email']
      end
    end

    def fb_params profile
      {
        email: fb_email(profile),
        uid: profile['id'],
        name: profile['name'],
        provider: 'facebook',
        type: profile['type']
      }
    end

    # temp code

    def search_params
      {
        onduty: true
      }
    end
  end
end
