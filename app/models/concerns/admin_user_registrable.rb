module AdminUserRegistrable
  extend ActiveSupport::Concern

  def add_company
    create_company(
      title: 'Company title',
      description: 'Company description',
      platform: 'freelancing'
    )
  end
end
