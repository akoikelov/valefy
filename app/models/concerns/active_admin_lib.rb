module ActiveAdminLib
  extend ActiveSupport::Concern

  included do
    ransacker :activity,
      formatter: proc { |selected|
          results = joins(:order_items)
                    .where("order_items.order_time >= ?", Time.zone.now.beginning_of_day)
                    .where(order_items: { status: 11 })
                    .map(& :id)

        results = results.present? ? results : nil
      }, splat_params: true do |parent|
      parent.table[:id]
    end
  end
end
