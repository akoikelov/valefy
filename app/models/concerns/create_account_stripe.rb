module CreateAccountStripe
  extend ActiveSupport::Concern

  included do
    after_create :create_managed_account
  end

  def create_managed_account
    Api::Masters::V1::StripeLib::CreateManaged.call( current_user: self )

  rescue Stripe::CardError => e
    Rails.logger.error("#{e.json_body}: #{e.backtrace}")
  rescue Stripe::RateLimitError => e
    Rails.logger.error("#{e.json_body}: #{e.backtrace}")
  rescue Stripe::InvalidRequestError => e
    Rails.logger.error("#{e.json_body}: #{e.backtrace}")
  rescue ActiveRecord::RecordNotFound => e
    Rails.logger.error("#{e.message}: #{e.backtrace}")
  rescue => e
    Rails.logger.error("#{e.message}: #{e.backtrace}")
  end
end
