class Contract < ActiveRecord::Base
  belongs_to :client, class_name: 'User::Client'
  has_many   :contract_dates
  has_many :contract_services
  has_many :services, through: :contract_services

  validates :expiration_date, presence: true

  accepts_nested_attributes_for :contract_dates, :allow_destroy => true, reject_if: proc { |att| att['order_date'].blank? }
end
