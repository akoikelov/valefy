class Calendar < ActiveRecord::Base
  belongs_to :master, class_name: "User::ServiceMan", foreign_key: :master_id

  validates :master_id, :time_plan, :start_work, :end_work, presence: true

  HOURS_OF_DAY=[
    '8:00', '9:00', '10:00', '11:00', '12:00', '13:00', '14:00',
    '15:00', '16:00', '17:00', '18:00', '19:00', '20:00'
  ]
end
