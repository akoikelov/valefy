class Team < ActiveRecord::Base
  has_many :masters, class_name: 'User::ServiceMan'
  has_many :contract_dates
end
