// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

//= require panel/perfect-scrollbar
//= require panel/bootstrap.bundle
//= require panel/app
//= require panel/jquery.flot
//= require panel/jquery.flot.pie
//= require panel/jquery.flot.time
//= require panel/jquery.flot.resize
//= require panel/jquery.flot.orderBars
//= require panel/curvedLines
//= require panel/jquery.flot.tooltip
//= require panel/jquery.sparkline
//= require panel/countUp
//= require panel/jquery-ui.min
//= require panel/jquery.vmap
//= require panel/jquery.vmap.world
//= require panel/app-dashboard
//= require panel/select2.full
//= require panel/app-form-elements
//= require panel/bootstrap-datetimepicker
//= require panel/bootstrap-slider
//= require panel/jquery.niftymodals
//= require panel/jquery.gritter
//= require panel/app-ui-notifications
//= require panel/fullcalendar
//= require panel/app-page-calendar
//= require panel/custom

