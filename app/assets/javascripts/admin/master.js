$(document).on('ready', function(){


  var $has_many_add = $('.calendar .has_many_add');
  var $has_many_remove = $('.calendar .has_many_remove');

  $(document).on('click', 'a', function(event){


    if (event.result) {
      // only for calendar
      if (event.result[0].className == 'has_many_container calendar') {

        if (event.target.text == 'Add New Calendar') {
          $(event.target).hide();
        } else if (event.target.text == 'Remove') {
          $has_many_add.show();
        }
      }
    }
  });

  // $(document).on('change', '#user_service_man_calendar_attributes_start_work', function(){
  //   $('#user_service_man_calendar_attributes_end_work').removeAttr('disabled');
  //
  //   selected_value = $(this).val();
  //   // $('#user_service_man_calendar_attributes_end_work option:selected').val(selected_value).next().attr('selected', 'selected');
  //
  //   $('#user_service_man_calendar_attributes_end_work option').each(function() {
  //     if ( $(this).val() == selected_value ) {
  //       return false;
  //     } else { $(this).hide(); }
  //   });
  // });
})
