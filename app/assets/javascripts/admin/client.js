$(document).on('ready', function(){

  var corpObj = document.getElementById('user_client_corp');

  if(corpObj) { showOrHide(corpObj) }

  $("#user_client_corp").change(function() {
    showOrHide(this)
  });

  function removeHide() {
    $('#user_client_tariff_plan_id_input').removeClass('hide')
    $('#user_client_pay_with_input').removeClass('hide')
    $('#user_client_corp_discount_input').removeClass('hide')
  }

  function addHide() {
    $('#user_client_tariff_plan_id_input').addClass('hide')
    $('#user_client_pay_with_input').addClass('hide')
    $('#user_client_corp_discount_input').addClass('hide')
  }

  function showOrHide(obj) {
    if(obj.checked) { removeHide() } else { addHide() }
  }
});
