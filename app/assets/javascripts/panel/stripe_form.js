// Create a Stripe client.
var stripe = Stripe('pk_test_g3dXVVVb3po84SQ6uAqoxhXY');

// Create an instance of Elements.
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
  base: {
    color: '#32325d',
    lineHeight: '18px',
    fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
    fontSmoothing: 'antialiased',
    fontSize: '16px',
    '::placeholder': {
      color: '#aab7c4'
    }
  },
  invalid: {
    color: '#fa755a',
    iconColor: '#fa755a'
  }
};

// Create an instance of the card Element.
var card = elements.create('card', {
  style: style
});

// Add an instance of the card Element into the `card-element` <div>.
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function (event) {
  var displayError = document.getElementById('card-errors');
  if (event.error) {
    displayError.textContent = event.error.message;
  } else {
    displayError.textContent = '';
  }
});

// Handle form submission.
var form = document.getElementById('add-card-form');
form.addEventListener('submit', function (event) {
  event.preventDefault();
  start_preloader();

  stripe.createToken(card).then(function (result) {
    if (result.error) {
      // Inform the user if there was an error.
      var errorElement = document.getElementById('card-errors');
      errorElement.textContent = result.error.message;
    } else {
      $.ajax({
        method: 'POST',
        url: '/admin/panel/add_cards',
        data: {
          stripe_token: result.token.id,
          client_id: $('.create-order_client-id').attr('data-id'),
          authenticity_token: $('meta[name="authenticity-token"]').attr('content')
        },
        success: function(data){
          if(data.success) { location.reload(); }
        }
      })
    }
  });
});