function stop_preloader() {
  $('.preloader-status').fadeOut();
  $('.preloader').delay(10).fadeOut('slow');
  $('body').delay(10).css({ 'overflow': 'visible' });
}

function start_preloader() {
  $('.preloader').show()
  $('.preloader-status').show()
}