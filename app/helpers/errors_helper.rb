module ErrorsHelper
  def capture_exception(e)
    Raven.capture_exception(e)
    Rails.logger.error("#{e.message}: #{e.backtrace}")
    Raven.user_context(id: current_user.id, email: current_user.email) if try(:current_user)
  end
end
