module ApplicationHelper
  def fetch_time_by zone
    time = OrderItem::TIME_ZONES[zone]

    time ||= OrderItem::TIME_ZONES[OrderItem::DEFAULT_TIME_ZONE]
  end

  def fetch_key_by zone
    zone ||= 'Europe/Dublin'
  end

  def order_time_by_zone(order, time_zone)
    order.order_time.in_time_zone(fetch_time_by(time_zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
  end

  def human_order_time_by_zone(order, time_zone)
    order.order_time.in_time_zone(fetch_time_by(time_zone)).strftime('%d - %b, %H:%M')
  end

  def current_date
    (DateTime.now + 2.hours).strftime('%Y-%m-%dT%H:%M:%SZ')
  end

  def time_with_format(order)
    order.order_time.strftime('%d %b %H:%M')
  end

  def greater_40_min?(diff)
    diff[:hour] > 0 || diff[:day] > 0 || diff[:minute] >= 40
  end

  def less_10_min?(diff)
    diff[:hour] == 0 && diff[:day] == 0 && diff[:minute] <= 10
  end

  def greater_10_less_40_min?(diff)
    diff[:hour].zero? && diff[:day].zero? && (11..39).include?(diff[:minute])
  end

  def exp_date_email(date)
    date.strftime('%B %d, %Y')
  end

  def end_work_date(date)
    date.strftime('%B %d, %Y')
  end

  def total_number_clients
    ((User::Client.count + Partner.count)/100.0).ceil * 100
  end

  def be_aside_css_class(client)
    'be-aside' if client.present?
  end

  def nick_from_email
    current_admin_user.email.split('@').first
  end

  def admin_user_role_hidden(admin_user)
    'hidden' if !@admin_user.company?
  end

  def clients_sum_of_orders(user)
    return 0 unless user.order_items.present?

    prices = []

    user.order_items.history.each do |order|
      prices << order.services.map(& :price).sum
    end

    prices.sum.to_i
  end

  def link_to_add_row(name, f, association, **args)
    new_object = association == (:offices || :contract_dates) ? f.object.send(association).klass.new : f.object.send(association)
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize, f: builder)
    end
    link_to(name, '', class: "add_fields " + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end

  def link_to_add_roww(name, f, association, **args)
    new_object = association == :contract_dates ? f.object.send(association).klass.new : f.object.send(association)
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render(association.to_s.singularize, f: builder)
    end

    link_to(name, '#', class: "add_fields " + args[:class], data: {id: id, fields: fields.gsub("\n", "")})
  end

  def sidebar_item_active(link)
    URI.decode(request.original_fullpath).eql?(link) ? 'active open' : ''
  end

  def get_custom_date(date)
    "#{date.strftime("%I:%M %p")}, #{date.strftime("%A")}, #{date.strftime("%d %B %Y")}" if date.present?
  end

  def get_phone_number(number)
    (number.starts_with?("+353") or number.starts_with?("+996")) ? "0#{number[4, number.length]}" : number
  end

  def get_auto_models
    [
      'Audi', 'Mazda', 'Mitsubishi', 'Nissan', 'Toyota', 'Ford', 'Bmw', 'Honda', 'Volkswagen', 'Subaru', 'Hyundai', 'Kia',
      'Chevy', 'Jeep', 'Dodge', 'Porsche', 'ALFA ROMEO', 'CHRYSLER', 'CITROEN', 'DACIA', 'PEUGEOT', 'RENAULT', 'SKODA', 'SUZUKI',
      'VOLVO', 'Chevrolet', 'Isuzu', 'Acura', 'Jaguar', 'Tesla', 'Cadillac', 'Lexus', 'Alfa Romeo', 'Genesis', 'Land Rover', 'Maserati'
    ]
  end

  def get_companies_for_clients_filter(companies)
    company_list = [['All companies', :all]]
    company_list += companies.map { |company| [ company.title, company.id ] }
  end

  def get_collection_models
    Rails.application.eager_load!

    ActiveRecord::Base.send(:subclasses).map(&:name)
  end
end
