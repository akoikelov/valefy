module ActiveAdminHelper
  def self.included(dsl)
  end

  def form_menu_items
    all_items = ActiveAdmin.application.namespace(:admin).resources
                           .select {|r| r.respond_to? :resource_name}
                           .map { |e| e.resource_name.human }
    all_items
  end

  def hash_menu_items
    ar_items = []

    all_items = ActiveAdmin.application.namespace(:admin)
                                       .resources
                                       .select {|r| r.respond_to? :resource_class_name}

    all_items.map{|e| ar_items << Hash[e.resource_class_name, e.resource_name.human]}

    ar_items
  end

  def resource_items
    all_items = ActiveAdmin.application.namespace(:admin)
                                       .resources
                                       .select {|r| r.respond_to? :resource_class_name}
                                       .map(& :resource_class_name)
  end

  def icons_feedback(feedback)
    i = 0

    while i < 5
      if i < feedback.rating
        img src: asset_path('admin/ic_rating_picked.png'), class: 'size_icon'
      else
        img src: asset_path('admin/ic_rating.png'), class: 'size_icon'
      end

      i = i + 1
    end
  end

  def sum_of_orders(user)
    return 0 unless user.order_items.present?

    prices = []

      user.order_items.history.each do |order|
          prices << order.services.map(& :price).sum
      end

    prices.sum.to_i
  end

  def avarage_earnings(master)
    sum = 0
    completed_orders = master.order_items.history

    completed_orders.each do |order|
      sum = sum + order.services.map(& :price).sum.to_i
    end

    sum.zero? ? "€ #{sum}" : "€ #{sum / completed_orders.count}"
  end

  def avarage_rating(master)
    ratings_arr = master.feedbacks.map(& :rating)
    sum = ratings_arr.sum

    sum.zero? ? sum : (sum / ratings_arr.count).to_i
  end

  def avarage_rating_icons(master)
    rating  = avarage_rating(master)

    i = 0

    while i < 5
      if i < rating
        img src: asset_path('admin/ic_rating_picked.png'), class: 'size_icon'
      else
        img src: asset_path('admin/ic_rating.png'), class: 'size_icon'
      end

      i = i + 1
    end
  end

  def color_of(status)
    status.eql?('in_transit') ? 'color_red' : 'color_green'
  end

  def from_timestamp_to_date(timestamp)
    "#{Time.at(timestamp).to_datetime.strftime("%I:%M %p")}, #{Time.at(timestamp).to_datetime.strftime("%A")}, #{Time.at(timestamp).to_datetime.strftime("%d %B %Y")}"
  end

  def time_with_sec(datetime)
    datetime.strftime("%H:%M:%S")
  end

  def is_image?(link)
    /\.(jpg|jpeg|png)/.match(link).to_s.present?
  end

  def schedule_info(schedule, day)
    start_time = schedule[day]['schedule']['start']
    end_time   = schedule[day]['schedule']['end']

    if schedule[day]['available']
      str = "#{day}: available, from: #{start_time} to: #{end_time}"
    else
      str = "#{day}: not available"
    end

    str
  end

  def checked(schedule, key)
    schedule[key]['available'] ? 'checked' : ''
  end

  def selected_str(schedule, hour, key, const_time)
    return selected(const_time, hour) if schedule.empty?

    _start = schedule[key]['schedule']['start']
    return selected(_start, hour)
  end

  def selected_end(schedule, hour, key, const_time)
    return selected(const_time, hour) if schedule.empty?

    _end = schedule[key]['schedule']['end']
    return selected(_end, hour)
  end

  def selected(first_time, second_time)
    first_time.eql?(second_time) ? 'selected' : ''
  end

  def feedbacks_list(params, feedbacks)
    if params[:master_id].present?
      feedbacks.where(serviceman_id: params[:master_id])
    else
      feedbacks
    end
  end

  def all_orders(params, orders)
    if params[:master_id].present?
      orders.where(master_id: params[:master_id])
    else
      orders
    end
  end
end
