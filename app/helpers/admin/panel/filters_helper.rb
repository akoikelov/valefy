module Admin
  module Panel
    module FiltersHelper
      def get_filter_options
        [['Contains', 'contains'], ['Equals', 'equals'], ['Starts with', 'starts_with'], ['Ends with', 'ends_with']]
      end

      def get_number_filter_options
        [['Equals', 'equals'], ['Greater than', 'greater_than'], ['Less than', 'less_than']]
      end
    end
  end
end