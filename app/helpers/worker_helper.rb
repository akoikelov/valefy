module WorkerHelper
  def send_push_notify(method_name, client)
    Notifications.new(send(:method_name, client)).push
  end

  def body_android_gift client
    {
      data: {
        title: I18n.t('sortd'),
        detail: 'You get gift card',
      },
      priority: 'high',
      registration_ids: tokens(client, 'android')
    }
  end

  def body_ios_gift client
    {
      notification: {
        body: 'You get gift card',
        title: I18n.t('sortd')
      },
      data: {},
      priority: 'high',
      registration_ids: tokens(client, 'IOS')
    }
  end

  def tokens user, platform
    user.devices.where(platform: platform).map(& :id_registration_firebase).uniq
  end
end
