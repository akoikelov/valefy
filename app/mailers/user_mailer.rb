class UserMailer < ActionMailer::Base
  default from: ENV['SMTP_USERNAME']

  helper :application
  
  def welcome_email(user)
    @user = user
    @url  = 'https://sortd.ie'
    mail(to: @user.email, subject: 'Welcome to VALEFY')
  end

  def join_master_email(user)
    @user = user
    @category = Category.find(user.category_id)

    email_to = User::ServiceMan::JOIN_EMAIL
    mail(to: email_to, subject: 'New Pro!')
  end

  def order_time_email(order, old_time)
    @user = order.client
    @order = order
    @old_time = old_time

    if order.additional_payment.present?
      if order.additional_payment.client_confirm?
        order.services.present? ? @amount = order.additional_payment.additional_price.to_i + order.total_sum.to_i : @amount = 0
      else
        order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
      end
    else
      order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
    end

    mail(to: @user.email, subject: 'Order time changed!')
  end

  def client_invoice_email(order)
    @user  = order.client
    @order = order

    if order.additional_payment.present?
      if order.additional_payment.client_confirm?
        order.services.present? ? @amount = order.additional_payment.additional_price.to_i + order.total_sum.to_i : @amount = 0
      else
        order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
      end
    else
      order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
    end


    mail(to: 'askar_kg_boy@mail.ru', subject: 'Invoice')
  end

  def client_created_order_email(order)
    @user  = order.client
    @order = order

    if order.additional_payment.present?
      if order.additional_payment.client_confirm?
        order.services.present? ? @amount = order.additional_payment.additional_price.to_i + order.total_sum.to_i : @amount = 0
      else
        order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
      end
    else
      order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
    end


    mail(to: @user.email, subject: 'Created order')
  end

  def owner_gift_card_email(gift)
    @gift = gift

    mail(to: gift.from_client.email, subject: 'Gift card')
  end

  def created_gift_to_exist_client_email(gift)
    @gift = gift

    mail(to: gift.to_client.email, subject: 'Gift card')
  end

  def created_gift_to_not_exist_client_email(gift)
    @gift = gift

    mail(to: gift.to_email, subject: 'Gift card')
  end

  def client_order_stationary_email(order)
    @order = order
    @user  = order.client
    @stationary = CompanyLocation.find(order.stationary_id)
    if order.additional_payment.present?
      if order.additional_payment.client_confirm?
        order.services.present? ? @amount = order.additional_payment.additional_price.to_i + order.total_sum.to_i : @amount = 0
      else
        order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
      end
    else
      order.services.present? ? @amount = order.total_sum.to_i : @amount = 0
    end
    mail(to: @stationary.email, subject: 'Created order for stationary')
  end

  def shared_user_first_order(shared_client, client, promo_code)
    @shared_client = shared_client
    @client = client
    @promo_code = promo_code
    mail(to: @shared_client.email, subject: 'First order of invited client!')
  end
end
