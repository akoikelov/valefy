# encoding: utf-8
# settings for image field
class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  # include CarrierWave::BombShelter

  def self.set_storage
    if Rails.env.qa? || Rails.env.production?
      :fog
    else
      :file
    end
  end

  storage set_storage

  def store_dir
    if Rails.env.qa? || Rails.env.production?
      "#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    else
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end
  end

  def extension_whitelist
    %w(jpg jpeg gif png pdf)
  end

  version :thumb_default do
    process resize_to_fit: [359, 135]
  end

  # def max_pixel_dimensions
  #   [4096, 4096]
  # end
end
