# encoding: utf-8

class BannerUploader < CarrierWave::Uploader::Base
  # Banner uploader, admin/banner.rb

  include CarrierWave::MiniMagick

  def self.set_storage
    env = Rails.env

    (env.qa? || env.production?) ? :fog : :file
  end

  storage set_storage

  def store_dir
    env = Rails.env

    (env.qa? || env.production?) ? aws_path : local_path
  end

  def extension_whitelist
    %w(jpg jpeg gif png)
  end

  def aws_path
    "#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def local_path
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :thumb_default do
    process resize_to_fit: [359, 135]
  end

  version :medium_default do
    process resize_to_fit: [800, 300]
  end
end
