# encoding: utf-8

class DocUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  def self.set_storage
    if Rails.env.qa? || Rails.env.production?
      :fog
    else
      :file
    end
  end

  storage set_storage

  def store_dir
    if Rails.env.qa? || Rails.env.production?
      "#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    else
      "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
    end
  end

  def extension_whitelist
    %w(jpg jpeg gif png pdf doc docx)
  end

end
