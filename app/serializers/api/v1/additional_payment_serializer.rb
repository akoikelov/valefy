module Api
  module V1
    class AdditionalPaymentSerializer < ActiveModel::Serializer
      attributes :id, :additional_price, :client_confirm, :rejected, :description

      def additional_price
        object.additional_price.to_i
      end
    end
  end
end
