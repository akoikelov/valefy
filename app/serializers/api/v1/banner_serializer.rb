module Api
  module V1
    class BannerSerializer < ActiveModel::Serializer
      attributes :id, :title, :image

      def image
        object.image.url
      end
    end
  end
end
