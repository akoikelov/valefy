module Api
  module V1
    module User
    # describe which attributes and relationships should be serialized
      class ClientSerializer < ActiveModel::Serializer
        attributes :id, :email, :name, :image, :phone_number, :confirm_phone_number,
                    :uid, :provider, :last_name, :b_day
      end
    end
  end
end
