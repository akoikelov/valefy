module Api
  module V1
    module User
    # describe which attributes and relationships should be serialized
      class MasterSerializer < ActiveModel::Serializer
        attributes :id, :name, :last_name, :phone_number, :image
      end
    end
  end
end
