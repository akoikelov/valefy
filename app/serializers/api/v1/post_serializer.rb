module Api
  module V1
    class PostSerializer < ActiveModel::Serializer
      attributes :id, :title, :description, :image
    end
  end
end
