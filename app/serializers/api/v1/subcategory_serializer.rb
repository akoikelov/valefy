module Api
  module V1
    # describe which attributes and relationships should be serialized
    class SubcategorySerializer < ActiveModel::Serializer
      attributes :id, :title, :title_ru, :image, :icon, :icon_web

      def image
        object.image.present? ? object.image.url : ''
      end

      def icon
        object.icon.present? ? object.icon.url : ''
      end

      def icon_web
        object.icon_web.present? ? object.icon_web.url : ''
      end
    end
  end
end
