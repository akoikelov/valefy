module Api
  module V1
    class OrderItemShowSerializer < BaseSerializer
      attributes :id, :service, :sub_category, :category,
                 :status, :order_now, :order_time, :lon, :lat, :accepted_time,
                 :address, :description, :images, :start_work, :end_work, :created_at,
                 :price

      attribute  :master,             if: :master_present?

      has_many     :services,                serializer: ServiceSerializer
      belongs_to   :additional_payment,      serializer: AdditionalPaymentSerializer, if: :additional_payment_present?

      def additional_payment
        object.additional_payment
      end

      def order_time
        object.order_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def created_at
        object.created_at.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def start_work
        object.start_work.present? ? object.start_work.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def end_work
        object.end_work_time.present? ? object.end_work_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def accepted_time
        object.accepted_time.present? ? object.accepted_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def price
        if object.service_id.present?
          srv = Service.find(object.service_id)
          if object.additional_payment.present?
            if object.additional_payment.client_confirm?
              price_order = object.additional_payment.additional_price.to_i + srv.price.to_i
            else
              price_order = srv.price.to_i
            end
          else
            price_order = srv.price.to_i
          end
        elsif object.services.present?
          if object.additional_payment.present?
            if object.additional_payment.client_confirm?
              price_order = object.additional_payment.additional_price.to_i + object.services.map(& :price).sum.to_i
            else
              price_order = object.services.map(& :price).sum.to_i
            end
          else
            price_order = object.services.map(& :price).sum.to_i
          end
        end

        price_order
      end

      def sub_category
        if object.service_id.present?
          srv = Service.find(object.service_id)
          result = srv.subcategory.attributes
        elsif object.services.present?
          service = object.services.first
          result = service.subcategory.attributes
        end

        result
      end

      def master
        object.master
      end

      def category
        if object.service_id.present?
          srv = Service.find(object.service_id)
          result = srv.subcategory.category.attributes
        elsif object.services.present?
          service = object.services.first
          result = service.subcategory.category.attributes
        end
      end

      def service
        if object.service_id.present?
          srv = Service.find(object.service_id)
          hash = {
            id: srv.id,
            title: srv.title,
            title_ru: srv.title_ru,
            price: srv.price.to_i,
            category_id: srv.subcategory_id
          }
        else
          hash = {}
        end

        hash
      end

      def images
        arr = []
        object.images.map { |i| arr << i }

        arr
      end

      def master_present?
        master.present?
      end

      def additional_payment_present?
        object.additional_payment.present?
      end

      def zone
        @instance_options[:time_zone] ||= OrderItem::DEFAULT_TIME_ZONE
      end
    end
  end
end
