module Api
  module V1
    class OrderItemSerializer < BaseSerializer
      attributes :id, :master, :status, :sub_category, :category,
                 :order_now, :order_time, :service,
                 :address, :description, :start_work, :end_work, :accepted_time, :lon, :lat, :images, :created_at

      has_many     :services,                serializer: ServiceSerializer
      belongs_to   :additional_payment,      serializer: AdditionalPaymentSerializer, if: :additional_payment_present?


      def master
        object.master.present? ? object.master : nil
      end

      def order_time
        object.order_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def created_at
        object.created_at.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def start_work
        object.start_work.present? ? object.start_work.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def end_work
        object.end_work_time.present? ? object.end_work_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def accepted_time
        object.accepted_time.present? ? object.accepted_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def service
        if object.service_id.present?
          srv = Service.find(object.service_id)
          hash = {
            id: srv.id,
            title: srv.title,
            title_ru: srv.title_ru,
            price: srv.price.to_i,
            category_id: srv.subcategory_id
          }
        else
          hash = {}
        end

        hash
      end

      def category
        if object.service_id.present?
          srv = Service.find(object.service_id)
          result = srv.subcategory.category.attributes
        elsif object.services.present?
          service = object.services.first
          result = service.subcategory.category.attributes
        end
      end

      def sub_category
        if object.service_id.present?
          srv = Service.find(object.service_id)
          result = srv.subcategory.attributes
        elsif object.services.present?
          service = object.services.first
          result = service.subcategory.attributes
        end

        result
      end

      def images
        arr = []
        object.images.map {|i| arr << i}

        arr
      end

      def additional_payment_present?
        object.additional_payment.present?
      end

      def zone
        @instance_options[:time_zone] ||= OrderItem::DEFAULT_TIME_ZONE
      end
    end
  end
end
