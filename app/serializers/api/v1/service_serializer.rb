module Api
  module V1
    class ServiceSerializer < ActiveModel::Serializer
      attributes :id, :category_id, :title, :title_ru, :price

      def category_id
        object.subcategory_id
      end

      def price
        object.price.to_i
      end
    end
  end
end
