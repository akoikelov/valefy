module Api
  module Web
    module V1
    # describe which attributes and relationships should be serialized
      class UserSerializer < ActiveModel::Serializer
        attributes :id, :email, :name, :image, :phone_number, :confirm_phone_number,
                    :uid, :provider, :last_name, :b_day, :type
      end
    end
  end
end
