module Api
  module V2
    class OrderItemShowSerializer < BaseSerializer
      attributes :id, :sub_category, :category,:status, :baby_seat,
                 :order_now, :order_time, :lon, :lat, :accepted_time,
                 :address, :description, :images, :before_work_images,
                 :after_work_images, :start_work, :end_work, :created_at,
                 :price,:corp, :pay_with, :count_vehicles, :total_price, :reg_numbers, :feedback,
                 :car_count,:suv_count, :minivan_count

      belongs_to   :master,                  serializer: MasterSerializer
      belongs_to   :client,                  serializer: ClientSerializer
      has_many     :services,                serializer: ServiceSerializer
      belongs_to   :additional_payment,      serializer: AdditionalPaymentSerializer, if: :additional_payment_present?


      def order_time
        object.order_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def created_at
        object.created_at.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def start_work
        object.start_work.present? ? object.start_work.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def end_work
        object.end_work_time.present? ? object.end_work_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def accepted_time
        object.accepted_time.present? ? object.accepted_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
      end

      def corp
        object.client.corp?
      end

      def pay_with
        if object.client.pay_with.eql?('card')
          'app'
        else
          object.client.pay_with
        end
      end

      def feedback
        if object.feedbacks.present?
          ClientFeedbackSerializer.new(object.feedbacks.last, scope: scope, root: false, event: object)
        else
          {}
        end
      end

      def price
        if object.services.present?
          if object.additional_payment.present?
            if object.additional_payment.client_confirm?
              return object.additional_payment.additional_price.to_i + object.services.map(& :price).sum.to_i
            else
              return object.services.map(& :price).sum.to_i
            end
          else
            return object.services.map(& :price).sum.to_i
          end
        end
      end

      def sub_category
        if object.services.present?
          service = object.services.first

          return service.subcategory.attributes
        end
      end

      def category
        if object.services.present?
          service = object.services.first

          return service.subcategory.category.attributes
        end
      end

      def count_vehicles
        object.car_count + object.minivan_count + object.suv_count
      end

      def images
        arr = []
        object.images.map { |i| arr << i }

        arr
      end

      def master_present?
        master.present?
      end

      def zone
        @instance_options[:time_zone]
      end
    end
  end
end
