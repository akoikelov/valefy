module Api
  module V2
    # describe which attributes and relationships should be serialized
    class CategorySerializer < ActiveModel::Serializer
      attributes :id, :title, :title_ru, :description, :image,
                 :icon, :icon_web, :duration, :min_price, :subcategories, :min_price_stations

      def image
        object.image.present? ? object.image.url : ''
      end

      def subcategories
        if current_user_not_corp?
          object.subcategories.map do |subcat|
            SubcategorySerializer.new(subcat, scope: scope, root: false, event: object)
          end
        elsif current_user_corp?
          object.corp_subcategories.map do |subcat|
            SubcategorySerializer.new(subcat, scope: scope, root: false, event: object)
          end
        end
      end

      def icon
        object.icon.present? ? object.icon.url : ''
      end

      def icon_web
        object.icon_web.present? ? object.icon_web.url : ''
      end

      def min_price
        object.services.map(&:price).min.to_i
      end

      def min_price_stations
        ids = object.services.map(&:id)
        min = StationService.where(service_id: ids).map(&:price).min.to_i

        min
      end

      def current_user_corp?
        current_user.present? && current_user.try(:corp)
      end

      def current_user_not_corp?
        current_user.try(:corp) ? false : true
      end

      private

      def current_user
        @instance_options[:current_user]
      end
    end
  end
end
