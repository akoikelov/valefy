module Api
  module V2
    class BannerSerializer < ActiveModel::Serializer
      attributes :id, :title, :description, :image

      def image
        object.image.url
      end
    end
  end
end
