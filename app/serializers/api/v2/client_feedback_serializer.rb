module Api
  module V2
    class ClientFeedbackSerializer < ActiveModel::Serializer
      attributes :id, :rating, :comment, :polite, :orderly, :friendly,
                 :name, :image, :created_at

      def name
        object.client.name
      end

      def image
        object.client.image
      end

      def created_at
        object.created_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end
    end
  end
end
