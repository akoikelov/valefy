module Api
  module V2
    class OfficeSerializer < BaseSerializer
      attributes :id, :title, :lat, :lon
    end
  end
end
