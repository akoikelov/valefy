module Api
  module V2
    class BaseSerializer < ActiveModel::Serializer
      include ApplicationHelper

      def category
        object.category.present? ? Api::CategorySingleSerializer.new(object.category) : {}
      end

      def sub_category
        object.subcategory.present? ? SubcategorySerializer.new(object.subcategory) : {}
      end

      def feedback
        object.feedbacks.present? ? ClientFeedbackSerializer.new(
            object.feedbacks.last, scope: scope, root: false, event: object
          ) : {}
      end

      def additional_payment_present?
        object.additional_payment.present?
      end
    end
  end
end
