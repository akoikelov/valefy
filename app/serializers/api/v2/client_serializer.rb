module Api
  module V2
    # describe which attributes and relationships should be serialized
    class ClientSerializer < BaseSerializer
      attributes :id, :email, :name, :image, :phone_number, :confirm_phone_number,
                 :last_name, :address, :b_day, :cash, :corp, :corp_discount, :corp_discount,
                 :pay_with, :promocodes, :referal_link

      has_many :to_me_gift_cards,   serializer: GiftCardSerializer
      has_many :from_me_gift_cards, serializer: GiftCardSerializer

      def promocodes
        object.promo_codes.available
      end

      def image
        object.image
      end

      def referal_link
        object.referal
      end
    end
  end
end