module Api
  module V2
    class PostSerializer < ActiveModel::Serializer
      attributes :id, :title, :description, :image
    end
  end
end
