module Api
  module V2
    # describe which attributes and relationships should be serialized
    class CompanyLocationSerializer < BaseSerializer
      attributes :id, :title, :address, :lat, :lon, :phone_number, :email,
                 :price, :description, :image, :rating
      
      has_many :client_feedbacks

      def price
        cs = object.services
        cs.price_by(service_id)
      end

      def image
        object.image.present? ? object.image.url : ""
      end

      def description
        "some description"
      end

      def service_id
        @instance_options[:service_id]
      end
    end
  end
end
