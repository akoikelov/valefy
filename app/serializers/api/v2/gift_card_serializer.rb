module Api
  module V2
    # describe which attributes and relationships should be serialized
    class GiftCardSerializer < BaseSerializer
      attributes :id, :to_client_id, :from_client_id, :amount, :message,
                 :exp_date, :to_email, :service, :category, :subcategory

      def category
        object.category.attributes
      end

      def subcategory
        object.subcategory.attributes
      end

      def exp_date
        object.exp_date.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      end

      def zone
        @instance_options[:time_zone]
      end
    end
  end
end