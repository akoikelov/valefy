module Api
  module V2
    # describe which attributes and relationships should be serialized
    class MasterSerializer < ActiveModel::Serializer
      attributes :id, :email, :name, :last_name, :phone_number, :image, :onduty,
                  :confirm_phone_number, :b_day, :lon, :lat, :address, :corp,
                  :pay_with, :need_send_location, :time_send_location


      def time_send_location
        Setting.last.time_send_location
      end

      def need_send_location
        object.order_items.do_order.count > 0 ? true : false
      end

      def image
        object.image
      end
    end
  end
end
