module Api
  module V2
    class ServiceSerializer < ActiveModel::Serializer
      attributes :id, :category_id, :title, :title_ru,
                 :short_title, :price, :description

      def category_id
        object.subcategory_id
      end

      def price
        if current_user.present? && current_user.try(:client_services).present?
          result = current_user.client_services.where(service_id: object.id)

          if result.present?
            result.first.price.to_i
          else
            object.price.to_i
          end
        else
          object.price.to_i
        end
      end

      private

      def current_user
        @instance_options[:scope]
      end
    end
  end
end
