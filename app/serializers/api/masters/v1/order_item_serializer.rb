module Api
  module Masters
    module V1
      class OrderItemSerializer < BaseSerializer
        attributes :id, :master_id, :service, :baby_seat, :category,
                   :sub_category, :order_now, :order_time, :start_work, :end_work, :lon, :lat,
                   :car_count, :minivan_count, :suv_count, :address,
                   :description, :accepted_time, :status, :images, :before_work_images,
                   :after_work_images, :pets, :parking, :created_at, :count_vehicles, :is_station,
                   :model_auto, :reg_numbers, :total_price

        belongs_to   :master,                  serializer: MasterSerializer
        belongs_to   :client,                  serializer: ClientSerializer
        has_many     :services,                serializer: ServiceSerializer
        belongs_to   :additional_payment,      serializer: AdditionalPaymentSerializer, if: :additional_payment_present?

        def order_time
          object.order_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ')
        end

        def start_work
          object.start_work.present? ? object.start_work.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
        end

        def end_work
          object.end_work_time.present? ? object.end_work_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
        end

        def created_at
          object.created_at.present? ? object.created_at.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
        end

        def accepted_time
          object.accepted_time.present? ? object.accepted_time.in_time_zone(fetch_time_by(zone)).strftime('%Y-%m-%dT%H:%M:%S.%LZ') : ''
        end

        def is_station
          object.stationary_id.present? ? true : false
        end

        def count_vehicles
          object.car_count + object.minivan_count + object.suv_count
        end

        def service
          if object.service_id.present?
            srv = Service.find(object.service_id)
            hash = {
              id: srv.id,
              title: srv.title,
              title_ru: srv.title_ru,
              price: srv.price.to_i,
              category_id: srv.subcategory_id
            }
          else
            hash = {}
          end

          hash
        end

        def images
          arr = []
          object.images.map {|i| arr << i}

          arr
        end

        def additional_payment_present?
          object.additional_payment.present?
        end

        def zone
          @instance_options[:time_zone] ||= OrderItem::DEFAULT_TIME_ZONE
        end
      end
    end
  end
end
