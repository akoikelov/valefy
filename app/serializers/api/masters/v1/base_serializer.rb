module Api
  module Masters
    module V1
      class BaseSerializer < ActiveModel::Serializer
        include ApplicationHelper

        def category
          object.category.present? ? Api::CategorySingleSerializer.new(object.category) : {}
        end

        def sub_category
          object.subcategory.present? ? Api::V2::SubcategorySerializer.new(object.subcategory) : {}
        end
      end
    end
  end
end
