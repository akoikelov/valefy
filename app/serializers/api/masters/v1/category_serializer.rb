module Api
  module Masters
    module V1
      # describe which attributes and relationships should be serialized
      class CategorySerializer < ActiveModel::Serializer
        attributes :id, :title, :image, :subcategories, :description

        def subcategories
          build_nested_categories([], object)
        end

        def image
          object.image.present? ? object.image.thumb_default.url : ''
        end

        private

        # build categories like tree, for app get json format like this:
        #[{
        #    "title": "some title",
        #    "subcategories": [{
        #        "title": "some title",
        #        "subcategories": [{
        #            "title": "some title",
        #            "subcategories": [{
        #                "title": "some title",
        #                "subcategories": []}]
        #          }]
        #      }]
        #  }]

        def build_nested_categories(categories_arr, category)
          category.subcategories.each do |item|
            categories_arr << {
              id:            item.id,
              title:         item.title,
              image:         item.image.present? ? item.image.thumb_default.url : '',
              subcategories: build_nested_categories([], item)
            }
          end

          categories_arr
        end
      end
    end
  end
end
