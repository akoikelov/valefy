module Api
  module Masters
    module V1
      module User
      # describe which attributes and relationships should be serialized
        class ClientSerializer < ActiveModel::Serializer
          attributes :id, :name, :last_name, :image, :phone_number, :image
        end
      end
    end
  end
end
