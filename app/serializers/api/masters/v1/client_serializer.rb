module Api
  module Masters
    module V1
      class ClientSerializer < BaseSerializer
        attributes :id, :email, :name, :image, :phone_number, :confirm_phone_number,
                  :last_name, :address, :b_day, :cash, :corp, :corp_discount, :corp_discount,
                  :pay_with, :referal_link

        def image
          object.image
        end

        def referal_link
          object.referal
        end
      end
    end
  end
end