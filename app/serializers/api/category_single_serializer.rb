module Api
  # describe which attributes and relationships should be serialized
  class CategorySingleSerializer < ActiveModel::Serializer
    attributes :id, :title, :image, :description

    def image
      object.image.present? ? object.image.thumb_default.url : ''
    end
  end
end
