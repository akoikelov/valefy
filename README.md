## Setup ##

* Save Initial data: *rake db:seed*
* Load temp data: *rake db:populate*
* Get categories list: */api/categories*

## Deployment ##

* Create an environment and set the software configuration to correct
env variable settings.
* For qa this additional env var is required: QA_SECRET_KEY_BASE
* To seed the database run as root: RAILS_ENV=qa bundle exec rake db:seed

Additional documentation is available [here](https://docs.zoho.com/home#folder/0m8kybd10e818987c4f58957877a8e4de1eb3)

### More useful commands on EB ###

Under root user (sudo bash):

```
RAILS_ENV=qa bundle exec rails console
RAILS_ENV=qa bundle exec rake db:drop:all
RAILS_ENV=qa bundle exec rake db:reset
RAILS_ENV=qa bundle exec rake db:migrate
RAILS_ENV=qa bundle exec pry-remote
```
