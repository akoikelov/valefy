module ApiV1Helper
  def self.random_image
    Dir.glob(File.join(Rails.root, 'lib/fake_data/images', '*.{jpg,png}')).sample
  end
end
  
