require 'rails_helper'

describe Api::Masters::V1::Auth::VerificationCode do
  context 'Когда клиент отправляет код с телефоном' do

    it 'то мы отвечаем успешно' do
      create(:master, { phone_number: '+996553559537' })
      params = { phone_number: '+996553559537' }

      result_send_sms = Api::Masters::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::ServiceMan
      )

      code = result_send_sms.master.sms_histories.last.code
      params = { phone_number: '+996553559537', code: code }

      context = Api::Masters::V1::Auth::VerificationCode.call(
        params: params, object: ::User::ServiceMan
      )

      expect(context.success?).to eql true
    end
  end

  context 'Когда не известный клиент отправляет код с телефоном' do

    it 'то мы отвечаем не успешно' do
      create(:master, { phone_number: '+996553559537' })
      params = { phone_number: '+996553559537' }

      result_send_sms = Api::Masters::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::ServiceMan
      )

      code = result_send_sms.master.sms_histories.last.code
      params = { phone_number: '+996553559536', code: '98712' }

      context = Api::Masters::V1::Auth::VerificationCode.call(
        params: params, object: ::User::ServiceMan
      )

      expect(context.success?).to eql false
      expect(context.error).to include I18n.t('phone_verification.user_does_not_have_phone')
    end
  end

  context 'Когда попытались сразу отправить код, не отправляя смс' do

    it 'то мы отвечаем не успешно' do
      create(:master, { phone_number: '+996553559537' })

      params = { phone_number: '+996553559537', code: '54321' }
      context = Api::Masters::V1::Auth::VerificationCode.call(
        params: params, object: ::User::ServiceMan
      )

      expect(context.success?).to eql false
      expect(context.error).to include I18n.t('phone_verification.user_not_found')
    end
  end
end
