require 'rails_helper'

describe Api::V1::Auth::CheckAndSendSms do
  context 'Когда клиент есть в базе и он отправляет номер' do

    it 'то мы отвечаем успешно' do
      params = { phone_number: '+353553559536' }

      create(:client, phone_number: params[:phone_number])

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true
    end
  end

  context 'Когда клиента нет в базе и он отправляет номер' do

    it 'то мы отвечаем не успешно' do
      params = { phone_number: '+996553559536' }

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql false
      expect(context.error).to eql    "Client doesn't exist"
    end
  end

  context 'Когда клиент хочет получить смс отправив свой номер 1-ый раз' do

    it 'то отвечаем дальше успешно' do
      params = { phone_number: '+353553559536' }

      create(:client, phone_number: params[:phone_number])

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true
    end
  end

  context 'Когда клиент хочет получить смс отправив свой номер 2-ый раз' do

    it 'то отвечаем дальше успешно' do
      params = { phone_number: '+353553559536' }

      create(:client, phone_number: params[:phone_number])

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true
    end
  end

  context 'Когда клиент хочет получить смс отправив свой номер 3-ый раз и более' do

    it 'то отвечаем не успешно' do
      params = { phone_number: '+353553559536' }

      create(:client, phone_number: params[:phone_number])

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql true

      context = Api::V1::Auth::CheckAndSendSms.call(
        params: params, object: ::User::Client
      )

      expect(context.success?).to eql false
      expect(context.error).to include('Next query after')
    end
  end
end
