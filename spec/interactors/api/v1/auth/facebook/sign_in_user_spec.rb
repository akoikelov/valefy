require 'rails_helper'

describe Api::V1::Auth::SignInUser do
  context 'Когда приходят валидные данные' do
    let(:params) { { type: 'User::Client' } }

    it 'Facebook отдал все нужные параметры' do
      profile = {
        'name' => 'Даниэль Сайфулин',
        'email'=>'dan.s92@mail.ru',
        'id'=>'1278163518934416'
      }

      context = Api::V1::Auth::SignInUser.call(
        profile: profile, params: params, object: User
      )

      expect(context.user.email).to eql       profile['email']
      expect(context.user.name).to eql        profile['name']
      expect(context.user.facebook_id).to eql profile['id']
    end

    it 'Клиент заходит через Facebook первый раз' do
      profile = {
        'name' => 'Даниэль Сайфулин',
        'email'=>'dan.s92@mail.ru',
        'id'=>'1278163518934416'
      }

      context = Api::V1::Auth::SignInUser.call(
        profile: profile, params: params, object: User
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql User::PHONE_NOT_EXIST
      expect(context.user.email).to eql profile['email']
      expect(context.user.name).to eql profile['name']
      expect(context.user.facebook_id).to eql profile['id']
    end

    it 'Клиент заходит через Facebook второй раз, и в бд он уже есть' do
      profile = {
        'name' => 'Даниэль Сайфулин',
        'email'=>'dan.s92@mail.ru',
        'id'=>'1278163518934416'
      }

      password = SecureRandom.urlsafe_base64(nil, false)
      params_client = {
        email: profile['email'],
        name: profile['name'],
        phone_number: User::PHONE_NOT_EXIST,
        facebook_id: profile['id'],
        password: password,
        password_confirmation: password
      }

      client = create(:client, params_client)

      context = Api::V1::Auth::SignInUser.call(
        profile: profile, params: params, object: User
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql User::PHONE_NOT_EXIST
      expect(context.user.email).to eql client.email
      expect(context.user.name).to eql client.name
      expect(context.user.facebook_id).to eql client.facebook_id
    end

    it 'Когда клиент зарегистрировался, а потом решил зайти через Facebook' do
      User::Client.destroy_all

      params_client = {
        email: 'dan.s92@mail.ru',
        name: 'Даниэль Сайфулин',
        phone_number: '996553559536',
        facebook_id: '',
        password: 'password',
        password_confirmation: 'password'
      }

      client = create(:client, params_client)

      profile = {
        'name' => 'Даниэль Сайфулин',
        'email'=>'dan.s92@mail.ru',
        'id'=>'1278163518934416'
      }

      context = Api::V1::Auth::SignInUser.call(
        profile: profile, params: params, object: User
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql client.phone_number
      expect(context.user.email).to eql client.email
      expect(context.user.name).to eql client.name
      expect(context.user.facebook_id).to eql profile['id']
    end
  end
end
