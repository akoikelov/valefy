require 'rails_helper'

describe Api::V1::Auth::SignInGoogleUser do
  context 'Когда приходят валидные данные' do
    let(:params) { { type: 'User::Client' } }

    it 'Google отдал все нужные параметры' do
      profile = {
        "id"=>"118401876327265880558",
        "email"=>"saifulindaniel@gmail.com",
        "name"=>"Daniel Saifulin",
        "given_name"=>"Daniel",
        "family_name"=>"Saifulin",
        "picture"=>"https://lh3.googleusercontent.com/-2tc8As3yeZc/AAAAAAAAAAI/AAAAAAAAANI/fL_7_nTlV6I/photo.jpg",
      }

      context = Api::V1::Auth::SignInGoogleUser.call(
        profile: profile, params: params, object: User::Client
      )

      expect(context.user.email).to eql       profile['email']
      expect(context.user.name).to eql        profile['given_name']
      expect(context.user.google_id).to eql   profile['id']
      expect(context.user.image).to eql       profile['picture']
      expect(context.user.last_name).to eql   profile['family_name']
    end

    it 'Google не отдал family_name' do
      profile = {
        "id"=>"118401876327265880558",
        "email"=>"saifulindaniel@gmail.com",
        "name"=>"Daniel Saifulin",
        "given_name"=>"Daniel",
        "picture"=>"https://lh3.googleusercontent.com/-2tc8As3yeZc/AAAAAAAAAAI/AAAAAAAAANI/fL_7_nTlV6I/photo.jpg",
      }

      context = Api::V1::Auth::SignInGoogleUser.call(
        profile: profile, params: params, object: User::Client
      )

      expect(context.user.email).to eql       profile['email']
      expect(context.user.name).to eql        profile['given_name']
      expect(context.user.google_id).to eql   profile['id']
      expect(context.user.image).to eql       profile['picture']
      expect(context.user.last_name).to eql   User::Client::LAST_NAME_NOT_EXIST
    end

    it 'Клиент заходит через Google первый раз' do
      profile = {
        "id"=>"118401876327265880558",
        "email"=>"saifulindaniel@gmail.com",
        "name"=>"Daniel Saifulin",
        "given_name"=>"Daniel",
        "family_name"=>"Saifulin",
        "picture"=>"https://lh3.googleusercontent.com/-2tc8As3yeZc/AAAAAAAAAAI/AAAAAAAAANI/fL_7_nTlV6I/photo.jpg",
      }

      context = Api::V1::Auth::SignInGoogleUser.call(
        profile: profile, params: params, object: User::Client
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql User::PHONE_NOT_EXIST
      expect(context.user.email).to eql profile['email']
      expect(context.user.name).to eql profile['given_name']
      expect(context.user.google_id).to eql profile['id']
      expect(context.user.last_name).to eql profile['family_name']
    end

    it 'Клиент заходит через Google, и в бд он уже есть' do
      profile = {
        "id"=>"118401876327265880558",
        "email"=>"saifulindaniel@gmail.com",
        "name"=>"Daniel Saifulin",
        "given_name"=>"Daniel",
        "family_name"=>"Saifulin",
        "picture"=>"https://lh3.googleusercontent.com/-2tc8As3yeZc/AAAAAAAAAAI/AAAAAAAAANI/fL_7_nTlV6I/photo.jpg",
      }

      password = SecureRandom.urlsafe_base64(nil, false)
      params_client = {
        email: profile['email'],
        name: profile['given_name'],
        phone_number: User::PHONE_NOT_EXIST,
        google_id: profile['id'],
        password: password,
        password_confirmation: password
      }

      client = create(:client, params_client)

      context = Api::V1::Auth::SignInGoogleUser.call(
        profile: profile, params: params, object: User::Client
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql User::PHONE_NOT_EXIST
      expect(context.user.email).to eql client.email
      expect(context.user.name).to eql client.name
      expect(context.user.google_id).to eql client.google_id
      expect(context.user.last_name).to eql client.last_name
    end

    it 'Когда клиент зарегистрировался, а потом решил зайти через Google' do
      User::Client.destroy_all

      params_client = {
        email: 'saifulindaniel@gmail.com',
        name: 'Даниэль Сайфулин',
        phone_number: '996553559536',
        google_id: '',
        password: 'password',
        password_confirmation: 'password'
      }

      client = create(:client, params_client)

      profile = {
        "id"=>"118401876327265880558",
        "email"=>"saifulindaniel@gmail.com",
        "name"=>"Daniel Saifulin",
        "given_name"=>"Daniel",
        "family_name"=>"Saifulin",
        "picture"=>"https://lh3.googleusercontent.com/-2tc8As3yeZc/AAAAAAAAAAI/AAAAAAAAANI/fL_7_nTlV6I/photo.jpg",
      }

      context = Api::V1::Auth::SignInGoogleUser.call(
        profile: profile, params: params, object: User::Client
      )

      expect(context.user.valid?).to eql true
      expect(context.user.phone_number).to eql client.phone_number
      expect(context.user.email).to eql client.email
      expect(context.user.name).to eql client.name
      expect(context.user.google_id).to eql profile['id']
      expect(context.user.last_name).to eql client.last_name
    end
  end
end
