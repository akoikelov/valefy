require 'rails_helper'

describe Api::V1::CreateImage do
  context 'Когда картинка не валидна' do
    it 'вместо изображения передал строку' do
      params = { image: 'some str' }

      context = Api::V1::CreateImage.call(params: params)

      expect(context.success?).to eql false
      expect(context.error).to eql 'Not correct format'
    end

    it 'вместо изображения передал цифру' do
      params = { image: 123 }

      context = Api::V1::CreateImage.call(params: params)

      expect(context.success?).to eql false
      expect(context.error).to eql 'Not correct format'
    end

    it 'когда передал пустое значение' do
      params = { image: '' }

      context = Api::V1::CreateImage.call(params: params)

      expect(context.success?).to eql false
      expect(context.error).to eql "image can't be blank"
    end

    it 'когда нет ключа image' do
      params = {}

      context = Api::V1::CreateImage.call(params: params)

      expect(context.success?).to eql false
      expect(context.error).to eql "image can't be blank"
    end
  end
end
