require 'rails_helper'

describe Api::V1::OrderItems::Cancel do
  context 'Когда приходят валидные данные' do
    let(:current_user) { create(:client_full_information) }
    let(:order)      { create(:order_with_full, { client: current_user }) }

    # it 'Пытаемся отменить заказ со статусом created' do
    #   params = { order_id: order.id }
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом pre_authorize_charged' do
    #   params = { order_id: order.id }
    #   order.pre_authorize_charged!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом searching' do
    #   params = { order_id: order.id }
    #   order.searching!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом in_pool' do
    #   params = { order_id: order.id }
    #   order.in_pool!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом accepted' do
    #   params = { order_id: order.id }
    #   order.accepted!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом master_arriving' do
    #   params = { order_id: order.id }
    #   order.master_arriving!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом delayed' do
    #   params = { order_id: order.id }
    #   order.delayed!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql true
    #   expect(context.order.status).to eql 'canceled'
    # end

    # it 'Пытаемся отменить заказ со статусом in_progress' do
    #   params = { order_id: order.id }
    #   order.in_progress!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql false
    #   expect(context.order.status).to eql 'in_progress'
    #   expect(context.error).to eql "You can't change order status"
    # end

    # it 'Пытаемся отменить заказ со статусом completed_unpaid' do
    #   params = { order_id: order.id }
    #   order.completed_unpaid!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql false
    #   expect(context.order.status).to eql 'completed_unpaid'
    #   expect(context.error).to eql "You can't change order status"
    # end

    # it 'Пытаемся отменить заказ со статусом completed_paid' do
    #   params = { order_id: order.id }
    #   order.completed_paid!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql false
    #   expect(context.order.status).to eql 'completed_paid'
    #   expect(context.error).to eql "You can't change order status"
    # end

    # it 'Пытаемся отменить заказ со статусом cancelled' do
    #   params = { order_id: order.id }
    #   order.canceled!
    #
    #   context = Api::V1::OrderItems::Cancel.call(
    #     params: params, current_user: current_user
    #   )
    #
    #   expect(context.success?).to     eql false
    #   expect(context.order.status).to eql 'canceled'
    #   expect(context.error).to eql 'Order already canceled'
    # end
  end
end
