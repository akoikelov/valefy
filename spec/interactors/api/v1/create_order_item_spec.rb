require 'rails_helper'

describe Api::V1::CreateOrderItem do
  before do
  end

  let(:current_user) { create(:client_full_information) }
  let(:service) { Service.all.sample }

  context 'Когда приходят не валидные данные' do
    it 'отправляю пустой параметр и заказ не создается' do
      # params = {}
      #
      # context = Api::V1::CreateOrderItem.call(
      #   params: params,
      #   current_user: current_user
      # )
      #
      # expect(context.success?).to     eql false
      # expect(context.order.valid?).to eql false
      # expect(context.error.messages[:service_id].first).to eql "can't be blank"
      # expect(context.error.messages[:address].first).to    eql "can't be blank"
      # expect(current_user.order_items.count).to eql 0
    end

    it 'указываю только адресс и заказ не создается' do
      # params = { address: 'Some address' }
      #
      # context = Api::V1::CreateOrderItem.call(
      #   params: params,
      #   current_user: current_user
      # )
      #
      # expect(context.success?).to     eql false
      # expect(context.order.valid?).to eql false
      # expect(context.error.messages[:service_id].first).to eql "can't be blank"
      # expect(current_user.order_items.count).to eql 0
    end

    it 'заказ не создается, если указываю только адрес и сылки' do
      # params = {
      #   address: 'Some address',
      #   images: ['1.jpg', '2.jpg', '3.jpg']
      # }
      #
      # context = Api::V1::CreateOrderItem.call(
      #   params: params,
      #   current_user: current_user
      # )
      #
      # expect(context.success?).to     eql false
      # expect(context.order.valid?).to eql false
      # expect(context.error.messages[:service_id].first).to eql "can't be blank"
      # expect(current_user.order_items.count).to eql 0
    end

    it 'указываю не существущий сервис и заказ не создается' do
      # params = {
      #   address: 'Some address',
      #   service_id: 100
      # }
      #
      # expect do
      #   Api::V1::CreateOrderItem.call(
      #     params: params,
      #     current_user: current_user
      #   )
      # end.to raise_exception(ActiveRecord::RecordNotFound)
      #
      # expect(current_user.order_items.count).to eql 0
    end

    it 'клиент с не подтвержденным номером не может создать заказ' do
      # current_user.update(confirm_phone_number: false)
      #
      # params = {
      #   address: 'Some address',
      #   service_id: Service.all.sample.id
      # }
      #
      # context = Api::V1::CreateOrderItem.call(
      #   params: params,
      #   current_user: current_user
      # )

      # expect(context.success?).to eql false
      # expect(context.error).to eql 'Phone number not confirm'
      # expect(current_user.order_items.count).to eql 0
    end
  end
end
