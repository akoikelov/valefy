require 'rails_helper'

describe Api::V2::CreateInvoice do

  let(:current_user) { create(:client) }
  let(:order)        { create(:order_item_new, :with_service)  }

  context 'Когда создается invoice' do
    it 'то, создание проходит успешно' do
      context = Api::V2::CreateInvoice.call(
        order: order,
        current_user: current_user
      )

      expect(context.success?).to eql true
      expect(context.invoice.line_items.count).to eql 1
    end
  end
end
