require 'rails_helper'

describe LineItem, 'association' do
  it 'expect belongs_to with invoice' do
    association = described_class.reflect_on_association(:invoice)
    expect(association.macro).to eq :belongs_to
  end

  it 'expect belongs_to with service' do
    association = described_class.reflect_on_association(:service)
    expect(association.macro).to eq :belongs_to
  end
end
