require 'rails_helper'

RSpec.describe Subscription, 'association' do
  it 'expect has_many order items' do
    association = described_class.reflect_on_association(:order_items)
    expect(association.macro).to eq :has_many
  end

  it 'expect belongs_to client' do
    association = described_class.reflect_on_association(:client)
    expect(association.macro).to eq :belongs_to
  end
end
