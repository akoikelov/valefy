require 'rails_helper'

describe Invoice, 'association' do
  it 'expect belongs_to with order item' do
    association = described_class.reflect_on_association(:order)
    expect(association.macro).to eq :belongs_to
  end

  it 'expect belongs_to with client' do
    association = described_class.reflect_on_association(:client)
    expect(association.macro).to eq :belongs_to
  end

  it 'expect belongs_to with master' do
    association = described_class.reflect_on_association(:master)
    expect(association.macro).to eq :belongs_to
  end

  it 'expect has_many with line items' do
    association = described_class.reflect_on_association(:line_items)
    expect(association.macro).to eq :has_many
  end
end
