require 'rails_helper'

RSpec.describe Company, 'association' do
  it 'expect has_many company locations' do
    association = described_class.reflect_on_association(:locations)
    expect(association.macro).to eq :has_many
  end

  it 'expect has_many company services' do
    association = described_class.reflect_on_association(:services)
    expect(association.macro).to eq :has_many
  end

  it 'expect has_many company professionals' do
    association = described_class.reflect_on_association(:pros)
    expect(association.macro).to eq :has_many
  end

  it 'expect belongs_to admin user' do
    association = described_class.reflect_on_association(:admin_user)
    expect(association.macro).to eq :belongs_to
  end
end
