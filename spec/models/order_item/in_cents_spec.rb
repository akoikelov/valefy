require 'rails_helper'

describe OrderItem, type: :model do
  context "#in_cents" do
    it 'should eql 200' do
      order = create(:order_item_new, { total_price: 2 })
      subcategory = create(:subcategory)
      services = create_list(:service, 2, { price: 1, subcategory: subcategory})

      order.services << services
      expect(order.in_cents).to eq 200  # (1 + 1) * 100 => 200
    end

    it 'should eql 400' do
      order = create(:order_item_new, { total_price: 4 })
      subcategory = create(:subcategory)
      services = create_list(:service, 2, { price: 2, subcategory: subcategory})

      order.services << services
      expect(order.in_cents).to eq 400  # (2 + 2) * 100 => 400
    end

    it 'should eql 600' do
      order = create(:order_item_new, { total_price: 6 })
      subcategory = create(:subcategory)
      services = create_list(:service, 2, { price: 3, subcategory: subcategory})

      order.services << services
      expect(order.in_cents).to eq 600  # (3 + 3) * 100 => 600
    end
  end
end
