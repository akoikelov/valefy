require 'rails_helper'

RSpec.describe User::ServiceMan, type: :model do
  describe "#Fetch tokens firebase" do
    let(:client) { create(:client) }

    context "status searching" do

      it 'Вернет все заказы со статусом searching' do

        create_list(:order_item_new, 5, { status: 'searching', client: client })

        results = OrderItem.searching

        expect(results.count).to eq 5
      end

      it 'Вернет все заказы со статусом searching отсортированные по дате обновления' do

        create_list(:order_item_new, 5, { status: 'searching', client: client })

        results = OrderItem.serching

        expect(OrderItem.where(status: 2).order(updated_at: :desc).map(& :id)).to eql results.map(& :id)
      end
    end
  end
end
