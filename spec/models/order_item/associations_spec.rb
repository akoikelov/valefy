require 'rails_helper'

describe OrderItem, 'association' do
  it 'expect has_many throug with services' do
    association = described_class.reflect_on_association(:services)
    expect(association.options[:through]).to eq :order_item_services
    expect(association.macro).to eq :has_many
  end

  it 'expect two services' do
    order_item = create(:order_item_new)
    list = create_list(:service, 2)

    list.map { |item| order_item.services << item }

    expect(order_item.services.count).to eq 2
    expect(order_item.services.class.name).to eq "ActiveRecord::Associations::CollectionProxy"
    expect(order_item.services.first.id).to eq list.first.id
    expect(order_item.services.second.id).to eq list.second.id
  end

  it 'expect belongs_to with subscription' do
    association = described_class.reflect_on_association(:subscription)
    expect(association.macro).to eq :belongs_to
  end
end
