require 'rails_helper'

RSpec.describe User, type: :model do
  describe "#Find or create FB user profile" do

    context "FB return all params" do
      let(:fb_params) do
        {
          'email' =>    'fb_user@gmail.com',
          'id'    =>     '1234567890',
          'name'  =>     'daniel',
          'type'  =>     'User::Client'
        }
      end
      
      it 'we got valid params from FB' do
        user = User.find_or_create_fb(fb_params)
        user.password = 'password'
        user.password_confirmation = 'password'

        expect(user.valid?).to eql true
      end

      it 'user fields got from FB should be not empty' do
        user = User.find_or_create_fb(fb_params)
        user.password = 'password'
        user.password_confirmation = 'password'

        expect(user.email).to eql 'fb_user@gmail.com'
        expect(user.uid).to eql '1234567890'
        expect(user.name).to eql 'daniel'
        expect(user.type).to eql 'User::Client'
      end
    end

    context "FB dont't give the email" do
      let(:fb_params) do
        {
          'email' =>    '',
          'id'    =>     '1234567890',
          'name'  =>     'daniel',
          'type'  =>     'User::Client'
        }
      end
      
      it 'we got valid params from FB without email' do
        user = User.find_or_create_fb(fb_params)
        user.password = 'password'
        user.password_confirmation = 'password'

        expect(user.valid?).to eql true
      end

      it 'user fields got from FB should be not empty' do
        user = User.find_or_create_fb(fb_params)
        user.password = 'password'
        user.password_confirmation = 'password'

        expect(user.email).to eql 'fb_1234567890@example.com'
        expect(user.uid).to eql '1234567890'
        expect(user.name).to eql 'daniel'
        expect(user.type).to eql 'User::Client'
      end
    end
  end
end
