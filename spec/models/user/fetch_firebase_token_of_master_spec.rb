require 'rails_helper'

RSpec.describe User::ServiceMan, type: :model do
  describe "#Fetch tokens firebase" do

    let(:count) { 100 }

    before do
      create_list(:master, count)
    end

    # context "Когда надо найти всех мастеро и вытащить у них токены то" do
    #
    #   it "вернет 100 токенов андроид устройств по категории из заказа" do
    #     order = create(:order_item_with_services)
    #
    #     init_category_to_masters(order.services.first, count)
    #     init_token_to_master_device(count)
    #
    #     tokens = User::ServiceMan.masters_token(order, 'android')
    #
    #     expect(tokens.class.name).to eql 'Array'
    #     expect(tokens.count).to eql count
    #   end
    #
    #   it "вернет 100 токенов ios устройств по категории из заказа" do
    #     order = create(:order_item_with_services)
    #
    #     init_category_to_masters(order.services.first, count)
    #     init_token_to_master_device(count)
    #
    #     tokens = User::ServiceMan.masters_token(order, 'IOS')
    #
    #     expect(tokens.class.name).to eql 'Array'
    #     expect(tokens.count).to eql count
    #   end
    #
    #   it "вернет 0 токенов ios устройств по категории из заказа" do
    #     order = create(:order_item_with_services)
    #
    #     init_category_to_masters(order.services.first, 1)
    #     init_token_to_master_device(1)
    #
    #     tokens = User::ServiceMan.masters_token(order, 'IOS')
    #
    #     expect(tokens.class.name).to eql 'Array'
    #     expect(tokens.count).to eql 1
    #   end
    #
    #   it "вернет на 1 меньше так как стоит галочка не получать пуш" do
    #     order = create(:order_item_with_services)
    #
    #     User::ServiceMan.all.sample.update(onduty: false) # disable push notify
    #
    #     init_category_to_masters(order.services.first, count)
    #     init_token_to_master_device(count)
    #
    #     tokens = User::ServiceMan.masters_token(order, 'IOS')
    #
    #     expect(tokens.class.name).to eql 'Array'
    #     expect(tokens.count).to eql count-1
    #   end
    # end
  end

  def init_category_to_masters service, count
    User::ServiceMan.first(count).map do |master|
      master.categories << service.subcategory.category
      master.save!
    end
  end

  def init_token_to_master_device count
    User::ServiceMan.first(count).map do |master|
      master.devices.create! id_registration_firebase: 'token_string_for_android', platform: 'android'
      master.devices.create! id_registration_firebase: 'token_string_for_ios', platform: 'IOS'
    end
  end
end
