require 'rails_helper'

RSpec.describe User::ServiceMan, 'association' do
  it 'expect has_one pro_information' do
    association = described_class.reflect_on_association(:pro_information)
    expect(association.macro).to eq :has_one
  end
end
