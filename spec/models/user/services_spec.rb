require 'rails_helper'

RSpec.describe User, type: :model do
  describe "#services" do

    it 'return services of his categories' do
      master = create(:master)
      subcategory = create(:subcategory)

      services = create_list(:service, 5, { subcategory: subcategory })

      master.categories << subcategory.category

      expect(master.services.class.name).to eq "Array"
      expect(master.services.count).to eq 5
    end
  end
end
