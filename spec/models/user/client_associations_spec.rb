require 'rails_helper'

RSpec.describe User::Client, 'association' do
  it 'expect has_many subscriptions' do
    association = described_class.reflect_on_association(:subscriptions)
    expect(association.macro).to eq :has_many
  end

  it 'expect has_many offices' do
    association = described_class.reflect_on_association(:offices)
    expect(association.macro).to eq :has_many
  end

  it 'expect has_many promocodes' do
    association = described_class.reflect_on_association(:promo_codes)
    expect(association.macro).to eq :has_many
  end

  it 'expect has_one contract' do
    association = described_class.reflect_on_association(:contract)
    expect(association.macro).to eq :has_one
  end
end
