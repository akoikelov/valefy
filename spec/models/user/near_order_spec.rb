require 'rails_helper'

RSpec.describe User::ServiceMan, type: :model do
  describe "#Find pro near order" do

    context "создаю заказ и указываю место в пределах 5 км" do
      it 'вернет одного мастера' do
        master = create(:master, { lat: 53.353823, lon: -6.254495 })
        order  = create(:order_item_new, { lat: 53.339995, lon: -6.253983 })

        result = User::ServiceMan.near_order?(order, master)

        expect(result).to eql true
      end
    end

    context "создаю заказ и указываю место за пределами 5 км" do
      it 'не найдет ниодного мастера' do
        master = create(:master, { lat: 53.353823, lon: -6.254495 })
        order  = create(:order_item_new, { lat: 53.289565, lon: -6.198444 })

        result = User::ServiceMan.near_order?(order, master)

        expect(result).to eql false
      end
    end
  end
end
