require 'rails_helper'

RSpec.describe Team, 'association' do
  it 'expect has_many masters' do
    association = described_class.reflect_on_association(:masters)
    expect(association.macro).to eq :has_many
  end
end
