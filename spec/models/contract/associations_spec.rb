require 'rails_helper'

describe Contract, 'association' do
  it 'expect belongs_to with client' do
    association = described_class.reflect_on_association(:client)
    expect(association.macro).to eq :belongs_to
  end

  it 'expect has_many with contract dates' do
    association = described_class.reflect_on_association(:contract_dates)
    expect(association.macro).to eq :has_many
  end
end
