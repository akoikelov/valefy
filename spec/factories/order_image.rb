FactoryGirl.define do
	factory :order_image do
		image Rack::Test::UploadedFile.new(File.open(File.join(Rails.root, '/lib/fake_data/images/settings.jpg')))
	end
end