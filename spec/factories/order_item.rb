FactoryGirl.define do
  factory :order_item_new, class: 'OrderItem' do
    address     FFaker::Address.street_address
    description FFaker::Book.description(2)
    order_now   true
    order_time  { DateTime.now }
    lat         53.348838
    lon         -6.262190
    client

    trait :with_service do
      after(:create) do |instance|
        instance.services << create(:service)
      end
    end

    factory :order_item_with_corp_client do
      count_vehicles 1
      client_corp
      after(:create) do |order|
        order.services << create(:service)
      end
    end

    factory :order_item_with_master do
      master

      after(:create) do |order|
        order.services << create(:service)
      end
    end

    factory :order_item_with_feedback do
      client
      master

      after(:create) do |order|
        order.services << create(:service)

        create(:client_feedback, {
          order_item_id: order.id,
          serviceman_id: order.master.id,
          client_id: order.client.id
        })
      end
    end
  end

  factory :promo_code do
    promo_code_id     { "Test_promo_code" }
    description       "It's promo code for testing"
    discount          3
    exp_date          DateTime.now + 1.week
  end

  factory :service do
    title             { "#{FFaker::Lorem.word} - #{Random.rand}" }
    price             1
    description       FFaker::Book.description
    duration          2
    subcategory
  end

  factory :category do
    title   { "#{FFaker::Lorem.word} - #{Random.rand}" }
    visible true
  end

  factory :subcategory do
    title   { "#{FFaker::Lorem.word} - #{Random.rand}" }
    visible true
    category
  end

  factory :client, class: 'User::Client' do
    name FFaker::Name.name
    last_name FFaker::Name.last_name
    email { FFaker::Internet.email }
    phone_number { FFaker::PhoneNumber.phone_number }
    confirm_phone_number true
    type 'User::Client'
    password 'Passw0rd'

    factory :client_without_phone do
      phone_number ''
      confirm_phone_number false
    end

    factory :client_corp do
      corp true
    end
  end

  factory :master, class: 'User::ServiceMan' do
    name                 { FFaker::Name.name }
    last_name            { FFaker::Name.last_name }
    email                { FFaker::Internet.email }
    phone_number         { "#{+353}#{rand(111111111..999999999)}" }
    confirm_phone_number true
    b_day                Date.today
    lat                  53.348838
    lon                  -6.262190
    password             'password'
    type                 'User::ServiceMan'
    company

    after(:create) do |master|
      create(:setting) if Setting.count.zero?
    end
  end

  factory :additional_payment do
    additional_price     23
    client_confirm       false
    rejected             false
    description          "Text description"
  end

  factory :client_feedback do
    rating 3
    comment "Good feedback"
    polite true
    orderly false
    friendly true
  end

  factory :setting do
    time_send_location 30
  end

  factory :admin_user do
    role         AdminUser::roles['company']
    title        FFaker::Lorem.word
    description  FFaker::Book.description(2)
    email        { FFaker::Internet.email }
  end

  factory :company do
    platform     Company::platforms[:valefy]
    title        FFaker::Lorem.word
    description  FFaker::Book.description(2)
  end

  factory :company_location do
    title  FFaker::Lorem.word
    address  FFaker::Book.description(2)
    lat  53.348838
    lon  -6.262190
    work_schedule {}
    phone_number 123456789
    company
  end
end
