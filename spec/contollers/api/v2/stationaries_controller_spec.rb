require 'rails_helper'

module Api
  module V2
    describe Api::V2::StationariesController, type: :controller do
      let(:response_body) { JSON.parse(response.body) }
      let(:client) { create(:client_without_phone) }

      before do
        @request.env['devise.mapping'] = Devise.mappings[:api_v1_user_client]
        sign_in(client)
        request.headers.merge!(client.create_new_auth_token)
      end

      context "GET INDEX" do
        it "Успешно проходит запрос на список автомоек" do
          get :index

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
        end

        it "Успешно получаю список автомоек" do
          create_list(:company_location, 3)

          get :index

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data'].count).to eq(3)
        end
      end

      context "GET SHOW" do
        it "Успешно проходит запрос на конкретную автомойку" do
          cs = create(:company_location)

          get :show, id: cs.id

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
        end

        it "Успешно проходит запрос на конкретную автомойку" do
          cs = create(:company_location)

          get :show, id: cs.id

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']).to eq(
            {
              "id"=>cs.id,
              "title"=>cs.title,
              "address"=>cs.address,
              "lat"=>cs.lat,
              "lon"=>cs.lon,
              "image"=>"",
              "phone_number"=>cs.phone_number,
              "email"=>'example@example.com', # TODO позже что то решиться с этой заглушкой
              "price"=>0.0,
              "description"=>cs.company.description,
              "rating"=>4,
              "client_feedbacks"=>[]
            }
          )
        end
      end
    end
  end
end
