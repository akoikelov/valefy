require 'rails_helper'

describe Api::V2::Users::ClientController, type: :controller do
  describe 'GET show' do
    let(:client) { create(:client, { last_name: 'Some last name of client', name: 'Some name' }) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "Когда нужно увидеть взять данные клиента" do
      it "то при запросе видим все нужные поля" do
        get :show, id: client.id

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['last_name']).to eql 'Some last name of client'
        expect(response_body['data']['email']).to eql client.email
        expect(response_body['data']['name']).to eql 'Some name'
        expect(response_body['data']['image']).to eql client.image
        expect(response_body['data']['phone_number']).to eql client.phone_number
      end
    end
  end
end
