require 'rails_helper'

describe Api::V2::Users::MastersController, type: :controller do
  describe 'GET show' do
    let(:client) { create(:client) }
    let(:master) { create(:master) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "Когда клиент хочет посмотреть инфу мастера" do
      it "то при запросе видим нужные поля" do
        get :show, master_id: master.id

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['last_name']).to eql master.last_name
        expect(response_body['data']['name']).to eql master.name
        expect(response_body['data']['image']).to eql master.image
        expect(response_body['data']['phone_number']).to eql master.phone_number
      end
    end
  end
end
