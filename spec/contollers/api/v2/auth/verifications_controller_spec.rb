require 'rails_helper'

module Api
  module V2
    module Auth
      describe VerificationsController, type: :controller do
        let (:response_json) { JSON.parse(response.body) }
        let (:client) { create(:client) }

        before do
          @request.env['devise.mapping'] = Devise.mappings[:api_v2_user_client]
          sign_in(client)
          request.headers.merge!(client.create_new_auth_token)
        end

        describe "POST generate" do
          context 'Когда клиент отправляет номер ' do
            it 'то успешно получает смс' do
              # stub_const("Twilio::REST::Client", ::Support::FakeSms)

              create(:client, { phone_number: '+353553559536' })
              post :generate, { phone_number: '+353553559536' }

              expect(response_json['success']).to eql true
            end
          end

          context 'Когда не известный клиент отправляет номер ' do
            it 'то получаем ошибку, что такого клиента нет' do
              post :generate, { phone_number: '+353553559536' }

              expect(response_json['success']).to eql false
              expect(response_json['error']).to eql "Client doesn't exist"
            end
          end
        end
      end
    end
  end
end
