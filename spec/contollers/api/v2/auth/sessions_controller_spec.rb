require 'rails_helper'

describe Api::V2::Auth::SessionsController, type: :controller do
  describe 'POST CREATE' do
    let (:client) { create(:client) }
    
    let (:response_json) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v2_user_client]
    end

    describe 'Sign in client' do
      it 'get 200' do
        client_params = { email: client.email, password: 'Passw0rd' }

        post :create, client_params

        expect(response.status).to eql 200
        expect(response_json['success']).to eql true
      end

      it 'get data' do
        client_params = { email: client.email, password: 'Passw0rd' }

        post :create, client_params

        expect(response_json['data']).to eql (
          {
            "id"=>client.id,
            "email"=>client.email,
            "name"=>client.name,
            "image"=>client.image,
            "phone_number"=>client.phone_number,
            "confirm_phone_number"=>client.confirm_phone_number,
            "last_name"=>client.last_name,
            "address"=>client.address,
            "b_day"=>client.b_day,
            "corp"=>client.corp,
            "cash"=>false,
            "corp_discount"=>client.corp_discount,
            "pay_with"=>client.pay_with,
            "promocodes"=>[],
            "referal_link"=>client.referal,
            "to_me_gift_cards" => [],
            "from_me_gift_cards" => []
          }
        )
      end
    end

    describe 'Sign out client' do
      it 'get 200' do
        sign_in(client)
        delete :destroy, headers: request.headers.merge!(client.create_new_auth_token)

        expect(response.status).to eql 200
        expect(response_json['success']).to eql true
      end
    end
  end
end
