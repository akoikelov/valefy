require 'rails_helper'

describe Api::V2::Auth::PasswordsController, type: :controller do
  describe 'PUT UPDATE' do

    let (:client) { create(:client) }
    let (:response_json) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v2_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "Когда клиент хочет сменить пароль" do
      context "и клиент заполнил все поля правильно" do
        it "тогда мы отвечаем успешно" do
          params = {
            current_password: 'Passw0rd',
            password: 'new_password',
            password_confirmation: 'new_password'
          }

          put :update, params

          expect(response.status).to          eq 200
          expect(response_json['success']).to eq true
        end
      end

      context "и клиент не верно заполнил свой текущий пароль" do
        it "тогда мы отвечаем не успешно" do
          params = {
            current_password: 'password1',
            password: 'new_password',
            password_confirmation: 'new_password'
          }

          put :update, params

          expect(response.status).to eq 200
          expect(response_json['success']).to eq false
          expect(response_json['error']).to include 'Current password is invalid'
        end
      end

      context "и клиент не верно заполнил новый пароль" do
        it "тогда мы отвечаем не успешно" do
          params = {
            current_password: 'password',
            password: 'new_password1',
            password_confirmation: 'new_password'
          }

          put :update, params

          expect(response.status).to eq 200
          expect(response_json['success']).to eq false
          expect(response_json['error']).to include "Password confirmation doesn't match Password"
        end
      end

      context "и клиент не верно заполнил новый подтвержденный пароль" do
        it "тогда мы отвечаем не успешно" do
          params = {
            current_password: 'password',
            password: 'new_password',
            password_confirmation: 'new_password1'
          }

          put :update, params

          expect(response.status).to eq 200
          expect(response_json['success']).to eq false
          expect(response_json['error']).to include "Password confirmation doesn't match Password"
        end
      end
    end
  end
end
