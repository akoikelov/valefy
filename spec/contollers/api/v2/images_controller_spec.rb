require 'rails_helper'

describe Api::V2::ImagesController, type: :controller do

  describe 'POST create' do
    let(:client)        { create(:client) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    it 'возвратит ошибку, если не передадим картинку' do
      post :create

      expect(response_body['success']).to eq(false)
      expect(response_body['error']).to eq("image can't be blank")
    end

    it 'успех, если передадим картинку' do
      file = ActionDispatch::Http::UploadedFile.new({
        :filename => 'test.jpg',
        :type => 'image/jpeg',
        :tempfile => File.new("#{Rails.root}/lib/fake_data/images/settings.jpg")
      })
      
      post :create, { image: file }

      expect(response.status).to eq(201)
      expect(response_body['success']).to eq(true)
    end
  end
end
