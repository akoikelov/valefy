require 'rails_helper'

describe Api::V2::PromoCodes::CheckController, type: :controller do

  describe 'POST create' do
    let(:client)        { create(:client) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "client send" do
      it "valid promo code" do
        order = create(:order_item_new, { client: client, total_price: 2 })
        order.services << create(:service, {price: 2})
        promo_code = create(:promo_code, {promo_code_id: 'PROMO_CODE_TEST', discount: 1})

        post :create, { order_id: order.id, promo_code: promo_code.promo_code_id } # promo_code.promo_code_id => PROMO_CODE_TEST

        expect(response_body['success']).to eq true
        expect(response_body['data']['total_price']).to eq 2
        expect(response_body['data']['total_price_with_discount']).to eq 1
        expect(response_body['data']['promo_code_id']).to eq promo_code.id
        expect(response_body['data']['promo_code']).to eq promo_code.promo_code_id
      end

      it "valid promo code" do
        order = create(:order_item_new, { client: client, total_price: 20 })
        order.services << create_list(:service, 2 , {price: 10})
        promo_code = create(:promo_code, {promo_code_id: 'PROMO_CODE_TEST', discount: 1})

        post :create, { order_id: order.id, promo_code: promo_code.promo_code_id } # promo_code.promo_code_id => PROMO_CODE_TEST

        expect(response_body['success']).to eq true
        expect(response_body['data']['total_price']).to eq 20
        expect(response_body['data']['total_price_with_discount']).to eq 19
        expect(response_body['data']['promo_code_id']).to eq promo_code.id
        expect(response_body['data']['promo_code']).to eq promo_code.promo_code_id
      end

      it "valid promo code" do
        order = create(:order_item_new, { client: client, total_price: 20 })
        order.services << create_list(:service, 2 , {price: 10})
        promo_code = create(:promo_code, {promo_code_id: 'PROMO_CODE_TEST', discount: 5})

        post :create, { order_id: order.id, promo_code: promo_code.promo_code_id } # promo_code.promo_code_id => PROMO_CODE_TEST

        expect(response_body['success']).to eq true
        expect(response_body['data']['total_price']).to eq 20
        expect(response_body['data']['total_price_with_discount']).to eq 15
        expect(response_body['data']['promo_code_id']).to eq promo_code.id
        expect(response_body['data']['promo_code']).to eq promo_code.promo_code_id
      end

      it "invalid promo code" do
        order = create(:order_item_new, { client: client, total_price: 20 })
        order.services << create_list(:service, 2 , {price: 10})
        promo_code = create(:promo_code, {promo_code_id: 'PROMO_CODE_TEST', discount: 5})

        post :create, { order_id: order.id, promo_code: 'PROMO_CODE_TESTTT' } # promo_code.promo_code_id => PROMO_CODE_TEST

        expect(response_body['success']).to eq false
        expect(response_body['error']).to eq "invalid or expired promo code"
        expect(response_body['code']).to eq "invalid_or_expired_promo_code"
      end
    end
  end
end
