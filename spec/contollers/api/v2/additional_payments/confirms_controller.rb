require 'rails_helper'

describe Api::V2::AdditionalPayments::ConfirmsController, type: :controller do

  describe 'POST create' do
    let(:client)                    { create(:client) }
    let(:order)                     { create(:order_item_with_master) }
    let(:additional_payment)        { create(:additional_payment, { order: order }) }
    let(:response_body)             { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    it 'возвратит список баннеров' do
      post :create, payment_id: additional_payment.id

      expect(response_body['success']).to eq(true)
    end
  end
end
