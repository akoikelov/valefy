require 'rails_helper'

describe Api::V2::GiftCardsController, type: :controller do
  describe 'POST create' do
    let(:client) { create(:client) }
    let(:client_will_get_gift) { create(:client) }
    let(:service) { create(:service) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v2_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "When client want gift a card to exist client" do
      it "then get 200" do
        params = {
          client_email: client_will_get_gift.email,
          message: 'Some text',
          exp_date: 10.days.from_now,
          service_id: service.id
        }

        post :create, params

        expect(response.status).to eq(200)
        expect(response_body['success']).to eq(true)
      end

      it "then current client has list gift cards" do
        params = {
          client_email: client_will_get_gift.email,
          message: 'Some text',
          exp_date: 10.days.from_now,
          service_id: service.id
        }

        post :create, params

        expect(response.status).to eq(200)
        expect(client.from_me_gift_cards.count).to eq(1)
      end
    end

    context "When client want gift a card to not exist client" do
      it "then get 200" do
        params = {
          client_email: 'some_email@gmail.com',
          message: 'Some text',
          exp_date: 10.days.from_now,
          service_id: service.id
        }

        post :create, params

        expect(response.status).to eq(200)
        expect(response_body['success']).to eq(true)
      end

      it "then current client has list gift cards" do
        params = {
          client_email: 'some_email@gmail.com',
          message: 'Some text',
          exp_date: 10.days.from_now,
          service_id: service.id
        }

        post :create, params

        expect(response.status).to eq(200)
        expect(client.from_me_gift_cards.count).to eq(1)
      end
    end
  end
end
