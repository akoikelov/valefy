require 'rails_helper'

describe Api::V2::FeedbackAppsController, type: :controller do
  describe 'POST create' do
    let(:client)            { create(:client) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context 'When client want leave feedback' do
      it 'fill message field and send' do
        post(:create, message: 'Some feedback text')

        expect(response.status).to             eq(201)
        expect(response_body['success']).to    eq(true)
        expect(client.feedback_apps.count).to  eq(1)
      end
    end

    context 'When client want leave feedback' do
      it 'fill message and platform fields' do
        post(:create, message: 'Some feedback text', platform: 'web')

        expect(response.status).to                      eq(201)
        expect(response_body['success']).to             eq(true)
        expect(client.feedback_apps.count).to           eq(1)
        expect(client.feedback_apps.first.platform).to  eq('web')
      end
    end

    context 'When client want leave feedback' do
      it 'sending empty message field' do
        post(:create, message: '')

        expect(response.status).to             eq(200)
        expect(response_body['success']).to    eq(false)
        expect(response_body['error']).not_to   be_empty
        expect(client.feedback_apps.count).to  eq(0)
      end
    end
  end
end
