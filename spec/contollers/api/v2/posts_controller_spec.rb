require 'rails_helper'

module Api
  module V2
    module Auth
      describe Api::V2::PostsController, type: :controller do
        let(:response_body) { JSON.parse(response.body) }

        it "Успешно получаю список постов" do
          Post.create! title: 'title', description: 'description', image: '1.jpg'

          get :index

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data'].count).to eq(1)
        end
      end
    end
  end
end
