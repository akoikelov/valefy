require 'rails_helper'

describe Api::V2::BannersController, type: :controller do

  describe 'GET index' do
    let(:client)        { create(:client) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    it 'get success' do
      get :index

      expect(response_body['success']).to eq(true)
    end

    it 'get data' do
      get :index

      expect(response_body['data'].count).to eq(0)
    end
  end
end
