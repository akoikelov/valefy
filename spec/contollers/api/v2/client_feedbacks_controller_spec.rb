require 'rails_helper'

module Api
  module V2
    module Auth
      describe Api::V2::ClientFeedbacksController, type: :controller do
        let(:client)        { create(:client) }
        let(:master)        { create(:master) }
        let(:order_item)    { create(:order_item_with_master, client: client, master: master) }
        let(:response_body) { JSON.parse(response.body) }

        before do
          @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]

          sign_in(client)
          request.headers.merge!(client.create_new_auth_token)
        end

        it "Feedback about pro will be success" do
          order_item.end_work!
          post :create, { order_item_id: order_item.id, rating: 1, comment: 'good master' }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']['rating']).to eq(1)
          expect(response_body['data']['comment']).to eq('good master')
        end

        it "Feedback about pro return error" do
          post :create, { order_item_id: order_item.id, rating: 1, comment: 'good master' }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(false)
          expect(response_body['error']).to eq('You can left feedback when order will be completed')
        end

        it "feedback about pro with other params" do
          order_item.end_work!

          post :create, {
            order_item_id: order_item.id,
            rating: 1,
            comment: 'good master',
            polite: 1,
            friendly: 1
          }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']['polite']).to eq(1)
          expect(response_body['data']['orderly']).to eq(4)
          expect(response_body['data']['friendly']).to eq(1)
        end

        it "feedback about stationary" do
          station = create(:company_location)
          order   = create(:order_item_with_master,
            client: client, 
            master: master, 
            stationary_id: station.id)

          order.end_work!

          post :create, {
            order_item_id: order.id,
            rating: 1,
            comment: 'good master'
          }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']['stationary_id']).to eq(station.id)
        end
      end
    end
  end
end
