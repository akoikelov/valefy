require 'rails_helper'

describe Api::V2::OrderItemsController, type: :controller do
  describe 'POST create' do
    let(:client) { create(:client_without_phone) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user_client]
      sign_in(client)
      request.headers.merge!(client.create_new_auth_token)
    end

    context "When client create order and phone_number_confirm = false" do
      it "then can't create order" do
        params = {
          address: 'Some address',
          service_ids: [create(:service).id],
          description: 'Some description',
          order_time: 'Thu Sep 28 17:00:39 GMT+06:00 2018',
          lon: '42.873928',
          lat: '74.583043'
        }
    
        post :create, params

        expect(response_body['success']).to eq(false)
        expect(response_body['code']).to eq('phone_not_confirm')
        expect(response_body['error']).to eq('Phone number not confirm')
      end
    end

    context "When client create order" do
      it "sending all fiels" do
        subcategory  = create(:subcategory)
        service_1 = create(:service, { subcategory: subcategory })
        service_2 = create(:service, { subcategory: subcategory })
        client.update(confirm_phone_number: true)

        params = {
          address: 'Bishkek',
          service_ids: [service_1.id,service_2.id],
          order_time: 'Thu Sep 28 17:00:39 GMT+06:00 2017',
          lon: 74.581903,
          lat: 42.875093,
          description: 'Description text',
          plate_number: 'AN123B'
        }

        post :create, params

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['address']).to eq params[:address]
        expect(response_body['data']['description']).to eq params[:description]
        expect(response_body['data']['lon']).to eq params[:lon]
        expect(response_body['data']['lat']).to eq params[:lat]
        expect(response_body['data']['order_time']).not_to be_empty
      end
    end
  end
end
