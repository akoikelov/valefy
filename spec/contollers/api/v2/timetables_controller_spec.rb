require 'rails_helper'

describe Api::V2::TimetablesController, type: :controller do
  describe 'GET index' do
    let(:response_body) { JSON.parse(response.body) }

    it "запрос на доступное время выполнится успешно" do
      date                 = Time.now
      today_date           = date.strftime('%d-%m-%Y') # d-m-y
      current_hour_minutes = '12:00'              # pm
      current_day_name     = date.strftime('%A')

      get :index, { date: today_date, current_hour_minutes: current_hour_minutes }

      expect(response_body['success']).to eq(true)
    end

    context 'Когда на сегодня мастер доступен' do
      xit "клиент хочет сделать заказ" do
        date                 = Time.now
        today_date           = date.strftime('%d-%m-%Y') # d-m-y
        current_hour_minutes = '12:00'                   # pm
        today_day_name       = date.strftime('%A')

        schedule = {
          user_service_man: 
          {
            "#{today_day_name.downcase}"=> "on",
            "#{today_day_name.downcase}_start": "8:00",
            "#{today_day_name.downcase}_end": "16:30"
          }
        }

        result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
        pro = create(:master)
        pro.build_pro_information(work_schedule: result.schedule)
        pro.save!

        get :index, { date: today_date, current_hour_minutes: current_hour_minutes }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']).to eq(
          {"#{today_day_name}"=>
            [
              {"time"=>"13:00", "available"=>true},
              {"time"=>"13:30", "available"=>true},
              {"time"=>"14:00", "available"=>true},
              {"time"=>"14:30", "available"=>true},
              {"time"=>"15:00", "available"=>true},
              {"time"=>"15:30", "available"=>true},
              {"time"=>"16:00", "available"=>true},
              {"time"=>"16:30", "available"=>true}
            ]
          }
        )
      end
    end

    context 'Когда на сегодня мастер не доступен' do
      xit "клиент хочет сделать заказ на сегодня" do
        date                 = Time.now
        today_date           = date.strftime('%d-%m-%Y') # d-m-y
        current_hour_minutes = '12:00'                   # pm
        today_day_name       = date.strftime('%A')

        schedule = {
          user_service_man: 
          {
            "#{today_day_name.downcase}"=> "",
            "#{today_day_name.downcase}_start": "8:00",
            "#{today_day_name.downcase}_end": "16:30"
          }
        }

        result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
        pro = create(:master)
        pro.build_pro_information(work_schedule: result.schedule)
        pro.save!

        get :index, { date: today_date, current_hour_minutes: current_hour_minutes }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']).to eq(
          {"#{today_day_name}"=>
            [
              {"id"=>1, "time"=>"13:00", "available"=>false},
              {"id"=>2, "time"=>"13:30", "available"=>false},
              {"id"=>3, "time"=>"14:00", "available"=>false},
              {"id"=>4, "time"=>"14:30", "available"=>false},
              {"id"=>5, "time"=>"15:00", "available"=>false},
              {"id"=>6, "time"=>"15:30", "available"=>false},
              {"id"=>7, "time"=>"16:00", "available"=>false},
              {"id"=>8, "time"=>"16:30", "available"=>false}
            ]
          }
        )
      end
    end

    context 'Когда на сегодня мастер доступен только до обеда' do
      xit "клиент хочет сделать заказ на после обеда" do
        date                 = Time.now
        today_date           = date.strftime('%d-%m-%Y') # d-m-y
        current_hour_minutes = '12:10'                   # pm
        today_day_name       = date.strftime('%A')

        schedule = {
          user_service_man: 
          {
            "#{today_day_name.downcase}"=> "on",
            "#{today_day_name.downcase}_start": "07:30",
            "#{today_day_name.downcase}_end": "12:00"
          }
        }

        result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
        pro = create(:master)
        pro.build_pro_information(work_schedule: result.schedule)
        pro.save!

        get :index, { date: today_date, current_hour_minutes: current_hour_minutes }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']).to eq(
          {"#{today_day_name}"=>
            [
              {"id"=>1, "time"=>"13:00", "available"=>false},
              {"id"=>2, "time"=>"13:30", "available"=>false},
              {"id"=>3, "time"=>"14:00", "available"=>false},
              {"id"=>4, "time"=>"14:30", "available"=>false},
              {"id"=>5, "time"=>"15:00", "available"=>false},
              {"id"=>6, "time"=>"15:30", "available"=>false},
              {"id"=>7, "time"=>"16:00", "available"=>false},
              {"id"=>8, "time"=>"16:30", "available"=>false}
            ]
          }
        )
      end
    end

    context 'Когда на сегодня мастер доступен только после обеда' do
      xit "клиент хочет сделать заказ до обеда" do
        date                 = Time.now
        today_date           = date.strftime('%d-%m-%Y') # d-m-y
        current_hour_minutes = '09:10'                   # pm
        today_day_name       = date.strftime('%A')

        schedule = {
          user_service_man: 
          {
            "#{today_day_name.downcase}"=> "on",
            "#{today_day_name.downcase}_start": "12:00",
            "#{today_day_name.downcase}_end": "16:30"
          }
        }

        result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
        pro = create(:master)
        pro.build_pro_information(work_schedule: result.schedule)
        pro.save!

        get :index, { date: today_date, current_hour_minutes: current_hour_minutes }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']).to eq(
          {"#{today_day_name}"=>
            [
              {"id"=>1, "time"=>"10:00", "available"=>false},
              {"id"=>2, "time"=>"10:30", "available"=>false},
              {"id"=>3, "time"=>"11:00", "available"=>false},
              {"id"=>4, "time"=>"11:30", "available"=>false},
              {"id"=>5, "time"=>"12:00", "available"=>true},
              {"id"=>6, "time"=>"12:30", "available"=>true},
              {"id"=>7, "time"=>"13:00", "available"=>true},
              {"id"=>8, "time"=>"13:30", "available"=>true},
              {"id"=>9, "time"=>"14:00", "available"=>true},
              {"id"=>10, "time"=>"14:30", "available"=>true},
              {"id"=>11, "time"=>"15:00", "available"=>true},
              {"id"=>12, "time"=>"15:30", "available"=>true},
              {"id"=>13, "time"=>"16:00", "available"=>true},
              {"id"=>14, "time"=>"16:30", "available"=>true}
            ]
          }
        )
      end
    end

    context 'Когда есть один свободный мастер' do
      context 'И нет других заказов' do
        xit "то клиент хочет сделать заказ на завтра относительно 09:00" do
          date                    = Time.now + 1.day
          tomorrow_date           = date.strftime('%d-%m-%Y') # d-m-y
          current_hour_minutes    = '09:00'                   # pm
          tomorrow_day_name       = date.strftime('%A')

          schedule = {
            user_service_man: 
            {
              "#{tomorrow_day_name.downcase}"=> "on",
              "#{tomorrow_day_name.downcase}_start": "8:00",
              "#{tomorrow_day_name.downcase}_end": "16:30"
            }
          }

          result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
          pro = create(:master)
          pro.build_pro_information(work_schedule: result.schedule)
          pro.save!

          get :index, { date: tomorrow_date, current_hour_minutes: current_hour_minutes }

          expect(response_body['success']).to eq(true)
          expect(response_body['data']).to eq(
            {"#{tomorrow_day_name}"=>
              [
                {"id"=>1, "time"=>"10:00", "available"=>true},
                {"id"=>2, "time"=>"10:30", "available"=>true},
                {"id"=>3, "time"=>"11:00", "available"=>true},
                {"id"=>4, "time"=>"11:30", "available"=>true},
                {"id"=>5, "time"=>"12:00", "available"=>true},
                {"id"=>6, "time"=>"12:30", "available"=>true},
                {"id"=>7, "time"=>"13:00", "available"=>true},
                {"id"=>8, "time"=>"13:30", "available"=>true},
                {"id"=>9, "time"=>"14:00", "available"=>true},
                {"id"=>10, "time"=>"14:30", "available"=>true},
                {"id"=>11, "time"=>"15:00", "available"=>true},
                {"id"=>12, "time"=>"15:30", "available"=>true},
                {"id"=>13, "time"=>"16:00", "available"=>true},
                {"id"=>14, "time"=>"16:30", "available"=>true}
              ]
            }
          )
        end
        xit "то клиент хочет сделать заказ на завтра относительно 12:00" do
          date                    = Time.now + 1.day
          tomorrow_date           = date.strftime('%d-%m-%Y') # d-m-y
          current_hour_minutes    = '12:00'                   # pm
          tomorrow_day_name       = date.strftime('%A')

          schedule = {
            user_service_man: { "#{tomorrow_day_name.downcase}"=> "on", "#{tomorrow_day_name.downcase}_start": "8:00", "#{tomorrow_day_name.downcase}_end": "16:30" }
          }

          result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
          pro = create(:master)
          pro.build_pro_information(work_schedule: result.schedule)
          pro.save!

          get :index, { date: tomorrow_date, current_hour_minutes: current_hour_minutes }

          expect(response_body['success']).to eq(true)
          expect(response_body['data']).to eq(
            {"#{tomorrow_day_name}"=>
              [
                {"id"=>1, "time"=>"13:00", "available"=>true},
                {"id"=>2, "time"=>"13:30", "available"=>true},
                {"id"=>3, "time"=>"14:00", "available"=>true},
                {"id"=>4, "time"=>"14:30", "available"=>true},
                {"id"=>5, "time"=>"15:00", "available"=>true},
                {"id"=>6, "time"=>"15:30", "available"=>true},
                {"id"=>7, "time"=>"16:00", "available"=>true},
                {"id"=>8, "time"=>"16:30", "available"=>true}
              ]
            }
          )
        end
      end

      context 'И на этот день есть один принятый заказ единственным мастером' do
        xit "другой клиент хочет заказать на другой день" do
          date                     = Time.now
          today_date               = date.strftime('%d-%m-%Y') # d-m-y
          tomorrow_date            = (date+1.day).strftime('%d-%m-%Y') # d-m-y
          current_hour_minutes     = '9:12'                            # pm
          tomorrow_day_name        = (date+1.day).strftime('%A')

          # make schedule for pro
          schedule = {
            user_service_man: { 
              "#{tomorrow_day_name.downcase}"=> "on",
              "#{tomorrow_day_name.downcase}_start": "8:00",
              "#{tomorrow_day_name.downcase}_end": "16:30"
            }
          }

          result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
          pro = create(:master)
          pro.build_pro_information(work_schedule: result.schedule)
          pro.save!
          # end schedule

          # order for pro
          order = create(:order_item_with_master, {
            master_id: pro.id, order_time: "#{today_date}-8:30", status: 'accepted', count_vehicles: 1
          })
          #end order

          get :index, { date: tomorrow_date, current_hour_minutes: current_hour_minutes }

          expect(response_body['success']).to eq(true)
          expect(response_body['data']).to eq(
            {"#{tomorrow_day_name}"=>
              [
                {"id"=>1, "time"=>"10:00", "available"=>true},
                {"id"=>2, "time"=>"10:30", "available"=>true},
                {"id"=>3, "time"=>"11:00", "available"=>true},
                {"id"=>4, "time"=>"11:30", "available"=>true},
                {"id"=>5, "time"=>"12:00", "available"=>true},
                {"id"=>6, "time"=>"12:30", "available"=>true},
                {"id"=>7, "time"=>"13:00", "available"=>true},
                {"id"=>8, "time"=>"13:30", "available"=>true},
                {"id"=>9, "time"=>"14:00", "available"=>true},
                {"id"=>10, "time"=>"14:30", "available"=>true},
                {"id"=>11, "time"=>"15:00", "available"=>true},
                {"id"=>12, "time"=>"15:30", "available"=>true},
                {"id"=>13, "time"=>"16:00", "available"=>true},
                {"id"=>14, "time"=>"16:30", "available"=>true}
              ]
            }
          )
        end

        xit "другой клиент хочет заказать на следующую среду" do
          # make schedule for pro
          schedule = {
            user_service_man: {'wednesday'=> "on",wednesday_start: "8:00",wednesday_end: "16:30"}
          }
          result = ::Api::Masters::V2::BuildSchedule.call(params: schedule)
          pro = create(:master)
          pro.build_pro_information(work_schedule: result.schedule)
          # end schedule

          # order for pro
          order = create(:order_item_with_master, {
            master_id: pro.id,order_time: '26-09-2018-08:30', status: 'accepted', count_vehicles: 1
          })
          #end order

          # new order
          current_date         = '19-09-2018' # d-m-y
          current_hour_minutes = '12:17'      # pm
          current_day_name     = 'Wednesday'

          get :index, { date: current_date }

          expect(response_body['success']).to eq(true)
          expect(response_body['data']).to eq(
            {"Wednesday"=>
              [
                {"id"=>19, "time"=>"13:30", "available"=>true},
                {"id"=>19, "time"=>"14:00", "available"=>true},
                {"id"=>19, "time"=>"14:30", "available"=>true},
                {"id"=>19, "time"=>"15:00", "available"=>true},
                {"id"=>19, "time"=>"15:30", "available"=>true},
                {"id"=>19, "time"=>"16:00", "available"=>true},
                {"id"=>19, "time"=>"16:30", "available"=>true}
              ]
            }
          )
        end
      end
    end
  end
end
