require 'rails_helper'

describe Api::V2::CategoriesController, type: :controller do
  describe 'GET index' do
    let (:response_json) { JSON.parse(response.body) }

    it 'get 200' do
      get :index

      expect(response.status).to eq(200)
    end

    it 'get 0 categories' do
      get :index

      expect(response_json.count).to eq(0)
    end
  end

  describe 'GET show' do
    let (:response_json) { JSON.parse(response.body) }

    it 'return 0 services by category' do
      subcategory = create :subcategory

      get :show, { id: subcategory.id }

      expect(response_json.count).to eq(0)
    end

    it 'return one service by subcategory' do
      service = create :service

      get :show, { id: service.subcategory.id }

      expect(response_json.count).to eq(1)
    end

    it 'return 2 services by subcategory' do
      subcategory = create(:subcategory)
      services = create_list :service, 2, { subcategory: subcategory }

      get :show, { id: services.first.subcategory.id }

      expect(response_json.count).to eq(2)
    end
  end
end
