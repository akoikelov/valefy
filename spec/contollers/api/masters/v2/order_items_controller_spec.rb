require 'rails_helper'

describe Api::Masters::V2::OrderItemsController, type: :controller do
  describe 'GET index' do
    let(:client)            { create(:client) }
    let(:master)            { create(:master, { onduty: true }) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      create_list(:category, 5)

      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    context 'Когда мастер смотрит доступные заказы' do
      it "то ему приходит один доступный заказ" do
        order = create(:order_item_new, :with_service, { client: client })
        order.searching!

        master.categories << order.category

        get :index, { sort: 1 }

        expect(response.status).to             eq(200)
        expect(response_body['success']).to    eq(true)
        expect(response_body['data'].count).to eq(1)
        expect(response_body['data'][0]['id']).to eq(order.id)
      end

      it "то ему приходят не позже сегодняшнего дня" do
        order_time = DateTime.new(2017, 07, 11, 18, 30, 0)

        order = create(:order_item_new, :with_service, { client: client, order_time: order_time })
        order.searching!

        master.categories << order.category

        get :index, { sort: 1 }

        expect(response.status).to             eq(200)
        expect(response_body['success']).to    eq(true)
        expect(response_body['data'].count).to eq(0)
      end
    end
  end

  private

  def parents_category
    Category.where(parent_id: nil)
  end

  def another_category(order)
    category = order.category
    another_cat = Category.where(parent_id: nil).where.not(id: category.id).first

    another_cat
  end
end
