require 'rails_helper'

describe Api::Masters::V1::DevicesController, type: :controller do
  describe 'POST create' do
    let(:master)            { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end


    it "Успешно сохраню токен, который отправит мастер" do
      post :create, { id_firebase: 'olo_token_98765432', platform: 'android' }

      expect(response.status).to                                       eq(201)
      expect(response_body['success']).to                              eq(true)
    end
  end
end
