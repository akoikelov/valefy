require 'rails_helper'

describe Api::Masters::V1::AdditionalPaymentsController, type: :controller do
  describe 'POST create' do
    let(:client)            { create(:client) }
    let(:master)            { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    context 'Когда мастер создал доп.оплату' do
      it "то создает ее успешно" do
        order = create(:order_item_new, { client: client, master: master })
        order.accepted!

        post :create, { order_id: order.id, additional_price: 23, description: "desc" }

        expect(response.status).to                                   eq(200)
        expect(response_body['success']).to                          eq(true)
        expect(order.additional_payment.present?).to                 eq(true)
        expect(order.additional_payment.additional_price.to_i).to    eq(23)
        expect(order.additional_payment.description).to              eq("desc")
      end
    end

    context 'Когда мастер создал 2 раза доп.оплату' do
      it "то создает ее успешно" do
        order = create(:order_item_new, { client: client, master: master })
        order.accepted!

        post :create, { order_id: order.id, additional_price: 23, description: "desc" }

        expect(response.status).to                                   eq(200)
        expect(response_body['success']).to                          eq(true)
        expect(response_body['data']['id']).to                       eq(order.additional_payment.id)
        expect(order.additional_payment.present?).to                 eq(true)
        expect(order.additional_payment.additional_price.to_i).to    eq(23)
        expect(order.additional_payment.description).to              eq("desc")
        expect(order.additional_payment.client_confirm).to           eq(false)


        post :create, { order_id: order.id, additional_price: 45, description: "desc-45" }

        order = OrderItem.find(order.id)

        expect(response.status).to                                   eq(200)
        expect(response_body['success']).to                          eq(true)
        expect(JSON.parse(response.body)['data']['id']).to           eq(order.additional_payment.id)
        expect(order.additional_payment.present?).to                 eq(true)
        expect(order.additional_payment.additional_price.to_i).to    eq(45)
        expect(order.additional_payment.description).to              eq("desc-45")
        expect(order.additional_payment.client_confirm).to           eq(false)
      end
    end

    context 'Когда мастер создал 2 раза доп.оплату, но первую уже подтвердили' do
      it "то создает ее успешно" do
        order = create(:order_item_new, { client: client, master: master })
        order.accepted!

        post :create, { order_id: order.id, additional_price: 23, description: "desc" }

        expect(response.status).to                                   eq(200)
        expect(response_body['success']).to                          eq(true)
        expect(response_body['data']['id']).to                       eq(order.additional_payment.id)
        expect(order.additional_payment.present?).to                 eq(true)
        expect(order.additional_payment.additional_price.to_i).to    eq(23)
        expect(order.additional_payment.description).to              eq("desc")
        expect(order.additional_payment.client_confirm).to           eq(false)

        order.additional_payment.update_attributes( client_confirm: true )

        post :create, { order_id: order.id, additional_price: 45, description: "desc-45" }

        order = OrderItem.find(order.id)

        expect(response.status).to                                   eq(200)
        expect(JSON.parse(response.body)['success']).to              eq(false)
        expect(JSON.parse(response.body)['code']).to                 eq('confirm_payment_error')
        expect(JSON.parse(response.body)['error']).to              eq('Payment already confirmed')
        expect(order.additional_payment.present?).to                 eq(true)
        expect(order.additional_payment.additional_price.to_i).to    eq(23)
        expect(order.additional_payment.description).to              eq("desc")
        expect(order.additional_payment.client_confirm).to           eq(true)
      end
    end
  end
end
