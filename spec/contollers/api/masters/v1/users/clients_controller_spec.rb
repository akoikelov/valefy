require 'rails_helper'

describe Api::Masters::V1::Users::ClientsController, type: :controller do
  describe 'GET show' do

    let(:master)        { create(:master) }
    let(:client)        { create(:client) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]

      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end


    context 'когда делаю запрос на профиль клиенте то' do
      it 'успешно получаю инфо клиента' do
        get :show, { client_id: client.id }

        expect(response.status).to eq(200)
        expect(response_body['success']).to eq(true)
        expect(response_body['data']['id']).to eq(client.id)
        expect(response_body['data']['name']).to eq(client.name)
        expect(response_body['data']['last_name']).to eq(client.last_name)
        expect(response_body['data']['phone_number']).to eq(client.phone_number)
        expect(response_body['data']['image']).to eq(client.image)
      end
    end
  end
end
