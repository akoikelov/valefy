require 'rails_helper'

describe Api::Masters::V1::OrderItemsController, type: :controller do
  describe 'GET index' do
    let(:master)            { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end


    it "Успешно получу список список всего доступного времени" do
      get :index

      expect(response.status).to                                       eq(200)
      expect(response_body['success']).to                              eq(true)
    end
  end
end
