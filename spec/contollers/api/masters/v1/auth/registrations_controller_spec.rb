require 'rails_helper'

describe Api::Masters::V1::Auth::RegistrationsController, type: :controller do
  describe 'PUT UPDATE' do
    let(:master)        { create(:master) }
    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    it 'Успешно обновляется информация клиента' do
      patch :update, {
        name: '123',
        phone_number: '+353987654345',
        last_name: 'some last_name',
        b_day: Date.new(2015,03,23).strftime('%Y-%m-%d'),
        image: '123.jpg'
      }

      expect(response_body['data']['name']).to eq('123')
      expect(response_body['data']['phone_number']).to eq('+353987654345')
      expect(response_body['data']['last_name']).to eq('some last_name')
      expect(response_body['data']['b_day']).to eq(Date.new(2015,03,23).strftime('%Y-%m-%d'))
      expect(response_body['data']['image']).to eq('123.jpg')
    end
  end
end
