require 'rails_helper'

module Api
  module Masters
    module V1
      describe Api::Masters::V1::MasterFeedbacksController, type: :controller do
        let(:client)        { create(:client) }
        let(:master)        { create(:master) }
        let(:order_item)    { create(:order_item_with_master, client: client, master: master) }
        let(:response_body) { JSON.parse(response.body) }

        before do
          @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]

          sign_in(master)
          request.headers.merge!(master.create_new_auth_token)
        end

        it "Успешно оставляем рэйтинг для клиента" do
          order_item.completed_paid!
          post :create, { order_item_id: order_item.id, rating: 1, message: 'good master' }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']['rating']).to eq(1)
          expect(response_body['data']['message']).to eq('good master')
        end

        it "Невозможно оставить рэйтинг с заказом не имеющем статус completed_paid" do
          post :create, { order_item_id: order_item.id, rating: 1, comment: 'good master' }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(false)
          expect(response_body['error']).to eq('You can left feedback when order will be completed')
        end

        it "Пытаюсь оставить рэйтинг второй раз, но вижу ошибку" do
          order_item.completed_paid!
          order_item_id = order_item.id
          post :create, { order_item_id: order_item_id, rating: 1, message: 'good master' }

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(true)
          expect(response_body['data']['rating']).to eq(1)

          post :create, { order_item_id: order_item_id, rating: 1, message: 'good master' }

          expect(response.status).to eq(200)
          expect(JSON.parse(response.body)['success']).to eq(false)
          expect(JSON.parse(response.body)['error']).to eq 'You can leave feedback only once'
        end
      end
    end
  end
end
