require 'rails_helper'

module Api
  module Masters
    module V1
      describe Api::Masters::V1::JoinMastersController, type: :controller do
        let(:category)      { create(:category) }
        let(:master)        { create(:master) }
        let(:response_body) { JSON.parse(response.body) }

        it "Успешно добавляется мастер" do
          params = {
            name: 'John',
            email: 'john@gmail.com',
            phone_number: '9876545678',
            category_id: category.id
          }

          post :create, params

          expect(response.status).to eq(201)
          expect(response_body['success']).to eq(true)
        end

        it "Мастер не указал телефон, то получаю ошибку" do
          params = {
            name: 'John',
            email: 'john@gmail.com',
            category_id: category.id
          }

          post :create, params

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(false)
          expect(response_body['error']).to eq('You need to fill an email, phone_number, name')
        end

        it "Мастер ввел не правильную почту, и получает ошибку" do
          params = {
            name: 'John',
            email: 'john',
            category_id: category.id,
            phone_number: '987654'
          }

          post :create, params

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(false)
          expect(response_body['error']).to eq('You need to fill an email, phone_number, name')
        end

        it "Если нет параметров, получаем ошибку" do
          post :create

          expect(response.status).to eq(200)
          expect(response_body['success']).to eq(false)
          expect(response_body['error']).to eq('unable params')
        end

        it "Мастер отправляет свой паспорт и cv" do
          params = {
            name: 'John',
            email: 'john@gmail.com',
            phone_number: '9876545678',
            cv: ['1.pdf'],
            id_document: ['1.jpeg', '2.jpeg'],
          }

          post :create, params

          expect(response.status).to eq(201)
          expect(response_body['success']).to eq(true)
        end

        it "Мастер не отправляет документы" do
          params = {
            name: 'John',
            email: 'john@gmail.com',
            phone_number: '9876545678'
          }

          post :create, params

          expect(response.status).to eq(201)
          expect(response_body['success']).to eq(true)
        end
      end
    end
  end
end
