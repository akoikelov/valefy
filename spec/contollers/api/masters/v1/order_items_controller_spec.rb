require 'rails_helper'

describe Api::Masters::V1::OrderItemsController, type: :controller do
  describe 'GET index' do
    let(:client)            { create(:client) }
    let(:master)            { create(:master, { onduty: true }) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      create_list(:category, 5)

      @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    context 'Когда клиент создал заказ c сервисом, который может выполнить мастер' do
      # it "то мастер успешно увидит этот заказ по параметру 1" do
      #   order = create(:order_item, { client: client })
      #   order.searching!
      #
      #   master.categories << order.category
      #   master.save!
      #
      #   get :index, { sort: 1 }
      #
      #   expect(response.status).to             eq(200)
      #   expect(response_body['success']).to    eq(true)
      #   expect(response_body['data'].count).to eq(1)
      #   expect(response_body['data'][0]['id']).to eq(order.id)
      # end
    end

    context 'Когда клиент создал заказ c сервисом, который не может выполнить мастер' do
      # it "то мастер не увидит этот заказ по параметру 1" do
      #   order = create(:order_item, { client: client })
      #   order.searching!
      #
      #   master.categories << another_category(order)
      #   master.save!
      #
      #   get :index, { sort: 1 }
      #
      #   expect(response.status).to             eq(200)
      #   expect(response_body['success']).to    eq(true)
      #   expect(response_body['data'].count).to eq(0)
      # end
    end

    context 'Когда клиенты создали несколько заказов' do
      # it "то мастер увидит последний созданный заказ" do
      #   order_first  = create(:order_item, { client: client })
      #   order_second = create(:order_item, { client: client })
      #
      #   order_first.searching!
      #   order_second.searching!
      #
      #   master.categories << order_first.category
      #   master.categories << order_second.category
      #   master.save!
      #
      #   get :index, { sort: 1 }
      #
      #   expect(response.status).to             eq(200)
      #   expect(response_body['success']).to    eq(true)
      #   expect(response_body['data'].count).to eq(2)
      #   expect(response_body['data'][0]['id']).to eq(order_second.id)
      #   expect(response_body['data'][1]['id']).to eq(order_first.id)
      # end
    end
  end

  describe 'PUT update' do
    let(:client)            { create(:client) }
    let(:master)            { create(:master) }
    let(:order_item)        { create(:order_item_with_master, client: client, master: master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end


    it "пытаюсь обновить время начала работы заказа" do
      put :update, { id: order_item.id, start_work: '2016-06-26 08:23:55' }

      expect(response.status).to                                       eq(200)
      expect(response_body['success']).to                              eq(true)
      expect(response_body['data']['start_work'].to_date).to       eq('2016-06-26 08:23:55'.to_date) # TODO добавить тайм зону и сравнивать еще по времени
    end
  end

  private

  def parents_category
    Category.where(parent_id: nil)
  end

  def another_category(order)
    category = order.category
    another_cat = Category.where(parent_id: nil).where.not(id: category.id).first

    another_cat
  end
end
