require 'rails_helper'

describe Api::Masters::V1::OrderItems::AcceptsController, type: :controller do
  describe 'POST create' do
    let(:client)                        { create(:client) }
    let(:master)                        { create(:master) }
    let(:master_without_account)        { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    context "когда мастер пытается принять заказ" do
      it 'то успешно примит заказ' do
        sign_in_master(master)

        order = create(:order_item_new, { client: client })
        order.searching!

        post :create, { order_id: order.id }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['status']).to eql('accepted')
        expect(response_body['data']['accepted_time']).not_to be_empty
      end

      it 'то принял на время которое уже занято' do
        sign_in_master(master)

        order_time = DateTime.new(2017, 07, 11, 18, 30, 0)

        order = create(:order_item_new, { order_time: order_time })
        order.searching!

        post :create, { order_id: order.id }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['status']).to eql('accepted')
        expect(response_body['data']['accepted_time']).not_to be_empty
        expect(response_body['data']['id']).to eql(master.order_items.last.id)

        order = create(:order_item_new, :with_service, { order_time: order_time, master: master })
        order.searching!

        post :create, { order_id: order.id }

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['success']).to eq(false)
        expect(JSON.parse(response.body)['error']).to eq(I18n.t('order_item.errors.accept_same_time_error'))
        expect(JSON.parse(response.body)['code']).to eq('accept_same_time_error')
      end

      it 'принимаю заказ, который между start и duration' do
        #start 18:30
        #end   20:30
        sign_in_master(master)

        order_time = DateTime.new(2017, 07, 11, 18, 30, 0) # 18:30

        order = create(:order_item_new, :with_service, { order_time: order_time })
        order.searching!

        post :create, { order_id: order.id }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['status']).to eql('accepted')
        expect(response_body['data']['accepted_time']).not_to be_empty
        expect(response_body['data']['id']).to eql(master.order_items.last.id)

        order_time = DateTime.new(2017, 07, 11, 19, 30, 0) # 19:30

        order = create(:order_item_new, :with_service, { order_time: order_time, master: master })
        order.searching!

        post :create, { order_id: order.id }

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['success']).to eq(false)
        expect(JSON.parse(response.body)['error']).to eq(I18n.t('order_item.errors.accept_same_time_error'))
        expect(JSON.parse(response.body)['code']).to eq('accept_same_time_error')
      end

      it 'принимаю заказ, который начаинается прям сразу когда другой кончается' do
        #start 18:30
        #end   20:30

        sign_in_master(master)

        order_time = DateTime.new(2017, 07, 11, 18, 30, 0) # 18:30

        order = create(:order_item_new, :with_service, { order_time: order_time })
        order.searching!

        post :create, { order_id: order.id }

        expect(response_body['success']).to eq(true)
        expect(response_body['data']['status']).to eql('accepted')
        expect(response_body['data']['accepted_time']).not_to be_empty
        expect(response_body['data']['id']).to eql(master.order_items.last.id)

        order_time = DateTime.new(2017, 07, 11, 20, 30, 0) # 20:30

        order = create(:order_item_new, :with_service, { order_time: order_time, master: master })
        order.searching!

        post :create, { order_id: order.id }

        expect(response.status).to eq(200)
        expect(JSON.parse(response.body)['success']).to eq(false)
        expect(JSON.parse(response.body)['error']).to eq(I18n.t('order_item.errors.accept_same_time_error'))
        expect(JSON.parse(response.body)['code']).to eq('accept_same_time_error')
      end
    end

    describe "Когда у мастера нет страйп аккаунта" do
      it "то получится принять заказ" do
        sign_in_master(master_without_account)

        order = create(:order_item_new, :with_service)
        order.searching!

        post :create, { order_id: order.id }

        expect(response_body['success']).to eq(true)
      end
    end
  end

  private

  def sign_in_master(master)
    @request.env['devise.mapping'] = Devise.mappings[:api_masters_v1_user_service_man]
    sign_in(master)
    request.headers.merge!(master.create_new_auth_token)
  end
end
