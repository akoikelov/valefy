require 'rails_helper'

describe Api::Masters::V1::OrderItems::StartsController, type: :controller do
  describe 'POST create' do
    let(:client)        { create(:client) }
    let(:master)        { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    it 'мастер успешно начинает выполнение заказа' do
      order = create(:order_item_new, { client: client, master: master })
      post :create, { order_id: order.id }

      expect(response_body['success']).to eq(true)
      expect(response_body['data']['status']).to eql('in_progress_confirmed')
      expect(response_body['data']['start_work']).not_to be_empty
    end

    it 'мастер успешно начинает работу с загруженными картинками' do
      order = create(:order_item_new, :with_service, { client: client, master: master })

      order.accepted!

      post :create, { order_id: order.id, before_work_images: ['1.jpg', '2.jpg', '3.jpg'] }

      expect(response_body['success']).to eq(true)
      expect(response_body['data']['status']).to eql('in_progress_confirmed')
      expect(response_body['data']['before_work_images']).to eql(['1.jpg', '2.jpg', '3.jpg'])
    end
  end
end
