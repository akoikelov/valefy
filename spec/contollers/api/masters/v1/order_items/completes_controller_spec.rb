require 'rails_helper'

describe Api::Masters::V1::OrderItems::CompletesController, type: :controller do
  describe 'POST create' do
    let(:client)        { create(:client) }
    let(:master)        { create(:master) }

    let(:response_body) { JSON.parse(response.body) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:api_v1_user]
      sign_in(master)
      request.headers.merge!(master.create_new_auth_token)
    end

    it 'мастер успешно завершает заказ' do
      order = create(:order_item_new, {
        client: client,
        master: master,
        order_time: 'Thu Sep 28 17:00:39 GMT+06:00 2017',
        start_work: '2017-09-28 17:17:02'
      })

      post :create, { order_id: order.id }

      expect(response_body['success']).to eq(true)
      expect(response_body['data']['status']).to eql('end_work')
      expect(response_body['data']['start_work']).not_to be_empty
      expect(response_body['data']['end_work']).not_to be_empty

      expect(response_body['data']['order_time']).not_to be_empty
    end

    it 'мастер успешно заканчивает работу с загруженными картинками' do
      order = create(:order_item_new, :with_service, { client: client, master: master })

      order.in_progress_confirmed!

      post :create, { order_id: order.id, after_work_images: ['1.jpg', '2.jpg', '3.jpg'] }

      expect(response_body['success']).to eq(true)
      expect(response_body['data']['status']).to eql('end_work')
      expect(response_body['data']['after_work_images']).to eql(['1.jpg', '2.jpg', '3.jpg'])
    end
  end
end
