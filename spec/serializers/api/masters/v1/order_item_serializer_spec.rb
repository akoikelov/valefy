require 'rails_helper'

describe Api::Masters::V1::OrderItemSerializer do

  describe "аттрибуты" do
    let(:client)            { create(:client) }
    let(:master)            { create(:master) }

    it 'вернет все нужные аттрибуты сервиса' do
      services = create_list(:service, 2, { price: 4 })
      order_item = create(:order_item_new)

      order_item.services << services

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:services]).to eql([{
        :id=>services.first.id,
        :title=>services.first.title,
        :title_ru=>nil,
        :price=>4,
        :category_id=>services.first.subcategory_id,
        :short_title=>nil
      },{
        :id=>services.second.id,
        :title=>services.second.title,
        :title_ru=>nil,
        :price=>4,
        :category_id=>services.second.subcategory_id,
        :short_title=>nil
      }])
    end

    it 'если не укажет service_id валится не будет' do
      service = create(:service, price: 4)
      order_item = create(:order_item_new)

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:service]).to eql({})
    end

    it 'вернет все нужные аттрибуты сервиса' do
      service = create(:service, price: 4)
      order_item = create(:order_item_new, service_id: service.id)

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:service]).to eql({
        :id=>service.id,
        :title=>service.title,
        :title_ru=>nil,
        :price=>4,
        :category_id=>service.subcategory_id
      })
    end

    it 'вернет в категории доп.оплату' do
      service = create(:service, price: 4)
      order_item = create(:order_item_new, { service_id: service.id, client: client, master: master } )
      additional_payment = create(:additional_payment, { order_id: order_item.id, additional_price: 3, description: 'some desc' })

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:additional_payment]).to eql({
        :id=>additional_payment.id,
        :additional_price=>3,
        :client_confirm=>false,
        :rejected=>false,
        description: 'some desc'
      })
    end

    it 'вернет категорию в заказе' do
      order_item = create(:order_item_new, :with_service)
      category   = order_item.services.first.subcategory.category

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:category].attributes).to eql({
        :id=> category.id,
        :title=> category.title,
        :description=> nil,
        :image=>""
      })
    end

    it 'вернет подкатегорию в заказе' do
      order_item   = create(:order_item_new, :with_service)
      sub_category = order_item.services.first.subcategory 

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:sub_category].attributes).to eql({
        :id=>sub_category.id,
        :title=>sub_category.title,
        :title_ru=>sub_category.title_ru,
        :image=>"",
        :icon=>"",
        :icon_web=>"",
        :code=>nil,
        :small_car=>false
      })
    end
  end
end
