require 'rails_helper'

describe Api::Masters::V1::User::ClientSerializer do

  describe "аттрибуты" do
    let(:client) { create(:client) }

    it 'вернет все нужные аттрибуты мастера' do
      serializer = described_class.new(client)

      expect(serializer.serializable_hash).to eql({
        :id=>client.id,
        :name=>client.name,
        :last_name=>client.last_name,
        :phone_number=>client.phone_number,
        :image=>client.image
      })
    end
  end
end
