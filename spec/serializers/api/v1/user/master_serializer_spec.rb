require 'rails_helper'

describe Api::V1::User::MasterSerializer do

  describe "аттрибуты" do
    let(:master) { create(:master) }

    it 'вернет все нужные аттрибуты мастера' do
      serializer = described_class.new(master)

      expect(serializer.serializable_hash).to eql({
        :id=>master.id,
        :name=>master.name,
        :last_name=>master.last_name,
        :phone_number=>master.phone_number,
        :image=>master.image
      })
    end
  end
end
