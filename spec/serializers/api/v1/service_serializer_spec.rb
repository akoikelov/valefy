require 'rails_helper'

describe Api::V1::ServiceSerializer do


  describe "аттрибуты" do
    it 'вернет price без нуля' do
      service = create(:service, price: 4)
      serializer = described_class.new(service)

      expect(serializer.attributes).to eql({
        id: service.id,
        title: service.title,
        title_ru: nil,
        price: service.price.to_i,
        category_id: service.subcategory_id
      })
    end

    it 'если десятые доли то вернет с нулем' do
      service = create(:service, price: 4)
      serializer = described_class.new(service)

      expect(serializer.attributes).to eql({
        id: service.id,
        title: service.title,
        title_ru: nil,
        price: 4,
        category_id: service.subcategory_id
      })
    end

    it 'если сотые доли то вернет с нулем' do
      service = create(:service, price: 4)
      serializer = described_class.new(service)

      expect(serializer.attributes).to eql({
        id: service.id,
        title: service.title,
        title_ru: nil,
        price: 4,
        category_id: service.subcategory_id
      })
    end
  end
end
