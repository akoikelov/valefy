require 'rails_helper'

describe Api::V2::OrderItemSerializer do

  describe "аттрибуты" do
    let(:client)            { create(:client_corp) }
    let(:master)            { create(:master) }

    it 'вернет все нужные аттрибуты сервиса' do
      services = create_list(:service, 2, { price: 4 })
      order_item = create(:order_item_new)

      order_item.services << services

      serializer = described_class.new(order_item.reload)

      expect(serializer.serializable_hash[:services]).to eql(
        [
          {
            :id=>services.first.id,
            :title=>services.first.title,
            :title_ru=>nil,
            :price=>4,
            :category_id=>services.first.subcategory_id,
            :description=>services.first.description,
            :short_title=>nil
          },
          {
            :id=>services.second.id,
            :title=>services.second.title,
            :title_ru=>nil,:price=>4,
            :category_id=>services.second.subcategory_id,
            :description=>services.first.description,
            :short_title=>nil
          }
        ]
      )
    end

    it 'вернет в категории доп.оплату' do
      service = create(:service, price: 4)
      order_item = create(:order_item_new, { service_id: service.id, client: client, master: master } )
      additional_payment = create(:additional_payment, { order_id: order_item.id, additional_price: 3, description: 'some desc' })

      serializer = described_class.new(order_item)

      expect(serializer.serializable_hash[:additional_payment]).to eql({
        :id=>additional_payment.id,
        :additional_price=>3,
        :client_confirm=>false,
        :rejected=>false,
        description: 'some desc'
      })
    end
    # TODO fix it
    # it 'вернет категорию в заказе' do
    #   service = create(:service, price: 4)
    #   order_item = create(:order_item_new, { service_id: service.id, client: client, master: master } )
    #
    #   serializer = described_class.new(order_item)
    #
    #   expect(serializer.serializable_hash[:category]).to eql({
    #     "id"=> service.subcategory.category.id,
    #     "title"=> service.subcategory.category.title,
    #     "image"=> nil,
    #     "created_at"=> serializer.serializable_hash[:category]['created_at'],
    #     "updated_at"=> serializer.serializable_hash[:category]['updated_at'],
    #     "icon"=> nil,
    #     "title_ru"=> nil,
    #     "description"=> nil,
    #     "icon_web"=> nil,
    #     "visible"=> true,
    #     "parent_id"=>nil,
    #     "position"=>1,
    #     "regular"=>false,
    #     "corp"=>false
    #   })
    # end

    # it 'вернет подкатегорию в заказе' do
    #   service = create(:service, price: 4)
    #   order_item = create(:order_item_new, { service_id: service.id, client: client, master: master } )
    #
    #   serializer = described_class.new(order_item)
    #
    #   expect(serializer.serializable_hash[:sub_category]).to eql({
    #     "id"=> service.subcategory.id,
    #     "category_id"=> service.subcategory.category_id,
    #     "title"=> service.subcategory.title,
    #     "image"=> nil,
    #     "position"=>1,
    #     "created_at"=> serializer.serializable_hash[:sub_category]['created_at'],
    #     "updated_at"=> serializer.serializable_hash[:sub_category]['updated_at'],
    #     "icon"=> nil,
    #     "title_ru"=> nil,
    #     "icon_web"=> nil,
    #     "visible"=> true
    #   })
    # end

    it 'вернет фидбек в заказе' do
      order_item = create(:order_item_new, {client: client, master: master})
      feedback = create(:client_feedback, {order_item_id: order_item.id, client_id: client.id, serviceman_id: master.id})

      serializer = described_class.new(order_item)
      expect(serializer.serializable_hash[:feedback].attributes).to eql({
          :id=>feedback.id,
          :rating=>3,
          :comment=>"Good feedback",
          :polite=>1,
          :orderly=>0,
          :friendly=>1,
          :name=>client.name,
          :image=>client.image,
          :created_at=>feedback.created_at.strftime('%Y-%m-%dT%H:%M:%S.%LZ')
      })
    end
  end
end
