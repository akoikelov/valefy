require 'rails_helper'

describe Api::V2::CategorySerializer do
  let(:service) { create(:service) }

  describe "аттрибуты" do
    it 'вернет количество аттрибутов категории' do
      category = service.category

      serializer = described_class.new(category)
      expect(serializer.attributes.count).to eql 10
    end

    it 'вернет список атрибутов категории' do
      category = service.category
      serializer = described_class.new(category)

      expect(serializer.serializable_hash[:id]).to eql(category.id)
      expect(serializer.serializable_hash[:title]).to eql(category.title)
      expect(serializer.serializable_hash[:title_ru]).to eql(category.title_ru)
      expect(serializer.serializable_hash[:description]).to eql(category.description)
      expect(serializer.serializable_hash[:duration]).to eql(30)
      expect(serializer.serializable_hash[:icon_web]).to eql("")
      expect(serializer.serializable_hash[:icon]).to eql("")
      expect(serializer.serializable_hash[:image]).to eql("")
      expect(serializer.serializable_hash[:min_price]).to eql(1)
      expect(serializer.serializable_hash[:subcategories].count).to eql(1)
    end
  end
end
