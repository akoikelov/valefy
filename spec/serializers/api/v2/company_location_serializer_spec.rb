require 'rails_helper'

describe Api::V2::CompanyLocationSerializer do
  let(:stationary) { create(:company_location) }

  describe "аттрибуты" do
    it 'вернет количество аттрибутов локации компании' do
      serializer = described_class.new(stationary)
      expect(serializer.attributes.count).to eql 11
    end

    it 'вернет список атрибутов локации компании' do
      serializer = described_class.new(stationary)

      expect(serializer.attributes).to eql(
        {
          :id=>stationary.id,
          :title=>stationary.title,
          :address=>stationary.address,
          :phone_number=>stationary.phone_number,
          :lat=>stationary.lat,
          :email => 'example@example.com',
          :image=>"",
          :lon=>stationary.lon,
          :price=>0.0,
          :description=>stationary.company.description,
          :rating=>4
        }
      )
    end
  end
end
