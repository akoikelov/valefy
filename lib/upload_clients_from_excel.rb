class UploadClientsFromExcel

  def initialize(file, admin_user)
    @file = file
    @admin_user = admin_user
  end

  def parse
    spreadsheet = open_spreadsheet(@file)
    return false if spreadsheet.eql?(1)

    save_db(spreadsheet)
  end

  private

  def open_spreadsheet(file)
    return 1 if file.nil?

    case File.extname(file.original_filename)
    when ".xls" then Roo::Excel.new(file.path)
    when ".xlsx" then Roo::Excelx.new(file.path)
    else return 1
    end
  end

  def save_db(spreadsheet)
    begin
      last_row = {}
      spreadsheet.sheets.each do |sheet|
        header = spreadsheet.row(1, sheet)

        (2..spreadsheet.last_row(sheet)).each do |i|
          last_row = row = Hash[[header, spreadsheet.row(i, sheet)].transpose]

          if data_present?(row)
            client = create_client(row)
            if client.present? && client.corp?
              info = CorpInformation.create(
                position_in_company: row['Position'],
                name_in_company: row['Place']
              )
              client.corp_information = info
              client.save!
            end
          end
        end
      end
    rescue => e
      fill_all_data(last_row) if e.message.eql?('Validation failed: Email already in use')
    end
  end

  def create_client(row)
    begin
      if row['corp'].present?
        client = User::Client.where(email: row['Email'])

        if client.present?
          client = client.first
          client.update(corp: true)
        else
          User::Client.create! name: row['First Name'],
                            email: row['Email'],
                            phone_number: row['Phone Number'],
                            address: row['Address'],
                            password: 'password',
                            password_confirmation: 'password',
                            admin_user_id: admin_user_id,
                            category_id: category_id(row),
                            corp: true
        end
      else
        User::Client.create! name: row['First Name'],
                            email: row['Email'],
                            phone_number: row['Phone Number'],
                            address: row['Address'],
                            password: 'password',
                            password_confirmation: 'password',
                            admin_user_id: admin_user_id,
                            category_id: category_id(row)
      end
    rescue => e
    end
  end

  def admin_user_id
    @admin_user.company? ? @admin_user.id : nil
  end

  def data_present?(row)
    row['Email'].present?
  end

  def fill_all_data(row)
    client = @admin_user.user_clients.where(row['Email'])

    if client.present?
      client = client.first

      client.phone_number = row['Phone Number'] if !client.phone_number.present?
      client.address      = row['Address']      if !client.address.present?
      client.save!
    end
  end

  def category_id(row)
    if row['Category'].present?
      category_title = row['Category']

      if category_title.eql?('Premium Plus')
        category = Category.where(title: 'FULL VALET PLUS')

        return category.first.id if category.present?
      elsif category_title.eql?('Interior only')
        category = Category.where(title: 'INTERIOR ONLY')

        return category.first.id if category.present?
      elsif category_title.eql?('Premium')
        category = Category.where(title: 'FULL VALET')

        return category.first.id if category.present?
      elsif category_title.eql?('Express')
        category = Category.where(title: 'MINI VALET')

        return category.first.id if category.present?
      else
        nil
      end
    else
      nil
    end
  end

end
