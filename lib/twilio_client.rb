class TwilioClient
  cattr_accessor :client
  self.client = Twilio::REST::Client

  def initialize(phone_client)
    @client = self.class.client.new(
      ENV['TWILIO_ACCOUNT_SID'], 
      ENV['TWILIO_AUTH_TOKEN']
    )
    @phone = phone_client
  end

  def send_sms
    return fail_response('Phone Number is empty') if !@phone.present?
    return success_response('12345') if Rails.env.eql?('test')

    code     = generate_code
    response = @client.api.account.messages.create(
      :from => twilio_phone_number,
      :to => @phone,
      :body => "Your code is - #{code}"
    )
    
    response.error_message.present? ? fail_response('SMS Not sent') : success_response(code)
  rescue
    fail_response('SMS Not sent')
  end

  private

  def success_response(code)
    { success: true, code: code }
  end

  def fail_response(message)
    { success: false, error: message }
  end

  def twilio_phone_number
    ENV['TWILIO_PHONE_NUMBER']
  end

  def generate_code
    Random.rand(10000..99999).to_s
  end
end