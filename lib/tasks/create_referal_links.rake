if !Rails.env.production?
  namespace :db do
    desc "Create referal link to each client"
    task create_referal_link_to_each_client: :environment do

      User::Client.all.each do |client|
        # 'username@example.com'.split('@') ==> ["username", "example.com"] ==> "username"

        if client.referal_link
          client.referal_link.update_attributes(link: "ref=#{client.email.split('@').first}")
        else
          client.create_referal_link!(link: "ref=#{client.email.split('@').first}")
        end
      end
    end
  end
end