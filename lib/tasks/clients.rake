if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Generate dummy clients"
    task clients: :environment do
      reset_clients

      client = User::Client.new email: 'dan.s92@mail.ru',
                          name: FFaker::Name.name,
                          password: 'password',
                          phone_number: '+996553559536',
                          last_name: FFaker::Name.last_name

      puts 'OK...' if client.save

      Rake::Task['db:create_customer_account'].invoke
    end

    private

    def reset_clients
      puts "reseting all clients..."
      User::Client.destroy_all
      puts "...done"
    end
  end
end