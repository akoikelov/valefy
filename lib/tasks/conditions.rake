if !Rails.env.production?
  namespace :db do
    desc "Populate database with dummy data"
    task :conditions => :environment do
      reset_conditions

      TermCondition.create! condition: 'Client condition'
      TermCondition.create! condition: 'Master condition'
    end

    private

    def reset_conditions
      TermCondition.destroy_all
    end

  end
end