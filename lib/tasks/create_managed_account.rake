if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Create stripe account to each master"
    task create_managed_account: :environment do
      reset_stripe_account

      User::ServiceMan.all.each do |item|
        MasterStripeInfo.create! master_id: item.id,
                                address_line1: FFaker::Address.street_address,
                                state: 'ie',
                                city: 'dublin',
                                personal_id: '123456789', # Individual's personal ID number
                                tax_id: '12-1234567', # employer identification number
                                id_document_image: id_document_image,
                                account_number: 'IE89370400440532013000' # IBAN

        puts 'OK...' if item.managed_account.present?
      end
    end

    def id_document_image
      File.open(File.join(Rails.root, 'lib/fake_data/images/success.png'))
    end

    def reset_stripe_account
      puts "reseting all pro's..."
      MasterStripeInfo.destroy_all
      puts "...done"
    end
  end
end