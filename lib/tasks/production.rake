if !Rails.env.production?
  namespace :db do
    desc "Categories for production"
    task production: :environment do
      categories = [
        {
          :title=>"Электрик",
          :image=>"https://image-sortd.s3.amazonaws.com/category/image/60/2a300efc-906a-4145-bcd2-73b0f1c65924.jpg",
          :subcategories=>[
            {
              :title=>"Заменить дверной звонок",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/25/554ba984-09ee-45f1-813d-500af371d106.jpg",
              :services=>[]
            },
            {
              :title=>"Свет",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/23/l6.jpg",
              :services=>[
                {:title=>"Заменить датчик света", :price=>59},
                {:title=>"Заменить свет в зеркале. ", :price=>59},
                {:title=>"Не работает Свет", :price=>59},
                {:title=>"Установить датчик света", :price=>169},
                {:title=>"Установить свет на зеркало", :price=>99},
                {:title=>"Ремонт обогревателя", :price=>99},
                {:title=>"Установка электрического-зеркала", :price=>49}
              ]
            },
            {
              :title=>"Розетки",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/22/l4.jpg",
              :services=>[
                {:title=>"Установить внешнюю розетку", :price=>99},
                {:title=>"Заменить разъем розетки", :price=>49},
                {:title=>"Розетки не работают", :price=>59}
              ]
            },
            {
              :title=>"Душ",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/24/5pl.jpg",
              :services=>[
                {:title=>"Unblock Shower Drain", :price=>49},
                {:title=>"Reseal Shower Tray", :price=>59},
                {:title=>"Shower Door Repair", :price=>59},
                {:title=>"Shower Runs Hot and Cold", :price=>59},
                {:title=>"Shower Service", :price=>59},
                {:title=>"Power Shower Replacement", :price=>84},
                {:title=>"Manual Shower Replacement", :price=>129},
                {:title=>"Shower Door Installation", :price=>119},
                {:title=>"Electric Shower Replacement", :price=>79}
              ]
            },
            {
              :title=>"Установка электрических ворот ",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/28/f9a8928a-2991-4bc4-8844-2a47215ad83d.jpg",
              :services=>[
                {:title=>"Установка электрических ворот", :price=>400}
              ]
            },
            {
              :title=>"Выключатели",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/27/b58d3a55-a87e-4690-8ac2-c476c60c0b1d.jpg",
              :services=>[
                {:title=>"Установка дополнительных электрических розеток", :price=>59},
                {:title=>"Установка выключателей", :price=>49},
                {:title=>"Отключилось питание", :price=>59},
                {:title=>"Ремонт Проводки", :price=>49}
              ]
            },
            {
              :title=>"Замена дверного звонка",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/26/ele1.jpg",
              :services=>[
                {:title=>"Replace existing Door bell", :price=>49}
              ]
            },
            {
              :title=>"Отопление",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/29/ele2.jpg",
              :services=>[]
            }
          ]
        },
        {
          :title=>"Сантехник",
          :image=>"https://image-sortd.s3.amazonaws.com/category/image/39/1pl.jpg",
          :subcategories=>[
            {
              :title=>"Раковина",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/35/3pl.jpg",
              :services=>[
                {:title=>"Заменить раковину", :price=>59},
                {:title=>"Ремонт крана ", :price=>49},
                {:title=>"Разблокировать слив", :price=>49},
                {:title=>"Ремонт течи раковины", :price=>49},
                {:title=>"Установка раковины", :price=>69},
                {:title=>"Установка маленькой раковины", :price=>129},
                {:title=>"Укрепить трубы", :price=>59},
                {:title=>"Заменить кран", :price=>59}
              ]
            },
            {
              :title=>"Ванная ",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/34/2pl.jpg",
              :services=>[
                {:title=>"Ремонт кранов ванной", :price=>49},
                {:title=>"Установка безопасных поручней  (x2)", :price=>49},
                {:title=>"Установка ванного зеркала", :price=>109},
                {:title=>"Reseal Bathtub", :price=>99},
                {:title=>"Заменить ванну", :price=>209},
                {:title=>"Заменить краны ванной", :price=>49},
                {:title=>"Ремонт течи ванны", :price=>59},
                {:title=>"Ремонт кранов", :price=>49}
              ]
            },
            {
              :title=>"Душ",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/37/l5.jpg",
              :services=>[
                {:title=>"Unblock Shower Drain", :price=>49},
                {:title=>"Reseal Shower Tray", :price=>59},
                {:title=>"Shower Door Repair", :price=>59},
                {:title=>"Shower Runs Hot and Cold", :price=>59},
                {:title=>"Shower Service", :price=>59},
                {:title=>"Power Shower Replacement", :price=>84},
                {:title=>"Manual Shower Replacement", :price=>129},
                {:title=>"Shower Door Installation", :price=>119},
                {:title=>"Electric Shower Replacement", :price=>79}
              ]
            },
            {
              :title=>"Туалет",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/36/4pl.jpg",
              :services=>[
                {:title=>"Заменить сиденье унитаза", :price=>49},
                {:title=>"Утечка туалета", :price=>59},
                {:title=>"Туалет не промывается", :price=>59},
                {:title=>"Разблокировать туалет", :price=>59}
              ]
            }
          ]
        },
        {
          :title=>"Бытовая техника",
          :image=>"https://image-sortd.s3.amazonaws.com/category/image/58/apl3.jpg",
          :subcategories=>[
            {
              :title=>"Кухня",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/33/c13d51e2-4c5f-49fd-b939-06a1d30402f7.jpg",
              :services=>[
                {:title=>"Замена переключателя плиты", :price=>49},
                {:title=>"Установка вытяжного вентилятора", :price=>69},
                {:title=>"Переместить розетку электрической плиты", :price=>69}
              ]
            },
            {
              :title=>"Ремонт / поиск неисправностей",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/31/apl1.jpg",
              :services=>[
                {:title=>"Посудомоечной машины", :price=>59},
                {:title=>"духовки", :price=>59},
                {:title=>"Сушилки белья", :price=>59},
                {:title=>"Tumble Dryer", :price=>59},
                {:title=>"Стиральной машины", :price=>59},
                {:title=>"Варочная панель", :price=>59}
              ]
            },
            {
              :title=>"Установка",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/32/6704c86f-2db7-4868-8581-aa54ca258ff3.jpg",
              :services=>[
                {:title=>"Газовой печи", :price=>79},
                {:title=>"Холодильника", :price=>59},
                {:title=>"Электрической духовки", :price=>59},
                {:title=>"Посудомоечной машины", :price=>59},
                {:title=>"Стиральной машины", :price=>59},
                {:title=>"Газовой плиты ", :price=>149},
                {:title=>"Электрической плиты", :price=>59}
              ]
            },
            {
              :title=>"Вытяжка",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/30/l7.jpg",
              :services=>[
                {:title=>"Установка вытяжки", :price=>149},
                {:title=>"Замена вытяжки", :price=>59}
              ]
            }
          ]
        },
        {
          :title=>"Водонагреватели",
          :image=>"https://image-sortd.s3.amazonaws.com/category/image/41/l3.jpg",
          :subcategories=>[
            {
              :title=>"Ремонт водонагревателя ",
              :image=>nil,
              :services=>[]
            },
            {
              :title=>"Установка водонагревателя",
              :image=>nil,
              :services=>[]
            },
            {
              :title=>"Печь",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/21/f660cd24-0c80-454e-a5f0-5e135e2a58fd.jpg",
              :services=>[
                {:title=>"Обслуживание нефтяных печей", :price=>99}
              ]
            },
            {
              :title=>"Проверка или техническое обслуживание водонагревателя",
              :image=>nil,
              :services=>[]
            },
            {
              :title=>"Разное ",
              :image=>"https://image-sortd.s3.amazonaws.com/subcategory/image/20/1pl.jpg",
              :services=>[
                {:title=>"Газовый Водонагреватель ", :price=>79},
                {:title=>"Масляный водонагреватель ", :price=>89},
                {:title=>"Поиск неисправностей водонагревателя", :price=>99}
              ]
            }
          ]
        },
      ]

      categories.map do |item|
        category = Category.create! title: item[:title], remote_image_url: item[:image]

        item[:subcategories].map do |i|
          subcategory = category.subcategories.create! title: i[:title], remote_image_url: i[:image]

          i[:services].map do |sub|
            subcategory.services.create! title: sub[:title], price: sub[:price]
          end
        end
      end
    end
  end
end