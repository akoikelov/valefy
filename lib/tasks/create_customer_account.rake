if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Generate dummy clients"
    task create_customer_account: :environment do
      reset_customer_accounts

      # https://stripe.com/docs/testing#cards
      User::Client.all.each do |item|

        result = Api::V1::Customer::CreateCustomer.call(
          params: { stripe_token: 'tok_visa' }, current_user: item
        )

        result.success? ? (puts 'OK...') : (puts 'Not created...')
      end
    end

    private

    def reset_customer_accounts
      puts "reseting all clients..."
      CustomerAccount.destroy_all
      puts "...done"
    end
  end
end