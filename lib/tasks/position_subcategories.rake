if !Rails.env.production?
  namespace :db do
    desc "Set position for service"
    task position_subcategories: :environment do
      i = 1
      Subcategory.all.map { |e| e.update(position: i); i = i+1 }
    end
  end
end