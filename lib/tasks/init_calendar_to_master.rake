if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Init calendar data to each master"
    task init_calendar_to_master: :environment do

      User::ServiceMan.all.each do |item|
        item.build_calendar(time_plan: schedule)
        item.save!

        puts '-------------------'
        puts "#{item.email} - has this time plan\n#{schedule}"
        puts '-------------------'
      end
    end

    private

    def schedule
      {
        "Monday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
        "Tuesday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
        "Wednesday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
        "Thursday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
        "Friday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
        "Saturday": {
          "start": "#{rand 8..12}:00",
          "end":   "#{rand 13..22}:00"
        },
      }
    end
  end
end