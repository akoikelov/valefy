if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Init category to each master"
    task init_additional_payment_to_order: :environment do

      OrderItem.active.each do |item|
        if item.additional_payment.present?
          puts "Already exist"
        else
          item.build_additional_payment(additional_price: rand(1..100),description: FFaker::HipsterIpsum.paragraph)

          puts item.save ? "OK..." : "Error"
        end
      end
    end
  end
end