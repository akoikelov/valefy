if !Rails.env.production?
  namespace :db do
    desc 'Create coordinates of order_items'
    task orders_coords: :environment do
      OrderItem.all.each do |order|
        order.coordinates = create_coords_for_order(order)
        order.save
      end
      puts 'OK...'
    end

    def create_coords_for_order(order)
      "POINT(#{order.lon} #{order.lat})"
    end
  end
end