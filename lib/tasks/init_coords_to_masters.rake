if !Rails.env.production?
  require 'ffaker'

  namespace :temp do
    desc "Generate dummy categories"
    task init_coords_to_masters: :environment do
      User::ServiceMan.all.map { |e| e.update(lon: 74.589456, lat: 42.878615) } # default location: bishkek
    end
  end
end