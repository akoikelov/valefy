if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Generate dummy service men"
    task update_clients: :environment do

      User::Client.all.each do |item|
        item.update( last_name: FFaker::Name.last_name )
      end
    end
  end
end