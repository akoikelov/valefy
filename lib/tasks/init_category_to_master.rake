if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Init category to each master"
    task init_category_to_master: :environment do

      User::ServiceMan.all.each do |item|
        Category.visible.each do |category|
          item.categories << category
        end

        puts 'OK...' if item.categories.present?
      end
    end
  end
end