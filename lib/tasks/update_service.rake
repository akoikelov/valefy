if !Rails.env.production?
  namespace :db do
    desc "Populate database with dummy data"
    task :update_service => :environment do
      Service.all.each do |item|
        item.update(duration: 3)
      end
      puts 'OK...'
    end
  end
end