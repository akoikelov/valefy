if !Rails.env.production?
  require 'ffaker'

  namespace :db do
    desc "Generate dummy service men"
    task masters: :environment do
      reset_masters

      master = User::ServiceMan.new email: 'daniel@example.com',
                              name:  'master Name',
                              password: 'password',
                              phone_number: '+996000000000',
                              b_day: 20.years.ago,
                              last_name: FFaker::Name.last_name,
                              lat: 53.348838,
                              lon: -6.262190,
                              onduty: true

      puts 'OK...' if master.save

      if Category.count > 0 && Subcategory.count > 0 && Service.count > 0
        puts 'Init category to master...'

        Rake::Task['db:init_category_to_master'].invoke
      else
      puts 'You need upload test categories'
      end

      puts 'Create stripe account to each master...'
      Rake::Task['db:create_managed_account'].invoke
    end

    private

    def reset_masters
      puts "reseting all pro's..."
      User::ServiceMan.destroy_all
      puts "...done"
    end
  end
end