if !Rails.env.production?
  namespace :db do
    desc "Generate dummy service men"

    task service_update: :environment do
      Service.all.map { |e| e.update(price: rand(5..20)) }
      
      puts 'OK...'
    end
  end
end