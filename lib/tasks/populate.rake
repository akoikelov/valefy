if !Rails.env.production?
  namespace :db do
    desc "Populate database with dummy data"
    task :populate => :environment do
      puts 'Creating dummy client...'
      Rake::Task['db:clients'].invoke

      puts 'Creating dummy master...'
      Rake::Task['db:masters'].invoke

      puts 'Update price to service'
      Rake::Task['db:service_update'].invoke

      puts 'Set duration for service'
      Rake::Task['db:update_service'].invoke

      puts 'Set coordinates for orders'
      Rake::Task['db:orders_coords'].invoke
    end

    private

    def full_reset
      Rake::Task['db:reset'].invoke
      FileUtils.rm_rf(Dir["#{Rails.root}/public/system/*"])
      FileUtils.rm_rf(Dir["#{Rails.root}/public/uploads/*"])
    end

    def dummy title, number=1
      puts "Creating dummy #{title}..."
      number.times { yield }
      puts "...done"
    end

    def dummy_master title, number=1, column, value
      puts "Creating dummy #{title} with #{column} - #{value}"
      number.times { yield }
      puts "...done"
    end

    def random_image
      Dir.glob(File.join(Rails.root, 'lib/fake_data/images', '*.{jpg,png}')).sample
    end

    def random_baner_image
      Dir.glob(File.join(Rails.root, 'lib/fake_data/images/main_baner', '*.{jpg,png}')).sample
    end

    def random_bundle_image
      Dir.glob(File.join(Rails.root, 'lib/fake_data/images/bundles', '*.{jpg,png}')).sample
    end

  end
end