if !Rails.env.production?
  namespace :db do
    desc "Generate dummy categories"
    task category: :environment do
      # reset_masters

      # Parent categories
      # puts '-------------------'
      # puts "Creating parent categories"
      # puts '-------------------'
      #
      # Category.create! title: 'Plumbing'
      # Category.create! title: 'Appliances'
      # Category.create! title: 'Boiler Services'
      # Category.create! title: 'Plumbing'
      #
      # # Child category
      # puts '-------------------'
      # puts "Creating child categories"
      # puts '-------------------'
      #
      # Category.where(parent_id: nil).each do |item|
      #   item.subcategories.create! title: "#{item.title} - sub category"
      # end
    end

    # private
    #
    # def reset_masters
    #   puts "reseting all categories..."
    #   Category.destroy_all
    #   puts "...done"
    # end
  end
end