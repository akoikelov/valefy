class PdfGenerator
  include ApplicationHelper

  def initialize(invoice)
    @invoice = invoice
  end
  attr_reader :invoice

  def render(file)
    Prawn::Document.generate(file, force_download: true) do |pdf|
      change_default_font(pdf)

      render_headers(pdf)
      render_details(pdf)
      render_summary(pdf)
    end
  end

  private

  def render_headers(pdf)
    pdf.table([ ['Invoice Receipt'] ], width: 540, cell_style: {padding: 0}) do
      row(0..10).borders = []
      cells.column(0).style(size: 20, font_style: :bold, valign: :center)
    end
  end

  # Renders details about pdf. Shows recipient name, invoice date and id
  def render_details(pdf)
    pdf.move_down 10
    pdf.stroke_horizontal_rule
    pdf.move_down 15

    billing_details =
      pdf.make_table([ ['Billed to:'], [invoice.client.name], ['Pro:'], [invoice.order.master.name] ],
                     width: 355,  cell_style: {padding: 3}) do
      row(0..10).style(size: 9, borders: [])
      row(0).column(0).style(font_style: :bold)
      row(2).column(0).style(font_style: :bold)
    end

    invoice_date = invoice.created_at.strftime('%e %b %Y')
    invoice_id   = invoice.id.to_s
    invoice_details =
      pdf.make_table(
        [ ['Invoice Date:', invoice_date],
           ['Invoice No:', invoice_id],
           ['Order ID:', invoice.order.id],
           ['Order date:', time_with_format(invoice.order)],
           ['Category:', invoice.order.category.title]
         ], width: 185, cell_style: {padding: 5, border_width: 0.5}) do
      row(0..10).style(size: 9)
      row(0..10).column(0).style(font_style: :bold)
    end

    pdf.table([ [billing_details, invoice_details] ], cell_style: {padding: 0}) do
      row(0..10).style(borders: [])
    end
  end

  # Renders details of invoice in a tabular format. Renders each line item, and
  # unit price, and total amount, along with tax. It also displays summary,
  # ie total amount, and total price along with tax.
  def render_summary(pdf)
    pdf.move_down 25
    pdf.text 'Invoice Summary', size: 12, style: :bold
    pdf.stroke_horizontal_rule
    table_details = [ ['Sl. no.', 'Service', 'Unit price', 'Total Price'] ]
    invoice.line_items.each_with_index do |line_item, index|
      net_amount = line_item.price
      table_details <<
        [index + 1, line_item.service.title, line_item.service.price, net_amount]
    end

    pdf.table(table_details, column_widths: [40, 380, 60, 60], header: true,
              cell_style: {padding: 5, border_width: 0.5}) do
      row(0).style(size: 10, font_style: :bold)
      row(0..100).style(size: 9)

      cells.columns(0).align = :right
      cells.columns(2).align = :right
      cells.columns(3).align = :right
      row(0..100).borders = [:top, :bottom]
    end

    summary_details = details(invoice)

    pdf.table(summary_details, column_widths: [480, 60], header: true,
              cell_style: {padding: 5, border_width: 0.5}) do
      row(0..100).style(size: 9, font_style: :bold)
      row(0..100).borders = []
      cells.columns(0..100).align = :right
    end

    pdf.move_down 25
    pdf.stroke_horizontal_rule
  end

  def change_default_font(pdf)
    pdf.font_families.update("Roboto-Light" => {
      :normal => "#{Rails.root}/app/assets/fonts/Roboto-Light.ttf",
      :bold => "#{Rails.root}/app/assets/fonts/Roboto-BoldItalic.ttf"
    })
    pdf.font "Roboto-Light"
  end

  def details(invoice)
    if invoice.type_invoice.eql?(Invoice::INVOICE_TYPE_CUSTOMER)
      [
        ['Total',    invoice.total]
      ]
    else
      [
        ['Subtotal', invoice.net_amount],
        ['Tax',      invoice.tax_amount],
        ['Total',    invoice.total_with_fees]
      ]
    end
  end

  def client_name
    invoice.client.name
  end
end
