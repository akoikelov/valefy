# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190218145336) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "postgis"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "additional_payments", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "service_id"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.boolean  "client_confirm",                           default: false
    t.text     "description"
    t.boolean  "rejected",                                 default: false
    t.decimal  "additional_price", precision: 8, scale: 2
  end

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "role"
    t.string   "access_items"
    t.string   "access_item_keys"
    t.string   "title"
    t.text     "description"
    t.integer  "platform"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "banners", force: :cascade do |t|
    t.string   "title"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.text     "description"
  end

  create_table "calendars", force: :cascade do |t|
    t.integer  "master_id"
    t.json     "time_plan"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "start_work"
    t.string   "end_work"
  end

  create_table "call_mes", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "client_feedback_id"
    t.boolean  "recall"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "categories", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "title"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.string   "image"
    t.string   "icon"
    t.string   "title_ru"
    t.boolean  "visible",     default: false
    t.string   "icon_web"
    t.integer  "position",    default: 1
    t.text     "description"
    t.boolean  "corp",        default: false
    t.integer  "duration",    default: 30
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.integer  "country_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "client_categories", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "category_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "client_categories", ["category_id"], name: "index_client_categories_on_category_id", using: :btree

  create_table "client_extra_infos", force: :cascade do |t|
    t.integer  "gender"
    t.integer  "family_status"
    t.string   "profession"
    t.integer  "client_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "client_feedbacks", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "client_id"
    t.integer  "serviceman_id"
    t.integer  "rating"
    t.text     "comment"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "stationary_id"
    t.integer  "polite",        default: 4
    t.integer  "friendly",      default: 4
    t.integer  "orderly",       default: 4
    t.decimal  "tip",           default: 0.0
  end

  create_table "client_promo_codes", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "promo_code_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "client_promo_codes", ["promo_code_id"], name: "index_client_promo_codes_on_promo_code_id", using: :btree

  create_table "client_segments", force: :cascade do |t|
    t.integer  "title"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "client_services", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "service_id"
    t.decimal  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "companies", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.integer  "platform"
    t.string   "logo"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "admin_user_id"
  end

  create_table "company_locations", force: :cascade do |t|
    t.integer  "admin_user_id"
    t.string   "title"
    t.string   "address"
    t.float    "lat"
    t.float    "lon"
    t.json     "work_schedule", default: {}
    t.string   "phone_number"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.string   "image"
    t.integer  "rating",        default: 4
    t.integer  "company_id"
    t.string   "email"
  end

  create_table "company_services", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "admin_user_id"
    t.decimal  "price"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "company_id"
  end

  create_table "contacts", force: :cascade do |t|
    t.string   "service_name"
    t.string   "link"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "contract_dates", force: :cascade do |t|
    t.integer  "contract_id"
    t.integer  "order_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "office_id"
    t.integer  "team_id"
  end

  create_table "contract_services", force: :cascade do |t|
    t.integer  "contract_id"
    t.integer  "service_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "contracts", force: :cascade do |t|
    t.integer  "client_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.datetime "expiration_date"
    t.text     "address"
    t.datetime "order_time"
    t.integer  "segment_id"
  end

  create_table "corp_informations", force: :cascade do |t|
    t.string   "position_in_company"
    t.string   "name_in_company"
    t.string   "doc"
    t.integer  "client_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "customer_accounts", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "customer_charges", force: :cascade do |t|
    t.string   "charge_id"
    t.integer  "customer_account_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "order_item_id"
    t.boolean  "capture",                      default: false
    t.string   "additional_payment_charge_id"
    t.boolean  "additional_payment_capture",   default: false
    t.integer  "charge_type"
  end

  create_table "demo_requests", force: :cascade do |t|
    t.string   "company"
    t.string   "email"
    t.string   "contact"
    t.string   "phone_number"
    t.text     "notes"
    t.text     "message"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "devices", force: :cascade do |t|
    t.integer  "client_id"
    t.string   "id_registration_firebase"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.string   "platform"
    t.integer  "master_id"
    t.string   "token_client_id"
    t.string   "app"
  end

  create_table "districts", force: :cascade do |t|
    t.string    "name"
    t.string    "image"
    t.text      "description"
    t.integer   "city_id"
    t.datetime  "created_at",                                                              null: false
    t.datetime  "updated_at",                                                              null: false
    t.float     "lat"
    t.float     "lon"
    t.string    "coords"
    t.geography "coordinates", limit: {:srid=>4326, :type=>"geometry", :geographic=>true}
  end

  add_index "districts", ["coordinates"], name: "index_districts_on_coordinates", using: :gist

  create_table "draft_masters", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "name"
    t.string   "phone_number"
    t.string   "email"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "cv",           default: [],              array: true
    t.string   "id_document"
  end

  create_table "email_texts", force: :cascade do |t|
    t.string   "email_type"
    t.string   "slug"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employees", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.string   "position"
    t.string   "email"
    t.string   "phone_number"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.datetime "b_day"
    t.integer  "admin_user_id"
  end

  create_table "faqs", force: :cascade do |t|
    t.text     "question"
    t.text     "answer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "type_user"
    t.integer  "position"
  end

  create_table "feedback_apps", force: :cascade do |t|
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "client_id"
    t.integer  "master_id"
    t.string   "platform"
  end

  create_table "gift_cards", force: :cascade do |t|
    t.integer  "to_client_id"
    t.integer  "from_client_id"
    t.decimal  "amount",             precision: 8, scale: 2
    t.text     "message"
    t.datetime "exp_date"
    t.datetime "created_at",                                                 null: false
    t.datetime "updated_at",                                                 null: false
    t.string   "to_email"
    t.integer  "service_id"
    t.integer  "customer_charge_id"
    t.boolean  "used",                                       default: false
  end

  create_table "helps", force: :cascade do |t|
    t.integer  "client_id"
    t.text     "question"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "master_id"
  end

  create_table "images", force: :cascade do |t|
    t.string   "link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "master_id"
    t.integer  "client_id"
    t.string   "number"
    t.string   "currency"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "type_invoice"
  end

  create_table "line_items", force: :cascade do |t|
    t.integer  "invoice_id"
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.integer  "service_id"
    t.decimal  "price",            precision: 8, scale: 2
    t.decimal  "additional_price", precision: 8, scale: 2, default: 0.0
  end

  create_table "managed_accounts", force: :cascade do |t|
    t.integer  "serviceman_id"
    t.string   "managed_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "routing_number"
    t.string   "account_number"
  end

  create_table "master_categories", force: :cascade do |t|
    t.integer  "service_man_id"
    t.integer  "category_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "master_categories", ["category_id"], name: "index_master_categories_on_category_id", using: :btree

  create_table "master_feedbacks", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "master_id"
    t.integer  "client_id"
    t.text     "message"
    t.integer  "rating"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "master_locations", force: :cascade do |t|
    t.integer  "master_id"
    t.string   "lon"
    t.string   "lat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "master_stripe_infos", force: :cascade do |t|
    t.integer  "master_id"
    t.string   "tax_id"
    t.string   "personal_id"
    t.string   "id_document_image"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "state"
    t.string   "postal_code"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "routing_number"
    t.string   "account_number"
    t.string   "city"
  end

  create_table "offices", force: :cascade do |t|
    t.string   "title"
    t.integer  "client_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.float    "lat"
    t.float    "lon"
    t.integer  "count_vehicles"
    t.string   "address"
  end

  create_table "order_images", force: :cascade do |t|
    t.integer  "order_item_id"
    t.string   "link"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "order_item_services", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "service_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "order_item_services", ["service_id"], name: "index_order_item_services_on_service_id", using: :btree

  create_table "order_items", force: :cascade do |t|
    t.integer   "client_id"
    t.integer   "service_id"
    t.string    "address"
    t.datetime  "created_at",                                                                                                              null: false
    t.datetime  "updated_at",                                                                                                              null: false
    t.json      "location_serviceman"
    t.json      "location_client"
    t.integer   "status",                                                                                                  default: 0
    t.text      "description"
    t.boolean   "order_now",                                                                                               default: false
    t.text      "images",                                                                                                  default: [],                 array: true
    t.datetime  "start_work"
    t.integer   "master_id"
    t.datetime  "end_work_time"
    t.datetime  "accepted_time"
    t.datetime  "order_time"
    t.float     "lon"
    t.float     "lat"
    t.boolean   "pets",                                                                                                    default: false
    t.boolean   "parking",                                                                                                 default: false
    t.integer   "subscription_id"
    t.decimal   "total",                                                                           precision: 8, scale: 2
    t.text      "before_work_images",                                                                                      default: [],                 array: true
    t.text      "after_work_images",                                                                                       default: [],                 array: true
    t.integer   "count_vehicles",                                                                                          default: 1
    t.string    "platform"
    t.integer   "stationary_id"
    t.integer   "promo_code_id"
    t.integer   "gift_card_id"
    t.string    "model_auto"
    t.geography "coordinates",         limit: {:srid=>4326, :type=>"geometry", :geographic=>true}
    t.integer   "contract_id"
    t.integer   "baby_seat",                                                                                               default: 0
    t.integer   "car_count",                                                                                               default: 0
    t.integer   "suv_count",                                                                                               default: 0
    t.integer   "minivan_count",                                                                                           default: 0
    t.decimal   "total_price",                                                                                             default: 0.1
    t.text      "reg_numbers"
  end

  add_index "order_items", ["coordinates"], name: "index_order_items_on_coordinates", using: :gist

  create_table "order_status_histories", force: :cascade do |t|
    t.integer  "order_item_id"
    t.integer  "status"
    t.integer  "author"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "partners", force: :cascade do |t|
    t.string   "title"
    t.string   "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "posts", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "pro_informations", force: :cascade do |t|
    t.json     "work_schedule"
    t.integer  "pro_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "pro_stripe_accounts", force: :cascade do |t|
    t.integer  "pro_id"
    t.string   "tax_id"
    t.string   "personal_id"
    t.string   "id_document_image"
    t.string   "address_line1"
    t.string   "address_line2"
    t.string   "account_number"
    t.string   "city"
    t.string   "state"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.string   "file_uid"
    t.string   "bank_account_uid"
    t.string   "account_uid"
  end

  create_table "promo_codes", force: :cascade do |t|
    t.string   "promo_code_id"
    t.datetime "exp_date"
    t.integer  "discount"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "gift_card_id"
    t.integer  "client_id"
    t.boolean  "used",          default: false
    t.string   "client_email"
    t.text     "description",   default: ""
    t.integer  "stationary_id"
  end

  create_table "referal_links", force: :cascade do |t|
    t.string   "link"
    t.integer  "client_id"
    t.integer  "came_from_client_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "referal_shares", force: :cascade do |t|
    t.integer  "client_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "promo_code_id"
    t.integer  "shared_client_id"
  end

  create_table "schedule_workloads", force: :cascade do |t|
    t.datetime "date"
    t.json     "schedule",   default: []
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "segments", force: :cascade do |t|
    t.string   "title"
    t.string   "slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "services", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.text     "description"
    t.integer  "subcategory_id"
    t.string   "title_ru"
    t.integer  "position"
    t.float    "duration",                               default: 0.0
    t.decimal  "price",          precision: 8, scale: 2
    t.string   "short_title"
    t.integer  "sort"
  end

  create_table "settings", force: :cascade do |t|
    t.integer  "radius"
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.boolean  "send_email_invoice_client",       default: true
    t.boolean  "send_email_created_order",        default: true
    t.integer  "time_send_location"
    t.boolean  "send_welcome_email",              default: false
    t.json     "authorization",                   default: {}
    t.integer  "freelancing_transfer_percentage", default: 0
    t.integer  "aggregator_transfer_percentage",  default: 0
    t.integer  "park_transfer_percentage",        default: 0
    t.integer  "franchise_transfer_percentage",   default: 0
    t.boolean  "send_email_owner_gift_card",      default: false
    t.integer  "bisy_days_for_order",             default: 0
  end

  create_table "sms_histories", force: :cascade do |t|
    t.integer  "client_id"
    t.integer  "master_id"
    t.string   "phone_number"
    t.string   "code"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "status",       default: false
  end

  create_table "station_services", force: :cascade do |t|
    t.integer  "service_id"
    t.integer  "station_id"
    t.decimal  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subcategories", force: :cascade do |t|
    t.integer  "category_id"
    t.string   "title"
    t.string   "image"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "icon"
    t.string   "title_ru"
    t.string   "icon_web"
    t.boolean  "visible",      default: false
    t.integer  "position"
    t.string   "regular_type"
    t.string   "code"
    t.boolean  "small_car",    default: false
    t.boolean  "corp",         default: false
  end

  create_table "subscriptions", force: :cascade do |t|
    t.datetime "closed_at"
    t.integer  "client_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "started_at"
  end

  create_table "tariff_plans", force: :cascade do |t|
    t.string   "plan"
    t.integer  "plan_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "teams", force: :cascade do |t|
    t.string   "title"
    t.integer  "contract_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "term_conditions", force: :cascade do |t|
    t.text     "condition"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "transfers", force: :cascade do |t|
    t.integer  "order_item_id"
    t.string   "transfer_id"
    t.decimal  "amount",              precision: 8, scale: 2
    t.string   "destination_payment"
    t.integer  "customer_account_id"
    t.integer  "managed_account_id"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "balance_transaction"
  end

  create_table "transports", force: :cascade do |t|
    t.integer  "master_id"
    t.string   "car"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "image"
    t.string   "email"
    t.string   "tokens"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "onduty",                 default: false
    t.datetime "calendar_work"
    t.string   "type"
    t.string   "phone_number"
    t.boolean  "confirm_phone_number",   default: false
    t.string   "last_name"
    t.string   "facebook_id"
    t.date     "b_day"
    t.string   "google_id"
    t.integer  "admin_user_id"
    t.float    "lon"
    t.float    "lat"
    t.text     "address"
    t.boolean  "corp",                   default: false
    t.integer  "tariff_plan_id"
    t.integer  "pay_with"
    t.integer  "corp_discount",          default: 0
    t.boolean  "need_send_location",     default: false
    t.string   "google_refresh_token"
    t.integer  "category_id"
    t.integer  "company_id"
    t.string   "doc"
    t.integer  "service_id"
    t.integer  "team_id"
    t.boolean  "mobile",                 default: false
    t.boolean  "cash",                   default: false
    t.boolean  "use_bank_details",       default: false
    t.string   "share_email"
    t.integer  "district_id"
    t.datetime "start_work_date"
  end

  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["phone_number"], name: "index_users_on_phone_number", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree

  create_table "verifications", force: :cascade do |t|
    t.string   "phone_number"
    t.string   "code"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.boolean  "status",       default: false
    t.integer  "client_id"
  end

  create_table "work_hours", force: :cascade do |t|
    t.string   "start_day"
    t.string   "end_day"
    t.integer  "day",        null: false
    t.integer  "master_id",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
