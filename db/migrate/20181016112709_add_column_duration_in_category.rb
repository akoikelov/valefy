class AddColumnDurationInCategory < ActiveRecord::Migration
  def change
    add_column :categories, :duration, :integer, default: 30
  end
end
