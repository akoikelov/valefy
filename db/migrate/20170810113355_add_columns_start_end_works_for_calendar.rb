class AddColumnsStartEndWorksForCalendar < ActiveRecord::Migration
  def change
    add_column :calendars, :start_work, :string
    add_column :calendars, :end_work,   :string
  end
end
