class ChangeColumnTypeLonLatInOrderItem < ActiveRecord::Migration
  def self.up
    remove_column :order_items, :lon
    remove_column :order_items, :lat

    add_column :order_items, :lon, :float
    add_column :order_items, :lat, :float
  end

  def self.down
    remove_column :order_items, :lon
    remove_column :order_items, :lat

    add_column :order_items, :lon, :string
    add_column :order_items, :lat, :string
  end
end
