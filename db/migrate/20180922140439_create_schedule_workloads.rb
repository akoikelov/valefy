class CreateScheduleWorkloads < ActiveRecord::Migration
  def change
    create_table :schedule_workloads do |t|
      t.datetime :date
      t.json     :schedule, default: []

      t.timestamps null: false
    end
  end
end
