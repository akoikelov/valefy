class RemoveColumnNewbieCreditFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :newbie_credit
  end
end
