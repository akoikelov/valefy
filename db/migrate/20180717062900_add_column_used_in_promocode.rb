class AddColumnUsedInPromocode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :used, :boolean, default: false
  end
end
