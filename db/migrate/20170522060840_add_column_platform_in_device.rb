class AddColumnPlatformInDevice < ActiveRecord::Migration
  def change
    add_column :devices, :platform, :string
  end
end
