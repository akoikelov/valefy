class AddColumnSubscriptionIdInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :subscription_id, :integer
  end
end
