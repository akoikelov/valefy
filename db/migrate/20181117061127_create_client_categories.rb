class CreateClientCategories < ActiveRecord::Migration
  def change
    create_table :client_categories do |t|
      t.integer :client_id
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
  end
end
