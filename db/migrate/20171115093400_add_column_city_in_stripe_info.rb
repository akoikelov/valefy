class AddColumnCityInStripeInfo < ActiveRecord::Migration
  def change
    add_column :master_stripe_infos, :city, :string
  end
end
