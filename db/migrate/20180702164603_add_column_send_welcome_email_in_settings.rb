class AddColumnSendWelcomeEmailInSettings < ActiveRecord::Migration
  def change
    add_column :settings, :send_welcome_email, :boolean, default: false
  end
end
