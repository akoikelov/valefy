class AddColumnsLonAndLatInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :lon, :string
    add_column :order_items, :lat, :string
  end
end
