class AddColumnClientIdInVerification < ActiveRecord::Migration
  def change
    add_column :verifications, :client_id, :integer
  end
end
