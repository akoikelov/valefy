class AddColumnCorpDiscountInUsers < ActiveRecord::Migration
  def change
    add_column :users, :corp_discount, :integer, default: 0
  end
end
