class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.string :email
      t.string :link_fb

      t.timestamps null: false
    end
  end
end
