class AddColumnDurationInServices < ActiveRecord::Migration
  def change
    add_column :services, :duration, :float, default: 0
  end
end
