class AddColumnOrderTimeInOrders < ActiveRecord::Migration
  def change
    add_column :order_items, :order_time, :datetime
  end
end
