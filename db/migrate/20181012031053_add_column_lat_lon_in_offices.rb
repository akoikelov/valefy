class AddColumnLatLonInOffices < ActiveRecord::Migration
  def change
    add_column :offices, :lat, :float
    add_column :offices, :lon, :float
  end
end
