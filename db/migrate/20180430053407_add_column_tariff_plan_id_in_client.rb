class AddColumnTariffPlanIdInClient < ActiveRecord::Migration
  def change
    add_column :users, :tariff_plan_id, :integer
  end
end
