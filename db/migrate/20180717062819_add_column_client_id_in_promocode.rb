class AddColumnClientIdInPromocode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :client_id, :integer
  end
end
