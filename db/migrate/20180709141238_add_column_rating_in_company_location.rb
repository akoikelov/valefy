class AddColumnRatingInCompanyLocation < ActiveRecord::Migration
  def change
    add_column :company_locations, :rating, :integer, default: 4
  end
end
