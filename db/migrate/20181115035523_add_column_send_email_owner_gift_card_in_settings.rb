class AddColumnSendEmailOwnerGiftCardInSettings < ActiveRecord::Migration
  def change
    add_column :settings, :send_email_owner_gift_card, :boolean, default: false
  end
end
