class AddColumnSegmentIdInContract < ActiveRecord::Migration
  def change
    add_column :contracts, :segment_id, :integer
  end
end
