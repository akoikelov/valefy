class ChangeDeciamlTypeOfAllFields < ActiveRecord::Migration
  def up
    remove_column :additional_payments, :additional_price
    add_column    :additional_payments, :additional_price, :decimal, precision: 8, scale: 2

    remove_column :services, :price
    add_column    :services, :price, :decimal, precision: 8, scale: 2

    remove_column :services, :today_price
  end

  def down
    remove_column :additional_payments, :additional_price
    add_column    :additional_payments, :additional_price, :decimal

    remove_column :services, :price
    add_column    :services, :price, :decimal

    add_column    :services, :today_price, :decimal
  end
end
