class CreateCalendars < ActiveRecord::Migration
  def change
    create_table :calendars do |t|
      t.integer :master_id
      t.json :time_plan

      t.timestamps null: false
    end
  end
end
