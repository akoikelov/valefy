class AddColumnAdditionalPriceClientRejectInAdditionalPayment < ActiveRecord::Migration
  def change
    add_column :additional_payments, :additional_price, :decimal
    add_column :additional_payments, :client_confirm, :boolean, default: false
  end
end
