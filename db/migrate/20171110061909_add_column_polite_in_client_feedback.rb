class AddColumnPoliteInClientFeedback < ActiveRecord::Migration
  def change
    add_column :client_feedbacks, :polite, :boolean, default: false
  end
end
