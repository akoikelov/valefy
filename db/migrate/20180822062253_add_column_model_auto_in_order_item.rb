class AddColumnModelAutoInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :model_auto, :string
  end
end
