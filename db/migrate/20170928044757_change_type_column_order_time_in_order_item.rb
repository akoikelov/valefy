class ChangeTypeColumnOrderTimeInOrderItem < ActiveRecord::Migration
  def up
    remove_column :order_items, :order_time
    add_column :order_items, :order_time, :datetime
  end

  def down
    remove_column :order_items, :order_time
    add_column :order_items, :order_time, :string
  end
end
