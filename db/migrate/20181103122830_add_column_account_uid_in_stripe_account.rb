class AddColumnAccountUidInStripeAccount < ActiveRecord::Migration
  def change
    add_column :pro_stripe_accounts, :account_uid, :string
  end
end
