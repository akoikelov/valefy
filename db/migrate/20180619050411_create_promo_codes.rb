class CreatePromoCodes < ActiveRecord::Migration
  def change
    create_table :promo_codes do |t|
      t.string   :promo_code_id
      t.datetime :exp_date
      t.integer  :discount

      t.timestamps null: false
    end
  end
end
