class AddColumnCashInUsers < ActiveRecord::Migration
  def change
    add_column :users, :cash, :boolean, default: false
  end
end
