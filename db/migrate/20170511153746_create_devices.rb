class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.integer :client_id
      t.string  :id_registration_firebase
      
      t.timestamps null: false
    end
  end
end
