class CreateOrderStatusHistories < ActiveRecord::Migration
  def change
    create_table :order_status_histories do |t|
      t.integer :order_item_id
      t.integer :status
      t.integer :author

      t.timestamps null: false
    end
  end
end
