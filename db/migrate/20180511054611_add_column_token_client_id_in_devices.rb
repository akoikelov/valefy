class AddColumnTokenClientIdInDevices < ActiveRecord::Migration
  def change
    add_column :devices, :token_client_id, :string
  end
end
