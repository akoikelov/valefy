class CreateMasterLocations < ActiveRecord::Migration
  def change
    create_table :master_locations do |t|
      t.integer :master_id
      t.string :lon
      t.string :lat

      t.timestamps null: false
    end
  end
end
