class AddSortToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :sort, :integer
  end
end
