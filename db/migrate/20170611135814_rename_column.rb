class RenameColumn < ActiveRecord::Migration
  def change
    rename_column :services, :additional_price, :today_price
  end
end
