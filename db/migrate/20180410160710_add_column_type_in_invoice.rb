class AddColumnTypeInInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :type_invoice, :string
  end
end
