class DeletePlateNumberAndRegular < ActiveRecord::Migration
  def change
    remove_column :categories, :regular
    remove_column :order_items, :plate_number
  end
end
