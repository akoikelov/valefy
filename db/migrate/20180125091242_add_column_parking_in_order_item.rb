class AddColumnParkingInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :parking, :boolean, default: false
  end
end
