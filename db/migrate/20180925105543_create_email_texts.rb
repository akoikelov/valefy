class CreateEmailTexts < ActiveRecord::Migration
  def change
    create_table :email_texts do |t|
      t.string :email_type
      t.string :slug
      t.text :content

      t.timestamps null: false
    end
  end
end
