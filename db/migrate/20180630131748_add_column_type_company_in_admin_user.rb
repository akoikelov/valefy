class AddColumnTypeCompanyInAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :type_company, :integer
  end
end
