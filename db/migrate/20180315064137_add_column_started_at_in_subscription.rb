class AddColumnStartedAtInSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :started_at, :datetime
  end
end
