class AddColumnPlatformInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :platform, :string
  end
end
