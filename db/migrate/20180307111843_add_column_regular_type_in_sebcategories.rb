class AddColumnRegularTypeInSebcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :regular_type, :string
  end
end
