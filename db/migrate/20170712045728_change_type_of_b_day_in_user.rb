class ChangeTypeOfBDayInUser < ActiveRecord::Migration
  def self.up
    remove_column :users, :b_day
    add_column    :users, :b_day, :date
  end

  def self.down
    remove_column :users, :b_day
    add_column    :users, :b_day, :string
  end
end
