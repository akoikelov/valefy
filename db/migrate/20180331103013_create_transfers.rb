class CreateTransfers < ActiveRecord::Migration
  def change
    create_table :transfers do |t|
      t.integer :order_item_id
      t.string  :transfer_id
      t.decimal :amount, precision: 8, scale: 2
      t.string  :destination_payment
      t.integer :customer_account_id
      t.integer :managed_account_id

      t.timestamps null: false
    end
  end
end
