class RenameColumnEndWorkInOrderItems < ActiveRecord::Migration
  def change
    rename_column :order_items, :end_work, :end_work_time
  end
end
