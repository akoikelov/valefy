class CreateClientFeedbacks < ActiveRecord::Migration
  def change
    create_table :client_feedbacks do |t|
      t.integer :order_item_id
      t.integer :client_id
      t.integer :serviceman_id
      t.integer :rating
      t.text    :comment

      t.timestamps null: false
    end
  end
end
