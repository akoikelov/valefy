class CreateMasterFeedbacks < ActiveRecord::Migration
  def change
    create_table :master_feedbacks do |t|
      t.integer :order_item_id
      t.integer :master_id
      t.integer :client_id
      t.text    :message
      t.integer :rating

      t.timestamps null: false
    end
  end
end
