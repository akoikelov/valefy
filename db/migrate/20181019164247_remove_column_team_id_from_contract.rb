class RemoveColumnTeamIdFromContract < ActiveRecord::Migration
  def up
    remove_column :contracts, :team_id
  end
  def down
    add_column :contracts, :team_id, :integer
  end
end
