class AddColumnCorpInSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :corp, :boolean, default: false
  end
end
