class AddColumnSuvCountInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :suv_count, :integer, default: 0
  end
end
