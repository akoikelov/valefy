class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
