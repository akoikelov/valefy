class AddColumnDescriptionInPromocode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :description, :text, default: ""
  end
end
