class ChangeTypeOfCvAndIdDocument < ActiveRecord::Migration
  def up
    remove_column :draft_masters, :id_document
    add_column    :draft_masters, :id_document, :string
  end

  def down
    remove_column :draft_masters, :id_document
    add_column    :draft_masters, :id_document, :string, default: [], array: true
  end
end
