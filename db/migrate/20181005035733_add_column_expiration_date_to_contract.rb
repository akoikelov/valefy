class AddColumnExpirationDateToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :expiration_date, :datetime
  end
end
