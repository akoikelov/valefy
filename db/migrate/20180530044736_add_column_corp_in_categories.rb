class AddColumnCorpInCategories < ActiveRecord::Migration
  def change
    add_column :categories, :corp, :boolean, default: false
  end
end
