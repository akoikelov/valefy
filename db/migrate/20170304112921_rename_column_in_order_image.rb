class RenameColumnInOrderImage < ActiveRecord::Migration
  def up
    rename_column :order_images, :image, :link
  end

  def down
    rename_column :order_images, :link, :image
  end
end
