class RemoveSortInSubcategories < ActiveRecord::Migration
  def change
    remove_column :subcategories, :sort
  end
end
