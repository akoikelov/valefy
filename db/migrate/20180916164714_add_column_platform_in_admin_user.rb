class AddColumnPlatformInAdminUser < ActiveRecord::Migration
  def up
    remove_column :admin_users, :type_company
    add_column :admin_users, :platform, :integer
  end

  def down
    remove_column :admin_users, :platform
    add_column :admin_users, :type_company, :integer
  end
end
