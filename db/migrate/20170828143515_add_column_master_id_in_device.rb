class AddColumnMasterIdInDevice < ActiveRecord::Migration
  def change
    add_column :devices, :master_id, :integer
  end
end
