class AddColumnChargeTypeInCustomerCharges < ActiveRecord::Migration
  def change
    add_column :customer_charges, :charge_type, :integer
  end
end