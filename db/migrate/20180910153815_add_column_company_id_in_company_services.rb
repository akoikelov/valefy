class AddColumnCompanyIdInCompanyServices < ActiveRecord::Migration
  def change
    add_column :company_services, :company_id, :integer
  end
end
