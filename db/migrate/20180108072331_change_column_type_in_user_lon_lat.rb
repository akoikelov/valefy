class ChangeColumnTypeInUserLonLat < ActiveRecord::Migration
  def self.up
    remove_column :users, :lon
    remove_column :users, :lat

    add_column :users, :lon, :float
    add_column :users, :lat, :float
  end

  def self.down
    remove_column :users, :lon
    remove_column :users, :lat

    add_column :users, :lon, :string
    add_column :users, :lat, :string
  end
end
