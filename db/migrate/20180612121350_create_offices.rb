class CreateOffices < ActiveRecord::Migration
  def change
    create_table :offices do |t|
      t.string :title
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
