class AddColumnCategoryidInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :category_id, :integer
  end
end
