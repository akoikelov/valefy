class ChangeTypeOfOrderlyColumnInClientFeedback < ActiveRecord::Migration
  def up
    remove_column :client_feedbacks, :orderly
    add_column :client_feedbacks, :orderly, :integer, default: 4
  end
  def down
    remove_column :client_feedbacks, :orderly
    add_column :client_feedbacks, :orderly, :boolean, default: false
  end
end
