class CreateCompanyServices < ActiveRecord::Migration
  def change
    create_table :company_services do |t|
      t.integer :service_id
      t.integer :admin_user_id
      t.decimal :price

      t.timestamps null: false
    end
  end
end
