class AddColumnServiceIdInGiftCards < ActiveRecord::Migration
  def change
    add_column :gift_cards, :service_id, :integer
  end
end
