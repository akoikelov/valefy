class AddColumnAddressToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :address, :text
  end
end
