class AddColumnForStripeInfo < ActiveRecord::Migration
  def change
    add_column :master_stripe_infos, :routing_number, :string
    add_column :master_stripe_infos, :account_number, :string
  end
end
