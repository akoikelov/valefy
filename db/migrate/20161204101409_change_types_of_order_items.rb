class ChangeTypesOfOrderItems < ActiveRecord::Migration
  def change
    remove_column :order_items, :location_serviceman
    add_column :order_items, :location_serviceman, :json

    remove_column :order_items, :location_user
    add_column :order_items, :location_client, :json
  end
end
