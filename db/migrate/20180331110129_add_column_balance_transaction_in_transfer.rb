class AddColumnBalanceTransactionInTransfer < ActiveRecord::Migration
  def change
    add_column :transfers, :balance_transaction, :string
  end
end
