class AddColumnRegNumbersToOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :reg_numbers, :text
  end
end
