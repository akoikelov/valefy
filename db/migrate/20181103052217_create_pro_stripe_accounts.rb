class CreateProStripeAccounts < ActiveRecord::Migration
  def change
    create_table :pro_stripe_accounts do |t|
      t.integer  :pro_id
      t.string   :tax_id
      t.string   :personal_id
      t.string   :id_document_image
      t.string   :address_line1
      t.string   :address_line2
      t.string   :account_number
      t.string   :city
      t.string   :state

      t.timestamps null: false
    end
  end
end
