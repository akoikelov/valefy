class AddColumnAppInDevices < ActiveRecord::Migration
  def change
    add_column :devices, :app, :string
  end
end
