class CreateDemoRequests < ActiveRecord::Migration
  def change
    create_table :demo_requests do |t|
      t.string :company
      t.string :email
      t.string :contact
      t.string :phone_number
      t.text :notes
      t.text :message

      t.timestamps null: false
    end
  end
end
