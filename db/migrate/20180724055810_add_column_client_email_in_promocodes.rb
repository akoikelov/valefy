class AddColumnClientEmailInPromocodes < ActiveRecord::Migration
  def change
    add_column :promo_codes, :client_email, :string
  end
end
