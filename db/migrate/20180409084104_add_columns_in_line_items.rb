class AddColumnsInLineItems < ActiveRecord::Migration
  def change
    add_column :line_items, :service_id, :integer
    add_column :line_items, :price, :decimal, precision: 8, scale: 2
    add_column :line_items, :additional_price, :decimal, precision: 8, scale: 2, default: 0
  end
end
