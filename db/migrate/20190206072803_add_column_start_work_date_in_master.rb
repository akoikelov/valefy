class AddColumnStartWorkDateInMaster < ActiveRecord::Migration
  def change
    add_column :users, :start_work_date, :datetime
  end
end
