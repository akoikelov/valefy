class ChangeTypeOfFriendlyColumnInClientFeedback < ActiveRecord::Migration
  def up
    remove_column :client_feedbacks, :friendly
    add_column :client_feedbacks, :friendly, :integer, default: 4
  end
  def down
    remove_column :client_feedbacks, :friendly
    add_column :client_feedbacks, :friendly, :boolean, default: false
  end
end
