class AddColumnRegularInCategory < ActiveRecord::Migration
  def change
    add_column :categories, :regular, :boolean, default: false
  end
end
