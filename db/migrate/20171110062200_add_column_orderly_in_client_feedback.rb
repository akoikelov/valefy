class AddColumnOrderlyInClientFeedback < ActiveRecord::Migration
  def change
    add_column :client_feedbacks, :orderly, :boolean, default: false
  end
end
