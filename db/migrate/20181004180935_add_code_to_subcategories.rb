class AddCodeToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :code, :string
  end
end
