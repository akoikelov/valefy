class CreateCustomerAccounts < ActiveRecord::Migration
  def change
    create_table :customer_accounts do |t|
      t.integer :client_id
      t.string  :customer_id

      t.timestamps null: false
    end
  end
end
