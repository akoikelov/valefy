class AddColumnOfficeIdInContractDates < ActiveRecord::Migration
  def change
    add_column :contract_dates, :office_id, :integer
  end
end
