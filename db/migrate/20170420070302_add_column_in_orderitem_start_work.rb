class AddColumnInOrderitemStartWork < ActiveRecord::Migration
  def change
    add_column :order_items, :start_work, :datetime
  end
end
