class RemoveCategoryIdFromOrder < ActiveRecord::Migration
  def self.up
    remove_column :order_items, :category_id
  end

  def self.down
    add_column :order_items, :category_id, :integer
  end
end
