class ChangeTypeOfPoliteColumnInClientFeedback < ActiveRecord::Migration
  def up
    remove_column :client_feedbacks, :polite
    add_column :client_feedbacks, :polite, :integer, default: 4
  end
  def down
    remove_column :client_feedbacks, :politer
    add_column :client_feedbacks, :polite, :boolean, default: true
  end
end
