class CreateManagedAccounts < ActiveRecord::Migration
  def change
    create_table :managed_accounts do |t|
      t.integer :serviceman_id
      t.string  :managed_id

      t.timestamps null: false
    end
  end
end
