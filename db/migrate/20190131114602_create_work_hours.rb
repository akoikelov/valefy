class CreateWorkHours < ActiveRecord::Migration
  def change
    create_table :work_hours do |t|
      t.string   :start_day
      t.string   :end_day
      t.integer  :day, null: false
      t.integer  :master_id, null: false

      t.timestamps null: false
    end
  end
end
