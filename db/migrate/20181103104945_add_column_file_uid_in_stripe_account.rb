class AddColumnFileUidInStripeAccount < ActiveRecord::Migration
  def change
    add_column :pro_stripe_accounts, :file_uid, :string
  end
end
