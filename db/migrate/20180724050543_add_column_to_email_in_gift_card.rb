class AddColumnToEmailInGiftCard < ActiveRecord::Migration
  def change
    add_column :gift_cards, :to_email, :string
  end
end
