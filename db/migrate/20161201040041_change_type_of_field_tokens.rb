class ChangeTypeOfFieldTokens < ActiveRecord::Migration
  def self.up
    change_column :users, :tokens, :string
  end

  def self.down
    remove_column :users, :tokens
    add_column :users, :tokens, :json
  end
end
