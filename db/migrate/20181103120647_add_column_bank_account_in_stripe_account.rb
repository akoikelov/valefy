class AddColumnBankAccountInStripeAccount < ActiveRecord::Migration
  def change
    add_column :pro_stripe_accounts, :bank_account_uid, :string
  end
end
