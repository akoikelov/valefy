class ChangeTypeOrderTime < ActiveRecord::Migration
  def self.up
    remove_column :order_items, :order_time
    add_column :order_items, :order_time, :string
  end

  def self.down
    remove_column :order_items, :order_time
    add_column :order_items, :order_time, :datetime
  end
end
