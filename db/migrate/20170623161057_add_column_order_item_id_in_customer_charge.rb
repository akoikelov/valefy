class AddColumnOrderItemIdInCustomerCharge < ActiveRecord::Migration
  def change
    add_column :customer_charges, :order_item_id, :integer
  end
end
