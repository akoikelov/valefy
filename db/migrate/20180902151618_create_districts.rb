class CreateDistricts < ActiveRecord::Migration
  def change
    create_table :districts do |t|
      t.string :name
      t.string :image
      t.text :description
      t.integer :city_id

      t.timestamps null: false
    end
  end
end
