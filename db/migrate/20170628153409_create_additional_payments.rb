class CreateAdditionalPayments < ActiveRecord::Migration
  def change
    create_table :additional_payments do |t|
      t.integer :order_id
      t.integer :service_id

      t.timestamps null: false
    end
  end
end
