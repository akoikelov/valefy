class RemoveSortInCategories < ActiveRecord::Migration
  def change
    remove_column :categories, :sort
  end
end
