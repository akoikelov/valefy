class CreateMasterStripeInfos < ActiveRecord::Migration
  def change
    create_table :master_stripe_infos do |t|
      t.integer :master_id
      t.string :tax_id, comment: 'Taxpayer Identification Numbers'
      t.string :personal_id, comment: "The individual's personal ID number"
      t.string :id_document_image, comment: 'ID document'
      t.string :address_line1, comment: '185 Berry st.'
      t.string :address_line2, comment: 'San Francisco'
      t.string :state, comment: 'Dublin'
      t.string :postal_code, comment: 'Postal code'

      t.timestamps null: false
    end
  end
end
