class ChangeTypeOfColumnTotalInOrderItems < ActiveRecord::Migration
  def self.up
    remove_column :order_items, :total
    add_column :order_items, :total, :decimal, precision: 8, scale: 2
  end

  def self.down
    remove_column :order_items, :total
    add_column :order_items, :total, :integer
  end
end
