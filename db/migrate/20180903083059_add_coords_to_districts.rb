class AddCoordsToDistricts < ActiveRecord::Migration
  def change
    add_column :districts, :coords, :string
  end
end
