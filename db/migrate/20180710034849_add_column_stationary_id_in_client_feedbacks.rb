class AddColumnStationaryIdInClientFeedbacks < ActiveRecord::Migration
  def change
    add_column :client_feedbacks, :stationary_id, :integer
  end
end
