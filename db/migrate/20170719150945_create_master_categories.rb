class CreateMasterCategories < ActiveRecord::Migration
  def change
    create_table :master_categories do |t|
      t.integer :service_man_id
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
  end
end
