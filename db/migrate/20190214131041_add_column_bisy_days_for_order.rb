class AddColumnBisyDaysForOrder < ActiveRecord::Migration
  def change
    add_column :settings, :bisy_days_for_order, :integer, default: 0
  end
end
