class CreateDraftMasters < ActiveRecord::Migration
  def change
    create_table :draft_masters do |t|
      t.integer :category_id
      t.string :name
      t.string :phone_number
      t.string :email

      t.timestamps null: false
    end
  end
end
