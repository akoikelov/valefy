class CreateHelps < ActiveRecord::Migration
  def change
    create_table :helps do |t|
      t.integer :client_id
      t.text :question

      t.timestamps null: false
    end
  end
end
