class AddCompanyIdInCompanyLocations < ActiveRecord::Migration
  def change
    add_column :company_locations, :company_id, :integer
  end
end
