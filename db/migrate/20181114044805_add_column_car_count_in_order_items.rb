class AddColumnCarCountInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :car_count, :integer, default: 0
  end
end
