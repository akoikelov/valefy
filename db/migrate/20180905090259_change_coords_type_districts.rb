class ChangeCoordsTypeDistricts < ActiveRecord::Migration
  def change
    add_column :districts, :coordinates, :geometry, :geographic => true
    add_index :districts, :coordinates, using: :gist
  end
end