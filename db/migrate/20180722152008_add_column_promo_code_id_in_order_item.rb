class AddColumnPromoCodeIdInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :promo_code_id, :integer
  end
end
