class AddColumnsCvIdDocumentInDraftMasters < ActiveRecord::Migration
  def change
    add_column :draft_masters, :cv, :string, default: [], array: true
    add_column :draft_masters, :id_document, :string, default: [], array: true
  end
end
