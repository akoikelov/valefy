class AddColumnOrderTimeInContract < ActiveRecord::Migration
  def change
    add_column :contracts, :order_time, :datetime
  end
end
