class CreateCustomerCharges < ActiveRecord::Migration
  def change
    create_table :customer_charges do |t|
      t.string  :charge_id
      t.integer :customer_account_id

      t.timestamps null: false
    end
  end
end
