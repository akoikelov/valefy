class AddColumnStationaryIdInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :stationary_id, :integer
  end
end
