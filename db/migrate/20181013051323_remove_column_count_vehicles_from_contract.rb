class RemoveColumnCountVehiclesFromContract < ActiveRecord::Migration
  def up
    remove_column :contracts, :count_vehicles
  end

  def down
    add_column :contracts, :count_vehicles, :integer, default: 1
  end
end
