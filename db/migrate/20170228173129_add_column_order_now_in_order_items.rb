class AddColumnOrderNowInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :order_now, :boolean, default: false
  end
end
