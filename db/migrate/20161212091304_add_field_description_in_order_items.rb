class AddFieldDescriptionInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :description, :text
  end
end
