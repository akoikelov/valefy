class ChangeColumnsInFeedbackapss < ActiveRecord::Migration
  def up
    remove_column :feedback_apps, :client
    add_column :feedback_apps, :client_id, :integer

    remove_column :feedback_apps, :master
    add_column :feedback_apps, :master_id, :integer
  end

  def down
    remove_column :feedback_apps, :client_id
    add_column :feedback_apps, :client, :integer

    remove_column :feedback_apps, :master_id
    add_column :feedback_apps, :master, :integer
  end
end
