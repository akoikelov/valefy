class CreateClientServices < ActiveRecord::Migration
  def change
    create_table :client_services do |t|
      t.integer :client_id
      t.integer :service_id
      t.decimal :price

      t.timestamps null: false
    end
  end
end
