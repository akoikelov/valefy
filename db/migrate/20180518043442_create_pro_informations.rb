class CreateProInformations < ActiveRecord::Migration
  def change
    create_table :pro_informations do |t|
      t.json :work_schedule
      t.integer :pro_id

      t.timestamps null: false
    end
  end
end
