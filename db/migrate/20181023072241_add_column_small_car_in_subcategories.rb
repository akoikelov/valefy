class AddColumnSmallCarInSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :small_car, :boolean, default: false
  end
end
