class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.datetime :closed_at
      t.integer  :client_id

      t.timestamps null: false
    end
  end
end
