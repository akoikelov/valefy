class AddColumnSendEmailInvoiceClientInSettings < ActiveRecord::Migration
  def change
    add_column :settings, :send_email_invoice_client, :boolean, default: true
  end
end
