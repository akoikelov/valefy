class AddColumnRejectInAdditionalPayment < ActiveRecord::Migration
  def change
    add_column :additional_payments, :rejected, :boolean, default: false
  end
end
