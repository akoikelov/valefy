class CreateOrderItemServices < ActiveRecord::Migration
  def change
    create_table :order_item_services do |t|
      t.integer :order_item_id
      t.belongs_to :service, index: true

      t.timestamps null: false
    end
  end
end
