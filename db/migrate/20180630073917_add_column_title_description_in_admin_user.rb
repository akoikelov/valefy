class AddColumnTitleDescriptionInAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :title, :string
    add_column :admin_users, :description, :text
  end
end
