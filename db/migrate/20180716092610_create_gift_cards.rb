class CreateGiftCards < ActiveRecord::Migration
  def change
    create_table :gift_cards do |t|
      t.integer      :to_client_id
      t.integer      :from_client_id
      t.decimal      :amount, precision: 8, scale: 2
      t.text         :message
      t.datetime    :exp_date

      t.timestamps null: false
    end
  end
end
