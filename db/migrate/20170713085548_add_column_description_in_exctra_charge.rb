class AddColumnDescriptionInExctraCharge < ActiveRecord::Migration
  def change
    add_column :additional_payments, :description, :text
  end
end
