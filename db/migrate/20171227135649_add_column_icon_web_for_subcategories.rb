class AddColumnIconWebForSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :icon_web, :string
  end
end
