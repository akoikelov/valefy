class AddColumnIconInSubcategory < ActiveRecord::Migration
  def change
    add_column :subcategories, :icon, :string
  end
end
