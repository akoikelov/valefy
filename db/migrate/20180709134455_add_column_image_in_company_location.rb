class AddColumnImageInCompanyLocation < ActiveRecord::Migration
  def change
    add_column :company_locations, :image, :string
  end
end
