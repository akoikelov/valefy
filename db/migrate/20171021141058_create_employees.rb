class CreateEmployees < ActiveRecord::Migration
  def change
    create_table :employees do |t|
      t.string :name
      t.string :last_name
      t.string :position
      t.string :email
      t.string :phone_number
      t.string :b_day

      t.timestamps null: false
    end
  end
end
