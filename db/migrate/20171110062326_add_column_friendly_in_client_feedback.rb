class AddColumnFriendlyInClientFeedback < ActiveRecord::Migration
  def change
    add_column :client_feedbacks, :friendly, :boolean, default: false
  end
end
