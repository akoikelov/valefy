class AddColumnCountVehiclesInOffice < ActiveRecord::Migration
  def change
    add_column :offices, :count_vehicles, :integer
  end
end
