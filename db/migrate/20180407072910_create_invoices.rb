class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer  :order_id
      t.integer :master_id
      t.integer :client_id
      t.string  :number
      t.string  :currency

      t.timestamps null: false
    end
  end
end
