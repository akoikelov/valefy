class AddColumnAdditionalPriceInService < ActiveRecord::Migration
  def change
    add_column :services, :additional_price, :decimal
  end
end
