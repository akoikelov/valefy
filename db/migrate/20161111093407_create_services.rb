class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.integer :category_id
      t.string :title
      t.integer :price
      
      t.timestamps null: false
    end
  end
end
