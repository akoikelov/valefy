class RemoveColumnCategoryIdFromService < ActiveRecord::Migration
  def self.up
    remove_column :services, :category_id
  end

  def self.down
    add_column :services, :category_id, :integer
  end
end
