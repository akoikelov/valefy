class CreateCompanyLocations < ActiveRecord::Migration
  def change
    create_table :company_locations do |t|
      t.integer :admin_user_id
      t.string  :title
      t.string  :address
      t.float   :lat
      t.float   :lon
      t.json    :work_schedule, default: {}
      t.string  :phone_number

      t.timestamps null: false
    end
  end
end
