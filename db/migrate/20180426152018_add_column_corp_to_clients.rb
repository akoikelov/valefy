class AddColumnCorpToClients < ActiveRecord::Migration
  def change
    add_column :users, :corp, :boolean, default: false
  end
end
