class AddFieldsForUserModel < ActiveRecord::Migration
  def change
    add_column :users, :tos, :boolean, default: false
    add_column :users, :onduty, :boolean, default: false
    add_column :users, :calendar_work, :datetime
    add_column :users, :onduty_force, :boolean, default: false
    add_column :users, :newbie_credit, :integer, default: 0
  end
end
