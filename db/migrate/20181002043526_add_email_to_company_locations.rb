class AddEmailToCompanyLocations < ActiveRecord::Migration
  def change
    add_column :company_locations, :email, :string
  end
end
