class ChangeTypeOfBdayInEmployee < ActiveRecord::Migration
  def self.up
    remove_column :employees, :b_day
    add_column :employees, :b_day, :datetime
  end

  def self.down
    remove_column :employees, :b_day
    add_column :employees, :b_day, :string
  end
end
