class AddColumnUsedInGiftCards < ActiveRecord::Migration
  def change
    add_column :gift_cards, :used, :boolean, default: false
  end
end
