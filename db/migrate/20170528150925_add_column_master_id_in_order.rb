class AddColumnMasterIdInOrder < ActiveRecord::Migration
  def change
    add_column :order_items, :master_id, :integer
  end
end
