class AddColumnsBeforeAfterWorkImagesInOrderItems < ActiveRecord::Migration
  def up
    add_column :order_items, :before_work_images, :text, array: true, default: []
    add_column :order_items, :after_work_images, :text, array: true, default: []
  end

  def down
    remove_column :order_items, :before_work_images
    remove_column :order_items, :after_work_images
  end
end
