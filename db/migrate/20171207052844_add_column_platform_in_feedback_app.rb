class AddColumnPlatformInFeedbackApp < ActiveRecord::Migration
  def change
    add_column :feedback_apps, :platform, :string
  end
end
