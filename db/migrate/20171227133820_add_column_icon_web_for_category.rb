class AddColumnIconWebForCategory < ActiveRecord::Migration
  def change
    add_column :categories, :icon_web, :string
  end
end
