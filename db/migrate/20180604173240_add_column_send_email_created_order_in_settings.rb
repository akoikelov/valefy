class AddColumnSendEmailCreatedOrderInSettings < ActiveRecord::Migration
  def change
    add_column :settings, :send_email_created_order, :boolean, default: true
  end
end
