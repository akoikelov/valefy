class AddColumnShortTitleInServices < ActiveRecord::Migration
  def change
    add_column :services, :short_title, :string
  end
end
