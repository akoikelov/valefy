class AddColumnAccessItemsToAdminUsers < ActiveRecord::Migration
  def change
    add_column :admin_users, :access_items, :string
  end
end
