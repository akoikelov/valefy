class AddColumnCountVehiclesToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :count_vehicles, :integer, default: 1
  end
end
