class SettingsForFreelancing < ActiveRecord::Migration
  def change
    add_column :settings, :freelancing_transfer_percentage, :integer, default: 0
    add_column :settings, :aggregator_transfer_percentage, :integer, default: 0
    add_column :settings, :park_transfer_percentage, :integer, default: 0
    add_column :settings, :franchise_transfer_percentage, :integer, default: 0
  end
end
