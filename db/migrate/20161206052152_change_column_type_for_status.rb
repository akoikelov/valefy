class ChangeColumnTypeForStatus < ActiveRecord::Migration
  def self.up
    remove_column :order_items, :status
    add_column :order_items, :status, :integer, default: 0
  end

  def self.down
    change_column :order_items, :status, :string
  end
end
