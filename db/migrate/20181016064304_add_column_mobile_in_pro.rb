class AddColumnMobileInPro < ActiveRecord::Migration
  def change
    add_column :users, :mobile, :boolean, default: false
  end
end
