class CreateContractDates < ActiveRecord::Migration
  def change
    create_table :contract_dates do |t|
      t.integer :contract_id
      t.integer :order_date

      t.timestamps null: false
    end
  end
end
