class AddColumnChargeIdInGiftCard < ActiveRecord::Migration
  def change
    add_column :gift_cards, :customer_charge_id, :integer
  end
end
