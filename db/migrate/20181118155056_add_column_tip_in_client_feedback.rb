class AddColumnTipInClientFeedback < ActiveRecord::Migration
  def change
    add_column :client_feedbacks, :tip, :decimal, default: 0.0
  end
end
