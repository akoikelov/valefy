class CreateOrderImages < ActiveRecord::Migration
  def change
    create_table :order_images do |t|
    	t.integer :order_item_id
    	t.string :image
    	
      t.timestamps null: false
    end
  end
end
