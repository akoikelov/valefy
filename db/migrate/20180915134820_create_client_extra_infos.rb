class CreateClientExtraInfos < ActiveRecord::Migration
  def change
    create_table :client_extra_infos do |t|
      t.integer :gender
      t.integer :family_status
      t.string :profession
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
