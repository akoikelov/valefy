class AddColumnMinivanCountInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :minivan_count, :integer, default: 0
  end
end
