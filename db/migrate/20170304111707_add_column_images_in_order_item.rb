class AddColumnImagesInOrderItem < ActiveRecord::Migration
  def self.up
    add_column :order_items, :images, :text, array: true, default: []
  end

  def self.down
    remove_column :order_items, :images
  end
end
