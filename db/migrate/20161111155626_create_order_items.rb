class CreateOrderItems < ActiveRecord::Migration
  def change
    create_table :order_items do |t|
      t.integer :user_id
      t.integer :service_id
      t.string :status
      t.string :location_serviceman
      t.string :location_user
      t.string :address
      t.integer :price
      t.integer :total

      t.timestamps null: false
    end
  end
end
