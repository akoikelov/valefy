class AddColumnContractIdInOrders < ActiveRecord::Migration
  def change
    add_column :order_items, :contract_id, :integer
  end
end
