class AddColumnPlateNumberToOrder < ActiveRecord::Migration
  def change
    add_column :order_items, :plate_number, :string
  end
end
