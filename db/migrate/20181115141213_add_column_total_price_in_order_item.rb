class AddColumnTotalPriceInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :total_price, :decimal, default: 0.1
  end
end
