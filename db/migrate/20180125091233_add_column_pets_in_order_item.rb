class AddColumnPetsInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :pets, :boolean, default: false
  end
end
