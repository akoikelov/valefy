class RenameColumnsInContacts < ActiveRecord::Migration
  def change
    rename_column :contacts, :email,   :service_name
    rename_column :contacts, :link_fb, :link
  end
end
