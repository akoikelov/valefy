class AddColumnTeamIdInContractDate < ActiveRecord::Migration
  def change
    add_column :contract_dates, :team_id, :integer
  end
end
