class AddColumnNeedSendLocationInMaster < ActiveRecord::Migration
  def change
    add_column :users, :need_send_location, :boolean, default: false
  end
end
