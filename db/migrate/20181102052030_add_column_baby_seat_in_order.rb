class AddColumnBabySeatInOrder < ActiveRecord::Migration
  def change
    add_column :order_items, :baby_seat, :integer, default: 0
  end
end
