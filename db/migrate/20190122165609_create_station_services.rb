class CreateStationServices < ActiveRecord::Migration
  def change
    create_table :station_services do |t|
      t.integer :service_id
      t.integer :station_id
      t.decimal :price

      t.timestamps null: false
    end
  end
end
