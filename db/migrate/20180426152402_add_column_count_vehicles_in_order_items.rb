class AddColumnCountVehiclesInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :count_vehicles, :integer, default: 1
  end
end
