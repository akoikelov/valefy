class AddCoordinatesToOrders < ActiveRecord::Migration
  def change
    add_column :order_items, :coordinates, :geometry, geographic: true
    add_index :order_items, :coordinates, using: :gist
  end
end
