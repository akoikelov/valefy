class CreateCorpInformations < ActiveRecord::Migration
  def change
    create_table :corp_informations do |t|
      t.string :position_in_company
      t.string :name_in_company
      t.string :doc
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
