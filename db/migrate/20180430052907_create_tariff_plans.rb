class CreateTariffPlans < ActiveRecord::Migration
  def change
    create_table :tariff_plans do |t|
      t.string :plan
      t.integer :plan_value # value in a week

      t.timestamps null: false
    end
  end
end
