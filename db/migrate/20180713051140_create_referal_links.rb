class CreateReferalLinks < ActiveRecord::Migration
  def change
    create_table :referal_links do |t|
      t.string :link
      t.integer :client_id
      t.integer :came_from_client_id
      
      t.timestamps null: false
    end
  end
end
