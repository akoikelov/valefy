class CreateTermConditions < ActiveRecord::Migration
  def change
    create_table :term_conditions do |t|
      t.text :condition
      
      t.timestamps null: false
    end
  end
end
