class AddColumnSubcategoryIdInService < ActiveRecord::Migration
  def change
    add_column :services, :subcategory_id, :integer
  end
end
