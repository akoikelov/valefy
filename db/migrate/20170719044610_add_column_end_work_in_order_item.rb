class AddColumnEndWorkInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :end_work, :datetime
  end
end
