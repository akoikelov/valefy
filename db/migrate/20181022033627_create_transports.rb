class CreateTransports < ActiveRecord::Migration
  def change
    create_table :transports do |t|
      t.integer :master_id
      t.string  :car

      t.timestamps null: false
    end
  end
end
