class AddColumnConfirmPhoneNumber < ActiveRecord::Migration
  def change
  	add_column :users, :confirm_phone_number, :boolean, default: false
  end
end
