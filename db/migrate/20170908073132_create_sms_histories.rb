class CreateSmsHistories < ActiveRecord::Migration
  def change
    create_table :sms_histories do |t|
      t.integer  :client_id
      t.integer  :master_id
      t.string   :phone_number
      t.string   :code

      t.timestamps null: false
    end
  end
end
