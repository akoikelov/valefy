class CreateFeedbackApps < ActiveRecord::Migration
  def change
    create_table :feedback_apps do |t|
      t.text :message
      t.integer :client
      t.integer :master
      t.timestamps null: false
    end
  end
end
