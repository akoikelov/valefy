class AddColumnsInManagedAccount < ActiveRecord::Migration
  def change
    add_column :managed_accounts, :routing_number, :string
    add_column :managed_accounts, :account_number, :string
  end
end
