class AddColumnAdditionalPaymentCaptureCustomerCharges < ActiveRecord::Migration
  def change
    add_column :customer_charges, :additional_payment_capture, :boolean, default: false
  end
end
