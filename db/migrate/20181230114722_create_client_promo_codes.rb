class CreateClientPromoCodes < ActiveRecord::Migration
  def change
    create_table :client_promo_codes do |t|
      t.integer :client_id
      t.belongs_to :promo_code, index: true

      t.timestamps null: false
    end
  end
end
