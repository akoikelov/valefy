class AddColumnGiftCardIdInPromocode < ActiveRecord::Migration
  def change
    add_column :promo_codes, :gift_card_id, :integer
  end
end
