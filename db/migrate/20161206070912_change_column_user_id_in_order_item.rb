class ChangeColumnUserIdInOrderItem < ActiveRecord::Migration
  def change
    rename_column :order_items, :user_id, :client_id
  end
end
