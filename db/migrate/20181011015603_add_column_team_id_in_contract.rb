class AddColumnTeamIdInContract < ActiveRecord::Migration
  def change
    add_column :contracts, :team_id, :integer
  end
end
