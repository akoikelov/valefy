class CreateCallMes < ActiveRecord::Migration
  def change
    create_table :call_mes do |t|
      t.integer :client_id
      t.integer :client_feedback_id
      t.boolean :recall

      t.timestamps null: false
    end
  end
end
