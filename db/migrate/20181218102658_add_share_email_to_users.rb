class AddShareEmailToUsers < ActiveRecord::Migration
  def change
    add_column :users, :share_email, :string
  end
end
