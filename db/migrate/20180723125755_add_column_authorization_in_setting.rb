class AddColumnAuthorizationInSetting < ActiveRecord::Migration
  def change
    add_column :settings, :authorization, :json, default: {}
  end
end
