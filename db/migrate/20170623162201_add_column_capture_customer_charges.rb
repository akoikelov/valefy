class AddColumnCaptureCustomerCharges < ActiveRecord::Migration
  def change
    add_column :customer_charges, :capture, :boolean, default: false
  end
end
