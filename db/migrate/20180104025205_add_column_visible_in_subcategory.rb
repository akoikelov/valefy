class AddColumnVisibleInSubcategory < ActiveRecord::Migration
  def change
    add_column :subcategories, :visible, :boolean, default: false
  end
end
