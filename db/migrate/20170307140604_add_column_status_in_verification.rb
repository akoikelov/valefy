class AddColumnStatusInVerification < ActiveRecord::Migration
  def up
    add_column :verifications, :status, :boolean, default: false
  end

  def down
    remove_column :verifications, :status
  end
end
