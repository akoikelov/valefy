class AddColumnGiftCardIdInOrderItems < ActiveRecord::Migration
  def change
    add_column :order_items, :gift_card_id, :integer
  end
end
