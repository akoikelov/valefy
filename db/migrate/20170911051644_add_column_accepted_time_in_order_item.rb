class AddColumnAcceptedTimeInOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :accepted_time, :datetime
  end
end
