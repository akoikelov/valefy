class AddColumnStatusInSmsHistory < ActiveRecord::Migration
  def change
    add_column :sms_histories, :status, :boolean, default: false
  end
end
