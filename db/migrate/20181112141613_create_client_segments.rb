class CreateClientSegments < ActiveRecord::Migration
  def change
    create_table :client_segments do |t|
      t.integer :title
      t.integer :client_id

      t.timestamps null: false
    end
  end
end
