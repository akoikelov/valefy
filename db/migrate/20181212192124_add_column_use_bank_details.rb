class AddColumnUseBankDetails < ActiveRecord::Migration
  def change
    add_column :users, :use_bank_details, :boolean, default: false
  end
end
