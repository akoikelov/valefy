class CreateContractServices < ActiveRecord::Migration
  def change
    create_table :contract_services do |t|
      t.integer :contract_id
      t.belongs_to :service

      t.timestamps null: false
    end
  end
end
