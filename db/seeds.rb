AdminUser.create(
  email: 'admin@example.com',
  password: 'password',
  password_confirmation: 'password',
  role: :admin
) if AdminUser.where(email: 'admin@example.com').count.zero?

Setting.create(
  radius: 5,
  time_send_location: 30,
  freelancing_transfer_percentage: 0,
  aggregator_transfer_percentage: 0,
  park_transfer_percentage: 0,
  franchise_transfer_percentage: 0
) if Setting.count.zero?

Sidekiq::Cron::Job.create(name: 'Pay for order', cron: '*/5 * * * *', class: '::Api::Masters::V1::PayForOrderWorker')
